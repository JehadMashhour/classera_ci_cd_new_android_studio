package com.classera.announcements

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */

@Module
abstract class AnnouncementsFragmentModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AnnouncementsViewModelFactory
        ): AnnouncementsViewModel {
            return ViewModelProvider(fragment, factory)[AnnouncementsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AnnouncementsFragment): Fragment
}
