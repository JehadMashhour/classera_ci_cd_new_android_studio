package com.classera.di

import com.classera.assignments.create.essay.EssayQuestionActivity
import com.classera.assignments.create.essay.EssayQuestionModule
import com.classera.assignments.mcq.MultipleQuestionActivity
import com.classera.assignments.mcq.MultipleQuestionModule
import com.classera.assignments.truefalse.TrueFalseQuestionActivity
import com.classera.assignments.truefalse.TrueFalseQuestionModule
import com.classera.attachment.AttachmentDetailsActivity
import com.classera.attachment.AttachmentDetailsActivityModule
import com.classera.attachmentcomponents.fullscreen.FullScreenActivity
import com.classera.attachmentcomponents.fullscreen.FullScreenActivityModule
import com.classera.authentication.login.LoginActivity
import com.classera.authentication.login.LoginActivityModule
import com.classera.fcm.FirebaseMessagingServiceModule
import com.classera.fcm.MyFirebaseMessagingService
import com.classera.filter.FilterActivity
import com.classera.filter.FilterActivityModule
import com.classera.mailbox.compose.ComposeActivity
import com.classera.mailbox.compose.ComposeActivityModule
import com.classera.mailbox.recipients.RecipientActivity
import com.classera.mailbox.recipients.RecipientActivityModule
import com.classera.main.MainActivity
import com.classera.main.MainActivityModule
import com.classera.main.parent.ParentChildActivity
import com.classera.main.parent.ParentChildModule
import com.classera.profile.publicprofile.PublicProfileActivity
import com.classera.profile.publicprofile.PublicProfileModule
import com.classera.selection.MultiSelectionActivity
import com.classera.selection.MultiSelectionActivityModule
import com.classera.selection.userrole.MultiUserRoleSelectionActivity
import com.classera.selection.userrole.MultiUserRoleSelectionActivityModule
import com.classera.splash.SplashActivity
import com.classera.splash.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class
ActivityBuilderModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [SplashActivityModule::class])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [AttachmentDetailsActivityModule::class])
    abstract fun bindAttachmentDetailsActivity(): AttachmentDetailsActivity

    @ContributesAndroidInjector(modules = [ComposeActivityModule::class])
    abstract fun bindComposeActivity(): ComposeActivity

    @ContributesAndroidInjector(modules = [RecipientActivityModule::class])
    abstract fun bindRecipientActivity(): RecipientActivity

    @ContributesAndroidInjector(modules = [FilterActivityModule::class])
    abstract fun bindFilterActivity(): FilterActivity

    @ContributesAndroidInjector(modules = [MultiSelectionActivityModule::class])
    abstract fun bindMultiSelectionActivity(): MultiSelectionActivity

    @ContributesAndroidInjector(modules = [MultiUserRoleSelectionActivityModule::class])
    abstract fun bindMultiUserRoleSelectionActivity(): MultiUserRoleSelectionActivity

    @ContributesAndroidInjector(modules = [PublicProfileModule::class])
    abstract fun bindPublicProfileActivity(): PublicProfileActivity

    @ContributesAndroidInjector(modules = [FirebaseMessagingServiceModule::class])
    abstract fun bindMyFirebaseMessagingService(): MyFirebaseMessagingService

    @ContributesAndroidInjector(modules = [ParentChildModule::class])
    abstract fun bindMyParentChildsActivity(): ParentChildActivity

    @ContributesAndroidInjector(modules = [EssayQuestionModule::class])
    abstract fun bindEssayQuestionActivity(): EssayQuestionActivity

    @ContributesAndroidInjector(modules = [MultipleQuestionModule::class])
    abstract fun bindMultipleQuestionActivity(): MultipleQuestionActivity

    @ContributesAndroidInjector(modules = [TrueFalseQuestionModule::class])
    abstract fun bindTrueFalseQuestionActivity(): TrueFalseQuestionActivity

    @ContributesAndroidInjector(modules = [FullScreenActivityModule::class])
    abstract fun bindFullScreenActivity(): FullScreenActivity
}
