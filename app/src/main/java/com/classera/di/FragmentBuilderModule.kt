package com.classera.di

import androidx.fragment.app.Fragment
import com.calssera.behaviours.add.AddBehaviourFragment
import com.calssera.behaviours.add.AddBehaviourFragmentModule
import com.calssera.behaviours.behaviourdetails.BehaviourDetailsFragment
import com.calssera.behaviours.behaviourdetails.BehaviourDetailsFragmentModule
import com.calssera.behaviours.student.BehavioursStudentFragment
import com.calssera.behaviours.student.BehavioursStudentFragmentModule
import com.calssera.behaviours.teacher.BehavioursTeacherFragment
import com.calssera.behaviours.teacher.BehavioursTeacherFragmentModule
import com.calssera.vcr.add.AddVcrFragment
import com.calssera.vcr.add.AddVcrModule
import com.calssera.vcr.vendorbottomsheet.VendorBottomSheetFragment
import com.calssera.vcr.vendorbottomsheet.VendorBottomSheetModule
import com.calssera.vcr.admin.VcrAdminFragment
import com.calssera.vcr.admin.VcrAdminModule
import com.calssera.vcr.confirmvcr.VCRConfirmBottomSheet
import com.calssera.vcr.confirmvcr.VCRConfirmModule
import com.calssera.vcr.lecturefragment.LectureFragment
import com.calssera.vcr.lecturefragment.LectureFragmentModule
import com.classera.utils.views.dialogs.listbottomdialog.ListBottomSheetFragment
import com.classera.utils.views.fragments.ListModule
import com.classera.utils.views.dialogs.datepickerbottomdialog.DateBottomSheetFragment
import com.classera.utils.views.dialogs.datepickerbottomdialog.DateBottomSheetModule
import com.classera.utils.views.dialogs.listbottomdialog.ListBottomSheetModule
import com.classera.utils.views.fragments.ListFragment
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetModule
import com.classera.utils.views.dialogs.timepickerbottomdialog.TimeBottomSheetFragment
import com.classera.utils.views.dialogs.timepickerbottomdialog.TimeBottomSheetModule
import com.calssera.vcr.student.VcrStudentFragment
import com.calssera.vcr.student.VcrStudentModule
import com.calssera.vcr.teacher.VcrTeacherFragment
import com.calssera.vcr.teacher.VcrTeacherModule
import com.calssera.vcr.teacherlist.TeacherListFragment
import com.calssera.vcr.teacherlist.TeacherListModule
import com.classera.announcements.AnnouncementsFragment
import com.classera.announcements.AnnouncementsFragmentModule
import com.classera.announcements.announcementsdetails.AnnouncementDetailsFragment
import com.classera.announcements.announcementsdetails.AnnouncementsDetailsFragmentModule
import com.classera.asessments.AssessmentsFragment
import com.classera.asessments.AssessmentsFragmentModule
import com.classera.asessments.assessmentdetails.AssessmentDetailsModule
import com.classera.asessments.assessmentdetails.AssessmentsDetailsFragment
import com.classera.assignments.AssignmentsFragment
import com.classera.assignments.AssignmentsFragmentModule
import com.classera.assignments.add.AddAssignmentFragment
import com.classera.assignments.add.AddAssignmentModule
import com.classera.assignments.details.AssignmentDetailsFragment
import com.classera.assignments.details.AssignmentDetailsFragmentModule
import com.classera.assignments.solve.SolveAssignmentFragment
import com.classera.assignments.solve.SolveAssignmentFragmentModule
import com.classera.assignments.solve.types.BaseQuestionTypeFragmentModule
import com.classera.assignments.solve.types.essaytype.EssayFragment
import com.classera.assignments.solve.types.essaytype.EssayFragmentModule
import com.classera.assignments.solve.types.fillblanktype.FillBlankFragment
import com.classera.assignments.solve.types.fillblanktype.FillBlankFragmentModule
import com.classera.assignments.solve.types.hotspottype.HotspotFragment
import com.classera.assignments.solve.types.hotspottype.HotspotFragmentModule
import com.classera.assignments.solve.types.matchingtype.MatchingFragment
import com.classera.assignments.solve.types.matchingtype.MatchingFragmentModule
import com.classera.assignments.solve.types.multiplechoicetype.MultipleChoiceFragment
import com.classera.assignments.solve.types.multiplechoicetype.MultipleChoiceFragmentModule
import com.classera.assignments.solve.types.multipleselecttype.MultipleSelectFragment
import com.classera.assignments.solve.types.multipleselecttype.MultipleSelectFragmentModule
import com.classera.assignments.solve.types.truefalsetype.TrueFalseFragment
import com.classera.assignments.solve.types.truefalsetype.TrueFalseFragmentModule
import com.classera.attachment.add.AddAttachmentFragment
import com.classera.attachment.add.AddAttachmentFragmentModule
import com.classera.attachment.comments.AttachmentCommentsBottomSheetFragment
import com.classera.attachment.comments.AttachmentCommentsModule
import com.classera.attendance.AttendanceFragment
import com.classera.attendance.AttendanceFragmentModule
import com.classera.attendance.advisor.EditAttendanceFragment
import com.classera.attendance.advisor.EditAttendanceFragmentModule
import com.classera.attendance.advisor.attachmentbottomsheet.AttendanceAttachmentBottomSheetFragment
import com.classera.attendance.advisor.attachmentbottomsheet.AttendanceAttachmentModule
import com.classera.attendance.teacher.TakeAttendanceFragment
import com.classera.attendance.teacher.TakeAttendanceFragmentModule
import com.classera.authentication.forgot.password.ForgetPasswordModule
import com.classera.authentication.forgot.password.ForgotPasswordBottomSheet
import com.classera.authentication.forgot.sms.VerificationCodeBottomSheet
import com.classera.authentication.forgot.sms.VerificationCodeModule
import com.classera.authentication.forgot.username.ForgetUserNameModule
import com.classera.authentication.forgot.username.ForgotUsernameBottomSheet
import com.classera.calendar.add.AddEventFragment
import com.classera.calendar.add.AddEventModule
import com.classera.calendar.student.AcademicCalenderStudentFragment
import com.classera.calendar.student.AcademicCalenderStudentModule
import com.classera.calendar.teacher.AcademicCalenderTeacherFragment
import com.classera.calendar.teacher.AcademicCalenderTeacherModule
import com.classera.callchildren.bottomsheet.CallStudentBottomSheetFragment
import com.classera.callchildren.bottomsheet.CallStudentBottomSheetModule
import com.classera.callchildren.driver.DriverCallStudentFragment
import com.classera.callchildren.driver.DriverCallStudentModule
import com.classera.callchildren.driver.DriverCallStudentViewModel
import com.classera.callchildren.guardian.GuardianCallStudentFragment
import com.classera.callchildren.guardian.GuardianCallStudentModule
import com.classera.callchildren.guardian.GuardianCallStudentViewModel
import com.classera.chat.ChatFragment
import com.classera.chat.ChatFragmentModule
import com.classera.chat.blockusers.ChatBlockedUsersFragment
import com.classera.chat.blockusers.ChatBlockedUsersFragmentModule
import com.classera.chat.chatmessages.ChatMessagesFragment
import com.classera.chat.chatmessages.ChatMessagesFragmentModule
import com.classera.chat.groupinfo.ChatGroupInfoFragment
import com.classera.chat.groupinfo.ChatGroupInfoFragmentModule
import com.classera.chat.groupusers.ChatGroupUsersFragment
import com.classera.chat.groupusers.ChatGroupUsersFragmentModule
import com.classera.chat.users.ChatUsersFragment
import com.classera.chat.users.ChatUsersFragmentModule
import com.classera.classvisits.ClassVisitsFragment
import com.classera.classvisits.ClassVisitsFragmentModule
import com.classera.classvisits.add.AddClassVisitFragment
import com.classera.classvisits.add.AddClassVisitFragmentModule
import com.classera.courses.CoursesFragment
import com.classera.courses.CoursesFragmentModule
import com.classera.courses.details.CourseDetailsFragment
import com.classera.courses.details.CourseDetailsFragmentModule
import com.classera.digitallibrary.DigitalLibraryFragment
import com.classera.digitallibrary.DigitalLibraryFragmentModule
import com.classera.discussionrooms.addpost.AddPostFragment
import com.classera.discussionrooms.addpost.AddPostFragmentModule
import com.classera.discussionrooms.addroom.AddDiscussionFragment
import com.classera.discussionrooms.addroom.AddDiscussionFragmentModule
import com.classera.discussionrooms.comments.DiscussionCommentsFragment
import com.classera.discussionrooms.comments.DiscussionCommentsFragmentModule
import com.classera.discussionrooms.details.DiscussionDetailsFragment
import com.classera.discussionrooms.details.DiscussionDetailsFragmentModule
import com.classera.discussionrooms.student.StudentDiscussionFragment
import com.classera.discussionrooms.student.StudentDiscussionsFragmentModule
import com.classera.discussionrooms.teacher.TeacherDiscussionFragment
import com.classera.discussionrooms.teacher.TeacherDiscussionsFragmentModule
import com.classera.eportfolio.EPortfolioFragment
import com.classera.eportfolio.EPortfolioFragmentModule
import com.classera.eportfolio.add.AddEportfolioFragment
import com.classera.eportfolio.add.AddEportfolioFragmentModule
import com.classera.eportfolio.details.EPortfolioDetailsFragment
import com.classera.eportfolio.details.EPortfolioDetailsFragmentModule
import com.classera.home.admin.AdminHomeFragment
import com.classera.home.admin.AdminHomeFragmentModule
import com.classera.home.admin.activesections.AdminHomeSchoolActiveSectionFragmentModule
import com.classera.home.admin.activesections.AdminHomeSchoolActiveSectionsFragment
import com.classera.home.admin.allschoolsactiveusers.AdminHomeAllSchoolActiveUsersFragment
import com.classera.home.admin.allschoolsactiveusers.AdminHomeAllSchoolActiveUsersFragmentModule
import com.classera.home.admin.contentstatistic.AdminHomeContentStatisticFragmentModule
import com.classera.home.admin.contentstatistic.AdminHomeContentStatisticsFragment
import com.classera.home.admin.onlinenow.AdminHomeOnlineNowFragmentModule
import com.classera.home.admin.onlinenow.AdminHomeSchoolOnlineNowFragment
import com.classera.home.admin.schoolactiveusers.AdminHomeSchoolActiveUsersFragment
import com.classera.home.admin.schoolactiveusers.AdminHomeSchoolActiveUsersFragmentModule
import com.classera.home.admin.schoolambassadors.AdminHomeSchoolAmbassadorsFragment
import com.classera.home.admin.schoolambassadors.AdminHomeSchoolAmbassadorsFragmentModule
import com.classera.home.admin.schoolgroupactiveusers.AdminHomeSchoolGroupActiveUsersFragment
import com.classera.home.admin.schoolgroupactiveusers.AdminHomeSchoolGroupActiveUsersFragmentModule
import com.classera.home.admin.smsusage.AdminHomeSchoolSmsUsageFragment
import com.classera.home.admin.smsusage.AdminHomeSmsUsageFragmentModule
import com.classera.home.admin.weekabsences.AdminHomeSchoolWeekAbsencesFragment
import com.classera.home.admin.weekabsences.AdminHomeWeekAbsencesFragmentModule
import com.classera.home.driver.DriverRolesHomeFragment
import com.classera.home.driver.DriverRolesHomeFragmentModule
import com.classera.home.other.OtherRolesHomeFragment
import com.classera.home.other.OtherRolesHomeFragmentModule
import com.classera.home.student.StudentHomeFragment
import com.classera.home.student.StudentHomeFragmentModule
import com.classera.home.teacher.TeacherHomeFragment
import com.classera.home.teacher.TeacherHomeFragmentModule
import com.classera.homelocation.HomeLocationFragment
import com.classera.homelocation.HomeLocationFragmentModule
import com.classera.mailbox.MailboxFragment
import com.classera.mailbox.MailboxFragmentModule
import com.classera.mailbox.details.MailboxDetailsFragment
import com.classera.mailbox.details.MailboxDetailsFragmentModule
import com.classera.mailbox.draft.DraftFragment
import com.classera.mailbox.draft.DraftFragmentModule
import com.classera.mailbox.inbox.InboxFragment
import com.classera.mailbox.inbox.InboxFragmentModule
import com.classera.mailbox.outbox.OutboxFragment
import com.classera.mailbox.outbox.OutboxFragmentModule
import com.classera.mailbox.trash.TrashFragment
import com.classera.mailbox.trash.TrashFragmentModule
import com.classera.main.fragment.MainFragment
import com.classera.main.fragment.MainFragmentModule
import com.classera.mycard.MyCardFragment
import com.classera.mycard.MyCardFragmentModule
import com.classera.notification.NotificationListFragment
import com.classera.notification.NotificationListFragmentModule
import com.classera.profile.ProfileFragment
import com.classera.profile.ProfileFragmentModule
import com.classera.profile.education.EducationFragment
import com.classera.profile.education.EducationFragmentModule
import com.classera.profile.education.addEducation.AddEducationBottomSheet
import com.classera.profile.education.addEducation.AddEducationModule
import com.classera.profile.experiences.ExperiencesFragment
import com.classera.profile.experiences.ExperiencesFragmentModule
import com.classera.profile.experiences.addExperiences.AddExperienceBottomSheet
import com.classera.profile.experiences.addExperiences.AddExperienceModule
import com.classera.profile.personal.PersonalFragment
import com.classera.profile.personal.PersonalFragmentModule
import com.classera.profile.personal.addcity.AddCountryCityBottomSheet
import com.classera.profile.personal.addcity.AddCountryCityModule
import com.classera.profile.skill.SkillsFragment
import com.classera.profile.skill.SkillsFragmentModule
import com.classera.profile.skill.addskill.AddModifySkillBottomSheet
import com.classera.profile.skill.addskill.AddModifySkillModule
import com.classera.reportcards.ReportCardsFragment
import com.classera.reportcards.ReportCardsFragmentModule
import com.classera.reportcards.reportcardsdetails.ReportCardDetailsFragment
import com.classera.reportcards.reportcardsdetails.ReportCardDetailsFragmentModule
import com.classera.schedule.ScheduleFragment
import com.classera.schedule.ScheduleFragmentModule
import com.classera.schools.SchoolsFragment
import com.classera.schools.SchoolsFragmentModule
import com.classera.schools.sub.SubSchoolsFragment
import com.classera.schools.sub.SubSchoolsFragmentModule
import com.classera.settings.SettingsFragment
import com.classera.settings.SettingsFragmentModule
import com.classera.support.SupportFragment
import com.classera.support.SupportFragmentModule
import com.classera.support.add.AddSupportTicketFragment
import com.classera.support.add.AddSupportTicketModule
import com.classera.support.details.SupportDetailsFragment
import com.classera.support.details.SupportDetailsFragmentModule
import com.classera.surveys.SurveysFragment
import com.classera.surveys.SurveysFragmentModule
import com.classera.switchroles.SwitchRolesFragment
import com.classera.switchroles.SwitchRolesFragmentModule
import com.classera.switchsemester.SwitchSemesterFragment
import com.classera.switchsemester.SwitchSemesterFragmentModule
import com.classera.teacher.QuestionListFragmentModule
import com.classera.teacher.QuestionsListFragment
import com.classera.utils.views.dialogs.alertdialog.AlertDialogFragment
import com.classera.utils.views.dialogs.alertdialog.AlertDialogModule
import com.classera.weeklyplan.WeeklyPlanFragment
import com.classera.weeklyplan.WeeklyPlanFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("TooManyFunctions")
@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector(modules = [ForgetPasswordModule::class])
    abstract fun bindForgotPasswordBottomSheet(): ForgotPasswordBottomSheet

    @ContributesAndroidInjector(modules = [VerificationCodeModule::class])
    abstract fun bindVerificationCodeBottomSheet(): VerificationCodeBottomSheet

    @ContributesAndroidInjector(modules = [ForgetUserNameModule::class])
    abstract fun bindForgotUsernameBottomSheet(): ForgotUsernameBottomSheet

    @ContributesAndroidInjector(modules = [StudentHomeFragmentModule::class])
    abstract fun bindStudentHomeFragment(): StudentHomeFragment

    @ContributesAndroidInjector(modules = [TeacherHomeFragmentModule::class])
    abstract fun bindTeacherHomeFragment(): TeacherHomeFragment


    @ContributesAndroidInjector(modules = [DriverRolesHomeFragmentModule::class])
    abstract fun bindDriverRolesHomeFragment(): DriverRolesHomeFragment

    @ContributesAndroidInjector(modules = [OtherRolesHomeFragmentModule::class])
    abstract fun bindOtherRolesHomeFragment(): OtherRolesHomeFragment

    @ContributesAndroidInjector(modules = [AdminHomeFragmentModule::class])
    abstract fun bindAdminHomeFragment(): AdminHomeFragment

    @ContributesAndroidInjector(modules = [AdminHomeContentStatisticFragmentModule::class])
    abstract fun bindAdminHomeContentStatisticsFragment(): AdminHomeContentStatisticsFragment

    @ContributesAndroidInjector(modules = [AdminHomeSchoolActiveSectionFragmentModule::class])
    abstract fun bindAdminHomeSchoolActiveSectionsFragment(): AdminHomeSchoolActiveSectionsFragment

    @ContributesAndroidInjector(modules = [AdminHomeSchoolActiveUsersFragmentModule::class])
    abstract fun bindAdminHomeSchoolActiveUsersFragment(): AdminHomeSchoolActiveUsersFragment

    @ContributesAndroidInjector(modules = [AdminHomeSchoolGroupActiveUsersFragmentModule::class])
    abstract fun bindAdminHomeSchoolGroupActiveUsersFragment(): AdminHomeSchoolGroupActiveUsersFragment

    @ContributesAndroidInjector(modules = [AdminHomeAllSchoolActiveUsersFragmentModule::class])
    abstract fun bindAdminHomeAllSchoolActiveUsersFragment(): AdminHomeAllSchoolActiveUsersFragment

    @ContributesAndroidInjector(modules = [AdminHomeSchoolAmbassadorsFragmentModule::class])
    abstract fun bindAdminHomeSchoolAmbassadorsFragment(): AdminHomeSchoolAmbassadorsFragment

    @ContributesAndroidInjector(modules = [AdminHomeWeekAbsencesFragmentModule::class])
    abstract fun bindAdminHomeSchoolWeekAbsencesFragment(): AdminHomeSchoolWeekAbsencesFragment

    @ContributesAndroidInjector(modules = [AdminHomeSmsUsageFragmentModule::class])
    abstract fun bindAdminHomeSchoolSmsUsageFragment(): AdminHomeSchoolSmsUsageFragment

    @ContributesAndroidInjector(modules = [AdminHomeOnlineNowFragmentModule::class])
    abstract fun bindAdminHomeSchoolOnlineNowFragment(): AdminHomeSchoolOnlineNowFragment

    @ContributesAndroidInjector(modules = [MyCardFragmentModule::class])
    abstract fun bindMyCardFragment(): MyCardFragment

    @ContributesAndroidInjector(modules = [DigitalLibraryFragmentModule::class])
    abstract fun bindDigitalLibraryFragment(): DigitalLibraryFragment

    @ContributesAndroidInjector(modules = [AssignmentsFragmentModule::class])
    abstract fun bindAssignmentsFragment(): AssignmentsFragment

    @ContributesAndroidInjector(modules = [AcademicCalenderTeacherModule::class])
    abstract fun bindAcademicCalenderTeacherFragment(): AcademicCalenderTeacherFragment

    @ContributesAndroidInjector(modules = [AcademicCalenderStudentModule::class])
    abstract fun bindAcademicCalenderStudentFragment(): AcademicCalenderStudentFragment

    @ContributesAndroidInjector(modules = [WeeklyPlanFragmentModule::class])
    abstract fun bindWeeklyPlanFragment(): WeeklyPlanFragment

    @ContributesAndroidInjector(modules = [AssessmentsFragmentModule::class])
    abstract fun bindAssessmentFragment(): AssessmentsFragment

    @ContributesAndroidInjector(modules = [TeacherDiscussionsFragmentModule::class])
    abstract fun bindTeacherDiscussionFragment(): TeacherDiscussionFragment

    @ContributesAndroidInjector(modules = [StudentDiscussionsFragmentModule::class])
    abstract fun bindStudentDiscussionFragment(): StudentDiscussionFragment

    @ContributesAndroidInjector(modules = [BehavioursStudentFragmentModule::class])
    abstract fun bindBehavioursStudentFragment(): BehavioursStudentFragment

    @ContributesAndroidInjector(modules = [BehavioursTeacherFragmentModule::class])
    abstract fun bindBehavioursTeacherFragment(): BehavioursTeacherFragment

    @ContributesAndroidInjector(modules = [BehaviourDetailsFragmentModule::class])
    abstract fun bindBehaviourDetailsFragment(): BehaviourDetailsFragment

    @ContributesAndroidInjector(modules = [AttendanceFragmentModule::class])
    abstract fun bindAttendanceFragment(): AttendanceFragment

    @ContributesAndroidInjector(modules = [DiscussionDetailsFragmentModule::class])
    abstract fun bindDiscussionDetailsFragment(): DiscussionDetailsFragment

    @ContributesAndroidInjector(modules = [DiscussionCommentsFragmentModule::class])
    abstract fun bindDiscussionCommentsFragment(): DiscussionCommentsFragment

    @ContributesAndroidInjector(modules = [MailboxFragmentModule::class])
    abstract fun bindMailboxFragment(): MailboxFragment

    @ContributesAndroidInjector(modules = [InboxFragmentModule::class])
    abstract fun bindInboxFragment(): InboxFragment

    @ContributesAndroidInjector(modules = [OutboxFragmentModule::class])
    abstract fun bindOutboxFragment(): OutboxFragment

    @ContributesAndroidInjector(modules = [MailboxDetailsFragmentModule::class])
    abstract fun bindMailboxDetailsFragment(): MailboxDetailsFragment

    @ContributesAndroidInjector(modules = [TrashFragmentModule::class])
    abstract fun bindTrashFragment(): TrashFragment

    @ContributesAndroidInjector(modules = [CoursesFragmentModule::class])
    abstract fun bindCoursesFragment(): CoursesFragment

    @ContributesAndroidInjector(modules = [CourseDetailsFragmentModule::class])
    abstract fun bindCourseDetailsFragment(): CourseDetailsFragment

    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun bindMainFragment(): MainFragment

    @ContributesAndroidInjector(modules = [ChatFragmentModule::class])
    abstract fun bindChatFragment(): ChatFragment

    @ContributesAndroidInjector(modules = [ChatUsersFragmentModule::class])
    abstract fun bindChatUsersFragment(): ChatUsersFragment

    @ContributesAndroidInjector(modules = [ChatBlockedUsersFragmentModule::class])
    abstract fun bindChatBlockedUsersFragment(): ChatBlockedUsersFragment

    @ContributesAndroidInjector(modules = [ChatGroupUsersFragmentModule::class])
    abstract fun bindChatGroupUsersFragment(): ChatGroupUsersFragment

    @ContributesAndroidInjector(modules = [ChatGroupInfoFragmentModule::class])
    abstract fun bindChatGroupInfoFragment(): ChatGroupInfoFragment

    @ContributesAndroidInjector(modules = [ChatMessagesFragmentModule::class])
    abstract fun bindChatMessagesFragment(): ChatMessagesFragment

    @ContributesAndroidInjector(modules = [SwitchRolesFragmentModule::class])
    abstract fun bindSwitchRolesFragment(): SwitchRolesFragment

    @ContributesAndroidInjector(modules = [SchoolsFragmentModule::class])
    abstract fun bindSwitchSchoolsFragment(): SchoolsFragment

    @ContributesAndroidInjector(modules = [SwitchSemesterFragmentModule::class])
    abstract fun bindSwitchSemesterFragment(): SwitchSemesterFragment

    @ContributesAndroidInjector(modules = [ScheduleFragmentModule::class])
    abstract fun bindScheduleFragment(): ScheduleFragment

    @ContributesAndroidInjector(modules = [EPortfolioFragmentModule::class])
    abstract fun bindEPortfolioFragment(): EPortfolioFragment

    @ContributesAndroidInjector(modules = [EPortfolioDetailsFragmentModule::class])
    abstract fun bindEPortfolioDetailsFragment(): EPortfolioDetailsFragment

    @ContributesAndroidInjector(modules = [HomeLocationFragmentModule::class])
    abstract fun bindHomeLocationActivityActivity(): HomeLocationFragment

    @ContributesAndroidInjector(modules = [AnnouncementsFragmentModule::class])
    abstract fun bindAnnouncementsFragment(): AnnouncementsFragment

    @ContributesAndroidInjector(modules = [AnnouncementsDetailsFragmentModule::class])
    abstract fun bindAnnouncementDetailsFragment(): AnnouncementDetailsFragment

    @ContributesAndroidInjector(modules = [AssessmentDetailsModule::class])
    abstract fun bindAssessmentsDetailsActivity(): AssessmentsDetailsFragment

    @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = [SkillsFragmentModule::class])
    abstract fun bindSkillsFragment(): SkillsFragment

    @ContributesAndroidInjector(modules = [AddModifySkillModule::class])
    abstract fun bindAddModifySkillBottomSheet(): AddModifySkillBottomSheet

    @ContributesAndroidInjector(modules = [PersonalFragmentModule::class])
    abstract fun bindPersonalFragment(): PersonalFragment

    @ContributesAndroidInjector(modules = [AddCountryCityModule::class])
    abstract fun bindAddCountryCityBottomSheet(): AddCountryCityBottomSheet

    @ContributesAndroidInjector(modules = [EducationFragmentModule::class])
    abstract fun bindEducationFragment(): EducationFragment

    @ContributesAndroidInjector(modules = [AddEducationModule::class])
    abstract fun bindAddEducationBottomSheet(): AddEducationBottomSheet

    @ContributesAndroidInjector(modules = [VcrAdminModule::class])
    abstract fun bindVcrAdminFragment(): VcrAdminFragment

    @ContributesAndroidInjector(modules = [VcrTeacherModule::class])
    abstract fun bindVcrTeacherFragment(): VcrTeacherFragment

    @ContributesAndroidInjector(modules = [VcrStudentModule::class])
    abstract fun bindVcrStudentFragment(): VcrStudentFragment

    @ContributesAndroidInjector(modules = [TeacherListModule::class])
    abstract fun bindTeacherListFragment(): TeacherListFragment


    @ContributesAndroidInjector(modules = [VCRConfirmModule::class])
    abstract fun bindVCRConfirmBottomSheet(): VCRConfirmBottomSheet

    @ContributesAndroidInjector(modules = [AddVcrModule::class])
    abstract fun bindAddVcrFragment(): AddVcrFragment

    @ContributesAndroidInjector(modules = [SupportFragmentModule::class])
    abstract fun bindSupportFragment(): SupportFragment

    @ContributesAndroidInjector(modules = [SupportDetailsFragmentModule::class])
    abstract fun bindSupportDetailsFragment(): SupportDetailsFragment

    @ContributesAndroidInjector(modules = [AddEventModule::class])
    abstract fun bindAddEventFragment(): AddEventFragment

    @ContributesAndroidInjector(modules = [SubSchoolsFragmentModule::class])
    abstract fun bindSubSchoolsFragment(): SubSchoolsFragment

    @ContributesAndroidInjector(modules = [TakeAttendanceFragmentModule::class])
    abstract fun bindTakeAttendanceFragment(): TakeAttendanceFragment

    @ContributesAndroidInjector(modules = [EditAttendanceFragmentModule::class])
    abstract fun bindEditAttendanceFragment(): EditAttendanceFragment

    @ContributesAndroidInjector(modules = [AttendanceAttachmentModule::class])
    abstract fun bindAttendanceAttachmentFragment(): AttendanceAttachmentBottomSheetFragment

    @ContributesAndroidInjector(modules = [AssignmentDetailsFragmentModule::class])
    abstract fun bindAssignmentDetailsFragment(): AssignmentDetailsFragment

    @ContributesAndroidInjector(modules = [SurveysFragmentModule::class])
    abstract fun bindSurveysFragmentFragment(): SurveysFragment

    @ContributesAndroidInjector(modules = [SolveAssignmentFragmentModule::class])
    abstract fun bindSolveAssignmentFragment(): SolveAssignmentFragment

    @ContributesAndroidInjector(modules = [AddDiscussionFragmentModule::class])
    abstract fun bindAddDiscussionFragment(): AddDiscussionFragment

    @ContributesAndroidInjector(modules = [AddBehaviourFragmentModule::class])
    abstract fun bindAddBehaviourFragment(): AddBehaviourFragment

    @ContributesAndroidInjector(modules = [ReportCardsFragmentModule::class])
    abstract fun bindReportCardsFragment(): ReportCardsFragment

    @ContributesAndroidInjector(modules = [ReportCardDetailsFragmentModule::class])
    abstract fun bindReportCardDetailsFragment(): ReportCardDetailsFragment

    @ContributesAndroidInjector(modules = [ClassVisitsFragmentModule::class])
    abstract fun bindClassVisitsFragment(): ClassVisitsFragment

    @ContributesAndroidInjector(modules = [AddClassVisitFragmentModule::class])
    abstract fun bindAddClassVisitFragment(): AddClassVisitFragment

    @ContributesAndroidInjector(modules = [AddPostFragmentModule::class])
    abstract fun bindAddPostFragment(): AddPostFragment

    @ContributesAndroidInjector(modules = [SettingsFragmentModule::class])
    abstract fun bindSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector(modules = [AddAttachmentFragmentModule::class])
    abstract fun bindAddAttachmentFragment(): AddAttachmentFragment

    @ContributesAndroidInjector(modules = [QuestionListFragmentModule::class])
    abstract fun bindQuestionsListFragment(): QuestionsListFragment

    @ContributesAndroidInjector(modules = [AddEportfolioFragmentModule::class])
    abstract fun bindAddEportfolioFragment(): AddEportfolioFragment

    @ContributesAndroidInjector(modules = [AddAssignmentModule::class])
    abstract fun bindAAddAssignmentFragment(): AddAssignmentFragment

    @ContributesAndroidInjector(modules = [AddSupportTicketModule::class])
    abstract fun bindAddSupportTicketFragment(): AddSupportTicketFragment

    @ContributesAndroidInjector(modules = [VendorBottomSheetModule::class])
    abstract fun bindAddVcrBottomSheetFragment(): VendorBottomSheetFragment

    @ContributesAndroidInjector(modules = [DateBottomSheetModule::class])
    abstract fun bindSelectDateBottomSheetFragment(): DateBottomSheetFragment

    @ContributesAndroidInjector(modules = [TimeBottomSheetModule::class])
    abstract fun bindSelectTimeBottomSheetFragment(): TimeBottomSheetFragment

    @ContributesAndroidInjector(modules = [ListBottomSheetModule::class])
    abstract fun bindListBottomSheetFragment(): ListBottomSheetFragment

    @ContributesAndroidInjector(modules = [ListModule::class])
    abstract fun bindGeneralListFragment(): ListFragment

    @ContributesAndroidInjector(modules = [MessageBottomSheetModule::class])
    abstract fun bindMessageBottomSheetFragment(): MessageBottomSheetFragment

    @ContributesAndroidInjector(modules = [AlertDialogModule::class])
    abstract fun bindAlertDialogFragment(): AlertDialogFragment

    @ContributesAndroidInjector(modules = [LectureFragmentModule::class])
    abstract fun bindLectureFragment(): LectureFragment

    @ContributesAndroidInjector(modules = [BaseQuestionTypeFragmentModule::class])
    abstract fun bindBaseQuestionTypeFragment(): Fragment

    @ContributesAndroidInjector(modules = [TrueFalseFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindTrueFalseFragment(): TrueFalseFragment

    @ContributesAndroidInjector(modules = [MultipleChoiceFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindMultipleChoiceFragment(): MultipleChoiceFragment

    @ContributesAndroidInjector(modules = [MultipleSelectFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindMultipleSelectFragment(): MultipleSelectFragment

    @ContributesAndroidInjector(modules = [EssayFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindEssayFragment(): EssayFragment

    @ContributesAndroidInjector(modules = [FillBlankFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindFillBlankFragment(): FillBlankFragment

    @ContributesAndroidInjector(modules = [HotspotFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindHotspotFragment(): HotspotFragment

    @ContributesAndroidInjector(modules = [MatchingFragmentModule::class, BaseQuestionTypeFragmentModule::class])
    abstract fun bindMatchingFragment(): MatchingFragment

    @ContributesAndroidInjector(modules = [NotificationListFragmentModule::class])
    abstract fun bindNotificationListFragment(): NotificationListFragment

    @ContributesAndroidInjector(modules = [DraftFragmentModule::class])
    abstract fun bindDraftFragmentFragment(): DraftFragment

    @ContributesAndroidInjector(modules = [ExperiencesFragmentModule::class])
    abstract fun bindExperiencesFragment(): ExperiencesFragment

    @ContributesAndroidInjector(modules = [AddExperienceModule::class])
    abstract fun bindAddExperienceBottomSheet(): AddExperienceBottomSheet

    @ContributesAndroidInjector(modules = [GuardianCallStudentModule::class])
    abstract fun bindGuardianCallChildFragment(): GuardianCallStudentFragment

    @ContributesAndroidInjector(modules = [DriverCallStudentModule::class])
    abstract fun bindDriverCallChildFragment(): DriverCallStudentFragment

    @ContributesAndroidInjector(modules = [CallStudentBottomSheetModule::class])
    abstract fun bindCallStudentBottomSheetFragment(): CallStudentBottomSheetFragment

    @ContributesAndroidInjector(modules = [AttachmentCommentsModule::class])
    abstract fun bindAttachmentCommentsBottomSheetFragment(): AttachmentCommentsBottomSheetFragment

}
