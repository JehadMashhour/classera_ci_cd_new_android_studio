package com.classera.di.qualifiers

import javax.inject.Qualifier

/**
 * Project: Classera
 * Created: Jan 08, 2020
 *
 * @author Mohamed Hamdan
 */
@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class W3w
