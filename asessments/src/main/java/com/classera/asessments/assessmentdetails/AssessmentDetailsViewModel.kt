package com.classera.asessments.assessmentdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.Assessment
import com.classera.data.models.assessments.AssessmentsDetails
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assessments.AssessmentsRepoistory
import kotlinx.coroutines.Dispatchers

class AssessmentDetailsViewModel(private val assessmentsRepository: AssessmentsRepoistory) : BaseViewModel() {

    private var assessments: List<Assessment?>? = null

    fun getAssessmentDetailsList(assessmentId: String?): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            assessmentsRepository.getAssessmentDetails(assessmentId)
        }

        assessments = resource.element<BaseWrapper<AssessmentsDetails>>()?.data?.assessments

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAssessmentCount(): Int {
        return assessments?.size ?: 0
    }

    fun getAssessment(position: Int): Assessment? {
        return assessments?.get(position)
    }
}
