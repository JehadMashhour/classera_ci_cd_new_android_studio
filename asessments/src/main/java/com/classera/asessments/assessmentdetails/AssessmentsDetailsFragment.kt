package com.classera.asessments.assessmentdetails

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.cardview.widget.CardView
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.classera.asessments.R
import com.classera.asessments.databinding.FragmentAssessmentsDetailsBinding
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.AssessmentsDetails
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

@Screen("Assessments Details")
class AssessmentsDetailsFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: AssessmentDetailsViewModel

    private var adapter: AssessmentDetailsAdapter? = null
    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var cardViewPercentage: CardView? = null

    private val args: AssessmentsDetailsFragmentArgs by navArgs()

    override val layoutId: Int = R.layout.fragment_assessments_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        getAssessments()
    }

    private fun initViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_assessments_details)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_assessments)
        errorView = view?.findViewById(R.id.error_view_fragment_assessment)
        cardViewPercentage = view?.findViewById(R.id.card_view_fragment_assessments_details_percentage)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<AssessmentsDetails>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun getAssessments() {
        viewModel.getAssessmentDetailsList(args.assessmentId).observe(this, this::handleResource)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            cardViewPercentage?.visibility = View.GONE
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<AssessmentsDetails>>) {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()

        success.data?.data?.let { assessmentDetails ->
            bind<FragmentAssessmentsDetailsBinding> {
                this?.assessmentDetails = assessmentDetails
                this?.executePendingBindings()
            }
        }
    }

    private fun initAdapter() {
        adapter = AssessmentDetailsAdapter(viewModel)
        recyclerView?.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
        recyclerView = null
        recyclerView?.adapter = null
        progressBar = null
        errorView = null
        cardViewPercentage = null
    }
}
