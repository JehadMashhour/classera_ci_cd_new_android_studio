package com.classera.assignments

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.databinding.RowAssignmentsBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.google.android.material.button.MaterialButton

/**
 * Created by Rawan Al-Theeb on 12/25/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AssignmentsAdapter(
    private val viewModel: AssignmentsViewModel
) : BasePagingAdapter<AssignmentsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAssignmentsBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getAssignmentsCount()
    }

    inner class ViewHolder(binding: RowAssignmentsBinding) : BaseBindingViewHolder(binding) {

        private var viewAssignment: MaterialButton? = null
        private var startAssignment: MaterialButton? = null
        private var imageViewDetailsMenu: ImageView? = null

        init {
            viewAssignment = itemView.findViewById(R.id.button_row_assignment_second_action)
            startAssignment = itemView.findViewById(R.id.button_row_assignment_action)
            imageViewDetailsMenu = itemView.findViewById(R.id.image_view_row_assignment_details_menu)
        }

        override fun bind(position: Int) {
            viewAssignment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            startAssignment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

//            if (viewModel.isTeacher()) {
//                imageViewDetailsMenu?.visibility = View.VISIBLE
//            }

            imageViewDetailsMenu?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }
            bind<RowAssignmentsBinding> {
                assignmentsItem = viewModel.getAssignment(position)
                currentRole = viewModel.getCurrentRole()
            }
        }
    }
}
