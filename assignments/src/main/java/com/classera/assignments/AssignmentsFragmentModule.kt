package com.classera.assignments

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AssignmentsFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AssignmentsModelFactory
        ): AssignmentsViewModel {
            return ViewModelProvider(fragment, factory)[AssignmentsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AssignmentsFragment): Fragment
}
