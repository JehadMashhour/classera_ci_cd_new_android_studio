package com.classera.assignments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AssignmentsModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository,private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AssignmentsViewModel(assignmentsRepository,prefs) as T
    }
}
