package com.classera.assignments

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentCourse
import com.classera.data.models.assignments.AssignmentsResponse
import com.classera.data.models.filter.Filterable
import com.classera.data.models.user.UserRole
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.assignments.AssignmentsRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AssignmentsViewModel(private val assignmentsRepository: AssignmentsRepository,private val prefs: Prefs)
    : BaseViewModel() {

    var selectedFilter: Filterable? = null
    private var assignmentsList: MutableList<Assignment?> = mutableListOf()
    private var filters: List<AssignmentCourse?>? = null
    private var filterKey: String? = null
    private var text: CharSequence? = null

    fun getAssignmentsList(filterKey: String?, text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        this.filterKey = filterKey
        this.text = text
        return getAssignmentsList(filterKey, text, pageNumber, selectedFilter?.filterId,
            pageNumber == DEFAULT_PAGE)
    }

    fun getAssignmentsListByFilter(filter: Filterable?): LiveData<Resource>? {
        if (filter == selectedFilter) {
            return null
        }
        this.selectedFilter = filter
        return getAssignmentsList(filterKey, text, DEFAULT_PAGE, filter?.filterId, false)
    }

    private fun getAssignmentsList(
        filter: String?,
        text: CharSequence?,
        pageNumber: Int,
        courseId: String? = null,
        showProgress: Boolean
    ) = liveData(Dispatchers.IO) {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        var resource = tryResource {
            if(prefs.userRole == UserRole.TEACHER){
                assignmentsRepository.getTeacherAssignmentsList(filter, text, pageNumber, courseId)
            }else{
                assignmentsRepository.getAssignmentsList(filter, text, pageNumber, courseId)
            }
        }
        if (
            resource is Resource.Error
            && (resource.error.message == EMPTY_EXAMS_ERROR_MESSAGE
                    || resource.error.message == EMPTY_HOMEWROKS_ERROR_MESSAGE)
        ) {
            resource = Resource.Success(BaseWrapper(success = true, data = AssignmentsResponse()))
        }
        if (pageNumber == DEFAULT_PAGE) {
            assignmentsList.clear()
        }
        assignmentsList.addAll(
            resource.element<BaseWrapper<AssignmentsResponse>>()?.data?.assignment
                ?: mutableListOf()
        )

        if (filters == null) {
            filters = resource.element<BaseWrapper<AssignmentsResponse>>()?.data?.getCourses()
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAssignment(position: Int): Assignment? {
        return assignmentsList[position]
    }

    fun getCurrentRole(): UserRole? {
        return prefs.userRole
    }

    fun getAssignmentsCount(): Int {
        return assignmentsList.size
    }

    fun getFilters(): Array<out Filterable?>? {
        return filters?.toTypedArray()
    }

    fun isTeacher(): Boolean {
        return prefs.userRole == UserRole.TEACHER
    }

    private companion object {

        private const val EMPTY_EXAMS_ERROR_MESSAGE = "There is no Exam in classera with this ID"
        private const val EMPTY_HOMEWROKS_ERROR_MESSAGE = "There is no Homework in classera with this ID"
    }
}
