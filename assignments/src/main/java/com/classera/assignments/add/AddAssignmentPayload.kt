package com.classera.assignments.add

import com.classera.core.utils.toInt
import com.classera.data.models.assignments.AssignmentTypeEnum
import com.classera.data.models.assignments.PublishToEnum
import com.classera.data.models.assignments.Student
import com.classera.data.models.selection.Selectable
import com.classera.data.models.user.User

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Suppress("ComplexMethod")
data class AddAssignmentPayload(
    val assignmentId: String? = null,
    val courseId: String? = null,
    val typePosition: Int? = null,
    val title: String? = null,
    val assignmentDuration: String? = null,
    val questionDuration: Boolean? = null,
    val publishDate: String? = null,
    val duoDate: String? = null,
    val publishLater: Boolean = false,
    val selectedPublishToPosition: Int? = null,
    var selectedEntries: MutableList<Selectable>? = null,
    var passingPercentage: String? = null,
    var preparationId: String? = null,
    var allowMultipleSubmission: Boolean? = null,
    var numberOfSubmission: String? = null,
    var distributeMarkOnQuestion: Boolean? = null,
    var totalMark: String? = null,
    var selectedMoreOptions: MutableList<Selectable>? = null
) {

    fun toMap(): MutableMap<String, Any?> {
        val fieldsMap = mutableMapOf<String, Any?>()
        assignmentId?.let { fieldsMap["assignment_id"] = it }
        fieldsMap["course_id"] = courseId
        fieldsMap["title"] = title
        fieldsMap["publish_later"] = publishLater.toInt()
        fieldsMap["publish_to_section"] =
            (selectedPublishToPosition == PublishToEnum.SECTIONS.ordinal).toInt()
        fieldsMap["publish_to_student"] =
            (selectedPublishToPosition == PublishToEnum.STUDENTS.ordinal).toInt()
        fieldsMap["training_course_passing_percentage"] = passingPercentage
        preparationId?.let { fieldsMap["preparation_id"] = it }
        fieldsMap["allow_multiple_submissions"] = allowMultipleSubmission.toInt()
        if (allowMultipleSubmission == true) {
            fieldsMap["max_submissions_allowed"] = numberOfSubmission
        }
        fieldsMap["distribute_mark_on_questions"] = distributeMarkOnQuestion.toInt()
        if (distributeMarkOnQuestion == true) {
            fieldsMap["mark"] = totalMark
        }

        fieldsMap["type"] = if (typePosition == AssignmentTypeEnum.EXAM.ordinal) {
            AssignmentTypeEnum.EXAM.toString().toLowerCase().capitalize()
        } else {
            AssignmentTypeEnum.HOMEWORK.toString().toLowerCase().capitalize()
        }

        if (typePosition == AssignmentTypeEnum.EXAM.ordinal) {
            fieldsMap["time_limit"] = assignmentDuration
            fieldsMap["set_time_limit_for_each_question"] = questionDuration.toInt()
        }

        if (!publishLater) {
            fieldsMap["publishing_datetime"] = publishDate
            fieldsMap["due_datetime"] = duoDate
            fieldsMap["cutoff_datetime"] = duoDate
        }

        selectedEntries?.forEachIndexed { index, selectable ->
            if (selectedPublishToPosition == PublishToEnum.SECTIONS.ordinal) {
                fieldsMap["section_ids[$index]"] = selectable.id
            } else {
                fieldsMap["student_ids[$index]"] = if ((selectable as Student).userId == null) {
                    selectable.id
                } else {
                    selectable.userId
                }
            }
        }

        fieldsMap["show_responses"] = (selectedMoreOptions?.any { it.id == "0" } == true).toInt()
        fieldsMap["show_hints"] = (selectedMoreOptions?.any { it.id == "1" } == true).toInt()
        fieldsMap["show_correct_answers_after_submission"] =
            (selectedMoreOptions?.any { it.id == "2" } == true).toInt()
        fieldsMap["show_correct_answers_after_cutoff_date"] =
            (selectedMoreOptions?.any { it.id == "3" } == true).toInt()

        return fieldsMap
    }

}
