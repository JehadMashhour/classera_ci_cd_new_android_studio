package com.classera.assignments.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.AssignmentDetails
import com.classera.data.models.assignments.Preparation
import com.classera.data.models.assignments.PublishToEnum
import com.classera.data.models.assignments.PublishUser
import com.classera.data.models.courses.Course
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assignments.AssignmentsRepository
import com.classera.data.repositories.courses.CoursesRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Suppress("ComplexCondition")
class AddAssignmentViewModel(
    private val coursesRepository: CoursesRepository,
    private val assignmentsRepository: AssignmentsRepository
) : BaseViewModel() {

    private var courses: MutableList<Course> = mutableListOf()
    private var publishUser: PublishUser = PublishUser()
    private var publishType: PublishToEnum? = null
    private var preparations: MutableList<Preparation> = mutableListOf()
    var selectedCourse: Course? = null
    var selectedPreparation: Preparation? = null
    var assignmentDetails: AssignmentDetails? = null

    fun getCourses() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { coursesRepository.getCourses() }
        courses.addAll(resource.element<BaseWrapper<List<Course>>>()?.data ?: listOf())
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getPublishUser() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        emit(fetchPublishUser())
        emit(Resource.Loading(show = false))
    }

    fun getPreparation() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        emit(fetchPreparations())
        emit(Resource.Loading(show = false))
    }

    fun addAssignment(assignmentPayload: AddAssignmentPayload) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryNoContentResource { assignmentsRepository.addAssignment(assignmentPayload.toMap()) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun editTeacherAssignment(assignmentPayload: AddAssignmentPayload) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryNoContentResource { assignmentsRepository.editTeacherAssignment(assignmentPayload.toMap()) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getTeacherAssignmentDetails(assignmentId: String?) = liveData {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { assignmentsRepository.getTeacherAssignmentDetails(assignmentId) }
        assignmentDetails = resource.element<BaseWrapper<AssignmentDetails>>()?.data

        fetchCourses()
        fetchPublishUser()
        fetchPreparations()

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun fetchCourses() {
        val courseResource = tryResource { coursesRepository.getCourses() }
        courses.addAll(courseResource.element<BaseWrapper<List<Course>>>()?.data ?: listOf())
        selectedCourse = courses.firstOrNull {
            it.courseId == assignmentDetails?.assignment?.courseId
        }
    }

    private suspend fun fetchPublishUser(): Resource {
        val publishUserResource = tryResource {
            assignmentsRepository.getPublishUser(selectedCourse?.courseId)
        }
        publishUser = publishUserResource.element<BaseWrapper<PublishUser>>()?.data ?: PublishUser()
        return publishUserResource
    }

    private suspend fun fetchPreparations(): Resource {
        val preparationsResource = tryResource {
            assignmentsRepository.getPreparation(selectedCourse?.courseId)
        }
        preparations.addAll(
            preparationsResource.element<BaseWrapper<MutableList<Preparation>>>()?.data
                ?: mutableListOf()
        )
        selectedPreparation = preparations.firstOrNull {
            it.id == assignmentDetails?.assignment?.preparationId
        }
        return preparationsResource
    }

    fun getCourseTitles(): Array<String?> {
        return courses.map { it.courseTitle }.toTypedArray()
    }

    fun setSelectedCourse(index: Int) {
        selectedCourse = courses[index]
    }

    fun setSelectedPreparation(index: Int) {
        selectedPreparation = preparations[index]
    }

    fun setSelectedPublishType(publishTypeIndex: Int) {
        this.publishType = if (publishTypeIndex == PublishToEnum.SECTIONS.ordinal) {
            PublishToEnum.SECTIONS
        } else {
            PublishToEnum.STUDENTS
        }
    }

    fun getPreparationTitles(): Array<String?> {
        return preparations.map { it.title }.toTypedArray()
    }

    fun getSelectedPublishTypeIndex(): Int? {
        if (!hasPublishType()) {
            return null
        }

        publishType = if (assignmentDetails?.lectures.isNullOrEmpty()) {
            PublishToEnum.STUDENTS
        } else {
            PublishToEnum.SECTIONS
        }
        return publishType?.ordinal
    }

    fun getSelectedEntries(): MutableList<Selectable>? {
        if (!hasPublishType()) {
            return null
        }
        return if (publishType == PublishToEnum.SECTIONS) {
            assignmentDetails?.lectures?.toMutableList()
        } else {
            assignmentDetails?.students?.toMutableList()
        }
    }

    private fun hasPublishType(): Boolean {
        if (assignmentDetails?.lectures.isNullOrEmpty()
            && assignmentDetails?.students.isNullOrEmpty()
        ) {
            return false
        }
        return true
    }

    fun getPublishEntries(): Array<Selectable?>? {
        return if (publishType == PublishToEnum.SECTIONS) {
            publishUser.lectures?.toTypedArray()
        } else {
            publishUser.students?.toTypedArray()
        }
    }

    fun hasOtherOptions(): Boolean {
        val assignment = assignmentDetails?.assignment
        if (assignment?.passingPercentage != null
            || assignment?.preparationId != null
            || assignment?.allowMultipleSubmissions == true
            || assignment?.totalMark != null
            || assignment?.showHints == true
            || assignment?.showResponses == true
            || assignment?.viewAnswersAfterCutOff == true
            || assignment?.viewAnswers == true
        ) {
            return true
        }
        return false
    }
}
