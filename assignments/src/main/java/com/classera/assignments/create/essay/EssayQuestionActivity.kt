package com.classera.assignments.create.essay

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Spinner
import android.widget.Toast
import com.classera.assignments.R
import com.classera.core.activities.BaseValidationActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

class EssayQuestionActivity : BaseValidationActivity() {


    @Inject
    lateinit var viewModel: EssayQuestionViewModel

    @NotEmpty(message = "Please add question title")
    private var editTextQuestionTitle: EditText? = null

    @NotEmpty(message = "Please add question answer")
    private var editTextQuestionCorrectAnswer: EditText? = null

    @NotEmpty(message = "Please add question mark")
    private var editTextQuestionMark: EditText? = null

    private var difficulties: Spinner? = null

    private var buttonSubmit: Button? = null
    private var progressBarSubmit: ProgressBar? = null
    private var errorView: ErrorView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_essay_question)
        findViews()
        initSubmitButtonListener()
    }

    private fun initSubmitButtonListener() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        viewModel.onSaveClicked(
            "5948646",
            "short",
            editTextQuestionTitle?.text.toString(),
            editTextQuestionMark?.text.toString(),
            difficulties?.selectedItemPosition.toString(),
            editTextQuestionCorrectAnswer?.text.toString()
        ).observe(this, ::handleSubmitResource)
    }

    private fun findViews() {
        editTextQuestionTitle = findViewById(R.id.edit_text_create_assignment_title)
        editTextQuestionCorrectAnswer = findViewById(R.id.edit_text_create_assignment_answer)
        editTextQuestionMark = findViewById(R.id.edit_text_create_assignment_mark)
        difficulties = findViewById(R.id.spinner_create_assignment_difficulty)
        progressBarSubmit = findViewById(R.id.progress_bar_fragment_add_question)
        progressBarSubmit?.visibility = View.INVISIBLE
        errorView = findViewById(R.id.error_view_fragment_add_question)
        buttonSubmit = findViewById(R.id.button_fragment_add_vcr_submit)
    }

    private fun handleSubmitResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitErrorResource(resource)
            }
        }
    }

    private fun handleSubmitSuccessResource() {
        Toast.makeText(
            this@EssayQuestionActivity,
            getString(R.string.question_added_successfully), Toast.LENGTH_LONG
        )
            .show()
        this@EssayQuestionActivity.finish()
    }

    private fun handleSubmitLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarSubmit?.visibility = View.VISIBLE
            buttonSubmit?.text = ""
            buttonSubmit?.isEnabled = false
        } else {
            progressBarSubmit?.visibility = View.GONE
            buttonSubmit?.setText(R.string.button_create_assignment_add_question)
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleSubmitErrorResource(resource: Resource.Error) {
        Toast.makeText(this@EssayQuestionActivity, resource.error.message, Toast.LENGTH_LONG).show()
    }

}
