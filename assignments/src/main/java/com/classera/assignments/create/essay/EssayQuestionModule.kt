package com.classera.assignments.create.essay

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class EssayQuestionModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: EssayQuestionViewModelFactory
        ): EssayQuestionViewModel{
            return ViewModelProvider(activity, factory)[EssayQuestionViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: EssayQuestionActivity): AppCompatActivity
}
