package com.classera.assignments.solve.response

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.classera.assignments.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment

/**
 * Project: Classera
 * Created: Mar 14, 2020
 *
 * @author Mohamed Hamdan
 */
class AnswerResponseBottomSheetFragment : BaseBottomSheetDialogFragment() {

    private var textViewMessage: TextView? = null
    private var buttonOk: Button? = null

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override val layoutId: Int = R.layout.bottom_sheet_answer_response

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initMessage()
        initListeners()
    }

    private fun findViews() {
        textViewMessage = view?.findViewById(R.id.text_view_bottom_sheet_answer_response_message)
        buttonOk = view?.findViewById(R.id.button_bottom_sheet_answer_response_submit)
    }

    private fun initMessage() {
        textViewMessage?.text = arguments?.getString("message")
    }

    private fun initListeners() {
        buttonOk?.setOnClickListener { dismiss() }
    }

    companion object {

        fun show(supportFragmentManager: FragmentManager, message: String?) {
            val answerResponseBottomSheetFragment = AnswerResponseBottomSheetFragment()
            answerResponseBottomSheetFragment.arguments = bundleOf("message" to message)
            answerResponseBottomSheetFragment.show(supportFragmentManager, "")
        }
    }
}
