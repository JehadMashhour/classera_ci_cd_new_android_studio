package com.classera.assignments.solve.types

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.classera.assignments.BR
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentSettings
import com.classera.data.models.assignments.Question
import com.classera.data.models.assignments.QuestionDraft
import dagger.android.support.AndroidSupportInjection
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
abstract class BaseQuestionTypeFragment : Fragment() {

    @Inject
    lateinit var viewModel: BaseQuestionTypeViewModel

    protected abstract var layoutResource: Int

    val question by lazy { arguments?.getParcelable<Question>("question") }
    val assignment by lazy { arguments?.getParcelable<Assignment>("assignment") }
    val settings by lazy { arguments?.getParcelable<AssignmentSettings>("settings") }
    val saveQuestionNumber by lazy { arguments?.getInt("saveQuestionNumber").toString() }
    val submitQuestionNumber by lazy { arguments?.getInt("submitQuestionNumber").toString() }

    val questionAnswer: ArrayList<QuestionDraft>? by lazy {
        arguments!!.getParcelableArrayList<QuestionDraft>("questionAnswer")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResource, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = DataBindingUtil.bind<ViewDataBinding>(view)
        binding?.setVariable(BR.question, question)
        binding?.executePendingBindings()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    abstract fun getData(): Map<String, Any?>

    abstract fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>)

    fun getSubmitData(): Map<String, Any?> {
        val mapToBeFilled = mutableMapOf<String, Any?>(
            "assignment_id" to assignment?.id,
            "submission_id" to settings?.submissionId,
            "course_id" to assignment?.courseId
        )
        getSubmitData(mapToBeFilled)
        return mapToBeFilled
    }

    fun isCorrectAnswer(): Boolean {
        return getData()["answer"].toString() == question?.correctAnswer
    }
}
