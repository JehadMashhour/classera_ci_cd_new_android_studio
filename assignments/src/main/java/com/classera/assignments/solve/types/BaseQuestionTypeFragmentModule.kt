package com.classera.assignments.solve.types

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: June 15, 2020
 *
 * @author ٌRawan Altheeb
 */
@Module
object BaseQuestionTypeFragmentModule {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: BaseQuestionTypeViewModelFactory): BaseQuestionTypeViewModel {
            return ViewModelProvider(fragment, factory)[BaseQuestionTypeViewModel::class.java]
        }
}
