package com.classera.assignments.solve.types.fillblanktype

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.Keep
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.R
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.assignments.solve.types.adapters.FillBlankAdapter
import com.classera.core.Screen
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.removeNull
import dagger.android.support.AndroidSupportInjection

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("FillBlank question")
@Keep
class FillBlankFragment : BaseQuestionTypeFragment() {

    override var layoutResource: Int = R.layout.fragment_fill_blank
    private var recyclerView: RecyclerView? = null
    private val adapter by lazy { FillBlankAdapter(question?.subQuestions) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        setDraftAnswer()
        initAdapter()
    }

    private fun setDraftAnswer() {
        question?.subQuestions?.getAllQuestions()?.forEach { question ->
            val answer = questionAnswer?.firstOrNull { it.questionId.toString() == question.id }
            if (answer != null) {
                question.draftAnswer = answer.answer
                question.studentAnswer = answer.answer
            }
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun initAdapter() {
        adapter.setonItemClickedListener { text, position ->
            val data = this?.getDataFields(position, text)?.removeNull()
            viewModel.saveAsDraft(data).observe(viewLifecycleOwner) {}
        }

        recyclerView?.adapter = adapter
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_fill_blank)
    }


    private fun getDataFields(position: Int, text: String): Map<String, Any?> {
        return mutableMapOf(
            "question_id" to question?.subQuestions?.questionIds?.get(position)?.id,
            "submission_id" to settings?.submissionId,
            "answer" to text
        )
    }

    override fun getData(): Map<String, Any?> {
        val allQuestions = question?.subQuestions?.getAllQuestions()
        val map = mutableMapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber
        )
        allQuestions?.filter { it.studentAnswer != null }?.forEachIndexed { index, question ->
            map["answer[$index]"] = question.studentAnswer
        }
        return map
    }

    override fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>) {
        val studentAnswers = question?.subQuestions?.getAllQuestions()
        studentAnswers?.forEachIndexed { index, question ->
            if (question.studentAnswer?.isNotBlank() == true) {
                val questionNumber = submitQuestionNumber.toInt() + index
                mapToBeFilled["answers[$questionNumber][question_id]"] = question.id
                mapToBeFilled["answers[$questionNumber][text]"] = question.studentAnswer
            }
        }
    }
}
