package com.classera.assignments.solve.types.hotspottype

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.annotation.Keep
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.classera.assignments.R
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.core.Screen
import com.classera.core.utils.android.dp
import com.classera.core.utils.android.ifClick
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.removeNull
import com.classera.data.glide.GlideApp
import dagger.android.support.AndroidSupportInjection

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("Hotspot question")
@Keep
class HotspotFragment : BaseQuestionTypeFragment() {

    override var layoutResource: Int = R.layout.fragment_hotspot
    private var viewPosition: View? = null
    private var viewUserPosition: View? = null
    private var imageView: ImageView? = null

    private var currentImage: Bitmap? = null

    private var x: Float = 0f
    private var y: Float = 0f

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        loadImage()
        initImageTouch()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun findViews() {
        viewPosition = view?.findViewById(R.id.view_fragment_hotspot_position)
        viewUserPosition = view?.findViewById(R.id.view_fragment_hotspot_user_position)
        imageView = view?.findViewById(R.id.image_view_fragment_hotspot)
    }

    private fun loadImage() {
        GlideApp.with(requireContext())
            .asBitmap()
            .load(question?.attachmentUrl)
            .into(object : SimpleTarget<Bitmap>() {

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val userPositions = question?.studentAnswer
                        ?.split(",")
                        ?.map { it.toFloatOrNull() ?: 0f } ?: listOf()

                    val draftPositions = questionAnswer?.firstOrNull()
                        ?.answer
                        ?.split(",")
                        ?.map { it.toFloatOrNull() ?: 0f } ?: listOf()

                    initCorrectPosition(resource)
                    initUserPosition(resource, userPositions, question?.canShowAnswerState() == true)
                    initUserPosition(resource, draftPositions, true)
                }
            })
    }

    private fun initCorrectPosition(resource: Bitmap) {
        this.currentImage = resource
        imageView?.setImageBitmap(resource)

        if (question?.canShowAnswerState() == true) {
            viewPosition?.visibility = View.VISIBLE
            val positions = question?.position?.map { it.toFloat() } ?: listOf()
            var left = positions[LEFT_POSITION_INDEX]
            var top = positions[TOP_POSITION_INDEX]
            var right = positions[RIGHT_POSITION_INDEX]
            var bottom = positions[BOTTOM_POSITION_INDEX]

            if (top > bottom) {
                val tempBottom = bottom
                bottom = top
                top = tempBottom
            }

            if (right < left) {
                val tempRight = right
                right = left
                left = tempRight
            }

            imageView?.post {
                val naturalWidth = resource.width.toFloat()
                val naturalHeight = resource.height.toFloat()
                val imageWidth = imageView?.width?.toFloat() ?: 0f
                val imageHeight = imageView?.height?.toFloat() ?: 0f

                bottom = (((bottom - top) / naturalHeight) * imageHeight)
                top = ((top / naturalHeight) * imageHeight)
                right = (((right - left) / naturalWidth) * imageWidth)
                left = ((left / naturalWidth) * imageWidth)

                val layoutParams = viewPosition?.layoutParams as RelativeLayout.LayoutParams
                layoutParams.topMargin = top.toInt()
                layoutParams.leftMargin = left.toInt()
                layoutParams.width = right.toInt()
                layoutParams.height = bottom.toInt()

                viewPosition?.requestLayout()
            }
        }
    }

    private fun initUserPosition(resource: Bitmap, positions: List<Float>, showAnswers: Boolean) {
        if (showAnswers) {
            viewUserPosition?.visibility = View.VISIBLE
            if (positions.size == 2) {
                var left = positions[LEFT_POSITION_INDEX]
                var top = positions[TOP_POSITION_INDEX]

                imageView?.post {
                    val viewWidth = USER_POSITION_VIEW_SIZE.dp
                    val naturalWidth = resource.width.toFloat()
                    val naturalHeight = resource.height.toFloat()
                    val imageWidth = imageView?.width?.toFloat() ?: 0f
                    val imageHeight = imageView?.height?.toFloat() ?: 0f

                    top = ((top / naturalHeight) * imageHeight)
                    left = ((left / naturalWidth) * imageWidth)

                    val layoutParams = viewUserPosition?.layoutParams as RelativeLayout.LayoutParams
                    layoutParams.topMargin = top.toInt() - (viewWidth / 2)
                    layoutParams.leftMargin = left.toInt() - (viewWidth / 2)

                    viewUserPosition?.requestLayout()
                }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initImageTouch() {
        imageView?.setOnTouchListener { _, event ->
            if (event.ifClick() && question?.canShowAnswerState() == false) {
                viewUserPosition?.visibility = View.VISIBLE
                x = event.x
                y = event.y
                val viewWidth = USER_POSITION_VIEW_SIZE.dp

                val layoutParams = viewUserPosition?.layoutParams as RelativeLayout.LayoutParams
                layoutParams.topMargin = (y - (viewWidth / 2)).toInt()
                layoutParams.leftMargin = (x - (viewWidth / 2)).toInt()

                viewUserPosition?.requestLayout()

                viewModel.saveAsDraft(getDataFields().removeNull()).observe(viewLifecycleOwner) {}
            }
            return@setOnTouchListener true
        }
    }

    private fun getDataFields(): Map<String, Any?> {
        return mutableMapOf(
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "answer" to "${getX()},${getY()}"
        )
    }

    override fun getData(): Map<String, Any?> {
        var answer: String? = "${getX()},${getY()}"
        if (answer == "0.0,0.0") {
            answer = null
        }
        return mapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber,
            "answer" to answer
        ).filterValues { it != null }
    }

    private fun getX(): Float {
        val naturalWidth = currentImage?.width?.toFloat() ?: 0f
        val imageWidth = imageView?.width?.toFloat() ?: 0f
        return ((x * naturalWidth) / imageWidth)
    }

    private fun getY(): Float {
        val naturalHeight = currentImage?.height?.toFloat() ?: 0f
        val imageHeight = imageView?.height?.toFloat() ?: 0f

        return ((y * naturalHeight) / imageHeight)
    }

    override fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>) {
        var answer: String? = "${getX()},${getY()}"
        if (answer == "0.0,0.0") {
            answer = null
        }
        if (answer != null) {
            mapToBeFilled["answers[$submitQuestionNumber][text]"] = answer
            mapToBeFilled["answers[$submitQuestionNumber][question_id]"] = question?.id
        }
    }

    private companion object {

        private const val USER_POSITION_VIEW_SIZE = 24
        private const val LEFT_POSITION_INDEX = 0
        private const val TOP_POSITION_INDEX = 1
        private const val RIGHT_POSITION_INDEX = 2
        private const val BOTTOM_POSITION_INDEX = 3
    }
}
