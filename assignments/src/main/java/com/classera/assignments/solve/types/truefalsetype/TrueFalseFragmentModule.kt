package com.classera.assignments.solve.types.truefalsetype

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module


/**
 * Created by Rawan Al-Theeb on 6/15/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class TrueFalseFragmentModule {

    @Binds
    abstract fun bindFragment(trueFalseFragment: TrueFalseFragment) : Fragment
}
