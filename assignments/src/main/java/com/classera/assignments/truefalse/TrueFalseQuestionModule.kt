package com.classera.assignments.truefalse

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 18/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Module
abstract class TrueFalseQuestionModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: TrueFalseQuestionModelFactory
        ): TrueFalseQuestionViewModel {
            return ViewModelProvider(activity, factory)[TrueFalseQuestionViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: TrueFalseQuestionActivity): AppCompatActivity
}
