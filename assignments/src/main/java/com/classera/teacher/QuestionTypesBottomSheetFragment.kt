package com.classera.teacher

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import com.classera.assignments.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment


/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Odai Nazzal
 */
class QuestionTypesBottomSheetFragment : BaseBottomSheetDialogFragment() {

    private var linearLayoutTrueOrFalse: LinearLayout? = null
    private var linearLayoutMultipleChoice: LinearLayout? = null
    private var linearLayoutEssay: LinearLayout? = null
    private var linearLayoutMultipleSelectAnswers: LinearLayout? = null
    //private val navArguments: CourseActionsBottomSheetFragmentArgs by navArgs()

    override val layoutId: Int = R.layout.fragment_question_types_bottom_sheet

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        linearLayoutTrueOrFalse = view?.findViewById(
            R.id.linear_layout_fragment_question_types_bottom_sheet_truefalse
        )
        linearLayoutMultipleChoice = view?.findViewById(
            R.id.linear_layout_fragment_question_types_bottom_sheet_multiple_choice
        )
        linearLayoutEssay = view?.findViewById(
            R.id.linear_layout_fragment_question_types_bottom_sheet_essay
        )

        linearLayoutMultipleSelectAnswers = view?.findViewById(
            R.id.linear_layout_fragment_question_types_bottom_sheet_multiple_select_answers
        )
    }

    private fun initListeners() {
        /*linearLayoutTakeAttendance?.setOnClickListener {
            val direction = CourseActionsBottomSheetFragmentDirections.takeAttendanceDirection(navArguments.course)
            findNavController().navigate(direction)
        }
        linearLayoutManageContent?.setOnClickListener {
            val course = navArguments.course
            val courseTitle = course?.courseTitle
            val teacherId = course?.teacherUserId
            val courseId = course?.courseId
            val direction = CourseActionsBottomSheetFragmentDirections.manageContentDirection(
                courseId,
                teacherId,
                courseTitle
            )
            findNavController().navigate(direction)
        }


        linearLayoutAddBehavior?.setOnClickListener {
            findNavController().navigate(CourseActionsBottomSheetFragmentDirections.addBehaviorDirection())
        }*/
    }
}
