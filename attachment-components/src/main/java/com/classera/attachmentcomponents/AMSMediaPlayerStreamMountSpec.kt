package com.classera.attachmentcomponents

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat
import com.classera.attachmentcomponents.fullscreen.FullScreenActivity
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.MountSpec
import com.facebook.litho.annotations.OnCreateMountContent
import com.facebook.litho.annotations.OnMeasure
import com.facebook.litho.annotations.OnMount
import com.facebook.litho.annotations.Prop
import com.facebook.litho.utils.MeasureUtils
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_full_screen_video.*


/**
 * Project: Classera
 * Created: JULY 01, 2020
 *
 * @author Saeed Halawani
 */
@MountSpec
object AMSMediaPlayerStreamMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): PlayerView {
        return PlayerView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, playerView: PlayerView, @Prop mediaUrl: String?) {
        playerView.setShowBuffering(PlayerView.SHOW_BUFFERING_ALWAYS)

        val trackSelector = DefaultTrackSelector(c.androidContext)
        val loadControl = DefaultLoadControl()
        val renderersFactory = DefaultRenderersFactory(c.androidContext)

        val exoPlayer = ExoPlayerFactory.newSimpleInstance(
            c.androidContext,
            renderersFactory,
            trackSelector,
            loadControl
        )

        val userAgent = Util.getUserAgent(c.androidContext, c.androidContext.getString(R.string.app_name))
        val mediaSource = HlsMediaSource.Factory(DefaultDataSourceFactory(c.androidContext, userAgent))
            .createMediaSource(Uri.parse(mediaUrl))

        exoPlayer.prepare(mediaSource)

        val audioAttributes: AudioAttributes = AudioAttributes.Builder()
            .setUsage(C.USAGE_MEDIA)
            .setContentType(C.CONTENT_TYPE_MOVIE)
            .build()

        exoPlayer.audioAttributes = audioAttributes
        exoPlayer.setHandleWakeLock(false)
        playerView.player = exoPlayer
        exoPlayer.addListener(object : Player.EventListener {
            override fun onPlayerError(error: ExoPlaybackException) {
                super.onPlayerError(error)
                exoPlayer.prepare(mediaSource)
            }
        })
        playerView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                //TOdo
            }

            override fun onViewDetachedFromWindow(v: View) {
                exoPlayer.stop()
            }
        })

        playerView.findViewById<View>(R.id.image_view_full_screen).background =
            getDrawable(c.androidContext,R.drawable.exo_icon_fullscreen_enter)
        playerView.findViewById<View>(R.id.image_view_full_screen).setOnClickListener {
            val intent = Intent(playerView.context, FullScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("url", mediaUrl)
            intent.putExtra("type", "stream")
            intent.putExtra("current_position", exoPlayer.currentPosition)

            ContextCompat.startActivity(playerView.context, intent,null)
        }
    }

    private fun getFileExtension(url: String?): String? {
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }
}
