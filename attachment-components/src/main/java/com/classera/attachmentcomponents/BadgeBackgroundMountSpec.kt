package com.classera.attachmentcomponents

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.*
import com.facebook.litho.utils.MeasureUtils

@MountSpec
object BadgeBackgroundMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): ImageView {
        return ImageView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, attachmentCoverImageView: ImageView, @Prop contentType: AttachmentTypes?, @Prop drawableRes: Int) {
        when (contentType) {
            AttachmentTypes.IMAGE -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.AUDIO -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.FLASH -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.GAME -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.WEBSITE -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.VIDEO -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.VIMEO -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.YOUTUBE -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.CODE -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.PROGRAM -> attachmentCoverImageView.visibility = View.GONE
            AttachmentTypes.UNKNOWN -> attachmentCoverImageView.visibility = View.GONE
            else -> attachmentCoverImageView.visibility = View.VISIBLE
        }

        attachmentCoverImageView.setImageDrawable(c.resources.getDrawable(drawableRes))
    }
}