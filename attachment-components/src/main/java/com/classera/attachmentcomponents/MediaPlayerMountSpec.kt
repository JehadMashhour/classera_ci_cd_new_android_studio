package com.classera.attachmentcomponents

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import com.classera.attachmentcomponents.fullscreen.FullScreenActivity
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.MountSpec
import com.facebook.litho.annotations.OnCreateMountContent
import com.facebook.litho.annotations.OnMeasure
import com.facebook.litho.annotations.OnMount
import com.facebook.litho.annotations.Prop
import com.facebook.litho.utils.MeasureUtils
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util


/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@MountSpec
object MediaPlayerMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): PlayerView {
        return PlayerView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, playerView: PlayerView, @Prop mediaUrl: String?,@Prop attachmentType: AttachmentTypes?) {
        playerView.setShowBuffering(PlayerView.SHOW_BUFFERING_ALWAYS)

        val trackSelector = DefaultTrackSelector(c.androidContext)
        val loadControl = DefaultLoadControl()
        val renderersFactory = DefaultRenderersFactory(c.androidContext)

        val exoPlayer = ExoPlayerFactory.newSimpleInstance(
            c.androidContext,
            renderersFactory,
            trackSelector,
            loadControl
        )

        val userAgent = Util.getUserAgent(c.androidContext, "Classera")
        val mediaSource = ExtractorMediaSource.Factory(DefaultDataSourceFactory(c.androidContext, userAgent))
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(Uri.parse(mediaUrl))

        exoPlayer.prepare(mediaSource)

        val audioAttributes: AudioAttributes = AudioAttributes.Builder()
            .setUsage(C.USAGE_MEDIA)
            .setContentType(C.CONTENT_TYPE_MOVIE)
            .build()

        exoPlayer.audioAttributes = audioAttributes
        exoPlayer.setHandleWakeLock(false)
        playerView.player = exoPlayer
        playerView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                //TOdo
            }

            override fun onViewDetachedFromWindow(v: View) {
                exoPlayer.stop()
            }
        })

        if (attachmentType != AttachmentTypes.AUDIO) {
            playerView.findViewById<View>(R.id.image_view_full_screen).background =
                AppCompatResources.getDrawable(
                    c.androidContext,
                    R.drawable.exo_icon_fullscreen_enter
                )

            playerView.findViewById<View>(R.id.image_view_full_screen).setOnClickListener {
                val intent = Intent(playerView.context, FullScreenActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("url", mediaUrl)
                intent.putExtra("type", "video")
                intent.putExtra("current_position", exoPlayer.currentPosition)
                ContextCompat.startActivity(playerView.context, intent,null)
            }
        } else {
            playerView.findViewById<View>(R.id.image_view_full_screen).visibility = View.GONE
        }


    }

    private fun getFileExtension(url: String?): String? {
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }
}
