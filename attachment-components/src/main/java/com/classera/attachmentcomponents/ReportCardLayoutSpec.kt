package com.classera.attachmentcomponents

import com.classera.data.BuildConfig
import com.classera.data.models.reportcards.ReportCardType
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop

/**
 * Project: Classera
 * Created: Feb 23, 2020
 *
 * @author Mohamed Hamdan
 */
@LayoutSpec
object ReportCardLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(
        componentContext: ComponentContext,
        @Prop type: ReportCardType?,
        @Prop reportCardDetails: ReportCardDetails?,
        @Prop id: String?
    ): Component {
        return when (type) {
            ReportCardType.PDF -> {
                PdfViewerLayout.create(componentContext).url(
                    "${BuildConfig.flavorData.apiBaseUrl}/ReportCards/student_download_pdf_from_mobile/?rid=$id&access_token=${BuildConfig.flavorData.accessToken}"
                ).build()
            }
            ReportCardType.DYNAMIC_REPORT -> {
                PdfViewerLayout.create(componentContext).url(reportCardDetails?.downloadUrl).build()
            }
            else -> {
                throw IllegalAccessException("The type should be one of ${ReportCardType::class.java.name} types")
            }
        }
    }
}
