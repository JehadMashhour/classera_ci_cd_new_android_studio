package com.classera.attachmentcomponents

import android.graphics.Color
import android.widget.ImageView
import com.classera.data.models.digitallibrary.Attachment
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop
import com.facebook.litho.widget.Image
import com.facebook.litho.widget.Text
import com.facebook.litho.widget.TextAlignment
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge
import com.facebook.yoga.YogaJustify

/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@LayoutSpec
object UnknownLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(componentContext: ComponentContext, @Prop attachment: Attachment?): Component {
        return Column.create(componentContext)
            .justifyContent(YogaJustify.CENTER)
            .alignContent(YogaAlign.CENTER)
            .alignItems(YogaAlign.CENTER)
            .alignSelf(YogaAlign.CENTER)
            .backgroundColor(Color.parseColor("#DDDDDD"))
            .child(
                Image.create(componentContext)
                    .widthDip(60f)
                    .heightDip(60f)
                    .scaleType(ImageView.ScaleType.CENTER_INSIDE)
                    .drawableRes(R.drawable.ic_empty)
            )
            .child(
                Text.create(componentContext)
                    .textColor(Color.BLACK)
                    .textRes(R.string.label_unknown_attachment)
                    .marginDip(YogaEdge.TOP, 25f)
                    .alignment(TextAlignment.CENTER)
                    .build()
            )
            .build()
    }
}
