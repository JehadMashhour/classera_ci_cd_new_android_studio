package com.classera.attachmentcomponents

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.classera.attachmentcomponents.fullscreen.FullScreenActivity
import com.ct7ct7ct7.androidvimeoplayer.model.PlayerState
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerActivity
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.MountSpec
import com.facebook.litho.annotations.OnCreateMountContent
import com.facebook.litho.annotations.OnMeasure
import com.facebook.litho.annotations.OnMount
import com.facebook.litho.annotations.Prop
import com.facebook.litho.utils.MeasureUtils
import com.google.android.youtube.player.YouTubePlayer

/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@MountSpec
object VimeoPlayerMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): VimeoPlayerView {
        return VimeoPlayerView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, vimeoPlayerView: VimeoPlayerView, @Prop videoUrl: String?) {
        val id = getIdFromUrl(videoUrl)?.toIntOrNull()
        if (id != null) {
            vimeoPlayerView.initialize(true, getIdFromUrl(videoUrl)?.toInt() ?: 0)
            vimeoPlayerView.setFullscreenVisibility(true)
            vimeoPlayerView.setFullscreenClickListener {
                c.applicationContext.startActivity(
                    VimeoPlayerActivity.createIntent(
                        c.applicationContext,
                        VimeoPlayerActivity.REQUEST_ORIENTATION_AUTO,
                        vimeoPlayerView
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
        } else {
            val message = c.androidContext.getString(R.string.invalid_vimeo_url)
            Toast.makeText(c.androidContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun getIdFromUrl(url: String?): String? {
        val uri = Uri.parse(url)
        return if (uri.getQueryParameter("v") != null) {
            uri.getQueryParameter("v")
        } else {
            Uri.parse(url).pathSegments.last()
        }
    }
}
