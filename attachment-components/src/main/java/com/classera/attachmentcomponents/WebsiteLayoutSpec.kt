package com.classera.attachmentcomponents

import com.classera.data.models.BackgroundColor
import com.classera.data.models.digitallibrary.Attachment
import com.facebook.litho.Border
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.LayoutSpec
import com.facebook.litho.annotations.OnCreateLayout
import com.facebook.litho.annotations.Prop
import com.facebook.litho.widget.Image
import com.facebook.litho.widget.Text
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge
import com.facebook.yoga.YogaPositionType

/**
 * Project: Classera
 * Created: Nov 8, 2020
 *
 * @author rawan altheeb
 */
@LayoutSpec
object WebsiteLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(
        componentContext: ComponentContext,
        @Prop attachment: Attachment?
    ): Component {
        return Column.create(componentContext)
            .alignSelf(YogaAlign.CENTER)
            .child(
                Text.create(componentContext)
                    .textColor(componentContext.resources.getColor(R.color.fontWhiteEnable))
                    .textSizeSp(13f)
                    .text(componentContext.resources.getString(R.string.title_activity_attachment_details_open_in_browser))
                    .paddingDip(YogaEdge.BOTTOM, 10f)
                    .paddingDip(YogaEdge.TOP, 10f)
                    .paddingDip(YogaEdge.START, 12f)
                    .paddingDip(YogaEdge.END, 12f)
                    .alignSelf(YogaAlign.CENTER)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .positionDip(YogaEdge.BOTTOM, 12f)
                    .border(
                        Border.create(componentContext)
                            .widthDip(YogaEdge.ALL, 1f)
                            .color(YogaEdge.ALL, componentContext.resources.getColor(R.color.fontWhiteEnable))
                            .radiusDip(10f)
                            .build()
                    )
            )
            .child(
                UnknownMount.create(componentContext).imageUrl(attachment?.attachmentUrl)
                    .build()
            )
            .background(
                componentContext.applicationContext.getDrawable(
                    attachment?.backgroundColor?.resId ?: BackgroundColor.GRAY.resId
                )
            ).child(
                Image.create(componentContext)
                    .widthDip(80f)
                    .heightDip(80f)
                    .alignSelf(YogaAlign.FLEX_END)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .marginDip(YogaEdge.END, 25f)
                    .marginDip(YogaEdge.TOP, -12f)
                    .drawableRes(R.drawable.ic_bookmark_60)
                    .build()
            ).child(
                MountBadge.create(componentContext)
                    .widthDip(30f)
                    .heightDip(30f)
                    .positionDip(YogaEdge.TOP, 8f)
                    .positionDip(YogaEdge.END, 20f)
                    .marginDip(YogaEdge.END, 5f)
                    .alignSelf(YogaAlign.FLEX_END)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .attachment(attachment)
                    .build()
            )
            .build()
    }
}
