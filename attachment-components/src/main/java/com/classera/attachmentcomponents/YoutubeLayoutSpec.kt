package com.classera.attachmentcomponents

import android.content.Intent
import android.view.View
import com.classera.attachmentcomponents.youtubeview.YoutubeActivity
import com.classera.attachmentcomponents.youtubeview.YoutubeActivity.Companion.YOUTUBE_URL
import com.facebook.litho.ClickEvent
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.*
import com.facebook.litho.widget.Image
import com.facebook.yoga.YogaAlign
import com.facebook.yoga.YogaEdge
import com.facebook.yoga.YogaPositionType

/**
 * Created by Rawan Al-Theeb on 23/11/2020.
 * Classera
 * r.altheeb@classera.com
 */
@LayoutSpec
object YoutubeLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(
        componentContext: ComponentContext,
        @Prop youtubeUrl: String?
    ): Component {
        return Column.create(componentContext)
            .child(YoutubeMount.create(componentContext).youtubeUrl(youtubeUrl).build())
            .child(
                Image.create(componentContext)
                    .widthDip(60f)
                    .heightDip(60f)
                    .alignSelf(YogaAlign.CENTER)
                    .positionType(YogaPositionType.ABSOLUTE)
                    .marginDip(YogaEdge.BOTTOM, 25f)
                    .marginDip(YogaEdge.TOP, 80f)
                    .drawableRes(R.drawable.ic_play_circle)
                    .build()
            )
            .clickHandler(YoutubeLayout.onViewClicked(componentContext))
            .build()
    }

    @OnEvent(ClickEvent::class)
    fun onViewClicked(context: ComponentContext?, @FromEvent view: View?, @Prop youtubeUrl: String?) {
        val intent = Intent(view?.context, YoutubeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(YOUTUBE_URL, youtubeUrl)
        view?.context?.startActivity(intent)
    }
}
