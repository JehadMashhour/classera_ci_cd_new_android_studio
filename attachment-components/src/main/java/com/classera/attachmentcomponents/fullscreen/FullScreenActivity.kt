package com.classera.attachmentcomponents.fullscreen

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.classera.attachmentcomponents.R
import com.classera.core.activities.BaseToolbarActivity
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_full_screen_video.*

@Suppress("DEPRECATION")
class FullScreenActivity : BaseToolbarActivity() {

    private lateinit var player: SimpleExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_video)
        intiViews()
        setupExoPlayer()
    }

    private fun intiViews() {
        player_view.findViewById<View>(R.id.image_view_full_screen).background =
            getDrawable(R.drawable.exo_icon_fullscreen_exit)
    }

    private fun setupExoPlayer() {
        val bandwithMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwithMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
        player_view.useController = true
        player_view.requestFocus()
        player_view.player = player
        player.playWhenReady = true

        val videoUri = Uri.parse(getVideoUrl())

        val userAgent = Util.getUserAgent(
            this, this.getString(
                com.classera.attachmentcomponents.R.string.app_name
            )
        )
        if (intent.getStringExtra("type") == "stream") {
            val mediaSource = HlsMediaSource.Factory(
                DefaultDataSourceFactory(
                    this, userAgent
                )
            ).createMediaSource(videoUri)
            player.prepare(mediaSource)
            player.seekTo(intent.getLongExtra("current_position", 0L))
            player.playWhenReady = true
        }
        else {
            val mediaSource = ExtractorMediaSource.Factory(
                DefaultDataSourceFactory(
                    this, userAgent
                )
            )
                .setExtractorsFactory(DefaultExtractorsFactory())
                .createMediaSource(videoUri)
            player.prepare(mediaSource)
            player.seekTo(intent.getLongExtra("current_position", 0L))
            player.playWhenReady = true
        }

        player_view.findViewById<View>(R.id.image_view_full_screen).setOnClickListener {
            onBackPressed()
        }
    }

    override fun onDestroy() {
        player.release()
        player.stop()
        super.onDestroy()
    }

    override fun onPause() {
        player.release()
        player.stop()
        super.onPause()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun getVideoUrl() = intent.getStringExtra("url")
}
