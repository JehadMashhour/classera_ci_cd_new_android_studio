package com.classera.attachment

import android.app.Application
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.storage.Storage
import com.classera.storage.StorageImpl
import com.classera.storage.StorageLegacyImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class AttachmentDetailsActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: AttachmentDetailsViewModelFactory
        ): AttachmentDetailsViewModel {
            return ViewModelProvider(activity, factory)[AttachmentDetailsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAttachmentDetailsViewModelFactory(
            application: Application,
            activity: AppCompatActivity,
            attachmentRepository: AttachmentRepository,
            storage: Storage
        ): AttachmentDetailsViewModelFactory {
            val args = AttachmentDetailsActivityArgs.fromBundle(activity.intent.extras!!)
            return AttachmentDetailsViewModelFactory(
                application,
                args,
                attachmentRepository,
                storage,
                args.imageBackground
            )
        }

        @Provides
        @JvmStatic
        fun provideStorage(activity: AppCompatActivity): Storage {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                StorageImpl(activity)
            } else {
                StorageLegacyImpl(activity)
            }
        }
    }

    @Binds
    abstract fun bindActivity(activity: AttachmentDetailsActivity): AppCompatActivity
}
