package com.classera.attachment

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.attachment.comments.AttachmentCommentsViewModel
import com.classera.data.models.BackgroundColor
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.storage.Storage

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AttachmentDetailsViewModelFactory(
    private val application: Application,
    private val args: AttachmentDetailsActivityArgs?,
    private val attachmentRepository: AttachmentRepository,
    private val storage: Storage,
    private val imageBackground: BackgroundColor
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AttachmentDetailsViewModel(
            application,
            args?.id,
            attachmentRepository,
            storage,
            imageBackground
        ) as T
    }
}

