package com.classera.attachment.actions

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.ActivityNotFoundException
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.net.toUri
import androidx.databinding.ViewDataBinding
import com.classera.attachment.BR
import com.classera.attachment.R
import com.classera.attachment.databinding.ViewHorizontalAttachmentActionsBinding
import com.classera.attachment.databinding.ViewVerticalAttachmentActionsBinding
import com.classera.attachment.rating.RatingBottomSheet
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.startAppSettingsIntent
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.storage.Storage
import com.github.abdularis.buttonprogress.DownloadButtonProgress
import java.io.File
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 04, 2020
 *
 * @author Mohamed Hamdan
 */
class AttachmentActionsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes), DownloadButtonProgress.OnClickListener {

    private lateinit var attachmentRepository: AttachmentRepository
    private lateinit var storage: Storage
    private lateinit var activityContext: Activity

    private var onLikeClickListener: (() -> Unit)? = null
    private var onUnderstandClickListener: (() -> Unit)? = null

    @Inject
    lateinit var viewModel: AttachmentActionsViewModel

    private var attachment: Attachment? = null
    private var viewDataBinding: ViewDataBinding? = null

    private var buttonDownload: DownloadButtonProgress? = null
    private var linearLayoutLike: LinearLayout? = null
    private var linearLayoutRate: LinearLayout? = null
    private var linearLayoutUnderstand: LinearLayout? = null
    private var linearLayoutDownload: LinearLayout? = null

    private var orientation: Int = HORIZONTAL
    private var showLike: Boolean = true
    private var showRate: Boolean = true
    private var showUnderstand: Boolean = true
    private var showDownload: Boolean = true

    init {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.AttachmentActionsView)

        orientation = attributes.getInt(R.styleable.AttachmentActionsView_orientation, orientation)
        showLike = attributes.getBoolean(R.styleable.AttachmentActionsView_showLike, showLike)
        showRate = attributes.getBoolean(R.styleable.AttachmentActionsView_showRate, showRate)
        showUnderstand =
            attributes.getBoolean(R.styleable.AttachmentActionsView_showUnderstand, showUnderstand)
        showDownload =
            attributes.getBoolean(R.styleable.AttachmentActionsView_showDownload, showDownload)
        setOrientation(orientation)

        attributes.recycle()
    }

    fun setOrientation(orientation: Int) {
        removeAllViews()
        if (isInEditMode) {
            when (orientation) {
                VERTICAL -> {
                    View.inflate(context, R.layout.view_vertical_attachment_actions, this)
                }
                HORIZONTAL -> {
                    View.inflate(context, R.layout.view_horizontal_attachment_actions, this)
                }
            }
        }
        findViews()
        handleVisibilityAndGone()
        if (isInEditMode) {
            return
        }
        val inflater = LayoutInflater.from(context)
        when (orientation) {
            VERTICAL -> {
                viewDataBinding = ViewVerticalAttachmentActionsBinding.inflate(inflater, this, true)
            }
            HORIZONTAL -> {
                viewDataBinding =
                    ViewHorizontalAttachmentActionsBinding.inflate(inflater, this, true)
            }
        }
        findViews()
        handleVisibilityAndGone()
        setAttachment(attachment)
    }

    private fun initViewListeners(attachment: Attachment) {

        if (attachment.allowDownload.equals(ALLOW_DOWNLOAD) && attachment.contentType != AttachmentTypes.WEBSITE) {
            linearLayoutDownload?.setOnClickListener {
                if (checkExternalStoragePermission()) {
                    when (buttonDownload?.currState) {
                        DownloadButtonProgress.STATE_IDLE -> {
                            if (viewModel.checkIfCanDownloadAndStart()) {
                                buttonDownload?.setIndeterminate()
                            }
                        }
                        DownloadButtonProgress.STATE_INDETERMINATE, DownloadButtonProgress.STATE_DETERMINATE -> {
                            buttonDownload?.setIdle()
                            viewModel.cancelDownload()
                        }
                        DownloadButtonProgress.STATE_FINISHED -> {
                            viewModel.openFile()
                        }
                    }
                }
            }
            buttonDownload?.addOnClickListener(this)
        } else {
            linearLayoutDownload?.isEnabled = false
            buttonDownload?.isEnabled = false
            linearLayoutDownload?.visibility = View.GONE
        }

        linearLayoutLike?.setOnClickListener {
            viewModel?.onLikeClicked()
            onLikeClickListener?.invoke()
        }

        linearLayoutUnderstand?.setOnClickListener {
            viewModel?.onUnderstandClicked()
            onUnderstandClickListener?.invoke()
        }

        if (viewModel?.isRated() == 0.0) {
            linearLayoutRate?.setOnClickListener {
                RatingBottomSheet.show((context as AppCompatActivity).supportFragmentManager) { rating ->
                    viewModel?.onSubmitRatingClicked(rating)
                }
            }
        }

    }

    override fun onFinishButtonClick(view: View?) {
        linearLayoutDownload?.callOnClick()
    }

    override fun onCancelButtonClick(view: View?) {
        linearLayoutDownload?.callOnClick()
    }

    override fun onIdleButtonClick(view: View?) {
        linearLayoutDownload?.callOnClick()
    }

    private fun findViews() {
        linearLayoutLike = findViewById(R.id.linear_layout_row_digital_library_like)
        linearLayoutRate = findViewById(R.id.linear_layout_row_digital_library_rate)
        linearLayoutUnderstand = findViewById(R.id.linear_layout_row_digital_library_understand)
        linearLayoutDownload = findViewById(R.id.linear_layout_row_digital_library_download)
        buttonDownload = findViewById(R.id.button_activity_attachment_details_download)
    }

    private fun handleVisibilityAndGone() {
        linearLayoutLike?.visibility = if (showLike) View.VISIBLE else View.GONE
        linearLayoutRate?.visibility = if (showRate) View.VISIBLE else View.GONE
        linearLayoutUnderstand?.visibility = if (showUnderstand) View.VISIBLE else View.GONE
        linearLayoutDownload?.visibility = if (showDownload) View.VISIBLE else View.GONE
    }

    fun setAttachmentRepository(attachmentRepository: AttachmentRepository) {
        this.attachmentRepository = attachmentRepository
    }

    fun setStorage(storage: Storage) {
        this.storage = storage
    }

    fun setActivityContext(activityContext: Activity) {
        this.activityContext = activityContext
    }

    fun setAttachment(attachment: Attachment?) {
        if (attachment == null) {
            return
        }
        viewDataBinding?.setVariable(BR.attachment, attachment)
        viewDataBinding?.executePendingBindings()

        val application = context.applicationContext.applicationContext as Application
        viewModel =
            AttachmentActionsViewModel(application, attachment, attachmentRepository, storage)

        initViewModelListeners()
        initViewListeners(attachment)
    }

    fun setOnLikeClickListener(onLikeClickListener: () -> Unit) {
        this.onLikeClickListener = onLikeClickListener
    }

    fun setOnUnderstandClickListener(onUnderstandClickListener: () -> Unit) {
        this.onUnderstandClickListener = onUnderstandClickListener
    }

    fun reInitializeView() {
        buttonDownload?.setIdle()
        viewModel.checkIfPathExists()
    }

    private fun initViewModelListeners() {
        viewModel?.downloadProgressLiveData?.observe(context as AppCompatActivity) { progress ->
            if (progress == 100f) {
                buttonDownload?.setFinish()
                return@observe
            }
            buttonDownload?.setDeterminate()
            buttonDownload?.currentProgress = progress.toInt()
        }

        viewModel.openFileLiveData.observe(context as AppCompatActivity) { uri ->
            uri?.let {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(uri, getMimeType(uri))
                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

                try {
                    context.startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        context, context.getString(R.string.there_is_no_application),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        viewModel.toastLiveData.observe(context as AppCompatActivity) { message ->
            val stringMessage =
                if (message is Int) context.getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }

        viewModel.notifyLiveData.observe(context as AppCompatActivity) {
            viewDataBinding?.setVariable(BR.attachment, it)
            viewDataBinding?.executePendingBindings()
        }
    }



    private fun getMimeType(uri: Uri): String? {
        var mimeType: String? = null
        mimeType = if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            val cr: ContentResolver = context.contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())

            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase()
            )
        }
        return mimeType
    }

    private fun checkExternalStoragePermission(): Boolean {
        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        return when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                context,
                permission
            ) -> {
                true
            }
            else -> {
                ActivityCompat.requestPermissions(
                    activityContext,
                    arrayOf(permission),
                    REQUEST_CODE_PERMISSION
                )
                false
            }
        }
    }

    companion object {

        const val VERTICAL = 1
        const val HORIZONTAL = 2
        const val ALLOW_DOWNLOAD = "1"
        private const val REQUEST_CODE_PERMISSION = 10
    }
}
