package com.classera.attachment.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.attachment.AttachmentDetailsActivityArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class AddAttachmentFragmentModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factoryAdd: AddAttachmentViewModelFactory
        ): AddAttachmentViewModel {
            return ViewModelProvider(fragment, factoryAdd)[AddAttachmentViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAttachmentDetailsActivityArgs(activity: Fragment): AddAttachmentFragmentArgs {
            return AddAttachmentFragmentArgs.fromBundle(activity.arguments!!)
        }
    }

    @Binds
    abstract fun bindActivity(fragmentAdd: AddAttachmentFragment): Fragment
}
