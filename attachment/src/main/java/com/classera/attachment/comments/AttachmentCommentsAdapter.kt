package com.classera.attachment.comments

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.attachment.AttachmentDetailsViewModel
import com.classera.attachment.R
import com.classera.attachment.databinding.RowAttachmentCommentBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter

/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
class AttachmentCommentsAdapter(
    private val viewModel: AttachmentCommentsViewModel
) : BasePagingAdapter<AttachmentCommentsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowAttachmentCommentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getCommentCount()
    }

    inner class ViewHolder(binding: RowAttachmentCommentBinding) : BaseBindingViewHolder(binding) {

        private var imageViewError: ImageView? = null

        init {
            imageViewError = itemView.findViewById(R.id.image_view_row_attachment_comment_error)
        }

        override fun bind(position: Int) {
            bind<RowAttachmentCommentBinding> {
                comment = viewModel.getComment(position)
            }

            imageViewError?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.onCommentRetryClicked(clickedPosition)
                }
            }
        }
    }
}
