package com.classera.attachment.comments

import android.view.View

interface AttachmentCommentsHandler {
    fun onSubmitAddCommentClicked(view: View)
    fun onCloseClicked(view: View)
}
