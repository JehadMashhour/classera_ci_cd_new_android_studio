package com.classera.attendance

import android.view.ViewGroup
import com.classera.attendance.databinding.RowAttendanceBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.data.models.attendance.Absences

class AttendanceAdapter(
    private val viewModel: AttendanceViewModel
) : BasePagingAdapter<AttendanceAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAttendanceBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getAttendancesCount()
    }

    inner class ViewHolder(private val binding: RowAttendanceBinding) :
        BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            val attendance = viewModel.getAttendance(position)
            handelCommentLabel(attendance)
            handelCommentLabelClick(attendance)
            bind<RowAttendanceBinding> {
                attendanceItem = viewModel.getAttendance(position)
            }
        }

        private fun handelCommentLabel(attendance: Absences?) {
            if (attendance?.isExpanded == true) {
                binding.commentLabelValue.maxLines = Int.MAX_VALUE
            } else {
                binding.commentLabelValue.maxLines = COMMENT_LABEL_MAX_TEXT_LINES
            }
        }

        private fun handelCommentLabelClick(attendance: Absences?) {
            binding.root.setOnClickListener {
                attendance?.isExpanded = attendance?.isExpanded != true
                handelCommentLabel(attendance)
                notifyItemChanged(adapterPosition)
                recyclerView?.smoothScrollToPosition(adapterPosition)
            }
        }
    }

    private companion object {

        private const val COMMENT_LABEL_MAX_TEXT_LINES = 3
    }
}
