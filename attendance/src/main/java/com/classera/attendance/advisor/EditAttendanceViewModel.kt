package com.classera.attendance.advisor

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.attendance.AbsenceSection
import com.classera.data.models.user.AbsenceStatus
import com.classera.data.models.user.User
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attendance.AttendanceRepository

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
class EditAttendanceViewModel(
    private val attendanceRepository: AttendanceRepository
) : BaseViewModel() {

    private val _statusChangedLiveData = MutableLiveData<Int>()
    val statusChangedLiveData: LiveData<Int> = _statusChangedLiveData

    private var date: String? = null
    private var selectedSectionId: String? = null
    private val students = mutableListOf<User>()
    private val sections = mutableListOf<AbsenceSection>()
    var selectedSectionIndex: Int = -1
    var currentPage: Int = DEFAULT_PAGE

    fun getSections() = liveData {
        emit(Resource.Loading(show = true))

        val resource = tryResource { attendanceRepository.getSections() }
        sections.clear()
        sections.addAll(resource.element<BaseWrapper<List<AbsenceSection>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getItemsCount(): Int {
        return students.size
    }

    fun getStudent(position: Int): User? {
        return students[position]
    }

    fun updateStudentStatus(studentId: String) {
        students.find { it.studentId == studentId }?.absenceStatus = AbsenceStatus.LEFT_WITH_PERMISSION
    }

    fun getStudents(sectionIndex: Int, pageNumber: Int = DEFAULT_PAGE) = liveData {
        currentPage = pageNumber

        if (pageNumber == DEFAULT_PAGE) {
            emit(Resource.Loading(show = true))
            students.clear()
        }

        val resource = tryResource {
            attendanceRepository.getStudents(
                sectionId = getCorrectSection(sectionIndex),
                courseId = null,
                lectureId = null,
                timeSlotId = null,
                date = date,
                page = pageNumber
            )
        }

        selectedSectionId = getCorrectSection(sectionIndex)
        students.addAll(resource.element<BaseWrapper<List<User>>>()?.data ?: listOf())
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun getCorrectSection(position: Int): String? {
        return if (position == 0) {
            sections.map { it.id }.joinToString(",")
        } else {
            sections[position - SECTION_OFFSET].id
        }
    }

    fun getSectionTitles(): List<String?> {
        return sections.map { it.sectionTitle }
    }

    fun onPresentSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.PRESENT)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.PRESENT, clickedPosition)
    }

    fun onAbsentSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.ABSENT)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.ABSENT, clickedPosition)
    }

    fun onLateSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.LATE)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.LATE, clickedPosition)
    }

    fun onExcusedSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.EXCUSED)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.EXCUSED, clickedPosition)
    }

    private fun changeStatusForStudent(status: AbsenceStatus, position: Int) = liveData {
        emit(Resource.Loading(show = true))
        val body = mapOf(
            "type" to status.value,
            "excused" to if (status == AbsenceStatus.EXCUSED) {
                1
            } else {
                0
            },
            "date" to "$date",
            "student_user_id" to getStudent(position)?.studentId
        )
        val resource = tryNoContentResource {
            attendanceRepository.changeStatusForStudent(body)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun setDate(year: Int, month: Int, dayOfMonth: Int) {
        this.date = "$year-${month + 1}-$dayOfMonth"
    }

    fun getSelectedSectionId(): String? = selectedSectionId

    fun setSelectedSectionId(selectedSectionId: String) {
        this.selectedSectionId = selectedSectionId
    }

    companion object {

        private const val SECTION_OFFSET = 1
    }
}
