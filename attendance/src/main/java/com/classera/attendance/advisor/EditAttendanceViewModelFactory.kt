package com.classera.attendance.advisor

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.courses.Course
import com.classera.data.repositories.attendance.AttendanceRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
class EditAttendanceViewModelFactory @Inject constructor(
    private val attendanceRepository: AttendanceRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EditAttendanceViewModel(attendanceRepository) as T
    }
}
