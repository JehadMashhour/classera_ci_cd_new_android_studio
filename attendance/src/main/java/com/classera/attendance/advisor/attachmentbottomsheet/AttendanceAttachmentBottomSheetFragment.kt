package com.classera.attendance.advisor.attachmentbottomsheet

import android.app.Activity
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.ImageView
import android.widget.TimePicker
import android.widget.FrameLayout
import com.classera.attendance.R
import com.classera.attendance.advisor.EditAttendanceFragment
import com.classera.core.Screen
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onTextChanged
import com.classera.data.network.errorhandling.Resource
import com.classera.data.toDate
import com.classera.data.toString
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
@Screen("Advisor Attendance")
class AttendanceAttachmentBottomSheetFragment : BaseBottomSheetDialogFragment(),
    TimePickerDialog.OnTimeSetListener, Validator.ValidationListener {

    private val validator: Validator by lazy {
        val validator = Validator(this)
        validator.setValidationListener(this)
        validator
    }

    override val layoutId: Int = R.layout.bottom_sheet_edit_attendance

    @Inject
    lateinit var viewModel: AttendanceAttachmentViewModel

    @NotEmpty(message = "err_please_select_time")
    private var attendanceAttachmentBottomSheetTimeEditText: EditText? = null

    @NotEmpty(message = "err_please_enter_reason")
    private var attendanceAttachmentBottomSheetReasonEditText: EditText? = null

    private var attendanceAttachmentBottomSheetStudentNameEditText: EditText? = null

    private var attendanceAttachmentBottomSheetAttachmentName: TextView? = null
    private var attachmentImageView: ImageView? = null
    private var attachmentSaveChangesButton: Button? = null
    private var attachmentSMSNotificationCheckBox: CheckBox? = null

    private val currentTimeCalendar = Calendar.getInstance()
    private var hour: Int = currentTimeCalendar.get(Calendar.HOUR_OF_DAY)
    private var minute: Int = currentTimeCalendar.get(Calendar.MINUTE)

    private var rootView: View? = null

    override fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = super.onCreateView(inflater, container, savedInstanceState)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initViews()
        initListeners()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheet =
                dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
            behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.isHideable = false
            imageViewClose?.isEnabled = true
            imageViewClose?.setOnClickListener {
                dialog.dismiss()
            }
        }
        return dialog
    }

    private fun findViews() {
        attachmentImageView = view?.findViewById(R.id.bottom_sheet_attendance_attachment_image_view)

        attendanceAttachmentBottomSheetTimeEditText =
            view?.findViewById(R.id.bottom_sheet_attendance_time_edit_text)

        attendanceAttachmentBottomSheetReasonEditText =
            view?.findViewById(R.id.bottom_sheet_attendance_reason_edit_text)

        attendanceAttachmentBottomSheetStudentNameEditText =
            view?.findViewById(R.id.bottom_sheet_attendance_name_edit_text)

        attendanceAttachmentBottomSheetAttachmentName =
            view?.findViewById(R.id.bottom_sheet_attendance_attachment_name_text_view)

        attachmentSMSNotificationCheckBox =
            view?.findViewById(R.id.bottom_sheet_attendance_sms_check_box)

        attachmentSaveChangesButton =
            view?.findViewById(R.id.bottom_sheet_attendance_save_button)
    }

    private fun initViews() {
        attendanceAttachmentBottomSheetStudentNameEditText?.setText(viewModel.getStudentName())
    }

    private fun initListeners() {
        attachmentImageView?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowImages(true)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .setSuffixes(
                        ".mp4",
                        ".flv",
                        ".avi",
                        ".msvideo",
                        ".x-msvideo",
                        ".quicktime",
                        ".x-flv",
                        ".mpeg",
                        ".3gp",
                        ".x-ms-wmv",
                        ".webm",
                        ".mov",
                        ".wmv"
                    )
                    .build()
            )
            startActivityForResult(
                intent,
                ATTACH_FILE_REQUEST_CODE
            )
        }

        attendanceAttachmentBottomSheetTimeEditText?.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                hour,
                minute,
                IS_24_HOURS
            ).show()
            hideKeyboard()
        }

        attendanceAttachmentBottomSheetReasonEditText?.onTextChanged {
            viewModel.setReason(it.toString())
        }

        attachmentSMSNotificationCheckBox?.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setSMSNotification(isChecked)
        }
        attachmentSaveChangesButton?.setOnClickListener {
            validator.validate()
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessResource(resource)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<*>) {
        rootView?.let {
            backWithResult(
                EditAttendanceFragment.UPDATE_UI,
                Pair(viewModel.getDate(), viewModel.getStudentId())
            )
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        rootView?.let {
            Snackbar.make(it, resource.error.resourceMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    @Suppress("EmptyFunctionBlock")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        viewModel.setDate(hourOfDay, minute)
        viewModel.getDate().toDate("yyyy-MM-dd")?.let {
            currentTimeCalendar.time = it
        }
        currentTimeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        currentTimeCalendar.set(Calendar.MINUTE, minute)
        attendanceAttachmentBottomSheetTimeEditText?.setText(
            currentTimeCalendar.time.toString(
                "yyyy-MM-dd HH:mm",
                Locale.US
            )
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ATTACH_FILE_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK && data != null
        ) {
            val files = data.getParcelableArrayListExtra<MediaFile>(
                FilePickerActivity.MEDIA_FILES
            )

            viewModel.setAttachment(files?.firstOrNull())
            attendanceAttachmentBottomSheetAttachmentName?.text = files?.firstOrNull()?.name
        }
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        errors?.forEach { error ->
            val errorMessageResourceName = error.getCollatedErrorMessage(requireContext())
            val errorMessageResource =
                resources.getIdentifier(errorMessageResourceName, "string", context?.packageName)
            val errorMessage = context?.getString(errorMessageResource)
            if (error.view.parent.parent is TextInputLayout) {
                val paren = (error.view.parent.parent as TextInputLayout)
                paren.isErrorEnabled = true
                paren.error = errorMessage
            } else if (error.view is EditText) {
                (error.view as EditText).error = errorMessage
            }
        }
    }

    override fun onValidationSucceeded() {
        viewModel.changeStatusForStudent().observe(this, this::handleResource)
    }

    private companion object {

        private const val ATTACH_FILE_REQUEST_CODE = 105

        private const val IS_24_HOURS = false
    }
}
