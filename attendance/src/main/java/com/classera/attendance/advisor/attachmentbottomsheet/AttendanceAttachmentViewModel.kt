package com.classera.attendance.advisor.attachmentbottomsheet

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.user.AbsenceStatus
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.attendance.AttendanceRepository
import com.jaiselrahman.filepicker.model.MediaFile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
class AttendanceAttachmentViewModel(
    private val arguments: AttendanceAttachmentBottomSheetFragmentArgs,
    private val attendanceRepository: AttendanceRepository
) : BaseViewModel() {

    private var date: String? = null

    private var reason = ""

    private var attachmentFile: MediaFile? = null

    private var smsNotification: Boolean = false

    internal fun changeStatusForStudent() = liveData {
        emit(Resource.Loading(show = true))
        val body = mapOf(
            "absences[0][type]" to AbsenceStatus.LEFT_WITH_PERMISSION.value,
            "type" to AbsenceStatus.LEFT_WITH_PERMISSION.value,
            "absences[0][excused]" to AbsenceStatus.LEFT_WITH_PERMISSION.value,
            "date" to date,
            "leave_date" to date,
            "notify_by_sms" to smsNotification,
            "student_user_id" to arguments.studentId,
            "section_id" to arguments.sectionId,
            "excused" to AbsenceStatus.LEFT_WITH_PERMISSION.value,
            "reason" to reason
        )
        val paramsList = ArrayList<MultipartBody.Part>()
        body.map {
            paramsList.add(MultipartBody.Part.createFormData(it.key, it.value.toString()))
        }

        attachmentFile?.let {
            if (it.path.isNotBlank() || it.path.isNotEmpty()) {
                val file = File(it.path)
                val attachmentRequestBody = RequestBody.create(MediaType.parse(it.mimeType), file)
                paramsList.add(MultipartBody.Part.createFormData("file", file.name, attachmentRequestBody))
            }
        }

        val resource = tryNoContentResource {
            attendanceRepository.setLeaveWithPermission(paramsList)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun setDate(hourOfDay: Int, minute: Int) {
        this.date = "${arguments.date} $hourOfDay:$minute"
    }

    fun getDate() : String = this.arguments.date

    fun getStudentId() : String = this.arguments.studentId

    fun setReason(reason: String) {
        this.reason = reason
    }

    fun setAttachment(attachmentFile: MediaFile?) {
        this.attachmentFile = attachmentFile
    }

    fun setSMSNotification(isChecked: Boolean) {
        this.smsNotification = isChecked
    }

    fun getSectionID() = arguments.sectionId

    fun getStudentName() = arguments.studentName
}
