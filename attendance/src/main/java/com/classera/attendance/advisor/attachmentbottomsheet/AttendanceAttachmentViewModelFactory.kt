package com.classera.attendance.advisor.attachmentbottomsheet

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.attendance.AttendanceRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
class AttendanceAttachmentViewModelFactory @Inject constructor(
    private val arguments: AttendanceAttachmentBottomSheetFragmentArgs,
    private val attendanceRepository: AttendanceRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AttendanceAttachmentViewModel(arguments, attendanceRepository) as T
    }
}
