package com.classera.attendance.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.models.courses.Course
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class TakeAttendanceFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: TakeAttendanceViewModelFactory
        ): TakeAttendanceViewModel {
            return ViewModelProvider(fragment, factory)[TakeAttendanceViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideCourse(fragment: Fragment): Course? {
            val args by fragment.navArgs<TakeAttendanceFragmentArgs>()
            return args.course
        }
    }

    @Binds
    abstract fun bindFragment(fragment: TakeAttendanceFragment): Fragment
}
