@file:Suppress("DEPRECATION")

package com.classera.attendance.teacher

import android.app.ProgressDialog
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.attendance.R
import com.classera.attendance.databinding.RowTakeAttendanceBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.data.network.errorhandling.Resource

/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
class TakeAttendanceStudentsAdapter(
    private val viewModel: TakeAttendanceViewModel,
    private val lifecycleOwner: LifecycleOwner
) : BasePagingAdapter<TakeAttendanceStudentsAdapter.ViewHolder>() {

    private var progressDialog: ProgressDialog? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(context)
        progressDialog?.setMessage(context?.getString(R.string.please_wait))
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowTakeAttendanceBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getItemsCount()
    }

    inner class ViewHolder(binding: RowTakeAttendanceBinding) : BaseBindingViewHolder(binding) {

        private var radioButtonPresent: RadioButton? = null
        private var radioButtonAbsent: RadioButton? = null
        private var radioButtonLate: RadioButton? = null
        private var radioButtonExcused: RadioButton? = null

        init {
            radioButtonPresent = itemView.findViewById(R.id.radio_button_row_take_attendance_present)
            radioButtonAbsent = itemView.findViewById(R.id.radio_button_row_take_attendance_absent)
            radioButtonLate = itemView.findViewById(R.id.radio_button_row_take_attendance_late)
            radioButtonExcused = itemView.findViewById(R.id.radio_button_row_take_attendance_excused)
        }

        override fun bind(position: Int) {
            radioButtonPresent?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onPresentSelected(clickedPosition))
                }
            }

            radioButtonAbsent?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onAbsentSelected(clickedPosition))
                }
            }

            radioButtonLate?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onLateSelected(clickedPosition))
                }
            }

            radioButtonExcused?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onExcusedSelected(clickedPosition))
                }
            }

            bind<RowTakeAttendanceBinding> {
                this.user = viewModel.getStudent(position)
                this.courseName = viewModel.getCourseName()
            }
        }

        private fun changeStatus(liveData: LiveData<Resource>) {
            liveData.observe(lifecycleOwner, ::handleResource)
        }

        private fun handleResource(resource: Resource) {
            when (resource) {
                is Resource.Loading -> {
                    handleLoadingResource(resource)
                }
                is Resource.Error -> {
                    handleErrorResource(resource)
                }
            }
        }

        private fun handleLoadingResource(resource: Resource.Loading) {
            if (resource.show) {
                progressDialog?.show()
            } else {
                progressDialog?.dismiss()
            }
        }

        private fun handleErrorResource(resource: Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        }
    }
}
