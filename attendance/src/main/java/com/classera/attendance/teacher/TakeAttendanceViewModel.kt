package com.classera.attendance.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attendance.AbsenceSection
import com.classera.data.models.attendance.AttendanceTimeSlot
import com.classera.data.models.courses.Course
import com.classera.data.models.user.AbsenceStatus
import com.classera.data.models.user.User
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.attendance.AttendanceRepository
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

@Suppress("LongMethod")
class TakeAttendanceViewModel(
    private val attendanceRepository: AttendanceRepository,
    private var course: Course?
) : BaseViewModel() {

    private val _noTimeSlotsLiveData = MutableLiveData<Unit>()
    val noTimeSlotsLiveData: LiveData<Unit> = _noTimeSlotsLiveData

    private val _newTimeSlotsLiveData = MutableLiveData<Unit>()
    val newTimeSlotsLiveData: LiveData<Unit> = _newTimeSlotsLiveData

    private var courses: MutableList<Course> = mutableListOf()

    private val _statusChangedLiveData = MutableLiveData<Int>()
    val statusChangedLiveData: LiveData<Int> = _statusChangedLiveData

    private val _statusResponseLiveData = MutableLiveData<String>()
    val statusResponseLiveData: LiveData<String> = _statusResponseLiveData

    private var date: String? = null
    private var isCreateNewTimeSlot = false
    private var lectureId: String? = null
    private var courseId: String? = null
    private var timeSlotId: String? = null
    private val students = mutableListOf<User>()
    private val timeSlots = mutableListOf<AttendanceTimeSlot>()
    private val sections = mutableListOf<AbsenceSection>()

    var timeSlotPosition: Int? = null

    fun getSections() = liveData {
        emit(Resource.Loading(show = true))

        val resource = tryResource { attendanceRepository.getSections(course?.courseId) }
        sections.clear()
        sections.addAll(
            resource.element<
                    BaseWrapper<List<AbsenceSection>>>()?.data ?: listOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCourses() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource = tryResource { attendanceRepository.getCourses() }

        courses.clear()
        courses.addAll(
            resource.element<
                    BaseWrapper<List<Course>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getItemsCount(): Int {
        return students.size
    }

    fun getStudent(position: Int): User? {
        return students[position]
    }

    fun onCourseSelected(position: Int): LiveData<Resource> {
        course = courses[position]
        return getSections()
    }

    fun onSectionSelected(position: Int) = liveData {
        lectureId = sections[position].lectureId

        emit(Resource.Loading(show = true))

        var resource = tryResource {
            attendanceRepository.getSectionTimeSlots(lectureId)
        }
        if (resource is Resource.Error && resource.error.code == NO_TIME_SLOTS_CODE) {
            resource =
                tryResource { attendanceRepository.getAllTimeSlots() }
            if (resource.isSuccess()) {
                isCreateNewTimeSlot = true
                _noTimeSlotsLiveData.postValue(Unit)
            }
        } else {
            isCreateNewTimeSlot = false
        }

        timeSlots.clear()
        timeSlots.addAll(
            resource.element<BaseWrapper<List<AttendanceTimeSlot>>>()?.data ?: listOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSectionTitles(): List<String?> {
        return sections.map { it.sectionTitle }
    }

    fun getCoursesTitles(): List<String?> {
        return courses.map { it.courseTitle }
    }

    fun getTimeSlotTitles(): List<String?> {
        return timeSlots.map { it.title }
    }

    // TODO Should be enhanced
    fun onTimeSlotSelected(position: Int, pageNumber: Int) = liveData {

        timeSlotId = timeSlots[position].id

        emit(Resource.Loading(show = true))

        if (isCreateNewTimeSlot) {
            val createNewTimeSlotResource =
                tryNoContentResource {
                    attendanceRepository.createNewTimeSlot(
                        lectureId,
                        timeSlotId
                    )
                }
            if (createNewTimeSlotResource is Resource.Error &&
                createNewTimeSlotResource.error.code == NO_TIME_SLOTS_CODE
            ) {
                emit(createNewTimeSlotResource)
                isCreateNewTimeSlot = true
            } else {
                timeSlotId = timeSlots[position].id

                var resource = tryResource {
                    attendanceRepository.getSectionTimeSlots(lectureId)
                }
                if (resource is Resource.Error && resource.error.code == NO_TIME_SLOTS_CODE) {
                    resource =
                        tryResource { attendanceRepository.getAllTimeSlots() }
                    if (resource.isSuccess()) {
                        isCreateNewTimeSlot = true
                        _noTimeSlotsLiveData.postValue(Unit)
                    }
                } else {
                    isCreateNewTimeSlot = false
                }

                timeSlots.clear()
                timeSlots.addAll(
                    resource.element<BaseWrapper<List<AttendanceTimeSlot>>>()?.data ?: listOf()
                )
                emit(resource)
                _newTimeSlotsLiveData.postValue(Unit)
            }
        } else {
            timeSlotId = timeSlots[position].id
            val resource =
                tryResource {
                    attendanceRepository.getStudents(
                        courseId = course?.courseId,
                        lectureId = lectureId,
                        timeSlotId = timeSlotId,
                        date = date,
                        page = pageNumber
                    )
                }
            students.clear()
            students.addAll(resource.element<BaseWrapper<List<User>>>()?.data ?: listOf())
            emit(resource)
        }

        emit(Resource.Loading(show = false))
    }

    fun getCourseName(): String? {
        return course?.courseTitle
    }

    fun onPresentSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.PRESENT)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.PRESENT, clickedPosition)
    }

    fun onAbsentSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.ABSENT)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.ABSENT, clickedPosition)
    }

    fun onLateSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.LATE)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.LATE, clickedPosition)
    }

    fun onExcusedSelected(clickedPosition: Int): LiveData<Resource> {
        students[clickedPosition] =
            students[clickedPosition].copy(absenceStatus = AbsenceStatus.EXCUSED)
        _statusChangedLiveData.value = clickedPosition
        return changeStatusForStudent(AbsenceStatus.EXCUSED, clickedPosition)
    }

    private fun changeStatusForStudent(
        status: AbsenceStatus,
        position: Int
    ) = liveData {
        emit(Resource.Loading(show = true))
        val body = mapOf(
            "absences[0][type]" to status.value,
            "absences[0][excused]" to if (status == AbsenceStatus.EXCUSED) 1 else 0,
            "date" to "$date",
            "absences[0][student_user_id]" to students[position].studentId,
            "timeslot_id" to timeSlotId,
            "lecture_id" to lectureId
        )
        val resource = tryNoContentResource {
            attendanceRepository.changeStatusForStudent(body)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun setDate(year: Int, month: Int, dayOfMonth: Int) {
        this.date = "$year-${month + 1}-$dayOfMonth"
    }

    fun takeAttendanceByFace(
        attachmentFileList: ArrayList<String>,
        attachmentTypeList: ArrayList<String>
    ) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()
        paramsList.add(MultipartBody.Part.createFormData("lecture_id", lectureId))
        paramsList.add(MultipartBody.Part.createFormData("timeslot_id", timeSlotId))
        attachmentPayload(paramsList, attachmentFileList, attachmentTypeList)
        val resource = tryNoContentResource {
            attendanceRepository.takeAttendanceByFace(paramsList)
        }
        if (resource.isSuccess()) {
            _statusResponseLiveData.postValue(
                ((resource as Resource.Success<*>).data as NoContentBaseWrapper).message)
            val resource =
                tryResource {
                    attendanceRepository.getStudents(
                        courseId = course?.courseId,
                        lectureId = lectureId,
                        timeSlotId = timeSlotId,
                        date = date,
                        page = DEFAULT_PAGE
                    )
                }
            students.clear()
            students.addAll(resource.element<BaseWrapper<List<User>>>()?.data ?: listOf())
            emit(resource)
            emit(Resource.Loading(show = false))
        }
    }

    private fun attachmentPayload(
        paramsList: ArrayList<MultipartBody.Part>,
        attachmentFileList: ArrayList<String>,
        attachmentFileTypeList: ArrayList<String>
    ) {
        attachmentFileList.forEachIndexed { index, _ ->
            val attachmentFile = File(attachmentFileList[index])
            val imageRequestBody =
                RequestBody.create(MediaType.parse(attachmentFileTypeList[index]), attachmentFile)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    "file[$index]",
                    attachmentFile.name,
                    imageRequestBody
                )
            )
        }
    }

    private companion object {

        private const val NO_TIME_SLOTS_CODE = 1045
    }
}
