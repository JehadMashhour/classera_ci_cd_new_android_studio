package com.calssera.behaviours

import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.adapter.BaseViewHolder


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
abstract class BehavioursAdapter<V : BaseViewHolder> : BasePagingAdapter<V>()

