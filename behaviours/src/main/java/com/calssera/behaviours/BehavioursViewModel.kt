package com.calssera.behaviours

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
abstract class BehavioursViewModel :
    BaseViewModel() {

    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var behaviours: MutableList<Behaviour> = mutableListOf()

    fun getUserBehaviours(
        text: CharSequence?,
        filterKey: String?,
        pageNumber: Int
    ): LiveData<Resource> {
        return getBehaviours(text, filterKey, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    fun refreshBehaviours(text: CharSequence?, key: String?) =
        getBehaviours(text, key, DEFAULT_PAGE, false)

    private fun getBehaviours(
        text: CharSequence?,
        filter: String?,
        pageNumber: Int,
        showProgress: Boolean
    ) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                getBehaviours(text = text, filter = filter, pageNumber = pageNumber)
            }
            if (pageNumber == DEFAULT_PAGE) {
                behaviours.clear()
            }
            behaviours.addAll(
                resource.element<BaseWrapper<List<Behaviour>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }


    fun getBehaviour(position: Int): Behaviour? {
        return behaviours[position]
    }

    fun getBehavioursCount(): Int {
        return behaviours.size
    }

    abstract suspend fun getBehaviours(
        text: CharSequence?,
        filter: String?,
        pageNumber: Int
    ): BaseWrapper<List<Behaviour>>


    fun deleteItem(position: Int) {
        behaviours.removeAt(position)
    }

    abstract fun deleteBehaviour(position: Int)
}
