package com.calssera.behaviours.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.models.behaviours.teacher.BehaviorAction
import com.classera.data.models.behaviours.teacher.BehaviorActionsWraper
import com.classera.data.models.behaviours.teacher.BehaviorGroup
import com.classera.data.models.courses.Course
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.behaviours.BehavioursRepository
import com.classera.data.repositories.courses.CoursesRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AddBehaviourViewModel(
    private val behavioursRepository: BehavioursRepository,
    private val coursesRepository: CoursesRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var selectedStudentPosition = 0
    private var selectedCoursePosition = 0
    private var selectedBehaviourPosition = 0
    private var selectedActionPosition = 0
    private var selectedGroupPosition = 0

    private val courses = mutableListOf<Course>()
    private val students = mutableListOf<User>()
    private val behaviorGroup = mutableListOf<BehaviorGroup>()
    private val behaviors = mutableListOf<Behaviour>()
    private val behaviorActions = mutableListOf<BehaviorAction>()

    private var behaviour: Behaviour? = null

    fun getCourses() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { coursesRepository.getCourses() }

        courses.addAll(resource.element<BaseWrapper<List<Course>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getBehaviorGroup(behaviorType: String) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource {
            behavioursRepository.getBehaviourGroups(behaviorType)
        }

        behaviorGroup.clear()
        behaviorGroup.addAll(resource.element<BaseWrapper<List<BehaviorGroup>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getBehaviors(position: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        selectedBehaviourPosition = position

        val resource = tryResource {
            behavioursRepository.getBehaviourActions(behaviorGroup[position].id)
        }

        behaviorActions.clear()
        behaviorActions.addAll(
            resource.element<BaseWrapper<BehaviorActionsWraper>>()?.data?.actions ?: listOf()
        )

        behaviors.clear()
        behaviors.addAll(
            resource.element<BaseWrapper<BehaviorActionsWraper>>()?.data?.behaviors ?: listOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCourseTitles(): List<String> {
        return courses.mapNotNull { it.courseTitle }
    }

    fun getBehaviorGroupTitles(): List<String> {
        return behaviorGroup.mapNotNull { it.title }
    }

    fun getBehaviorActionsTitles(): List<String> {
        return behaviorActions.mapNotNull { it.title }
    }

    fun getBehaviorsTitles(): List<String> {
        return behaviors.mapNotNull { it.title }
    }

    fun getStudentsByCourse(position: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource {
            selectedCoursePosition = position
            behavioursRepository.getStudentsByCourse(courses[position].courseId)
        }

        students.clear()
        students.addAll(resource.element<BaseWrapper<List<User>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun onStudentSelected(position: Int) {
        this.selectedStudentPosition = position
    }

    fun onCourseSelected(position: Int) {
        this.selectedCoursePosition = position
    }

    fun onBehaviourSelected(position: Int) {
        this.selectedBehaviourPosition = position
    }

    fun onActionSelected(position: Int) {
        this.selectedActionPosition = position
    }

    fun onGroupSelected(position: Int) {
        this.selectedGroupPosition = position
    }

    fun getSelectedStudentId(): String? {
        return students[selectedStudentPosition].studentBehaviorId
    }

    fun getSelectedLectureId(): String? {
        return courses[selectedCoursePosition].lectureId
    }

    fun getSelectedBehaviourId(): String? {
        return behaviors[selectedBehaviourPosition].id
    }

    fun getSelectedActionId(): String? {
        return behaviorActions[selectedActionPosition].id
    }

    fun actionsFound(): Boolean {
        return behaviorActions.size != 0
    }


    fun getSemesterId(): String? {
        return prefs.semesterId
    }

    fun addBehaviour(data: Map<String, Any?>, attachmentPath: String, mediaType: String) =
        liveData {
            emit(Resource.Loading(show = true))

            val resource = tryNoContentResource {
                behavioursRepository.addBehaviour(
                    data,
                    attachmentPath,
                    mediaType
                )
            }

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun editBehaviour(data: Map<String, Any?>, attachmentPath: String, mediaType: String) =
        liveData {
            emit(Resource.Loading(show = true))

            val resource = tryNoContentResource {
                behavioursRepository.editBehaviour(
                    data,
                    attachmentPath,
                    mediaType
                )
            }

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun setBehaviour(behaviour: Behaviour?) {
        this.behaviour = behaviour
    }

    fun getBehaviour(): Behaviour? {
        return behaviour
    }

    fun getSelectedCoursePosition(): Int {
        return courses.map { it.courseTitle }.indexOf(behaviour?.courseTitle)
    }

    fun getSelectedBehaviourGroupPosition(): Int {
        return behaviorGroup.map { it.id }.indexOf(behaviour?.behaviorGroupId)
    }

    fun getSelectedStudentPosition(): Int {
        return students.map { it.id }.indexOf(behaviour?.studentId)
    }

    fun getSelectedBehaviourPosition(): Int {
        return behaviors.map { it.id }.indexOf(behaviour?.behaviorId)
    }

    fun getSelectedActionPosition(): Int {
        return behaviorActions.map { it.id }.indexOf(behaviour?.actionId)
    }

    fun isEditMode(): Boolean {
        return behaviour != null
    }

    companion object {

        const val BEHAVIOR_TYPE_POSITIVE = "1"
        const val BEHAVIOR_TYPE_NEGATIVE = "2"
    }
}
