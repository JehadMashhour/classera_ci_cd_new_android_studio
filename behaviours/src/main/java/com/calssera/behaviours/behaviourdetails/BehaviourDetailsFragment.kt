package com.calssera.behaviours.behaviourdetails

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import com.calssera.behaviours.R
import com.calssera.behaviours.databinding.FragmentBehaviourDetailsBinding
import com.classera.core.Screen
import com.classera.core.fragments.BaseBindingFragment
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.models.user.UserRole
import com.google.android.material.button.MaterialButton

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Behaviour Details")
class BehaviourDetailsFragment : BaseBindingFragment() {

    private val args: BehaviourDetailsFragmentArgs by navArgs()

    private var studentLayout: LinearLayout? = null

    override val layoutId: Int = R.layout.fragment_behaviour_details

    private var behavioursResponse: Behaviour? = null

    private var materialButtonDownloadBehaviour: MaterialButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        behavioursResponse = args.behaviour
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind<FragmentBehaviourDetailsBinding> {
            this?.behaviour = behavioursResponse
        }
        studentLayout = view?.findViewById(R.id.behavior_student_row_details)
        if (args.role == UserRole.TEACHER) {
            studentLayout?.visibility = View.VISIBLE
        }

        materialButtonDownloadBehaviour = view?.findViewById(
            R.id.button_behaviour_details_download_attachment_button
        )

        if (behavioursResponse?.attachmentUrl.toString().isNotEmpty()
            || behavioursResponse?.attachmentUrl.toString().isNotBlank()
        ) {
            materialButtonDownloadBehaviour?.visibility = View.VISIBLE
        } else {
            materialButtonDownloadBehaviour?.visibility = View.GONE
        }

        materialButtonDownloadBehaviour?.setOnClickListener {
            if (requestDownloadReportPermission()) {
                startDownload()
            }
        }

    }

    private fun requestDownloadReportPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            true
        } else {
            requireActivity().let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    RC_STORAGE_PERMISSION
                )
            }
            false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == RC_STORAGE_PERMISSION
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            startDownload()
        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.message_storage_permission), Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun startDownload() {
        val request: DownloadManager.Request = DownloadManager.Request(
            Uri.parse(behavioursResponse?.attachmentUrl)
        )
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(
            DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
        )
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            behavioursResponse?.attachmentId + "." +
                    getFileExtension(behavioursResponse?.attachmentUrl)
        )
        request.setMimeType(
            getMimeFromFileName(
                behavioursResponse?.attachmentId + "." + getFileExtension(
                    behavioursResponse?.attachmentUrl
                )
            )
        )
        val dm: DownloadManager? =
            context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
        dm?.enqueue(request)
    }

    private fun getMimeFromFileName(fileName: String): String? {
        val map = MimeTypeMap.getSingleton()
        val ext = MimeTypeMap.getFileExtensionFromUrl(fileName)
        return map.getMimeTypeFromExtension(ext)
    }

    private fun getFileExtension(url: String?): String? {
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }

    private companion object {

        private const val RC_STORAGE_PERMISSION = 101
    }
}

