package com.calssera.behaviours.student

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.behaviours.BehavioursViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class BehavioursStudentFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: BehavioursViewModelFactory
        ): BehavioursStudentViewModel {
            return ViewModelProvider(fragment, factory)[BehavioursStudentViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideBehavioursAdapter(viewModel: BehavioursStudentViewModel): BehavioursStudentAdapter {
            return BehavioursStudentAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: BehavioursStudentFragment): Fragment
}
