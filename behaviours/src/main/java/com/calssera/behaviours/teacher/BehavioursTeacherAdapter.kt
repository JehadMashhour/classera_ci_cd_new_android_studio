package com.calssera.behaviours.teacher

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.calssera.behaviours.BehavioursAdapter
import com.calssera.behaviours.BehavioursViewModel
import com.calssera.behaviours.R
import com.calssera.behaviours.databinding.RowBehavioursBinding
import com.classera.core.adapter.BaseBindingViewHolder


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */

class BehavioursTeacherAdapter(
    private val viewModel: BehavioursViewModel
) : BehavioursAdapter<BehavioursTeacherAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowBehavioursBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getBehavioursCount()
    }

    inner class ViewHolder(binding: RowBehavioursBinding) : BaseBindingViewHolder(binding) {

        private var imageViewMore: ImageView? = null
        private var relativeViewMore: RelativeLayout? = null
        private var studentInformation: LinearLayout? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_behavior_more)
            relativeViewMore = itemView.findViewById(R.id.relative_layout_row_behaviors_more)
            studentInformation = itemView.findViewById(R.id.behavior_student_row)
        }

        override fun bind(position: Int) {
            studentInformation?.visibility = View.VISIBLE

            imageViewMore?.setOnClickListener { view ->
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(view, clickedPosition)
                }
            }
            bind<RowBehavioursBinding> { behavioursItem = viewModel.getBehaviour(position) }
        }
    }
}
