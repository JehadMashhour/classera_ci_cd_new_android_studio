package com.classera.calendar

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.dp
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.format.MonthArrayTitleFormatter
import com.prolificinteractive.materialcalendarview.spans.DotSpan
import java.util.*

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Academic Calendar")
abstract class AcademicCalendarFragment : BaseFragment(), AcademicCalender {

    private var progressBar: ProgressBar? = null
    private var calendarView: MaterialCalendarView? = null
    private var recyclerView: RecyclerView? = null
    private var selectedDay: CalendarDay? = null
    private var addFabButton: FloatingActionButton? = null

    override val layoutId: Int = R.layout.fragment_academic_calendar

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initCalendarCustomMonthsName()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_academic_calendar)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_academic_calendar)
        calendarView = view?.findViewById(R.id.calendar_view_academic_calendar)
        addFabButton = view?.findViewById(R.id.floating_action_button_fragment_calender_add)
    }

    private fun initCalendarCustomMonthsName() {
        calendarView?.setTitleFormatter(MonthArrayTitleFormatter(resources.getTextArray(R.array.custom_months_name)))
    }

    private fun initListeners() {
        calendarView?.setOnMonthChangedListener { _, date ->
            val calendar = Calendar.getInstance().apply {
                set(Calendar.MONTH, date.month - 1)
                set(Calendar.YEAR, date.year)
                set(Calendar.DAY_OF_MONTH, date.day)
            }
            getEvents(calendar)
        }

        calendarView?.setOnDateChangedListener { _, date, _ ->
            if (selectedDay == date) {
                calendarView?.selectedDate = null
                selectedDay = null
                updateEvents(selectedDay)
            } else {
                selectedDay = date
                updateEvents(selectedDay)
            }
        }
        if (selectedDay != null) {
            calendarView?.selectedDate = selectedDay
        } else {
            calendarView?.selectedDate = CalendarDay.today()
        }
        selectedDay = calendarView?.selectedDate
        getEvents(Calendar.getInstance())
    }

    private fun updateEvents(selectedDay: CalendarDay?) {
        getCalenderViewModel().updateEvents(selectedDay)
        initAdapter()
    }

    private fun getEvents(calendar: Calendar) {
        val firstDayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH)
        val lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + 1
        val startDate =
            "$year-${"%02d".format(Locale.US, month)}-${"%02d".format(Locale.US, firstDayOfMonth)}"
        val endDate =
            "$year-${"%02d".format(Locale.US, month)}-${"%02d".format(Locale.US, lastDayOfMonth)}"
        getCalenderViewModel().getUserEvents(startDate, endDate).observe(this, ::handleResource)
    }

    abstract fun getCalenderViewModel(): AcademicCalendarViewModel

    abstract fun getCalenderAdapter(): AcademicCalendarAdapter<*>

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<List<AcademicCalendarEvent>>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            recyclerView?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<List<AcademicCalendarEvent>>>) {
        val days = getCalenderViewModel().getEvents()
            ?.map { "${it?.getStartDateYear()}${it?.getStartDateMonth()}${it?.getStartDateDay()}" }
            ?.toHashSet()
        updateEvents(selectedDay)
        updateCalendarView(days)

    }

    private fun initAdapter() {
        recyclerView?.adapter = getCalenderAdapter()
        handleAdapterListeners()
    }

    abstract fun handleAdapterListeners()

    private fun updateCalendarView(days: HashSet<String>?) {
        calendarView?.addDecorator(object : DayViewDecorator {

            override fun shouldDecorate(day: CalendarDay?): Boolean {
                return days?.contains("${day?.year}${day?.month}${day?.day}") == true
            }

            override fun decorate(view: DayViewFacade?) {
                view?.addSpan(
                    DotSpan(
                        DOT_SPAN_RADIUS,
                        ContextCompat.getColor(requireContext(), R.color.colorPrimary)
                    )
                )
            }
        })
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun refreshAcademicCalender() {
        initListeners()
        updateEvents(calendarView?.selectedDate)
        calendarView?.removeDecorators()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        calendarView = null
        super.onDestroyView()
    }

    private companion object {

        private val DOT_SPAN_RADIUS = 3.dp.toFloat()
    }

}
