package com.classera.calendar

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.add
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.toDate
import com.classera.data.toString
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Project: Classera
 * Created: Dec 9, 2019
 * @author Odai Nazzal
 */

abstract class AcademicCalendarViewModel(private val prefs: Prefs) : BaseViewModel() {

    private var eventList: MutableList<AcademicCalendarEvent?>? = null
    private var newEventList: MutableList<AcademicCalendarEvent?> = mutableListOf()
    private var tempEventList: MutableList<AcademicCalendarEvent?>? = mutableListOf()
    private var mainEventList: MutableList<AcademicCalendarEvent?>? = mutableListOf()

    private var job = Job()

    fun getUserEvents(start: String?, end: String?): LiveData<Resource> {
        job.cancel()
        job = Job()
        return liveData(CoroutineScope(job + Dispatchers.IO).coroutineContext) {
            emit(Resource.Loading(show = true))

            val resource = tryResource {
                getEvents(
                    "$start 00:00:00",
                    "$end 23:59:59"
                )
            }
            eventList?.clear()
            eventList =
                resource.element<BaseWrapper<List<AcademicCalendarEvent>>>()?.data?.toMutableList()
            mainEventList?.clear()
            eventList?.distinct()?.let { mainEventList?.addAll(it) }
            tempEventList?.clear()
            newEventList?.clear()
            if (eventList?.size ?: 0 > 0) {

                eventList?.forEach { event ->
                    val listOfDiffDates = getDates(
                        event?.getStartDate().add(Calendar.DAY_OF_MONTH, 1) ?: "",
                        event?.getEndDate() ?: ""
                    )
                    listOfDiffDates?.forEach { dateDiff ->
                        newEventList.add(event?.copy(start = dateDiff))
                    }
                }

                eventList?.addAll(newEventList.distinct())
                tempEventList?.addAll(eventList ?: listOf())
            } else {
                tempEventList?.clear()
                eventList?.clear()
                newEventList?.clear()
            }
            emit(resource)
            emit(Resource.Loading(show = false))
        }
    }

    fun getEvents(): MutableList<AcademicCalendarEvent?>? {
        return tempEventList
    }

    fun getEventsCount(): Int {
        return tempEventList?.size ?: 0
    }

    fun getEvent(position: Int): AcademicCalendarEvent? {
        return tempEventList?.get(position)
    }

    abstract suspend fun getEvents(
        start: String?,
        end: String?
    ): BaseWrapper<List<AcademicCalendarEvent>>

    fun deleteItem(position: Int) {
        eventList?.remove(tempEventList?.get(position))
        tempEventList?.removeAt(position)
    }

    abstract fun deleteEvent(position: Int)

    fun isCreator(position: Int): Boolean {
        val event = getEvent(position)
        return event?.creatorId.equals(prefs.userId)
    }

    fun updateEvents(selectedDay: CalendarDay?) {
        val year = selectedDay?.year
        val month = selectedDay?.month
        val day = selectedDay?.day

        if (selectedDay == null) {
            tempEventList?.clear()
            tempEventList?.addAll(eventList ?: listOf())
        } else {
            val selectedDateString = "%04d-%02d-%02d".format(year, month, day)
            val selectedDate = selectedDateString.toDate("yyyy-MM-dd")
            tempEventList?.clear()
            tempEventList = eventList?.filter {
                it?.start?.toString("yyyy-MM-dd")?.toDate("yyyy-MM-dd")?.time == selectedDate?.time
            }?.toMutableList()
        }
    }

    private fun getDates(
        startDateString: String,
        endDateString: String
    ): List<Date>? {
        val dates = ArrayList<Date>()
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.UK)
        var startDate: Date? = null
        var endDate: Date? = null
        try {
            startDate = dateFormat.parse(startDateString)
            endDate = dateFormat.parse(endDateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val startDateCalendar: Calendar = Calendar.getInstance()
        startDateCalendar.time = startDate
        val endDateCalendar: Calendar = Calendar.getInstance()
        endDateCalendar.time = endDate
        while (!startDateCalendar.after(endDateCalendar)) {
            dates.add(startDateCalendar.time)
            startDateCalendar.add(Calendar.DATE, 1)
        }
        return dates
    }

    fun hideFabButton(): Boolean? {
        return prefs.userRole == UserRole.GUARDIAN || prefs.userRole == UserRole.STUDENT
    }

    fun getEventById(id: String?): AcademicCalendarEvent? {
        var event: AcademicCalendarEvent? = null
        mainEventList?.forEach { tempEvent ->
            if (tempEvent?.id == id) {
                event = tempEvent
            }
        }
        return event
    }
}
