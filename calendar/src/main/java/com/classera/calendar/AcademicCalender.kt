package com.classera.calendar


/**
 * Created by Rawan Al-Theeb on 3/3/2021.
 * Classera
 * r.altheeb@classera.com
 */
interface AcademicCalender {

    fun refreshAcademicCalender()
}
