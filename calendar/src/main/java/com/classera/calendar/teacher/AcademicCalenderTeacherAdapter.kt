package com.classera.calendar.teacher

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.classera.calendar.AcademicCalendarAdapter
import com.classera.calendar.AcademicCalendarViewModel
import com.classera.calendar.R
import com.classera.calendar.databinding.RowAcademicCalendarEventBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.google.android.material.floatingactionbutton.FloatingActionButton


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AcademicCalenderTeacherAdapter(private val viewModel: AcademicCalendarViewModel) :
    AcademicCalendarAdapter<AcademicCalenderTeacherAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowAcademicCalendarEventBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getEventsCount()
    }

    inner class ViewHolder(binding: RowAcademicCalendarEventBinding) : BaseBindingViewHolder(binding) {
        private var imageViewMore: ImageView? = null
        private var relativeLayoutMore: RelativeLayout
        private var floatingButtonAdd : FloatingActionButton? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_academic_calender_more)
            relativeLayoutMore = itemView.findViewById(R.id.relative_layout_row_academic_calender_more)
            floatingButtonAdd = itemView.findViewById(R.id.floating_action_button_fragment_calender_add)
        }

        override fun bind(position: Int) {
            floatingButtonAdd?.visibility = View.VISIBLE
            relativeLayoutMore.visibility =
                if (viewModel.isCreator(position)) View.VISIBLE
                else View.GONE

            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }
            bind<RowAcademicCalendarEventBinding> {
                this.event = viewModel.getEvent(position)
            }
        }
    }
}
