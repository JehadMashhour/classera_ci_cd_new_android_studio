package com.classera.callchildren.base

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.callchild.R
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.setItemsClickable
import com.classera.data.models.BaseWrapper
import com.classera.data.models.callchildren.BaseStudent
import com.classera.data.models.callchildren.CallStatus
import com.classera.data.models.callchildren.CallStatusTypes
import com.classera.data.models.callchildren.Request
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import kotlin.concurrent.fixedRateTimer

abstract class BaseCallStudentFragment : BaseFragment() {

    private var adapter: BaseCallStudentListAdapter<*>? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var errorView: ErrorView? = null
    override val layoutId: Int = R.layout.fragment_call_student

    abstract fun getViewModel(): BaseCallStudentViewModel

    abstract fun getAdapter(): BaseCallStudentListAdapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_parent_child_list)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_parent_child_list)
        swipeRefreshLayout =
            view?.findViewById(R.id.swipe_refresh_layout_fragment_parent_child_list)
        errorView = view?.findViewById(R.id.error_view_fragment_parent_child_list)
    }


    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getStudents()
        }

        getStudentsFrequently()
    }

    private fun getStudentsFrequently() {
        fixedRateTimer("timer", false, INITIAL_DELAY, TIME_PERIOD) {
            activity?.runOnUiThread {
                getStudents()
            }
        }
    }


    private fun getStudents() {
        getViewModel().getStudents().observe(this, this::handleResource)
    }


    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            //recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            //recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.apply {
                getViewModel().getBaseStudentDiffResult()?.dispatchUpdatesTo(this)
            }
        }
        adapter?.finishLoading()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getStudents() }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = getAdapter()
        adapter?.apply {
            setOnItemClickListener(::onItemClicked)
        }
        recyclerView?.adapter = adapter
    }

    private fun onItemClicked(view: View, position: Int) {
        when (view.id) {
            R.id.fragment_call_student_arrived_button -> {
                changeCallStatus(
                    position,
                    getViewModel().getChild(position).requestId,
                    CallStatusTypes.ARRIVED.id.toString()
                )
            }
            else -> {
                val baseStudent = getViewModel().getChild(position)

                setItemsClickableRecyclerView(false)
                if (baseStudent.canCall) {
                    callChild(getViewModel().getChild(position).id, position)
                } else {
                    showCallingDialog(baseStudent, position)
                }
            }
        }
    }

    private fun callChild(childId: String?, position: Int) {
        getViewModel().callChild(childId).observe(this, {
            when (it) {
                is Resource.Success<*> -> {
                    getCallStatus(
                        position,
                        it.element<BaseWrapper<Request>>()?.data?.requestId,
                        true
                    )
                }
                is Resource.Error -> {
                    showErrorDialog(it.error.message ?: it.error.cause?.message)
                }
            }
        })
    }


    private fun getCallStatus(
        position: Int,
        newRequestId: String?,
        showCallingDialog: Boolean = false
    ) {
        getViewModel().getCallStatus(newRequestId).observe(this, {
            when (it) {
                is Resource.Success<*> -> {
                    it.element<BaseWrapper<CallStatus>>()?.data?.let {
                        val baseStudent = getViewModel().getChild(position).apply {
                            status = it.callStatusTypes.id.toString()
                            requestId = newRequestId
                            isLoading = false
                        }

                        adapter?.notifyItemChanged(position)

                        if (showCallingDialog)
                            showCallingDialog(baseStudent, position)

                    }
                }
                is Resource.Error -> {
                    showErrorDialog(it.error.message ?: it.error.cause?.message)
                }
            }
        })
    }


    private fun changeCallStatus(position: Int, requestId: String?, status: String?) {
        getViewModel().changeCallStatus(requestId, status).observe(this, {
            when (it) {
                is Resource.Success<*> -> {
                    getCallStatus(position, requestId, false)
                }
                is Resource.Error -> {
                    showErrorDialog(it.error.message ?: it.error.cause?.message)
                }
            }
        })
    }


    abstract fun showCallingDialog(
        baseStudent: BaseStudent,
        dismissListener: ((dismissed: Boolean) -> Unit)?,
        listener: ((
            errorMessage: String?,
            baseStudent: BaseStudent
        ) -> Unit)? = null
    )

    private fun showCallingDialog(baseStudent: BaseStudent, position: Int) {
        showCallingDialog(
            baseStudent = baseStudent,
            dismissListener = {
                setItemsClickableRecyclerView(true)
            }
        ) { errorMessage, newBaseStudent ->
            getViewModel().setChild(position, newBaseStudent)
            adapter?.notifyItemChanged(position)
            errorMessage?.let {
                showErrorDialog(errorMessage)
            }
        }
    }


    private fun showErrorDialog(message: String?) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            message = message,
            buttonLeftText = getString(R.string.close),
            dismissListener = {
                setItemsClickableRecyclerView(true)
            }
        )

    }



    private fun setItemsClickableRecyclerView(isActive: Boolean) {
        recyclerView?.setItemsClickable(isActive)
    }

    companion object {
        private const val TIME_PERIOD: Long = 25000
        private const val INITIAL_DELAY: Long = 0L
    }
}
