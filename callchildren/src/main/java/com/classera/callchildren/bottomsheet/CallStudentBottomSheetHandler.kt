package com.classera.callchildren.bottomsheet

import android.view.View
import android.widget.Button

/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
interface CallStudentBottomSheetHandler {
    fun onButtonClicked(view:View)
}
