package com.classera.callchildren.bottomsheet

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.callchildren.CallStudentViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
@Module
abstract class CallStudentBottomSheetModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: CallStudentViewModelFactory
        ): CallStudentBottomSheetViewModel {
            return ViewModelProvider(fragment, factory)[CallStudentBottomSheetViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindFragment(fragment: CallStudentBottomSheetFragment): Fragment
}
