package com.classera.callchildren.bottomsheet

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.callchild.CallChildRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
class CallStudentBottomSheetViewModel(private val callChildRepository: CallChildRepository) :
    BaseViewModel() {

    fun changeCallStatus(requestId: String?, status: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryNoContentResource { callChildRepository.changeCallStatus(requestId, status) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }


    fun getCallStatus(requestId: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryResource { callChildRepository.getCallStatus(requestId) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }

}
