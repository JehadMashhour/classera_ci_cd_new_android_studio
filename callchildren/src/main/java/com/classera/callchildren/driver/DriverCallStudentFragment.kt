package com.classera.callchildren.driver


import com.classera.callchildren.base.BaseCallStudentFragment
import com.classera.callchildren.base.BaseCallStudentListAdapter
import com.classera.callchildren.base.BaseCallStudentViewModel
import com.classera.callchildren.bottomsheet.CallStudentBottomSheetFragment
import com.classera.data.models.callchildren.BaseStudent
import javax.inject.Inject

class DriverCallStudentFragment : BaseCallStudentFragment() {

    @Inject
    lateinit var viewModel: DriverCallStudentViewModel


    override fun getViewModel(): BaseCallStudentViewModel = viewModel

    override fun getAdapter(): BaseCallStudentListAdapter<*> =
        DriverCallStudentListAdapter(viewModel)

    override fun showCallingDialog(
        baseStudent: BaseStudent,
        dismissListener: ((dismissed: Boolean) -> Unit)?,
        listener: ((errorMessage: String?, baseStudent: BaseStudent) -> Unit)?
    ) {
        CallStudentBottomSheetFragment.show(
            fragment = this,
            navDirections = DriverCallStudentFragmentDirections.actionCallStudent(baseStudent),
            dismissListener = dismissListener,
            listener = listener
        )
    }

}
