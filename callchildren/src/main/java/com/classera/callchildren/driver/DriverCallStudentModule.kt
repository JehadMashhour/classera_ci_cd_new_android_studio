package com.classera.callchildren.driver

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.callchildren.CallStudentViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class DriverCallStudentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: CallStudentViewModelFactory
        ): DriverCallStudentViewModel {
            return ViewModelProvider(fragment, factory)[DriverCallStudentViewModel::class.java]
        }


    }

    @Binds
    abstract fun bindFragment(fragment: DriverCallStudentFragment): Fragment
}
