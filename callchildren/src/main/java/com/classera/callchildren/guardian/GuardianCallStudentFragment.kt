package com.classera.callchildren.guardian


import com.classera.callchildren.base.BaseCallStudentFragment
import com.classera.callchildren.base.BaseCallStudentListAdapter
import com.classera.callchildren.base.BaseCallStudentViewModel
import com.classera.callchildren.bottomsheet.CallStudentBottomSheetFragment
import com.classera.data.models.callchildren.BaseStudent
import javax.inject.Inject

class GuardianCallStudentFragment : BaseCallStudentFragment() {

    @Inject
    lateinit var viewModel: GuardianCallStudentViewModel


    override fun getViewModel(): BaseCallStudentViewModel = viewModel

    override fun getAdapter(): BaseCallStudentListAdapter<*> =
        GuardianCallStudentListAdapter(viewModel)

    override fun showCallingDialog(
        baseStudent: BaseStudent,
        dismissListener: ((dismissed: Boolean) -> Unit)?,
        listener: ((errorMessage: String?, baseStudent: BaseStudent) -> Unit)?
    ) {
        CallStudentBottomSheetFragment.show(
            fragment = this,
            navDirections = GuardianCallStudentFragmentDirections.actionCallStudent(baseStudent),
            dismissListener = dismissListener,
            listener = listener
        )
    }

}
