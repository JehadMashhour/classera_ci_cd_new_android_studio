package com.classera.chat

import android.view.ViewGroup
import com.classera.chat.databinding.RowThreadBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class ChatAdapter(private val viewModel: ChatViewModel) : BaseAdapter<ChatAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowThreadBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getThreadCount()
    }

    inner class ViewHolder(binding: RowThreadBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowThreadBinding> {
                thread = viewModel.getThread(position)
            }
        }
    }
}
