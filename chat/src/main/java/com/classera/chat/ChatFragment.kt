package com.classera.chat

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.chat.Status
import com.classera.data.models.chat.UserStatus
import com.classera.data.models.chat.UserStatusWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.filter.FilterActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan, Modified by Lana Manaseer
 */
@Screen("Chat")
class ChatFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ChatViewModel

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: ChatAdapter? = null
    private var errorView: ErrorView? = null
    private var floatingButtonChatAdd: FloatingActionButton? = null

    override val layoutId: Int = R.layout.fragment_chat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        getThreads(CHAT_LIMIT)
        getRoles()
        getUserStatus()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_chat, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_chat_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.refreshThreads(CHAT_LIMIT, searchName = searchView?.query).observe(this, ::handleResource)
        }

        searchView?.setOnCloseListener {
            viewModel.refreshThreads(CHAT_LIMIT, searchName = searchView?.query).observe(this, ::handleResource)
            return@setOnCloseListener false
        }

        val settingsMenuItem = menu.findItem(R.id.item_menu_chat_appear_status)
        viewModel.chatUserInverseStatus.observe(this) {
            settingsMenuItem?.title = getString(
                R.string.title_menu_chat_appear_status,
                when (it) {
                    UserStatus.OFFLINE.name -> getString(R.string.label_offline)
                    UserStatus.ONLINE.name -> getString(R.string.label_online)
                    else -> getString(R.string.label_offline)
                }
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_chat_filter -> {
                FilterActivity.start(this, viewModel.getFilterRoles())
            }

            R.id.item_menu_chat_blocked_users -> {
                navigateToChatBlockedUsers()
            }

            R.id.item_menu_chat_appear_status -> {
                updateUserStatus()
            }
            R.id.item_menu_chat_new_group -> {
                navigateToChatGroupUsers()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            viewModel.refreshThreads(CHAT_LIMIT, searchName = searchView?.query, roleID = filter?.filterId)
                .observe(this, ::handleResource)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.refreshThreads(CHAT_LIMIT, searchName = searchView?.query).observe(this, ::handleResource)
        }
    }

    private fun navigateToChatUsers() {
        findNavController().navigate(ChatFragmentDirections.chatUsersDirection())
    }

    private fun navigateToChatBlockedUsers() {
        findNavController().navigate(ChatFragmentDirections.chatBlockedUsersDirection())
    }

    private fun navigateToChatGroupUsers() {
        findNavController().navigate(ChatFragmentDirections.chatGroupUsersDirection(null))
    }

    private fun navigateToChatMessages(name: String?, threadId: String?, isGroup: Boolean, userId: String?) {
        findNavController().navigate(ChatFragmentDirections.chatMessagesDirection(name, threadId, isGroup, userId))
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_chat)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_chat)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_chat)
        errorView = view?.findViewById(R.id.error_view_fragment_chat)
        floatingButtonChatAdd = view?.findViewById(R.id.floating_action_button_fragment_chat_add)
    }

    private fun initListeners() {
        errorView?.setOnRetryClickListener { getThreads(CHAT_LIMIT) }

        floatingButtonChatAdd?.setOnClickListener { navigateToChatUsers() }

        viewModel.chatUserStatus.observe(this) {
            (activity as AppCompatActivity).supportActionBar?.title = getString(
                R.string.title_chat_fragment_user_status,
                when (it) {
                    UserStatus.OFFLINE.name -> getString(R.string.label_offline)
                    UserStatus.ONLINE.name -> getString(R.string.label_online)
                    else -> getString(R.string.label_offline)
                }
            )
        }

        swipeRefreshLayout?.setOnRefreshListener {
            getThreads(CHAT_LIMIT)
        }
    }

    private fun getRoles() {
        viewModel.getRoles().observe(this) {}
    }

    private fun getThreads(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ) {
        viewModel.getThreads(limit, pagination, searchName, roleID)
            .observe(this, ::handleResource)
    }

    private fun getUserStatus() {
        viewModel.getUserStatus().observe(this, ::handleUserStatusResource)
    }

    private fun handleUserStatusResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleUserStatusSuccessResource(resource as Resource.Success<UserStatusWrapper<Status>>)
            }
        }
    }

    private fun updateUserStatus() {
        viewModel.updateUserStatus().observe(this, ::handleUpdateUserStatusResource)
    }

    private fun handleUpdateUserStatusResource(resource: Resource) {
        when (resource) {
            is Resource.Error -> {
                handleErrorUpdateStatusResource(resource)
            }
            is Resource.Loading -> {
                handleUpdateUserStatusLoadingResource(resource)
            }
        }
    }

    private fun handleUpdateUserStatusLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            swipeRefreshLayout?.isRefreshing = false
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleUserStatusSuccessResource(success: Resource.Success<UserStatusWrapper<Status>>) {
        val status = getString(
            R.string.title_chat_fragment_user_status,
            getString(success.data?.result?.status?.label!!)
        )
        (activity as AppCompatActivity).supportActionBar?.title = status
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun initAdapter() {
        adapter = ChatAdapter(viewModel)
        initAdapterListener()
        recyclerView?.adapter = adapter
    }

    private fun initAdapterListener() {
        adapter?.setOnItemClickListener { _, position ->
            navigateToChatMessages(
                viewModel.getThread(position)?.fullName, viewModel.getThread(position)?.threadId,
                viewModel.getThread(position)?.group!!,
                viewModel.getCreatorID(position)
            )
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun handleErrorUpdateStatusResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        searchView = null
        progressBar = null
        recyclerView = null
        swipeRefreshLayout = null
        adapter = null
        errorView = null
        floatingButtonChatAdd = null
        super.onDestroyView()
    }

    companion object {

        const val CHAT_LIMIT = "1000"
    }
}
