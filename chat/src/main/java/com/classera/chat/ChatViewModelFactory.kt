package com.classera.chat

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class ChatViewModelFactory @Inject constructor(
    private val chatRepository: ChatRepository,
    private val chatServiceRepository: ChatServiceRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChatViewModel(chatRepository, chatServiceRepository,prefs) as T
    }
}
