package com.classera.chat.blockusers

import android.view.ViewGroup
import com.classera.chat.databinding.RowThreadBlockUserBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatBlockedUsersAdapter(private val viewModel: ChatBlockedUsersViewModel) :
    BaseAdapter<ChatBlockedUsersAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowThreadBlockUserBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getBlockedUsersCount()
    }

    inner class ViewHolder(binding: RowThreadBlockUserBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowThreadBlockUserBinding> {
                user = viewModel.getBlockedUser(position)
            }
        }
    }
}
