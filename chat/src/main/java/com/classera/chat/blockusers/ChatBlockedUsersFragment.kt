package com.classera.chat.blockusers

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.chat.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Screen("Chat Blocked Users")
class ChatBlockedUsersFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ChatBlockedUsersViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: ChatBlockedUsersAdapter? = null
    private var errorView: ErrorView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override val layoutId: Int = R.layout.fragment_chat_blocked_users

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListeners()
        getBlockedUsers()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_chat_blocked_users)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_chat_blocked_users)
        swipeRefreshLayout =
            view?.findViewById(R.id.swipe_refresh_layout_fragment_chat_blocked_users)
        errorView = view?.findViewById(R.id.error_view_fragment_chat_blocked_users)
    }

    private fun initListeners() {
        errorView?.setOnRetryClickListener { getBlockedUsers() }

        swipeRefreshLayout?.setOnRefreshListener {
            getBlockedUsers()
        }
    }

    private fun getBlockedUsers() {
        viewModel.getBlockedUsers().observe(this, ::handleResource)
    }

    private fun toggleUserBlockStatus(userBlockID: String?) {
        viewModel.toggleUserBlockStatus(userBlockID).observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
            else -> {
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initAdapter() {
        adapter = ChatBlockedUsersAdapter(viewModel)
        initAdapterListener()
        recyclerView?.adapter = adapter
    }

    private fun initAdapterListener() {
        adapter?.setOnItemClickListener { _, position ->
            showUnblockUserDialog(position)
        }
    }

    private fun showUnblockUserDialog(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.title_menu_chat_blocked_users))
            .setMessage(
                getString(
                    R.string.message_chat_blocked_users_dialog,
                    getString(R.string.message_chat_unblock_word)
                )
            )
            .setPositiveButton(android.R.string.yes) { _, _ ->
                toggleUserBlockStatus(viewModel.getBlockedUser(position)?.id)
            }
            .show()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

}
