package com.classera.chat.blockusers

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Module
abstract class ChatBlockedUsersFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ChatBlockedUsersViewModelFactory
        ): ChatBlockedUsersViewModel {
            return ViewModelProvider(fragment, factory)[ChatBlockedUsersViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ChatBlockedUsersFragment): Fragment
}
