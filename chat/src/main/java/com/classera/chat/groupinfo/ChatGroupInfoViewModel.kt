package com.classera.chat.groupinfo

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.ThreadUser
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import kotlinx.coroutines.Dispatchers
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer, Modified by Mohammad Hamdan
 */
class ChatGroupInfoViewModel(
    private val chatServiceRepository: ChatServiceRepository,
    private val chatRepository: ChatRepository,
    prefs: Prefs,
    chatGroupInfoFragmentArgs: ChatGroupInfoFragmentArgs
) : BaseViewModel(), LifecycleObserver {

    var addedParticipants: Array<ThreadUser>? = null
    val removedUsersList = mutableListOf<ThreadUser>()
    var users = mutableListOf<ThreadUser>()

    var threadId: String? = null
    var creatorId: String? = null
    var groupName: String? = null
    var isCreator: Boolean? = false

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    init {
        chatGroupInfoFragmentArgs.creatorID?.let { creatorIdArg ->
            creatorId = creatorIdArg
        }

        chatGroupInfoFragmentArgs.selectedUsers?.let { selectedUsers ->
            users.addAll(selectedUsers)
        }
        chatGroupInfoFragmentArgs.threadID?.let { threadIdArgs ->
            threadId = threadIdArgs
        }
        chatGroupInfoFragmentArgs.groupName?.let { groupNameArgs ->
            groupName = groupNameArgs
        }

        if (creatorId == prefs.userId) {
            isCreator = true
        }
    }

    fun getUserCount(): Int {
        return users.size
    }

    fun getUser(position: Int): ThreadUser? {
        return users[position]
    }

    fun deleteUser(position: Int): Boolean? {
        val selectedUser = users.filter { it.id == users[position].id }.getOrNull(position)

        selectedUser?.let { removedUsersList.add(0, it) }
        return users.remove(selectedUser)
    }

    fun createGroup(name: String?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource {
            chatServiceRepository.createGroup(name, users)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getListGroupParticipants() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatRepository.getListGroupParticipants(threadId) }
        users.clear()
        users.addAll(resource.element<BaseWrapper<List<ThreadUser>>>()?.data ?: mutableListOf())
        users.addAll(addedParticipants ?: arrayOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun deleteParticipants(participantList: List<ThreadUser>?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatServiceRepository.deleteParticipants(threadId, participantList) }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun deleteGroup() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatServiceRepository.deleteGroup(threadId) }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun updateGroupName(name: String?) = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource {
            chatServiceRepository.updateGroupName(name, threadId)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun addParticipants() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatServiceRepository.addParticipants(threadId, addedParticipants) }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    @Subscribe
    fun onAddParticipantsResult(addedParticipants: Array<ThreadUser>) {
        this.addedParticipants = addedParticipants
    }
}
