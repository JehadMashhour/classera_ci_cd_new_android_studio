package com.classera.classvisits.add

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Switch
import android.widget.Toast
import androidx.navigation.fragment.findNavController

import com.classera.classvisits.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.classvisits.Assessment
import com.classera.data.models.classvisits.Teacher
import com.classera.data.models.classvisits.Timeslot
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Add Class Visit")
class AddClassVisitFragment : BaseValidationFragment(),
    DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var viewModel: AddClassVisitViewModel

    private val currentDayCalendar = Calendar.getInstance()

    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)

    private var teacherId: String? = null
    private var dateTimeslot: String? = null
    private var assessmentID: String? = null
    private var timeslotId: String? = null

    @NotEmpty(message = "validation_fragment_add_visit_assessment")
    private var autoCompleteTextViewAssessment: AutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_visit_date")
    private var editTextDate: EditText? = null

    @NotEmpty(message = "validation_fragment_add_visit_teacher")
    private var autoCompleteTextViewTeacher: AutoCompleteTextView? = null

    @NotEmpty(message = "validation_fragment_add_visit_timeslot")
    private var autoCompleteTextViewTimeslot: AutoCompleteTextView? = null

    private var switchPrivate: Switch? = null
    private var private: String? = null

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var progressBarSubmit: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var linearLayoutParent: LinearLayout? = null

    override val layoutId: Int = R.layout.fragment_add_class_visit

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initSubmitButtonListener()
        initDateListener()
        initPrivateListener()
        initErrorViewListener()

        getAssessment()
        getTeachers()
    }

    private fun findViews() {
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_add_visit_parent)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_add_visit)
        errorView = view?.findViewById(R.id.error_view_fragment_add_visit)
        buttonSubmit = view?.findViewById(R.id.button_fragment_add_visit_submit)
        progressBarSubmit = view?.findViewById(R.id.progress_bar_fragment_add_visit_submit)
        autoCompleteTextViewTeacher = view?.findViewById(
            R.id.auto_complete_text_view_fragment_add_class_teachers
        )
        autoCompleteTextViewAssessment = view?.findViewById(R.id.auto_complete_text_view_fragment_add_visit_assessment)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_add_class_visit_date)
        autoCompleteTextViewTimeslot = view?.findViewById(
            R.id.auto_complete_text_view_fragment_add_class_visit_time_slot
        )
        switchPrivate = view?.findViewById(R.id.switch_fragment_add_visit_private)
    }

    private fun initDateListener() {
        editTextDate?.setOnClickListener {
            DatePickerDialog(requireContext(), R.style.AppTheme_PickerTheme, this, year, month, dayOfMonth).show()
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        currentDayCalendar.set(Calendar.YEAR, year)
        currentDayCalendar.set(Calendar.MONTH, month)
        currentDayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        editTextDate?.setText(sdf.format(currentDayCalendar.time))
        removeError(editTextDate?.parent?.parent as TextInputLayout)
        editTextDate?.error = null
        dateTimeslot = editTextDate?.text.toString()
        if(teacherId?.isNotBlank() == true || teacherId?.isNotEmpty() == true){
            getTimeslots()
        }
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            getAssessment()
            getTeachers()
        }
    }

    private fun getAssessment() {
        viewModel.getAssessments().observe(this, ::handleAssessmentsResource)
    }

    private fun handleAssessmentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAssessmentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAssessmentsSuccessResource(resource as Resource.Success<BaseWrapper<List<Assessment>>>)
            }
            is Resource.Error -> {
                handleAssessmentsErrorResource(resource)
            }
        }
    }

    private fun handleAssessmentsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutParent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutParent?.visibility = View.VISIBLE
        }
    }

    private fun handleAssessmentsSuccessResource(success: Resource.Success<BaseWrapper<List<Assessment>>>) {
        progressBar?.visibility = View.GONE
        val assessments = mutableListOf<Assessment>()
        success.data?.data?.let { it ->
            it.forEach { assessments.add(it) }
        }
        initAssessmentAdapter(assessments)
    }

    private fun handleAssessmentsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initAssessmentAdapter(assessments: MutableList<Assessment>) {
        val assessmentsTitles = mutableListOf<String>()
        assessments.forEach { it.title?.let { it1 -> assessmentsTitles.add(it1) } }

        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_menu_popup_item, assessmentsTitles)
        autoCompleteTextViewAssessment?.setAdapter(adapter)

        autoCompleteTextViewAssessment?.setOnItemClickListener { _, _, position, _ ->
            assessmentID = assessments[position].id.toString()
        }
    }

    private fun getTeachers() {
        viewModel.getTeachers().observe(this, ::handleTeachersResource)
    }

    private fun handleTeachersResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleTeachersLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleTeachersSuccessResource(resource as Resource.Success<BaseWrapper<List<Teacher>>>)
            }
            is Resource.Error -> {
                handleTeachersErrorResource(resource)
            }
        }
    }

    private fun handleTeachersLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleTeachersSuccessResource(success: Resource.Success<BaseWrapper<List<Teacher>>>) {
        progressBar?.visibility = View.GONE
        val teachers = mutableListOf<Teacher>()
        success.data?.data?.let { it ->
            it.forEach { teachers.add(it) }
        }
        initTeachersAdapter(teachers)
    }

    private fun handleTeachersErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initTeachersAdapter(teachers: MutableList<Teacher>) {
        val teachersTitles = mutableListOf<String>()
        teachers.forEach { it.name?.let { it1 -> teachersTitles.add(it1) } }

        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_menu_popup_item, teachersTitles)
        autoCompleteTextViewTeacher?.setAdapter(adapter)

        autoCompleteTextViewTeacher?.setOnItemClickListener { _, _, position, _ ->
            teacherId = teachers[position].id.toString()
            if(dateTimeslot.isNullOrBlank() || dateTimeslot.isNullOrEmpty()){
                editTextDate?.error = getString(R.string.validation_fragment_add_visit_date)
            }else{
                getTimeslots()
            }

        }
    }

    private fun getTimeslots() {
        viewModel.getTimeslots(dateTimeslot, teacherId).observe(this, ::handleTimeslotsResource)
    }

    private fun handleTimeslotsResource(resource: Resource) {

        when (resource) {
            is Resource.Loading -> {
                handleTimeslotsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleTimeslotsSuccessResource(resource as Resource.Success<BaseWrapper<List<Timeslot>>>)
            }
            is Resource.Error -> {
                handleTimeslotsErrorResource(resource)
            }
        }

    }

    private fun handleTimeslotsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleTimeslotsSuccessResource(success: Resource.Success<BaseWrapper<List<Timeslot>>>) {
        progressBar?.visibility = View.GONE
        var timeslots = mutableListOf<Timeslot>()
        timeslots.clear()
        autoCompleteTextViewTimeslot?.setText("")
        success.data?.data?.let { it ->
            it.forEach { timeslots.add(it) }
        }
        if (timeslots.size == 0) {
            Toast.makeText(
                context,
                resources.getString(R.string.toast_fragment_add_visit_error_timeslot),
                Toast.LENGTH_LONG
            ).show()
            initTimeslotsAdapter(timeslots)
        }
        else {
            initTimeslotsAdapter(timeslots)
        }
    }

    private fun handleTimeslotsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun initTimeslotsAdapter(timeslots: MutableList<Timeslot>) {
        val timeslotsTitles = mutableListOf<String>()
        timeslots.forEach { it.title?.let { it1 -> timeslotsTitles.add(it1) } }

        val adapter = ArrayAdapter(requireContext(), R.layout.dropdown_menu_popup_item, timeslotsTitles)
        autoCompleteTextViewTimeslot?.setAdapter(adapter)
        adapter.notifyDataSetChanged()
        autoCompleteTextViewTimeslot?.setOnItemClickListener { _, _, position, _ ->
            timeslotId = timeslots[position].id.toString()
        }
    }

    private fun initPrivateListener() {
        this.private = PRIVATE_FALSE
        switchPrivate?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                this.private = PRIVATE_TRUE
            } else {
                this.private = PRIVATE_FALSE
            }
        }
    }

    private fun initSubmitButtonListener() {
        buttonSubmit?.setOnClickListener { validator.validate() }
    }

    override fun onValidationSucceeded() {
        viewModel.onSaveClicked(
            dateTimeslot,
            teacherId,
            assessmentID,
            private,
            autoCompleteTextViewTimeslot?.text?.toString(),
            timeslotId
        ).observe(this, ::handleSubmitResource)
    }

    private fun handleSubmitResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitErrorResource(resource)
            }
        }
    }

    private fun handleSubmitLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarSubmit?.visibility = View.VISIBLE
            buttonSubmit?.text = ""
            buttonSubmit?.isEnabled = false
        } else {
            progressBarSubmit?.visibility = View.GONE
            buttonSubmit?.setText(R.string.button_fragment_add_visit_add)
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleSubmitSuccessResource() {
        requireContext().let {
            Toast.makeText(it,getString(R.string.success_message),Toast.LENGTH_SHORT).show()
        }

        findNavController().popBackStack()
    }

    private fun handleSubmitErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private companion object {
        private const val PRIVATE_TRUE = "1"
        private const val PRIVATE_FALSE = "0"
    }
}
