package com.classera.classvisits.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddClassVisitFragmentModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: AddClassVisitViewModelFactory): AddClassVisitViewModel {
            return ViewModelProvider(fragment, factory)[AddClassVisitViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddClassVisitFragment): Fragment
}
