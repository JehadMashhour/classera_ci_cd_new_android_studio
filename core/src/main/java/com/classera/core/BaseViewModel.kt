package com.classera.core

import androidx.lifecycle.ViewModel

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
abstract class BaseViewModel : ViewModel()
