package com.classera.core

/**
 * Created by Rawan Al-Theeb on 3/11/2020.
 * Classera
 * r.altheeb@classera.com
 */
annotation class Screen(val value: String)
