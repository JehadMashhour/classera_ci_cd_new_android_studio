package com.classera.core.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import com.classera.core.R
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
abstract class BaseToolbarActivity : BaseActivity() {

    var toolbar: Toolbar? = null

    open fun hasToolbar(): Boolean = true

    open fun hasBackButton(): Boolean = hasToolbar()

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        initAndSetToolbar()
        handleBackButton()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> if (hasBackButton()) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initAndSetToolbar() {
        if (hasToolbar()) {
            toolbar = findViewById(R.id.toolbar)
            if (toolbar != null) {
                val labelResource = packageManager.getActivityInfo(componentName, 0).labelRes
                if (labelResource > 0) {
                    toolbar?.setTitle(labelResource)
                }
                setSupportActionBar(toolbar)
            }
        }
    }

    private fun handleBackButton() {
        if (hasBackButton()) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy1(dummy: String) {
        // No impl
    }
}
