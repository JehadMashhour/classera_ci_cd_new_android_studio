package com.classera.core.custom.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.animation.GridLayoutAnimationController.AnimationParameters
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.R

/**
 * Project: Classera
 * Created: Dec 22, 2019
 *
 * @author Mohamed Hamdan
 */
open class BaseRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    var emptyMessage: String? = null

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.BaseRecyclerView, defStyleAttr, 0)
        emptyMessage = typedArray.getString(R.styleable.BaseRecyclerView_empty_message)
        typedArray.recycle()
    }

    override fun attachLayoutAnimationParameters(child: View, params: ViewGroup.LayoutParams, index: Int, count: Int) {
        val layoutManager = layoutManager
        if (adapter != null && layoutManager is GridLayoutManager) {
            var animationParams = params.layoutAnimationParameters as? AnimationParameters

            if (animationParams == null) {
                animationParams = AnimationParameters()
                params.layoutAnimationParameters = animationParams
            }
            animationParams.count = count
            animationParams.index = index

            val columns = layoutManager.spanCount
            animationParams.columnsCount = columns
            animationParams.rowsCount = count / columns

            val invertedIndex = count - 1 - index
            animationParams.column = columns - 1 - invertedIndex % columns
            animationParams.row = animationParams.rowsCount - 1 - invertedIndex / columns
        } else {
            super.attachLayoutAnimationParameters(child, params, index, count)
        }
    }
}
