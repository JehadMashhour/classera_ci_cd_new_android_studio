package com.classera.core.custom.views

import com.classera.core.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment

/**
 * Created by Odai Nazzal on 12/29/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class ToolbarFilterBottomSheet : BaseBottomSheetDialogFragment() {

    override val layoutId: Int = R.layout.sheet_toolbar_filter
}
