package com.classera.core.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import javax.inject.Inject

abstract class BaseDialogBindingFragment : BaseDialogFragment() {
    var viewDataBinding: ViewDataBinding? = null

    open fun isBindingEnabled(): Boolean {
        return true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View?
        if (isBindingEnabled()) {
            viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
            view = viewDataBinding?.root
        } else {
            view = super.onCreateView(inflater, container, savedInstanceState)
        }
        return view
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : ViewDataBinding> bind(binding: T?.() -> Unit) {
        binding(viewDataBinding as? T?)
        viewDataBinding?.executePendingBindings()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewDataBinding = null
    }


}
