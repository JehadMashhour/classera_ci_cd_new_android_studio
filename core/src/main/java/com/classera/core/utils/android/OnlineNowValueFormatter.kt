package com.classera.core.utils.android

import com.github.mikephil.charting.formatter.ValueFormatter

/**
 * Project: classeraandroidv3
 * Created: Mar 15, 2020
 *
 * @author Odai Nazzal
 */
class OnlineNowValueFormatter : ValueFormatter() {
    override fun getFormattedValue(value: Float): String? {
        if (value == NUMBER_ZERO) {
            return ""
        }
        return value.toInt().toString()
    }

    companion object {
        private const val NUMBER_ZERO = 0f
    }
}
