package com.classera.core.utils.android

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import com.classera.data.generateThumbnailFromLocalVideo
import com.classera.data.models.BackgroundColor
import com.jaiselrahman.filepicker.model.MediaFile
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream


/**
 * Project: Classera
 * Created: Dec 11, 2019
 *
 * @author Mohamed Hamdan
 */
fun Context.getStringOrElse(resource: Int, defaultValue: String?): String = if (resource == 0) {
    defaultValue ?: ""
} else {
    try {
        getString(resource).takeIf { it.isNotEmpty() } ?: defaultValue ?: ""
    } catch (ignored: Resources.NotFoundException) {
        defaultValue ?: ""
    }
}

@Suppress("MagicNumber")
fun getBackgroundColorBasedOn(position: Int): BackgroundColor {
    return when (position % 5) {
        0 -> BackgroundColor.BLUE
        1 -> BackgroundColor.BLUE_WHITE
        2 -> BackgroundColor.PURPLE
        3 -> BackgroundColor.BROWN
        4 -> BackgroundColor.GRAY
        else -> BackgroundColor.GRAY
    }
}

fun String?.toDrawableResource(context: Context): Int {
    return context.resources.getIdentifier(this, "drawable", context.packageName)
}

fun String?.toStringResource(context: Context): Int {
    return context.resources.getIdentifier(this, "string", context.packageName)
}

fun String?.toColorResource(context: Context): Int {
    return context.resources.getIdentifier(this, "color", context.packageName)
}

fun Bitmap.toByteArray(): ByteArray {
    return ByteArrayOutputStream().apply {
        compress(Bitmap.CompressFormat.PNG, 0, this)
    }.toByteArray()
}

@Throws(Throwable::class)
fun MediaFile.saveBitmapFromVideo(context: Context?): File? {
    val bitmap = generateThumbnailFromLocalVideo(context, this.path)

    val file = File(context?.cacheDir?.path?.plus("/image.png"))
    val fos = FileOutputStream(file)
    bitmap.compress(Bitmap.CompressFormat.PNG, 0, fos)
    fos.flush()
    bitmap.recycle()
    return file
}
