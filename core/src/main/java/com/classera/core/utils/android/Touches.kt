package com.classera.core.utils.android

import android.view.MotionEvent
import kotlin.math.abs

/**
 * Project: Classera
 * Created: Mar 14, 2020
 *
 * @author Mohamed Hamdan
 */

private const val CLICK_ACTION_THRESHOLD: Int = 200
private var startX = 0f
private var startY = 0f

fun MotionEvent.ifClick(): Boolean {
    when (action) {
        MotionEvent.ACTION_DOWN -> {
            startX = x
            startY = y
        }
        MotionEvent.ACTION_UP -> {
            val endX = x
            val endY = y
            if (isAClick(startX, endX, startY, endY)) {
                return true
            }
        }
    }
    return false
}

private fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
    val differenceX = abs(startX - endX)
    val differenceY = abs(startY - endY)
    return !(differenceX > CLICK_ACTION_THRESHOLD || differenceY > CLICK_ACTION_THRESHOLD)
}
