package com.classera.courses.details

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.calssera.vcr.teacher.VcrTeacherFragmentDirections
import com.classera.assignments.rating.RatingBottomSheet
import com.classera.core.Screen
import com.classera.core.adapter.BaseAdapter
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.network.NetworkConnectionLiveData
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.observe
import com.classera.courses.R
import com.classera.courses.details.adapters.*
import com.classera.data.BuildConfig
import com.classera.data.models.BackgroundColor
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.courses.PreparationFilterWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.filter.FilterActivity
import com.classera.utils.views.dialogs.actionsheetbottomdialog.ActionDialog
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

/**
 * Created by Odai Nazzal
 * Classera
 * o.nazzal@classera.com
 */
@Suppress("ComplexMethod", "TooManyFunctions", "LargeClass")
@Screen("Course Details")
class CourseDetailsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: CourseDetailsViewModel

    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var customTabsIntent: CustomTabsIntent

    private var menuItem: Menu? = null

    private val args: CourseDetailsFragmentArgs by navArgs()

    private val adapters = mapOf(
        CourseDetailsViewModel.VIDEOS_TYPE to CourseAdapterInstantiator(AttachmentsAdapter::class),
        CourseDetailsViewModel.MATERIAL_TYPE to CourseAdapterInstantiator(AttachmentsAdapter::class),
        CourseDetailsViewModel.EXAMS_TYPE to CourseAdapterInstantiator(AssignmentsAdapter::class),
        CourseDetailsViewModel.HOMEWORKS_TYPE to CourseAdapterInstantiator(AssignmentsAdapter::class),
        CourseDetailsViewModel.DISCUSSIONS_TYPE to CourseAdapterInstantiator(DiscussionRoomsAdapter::class),
        CourseDetailsViewModel.VCRS_TYPE to CourseAdapterInstantiator(VcrAdapter::class),
        CourseDetailsViewModel.ATTACHMENTS_OTHERS_TYPE to CourseAdapterInstantiator(
            AttachmentsAdapter::class
        )
    )
    private var isFilterInitialized = false

    private var canChangeFilterKey = true
    private var progressBar: ProgressBar? = null
    private var adapter: BaseAdapter<*>? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var filterView: QuickFilterView? = null
    private var floatingActionAdd: FloatingActionButton? = null
    private var filterKey: String? = null
    private var clickedPosition: Int = -1

    override val layoutId: Int = R.layout.fragment_course_details

    private val likeReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.likeCount =
                intent?.getIntExtra("new_count", 0) ?: 0
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.liked =
                intent?.getBooleanExtra("is_liked", false) ?: false
            adapter?.notifyDataSetChanged()
        }
    }

    private val understandReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.understandCount =
                intent?.getIntExtra("new_count", 0) ?: 0
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.understand =
                intent?.getBooleanExtra("understand", false) ?: false
            adapter?.notifyDataSetChanged()
        }
    }

    private val ratingReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.rate =
                intent?.getStringExtra("new_rating")
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.isRated = true
            viewModel.getAttachmentBy(filterKey!!, clickedPosition)?.isRate = true
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        context?.registerReceiver(likeReceiver, IntentFilter("LIKE_CHANGED"))
        context?.registerReceiver(understandReceiver, IntentFilter("UNDERSTAND_CHANGED"))
        context?.registerReceiver(ratingReceiver, IntentFilter("RATING_CHANGED"))
        getPreparation()
        observeOnNetworkConnection()
    }

    private fun getPreparation() {
        viewModel.getPreparation().observe(this, this::handlePreparationResource)
    }


    @Suppress("UNCHECKED_CAST")
    private fun handlePreparationResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                //No Impl
            }
            is Resource.Success<*> -> {
                handlePreparationSuccessResource(
                    resource as Resource.Success<BaseWrapper<List<PreparationFilterWrapper>>>
                )
            }
            is Resource.Error -> {
                handlePreparationErrorResource(resource)
            }
        }
    }

    private fun handlePreparationSuccessResource(
        success: Resource.Success<BaseWrapper<List<PreparationFilterWrapper>>>
    ) {
        success.data?.data?.let {
            if (it.isNotEmpty()) {
                setHasOptionsMenu(true)
            } else {
                setHasOptionsMenu(false)
            }
        }
    }

    private fun handlePreparationErrorResource(resource: Resource.Error) {
        setHasOptionsMenu(false)
    }

    private fun observeOnNetworkConnection() {
        NetworkConnectionLiveData(context ?: return)
            .observe(viewLifecycleOwner, Observer { isConnected ->
                if (!isConnected) {
                    getBrowseContent()
                    return@Observer
                }
            })
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_course_details)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_course_details)
        errorView = view?.findViewById(R.id.error_view_fragment_course_details)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_course_details)
        filterView = view?.findViewById(R.id.filter_view_fragment_course_details)
        floatingActionAdd =
            view?.findViewById(R.id.floating_action_button_fragment_course_details_add)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { getBrowseContent() }
        floatingActionAdd?.setOnClickListener {
            handleAddClicked()
        }
        viewModel.addButtonVisibility.observe(this) {
            floatingActionAdd?.visibility = if (it) View.VISIBLE else View.GONE
        }
        getBrowseContent()

        viewModel.notifyAdapterItemLiveData.observe(this) { adapter?.notifyItemChanged(it) }

        viewModel.notifyItemRemovedLiveData.observe(this) { adapter?.notifyItemRemoved(it) }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun handleAddClicked() {
        when (filterView?.getSelectedFilterKey()) {
            "videos", "materials", "others" -> {
                goToCreateAttachment(filterView?.getSelectedFilterKey())
            }
            "exams", "homeworks" -> {
            }
            "discussions" -> {
                goToCreateDiscussion()
            }
            "vcrs" -> {
                goToCreateVcr()
            }
        }
    }

    private fun goToCreateDiscussion() {
        val title = getString(R.string.title_fragment_add_discussion_add)
        findNavController().navigate(CourseDetailsFragmentDirections.addDiscussionDirection(title))
    }

    private fun goToCreateVcr() {
        val direction = VcrTeacherFragmentDirections.createVCRActionDirection()
        findNavController().navigate(direction)
    }

    private fun goToCreateAttachment(type: String?) {
        val title = getString(R.string.title_fragment_add_attachment)
        findNavController().navigate(
            CourseDetailsFragmentDirections.addAttachmentDirection(
                title,
                args.courseId,
                type
            )
        )
    }

    private fun getBrowseContent() {
        if (viewModel.selectedFilter == null) {
            viewModel.getBrowseContent(
                "", true
            )
                .observe(this, ::handleResource)
        } else {
            viewModel.getBrowseContent(
                viewModel.selectedFilter?.filterId.toString(), false
            )?.observe(this, ::handleResource)
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (filterKey != null) {
            canChangeFilterKey = false
            initFilter()
            setAdapterBy(filterKey!!)
            filterView?.setSelectedFilter(filterKey)
            canChangeFilterKey = true
            return
        }

        if (isFilterInitialized) {
            val key = filterView?.getSelectedFilterKey() ?: ""
            setAdapterBy(key)
        } else {
            initFilter()
        }
    }

    private fun initFilter() {
        isFilterInitialized = true
        val courseDetailsEntryValues = getCourseDetailsEntryValues()
        filterView?.setAdapter(R.array.array_course_details_entries, courseDetailsEntryValues)
        filterView?.setOnFilterSelectedListener(::setAdapterBy)
    }

    private fun getCourseDetailsEntryValues(): Array<String?> {
        val arrayValues = resources.getStringArray(R.array.array_course_details_entry_values)
        arrayValues[INDEX_ZERO] = arrayValues[INDEX_ZERO] + " (" +
                viewModel.getVideosCount() + ")"
        arrayValues[INDEX_ONE] = arrayValues[INDEX_ONE] + " (" +
                viewModel.getMaterialsCount() + ")"
        arrayValues[INDEX_TWO] = arrayValues[INDEX_TWO] + " (" +
                viewModel.getAssignmentsCountBy(CourseDetailsViewModel.EXAMS_TYPE) + ")"
        arrayValues[INDEX_THREE] = arrayValues[INDEX_THREE] + " (" +
                viewModel.getAssignmentsCountBy(CourseDetailsViewModel.HOMEWORKS_TYPE) + ")"
        arrayValues[INDEX_FOUR] = arrayValues[INDEX_FOUR] + " (" +
                viewModel.getDiscussionRoomsCount() + ")"
        arrayValues[INDEX_FIVE] = arrayValues[INDEX_FIVE] + " (" +
                viewModel.getVcrsCount() + ")"
        arrayValues[INDEX_SIX] = arrayValues[INDEX_SIX] + " (" +
                viewModel.getAttachmentsCountBy(CourseDetailsViewModel.ATTACHMENTS_OTHERS_TYPE) + ")"
        return arrayValues
    }

    @SuppressWarnings("ComplexMethod", "LongMethod")
    private fun setAdapterBy(key: String) {
        if (canChangeFilterKey) {
            filterKey = key
        }

        if (key == "others" ||
            key == CourseDetailsViewModel.EXAMS_TYPE ||
            key == CourseDetailsViewModel.HOMEWORKS_TYPE
        ) {
            floatingActionAdd?.visibility = View.GONE
        } else if (viewModel.canShowAddButton()) {
            floatingActionAdd?.visibility = View.VISIBLE
        }

        adapter = adapters[key]?.getInstance(key, viewModel)

        adapter?.setOnItemClickListener { view, position ->
            clickedPosition = position
            when (adapter) {
                is AttachmentsAdapter -> {
                    when (view.id) {
                        R.id.linear_layout_row_digital_library_like -> {
                            viewModel.onLikeClicked(key, position)
                        }
                        R.id.linear_layout_row_digital_library_rate -> {
                            RatingBottomSheet.show(childFragmentManager) { rating ->
                                viewModel.onSubmitRatingClicked(key, position, rating)
                            }
                        }
                        else -> {
                            val attachment = viewModel.getAttachmentBy(key, position)
                            val id = attachment?.id
                            val title = attachment?.title
                            val direction =
                                CourseDetailsFragmentDirections.showAttachmentDetails(
                                    id, attachment?.backgroundColor
                                        ?: BackgroundColor.GRAY
                                )
                            findNavController().navigate(direction)
                        }
                    }
                }
                is DiscussionRoomsAdapter -> {
                    val room = viewModel.getDiscussionRoomBy(position)
                    val id = room?.id
                    val title = room?.title
                    val instruction = room?.instruction
                    findNavController().navigate(
                        CourseDetailsFragmentDirections.showDiscussionRoomDetails(
                            title,
                            id
                        ).apply {
                            this.instruction = instruction
                        }
                    )
                }
                is VcrAdapter -> {
                    when (view.id) {
                        R.id.image_view_row_vcr_more -> {
                            handleMoreClicked(view, position)
                        }
                        else -> {
                            val vcr = viewModel.getVcrBy(position)
                            vcr?.run {
                                if (this.isUpComing == true) {
                                    getVCRUrl(this.id!!, VCRConfirmViewModel.UPCOMING)
                                } else {
                                    getVCRUrl(this.sessionCode!!, VCRConfirmViewModel.PASSED)
                                }
                            }

                        }
                    }
                }
                is AssignmentsAdapter -> {
                    when (view.id) {
                        R.id.button_row_assignment_second_action -> {
                            if (prefs.userRole != UserRole.TEACHER)
                                handleOtherStatuses(position, "true")
                        }
                        R.id.button_row_assignment_action -> {
                            handleAssignmentItemClicked(position)
                        }
                    }
                }
            }
        }

        recyclerView?.layoutManager = if (adapter is DiscussionRoomsAdapter) {
            GridLayoutManager(context, 2)
        } else {
            LinearLayoutManager(context)
        }
        recyclerView?.adapter = adapter
    }

    private fun handleAssignmentItemClicked(position: Int) {
        when (viewModel.getAssignmentBy(filterKey.orEmpty(), position)?.status) {
            AssignmentStatus.MISSED -> {
                handleAssignmentErrorStatus(
                    getString(com.classera.assignments.R.string.title_dialog_missed_exam),
                    getString(com.classera.assignments.R.string.message_dialog_missed_exam)
                )
            }
            AssignmentStatus.NOT_ATTEMPTED -> {
                handleAssignmentErrorStatus(
                    getString(com.classera.assignments.R.string.title_dialog_not_attempted_exam),
                    getString(com.classera.assignments.R.string.message_dialog_not_attempted_exam)
                )
            }
            AssignmentStatus.SUBMISSION_GRADE -> {
                if (prefs.userRole != UserRole.TEACHER)
                    handleOtherStatuses(position, "true")
            }
            AssignmentStatus.START_AGAIN -> {
                if (prefs.userRole == UserRole.STUDENT) {
                    handleOtherStatuses(position, "false")
                } else {
                    showMessageYourNotAllowed()
                }
            }
            else -> {
                if (prefs.userRole == UserRole.STUDENT) {
                    handleOtherStatuses(position, "false")
                } else {
                    showMessageYourNotAllowed()
                }
            }
        }
    }

    private fun showMessageYourNotAllowed() {
        Toast.makeText(
            requireContext(), getString(com.classera.assignments.R.string.your_not_allowed_message),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun handleOtherStatuses(position: Int, type: String) {
        val assignment = viewModel.getAssignmentBy(filterKey.orEmpty(), position)
        customTabsIntent.launchUrl(
            context,
            Uri.parse(
                viewModel.getExamsLink().format(
                    assignment?.id,
                    prefs.authenticationToken,
                    type,
                    prefs.childId
                )
            )
        )
    }

    private fun handleAssignmentErrorStatus(title: String, message: String) {
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    private fun handleMoreClicked(view: View, position: Int) {
        ActionDialog(requireContext())
            .apply {
                setTitle(getString(com.calssera.vcr.R.string.choose_option))
                addAction(getString(com.calssera.vcr.R.string.edit))
                addAction(getString(com.calssera.vcr.R.string.delete), true)
                show()
            }.setEventListener(object : ActionDialog.OnEventListener {
                override fun onActionItemClick(
                    dialog: ActionDialog,
                    item: ActionDialog.ActionItem,
                    itemPosition: Int
                ) {
                    when (item.title) {
                        getString(com.calssera.vcr.R.string.edit) -> {
                            handleEditItemClicked(position)
                        }

                        getString(com.calssera.vcr.R.string.delete) -> {
                            handleDeleteItemClicked(position)
                        }
                    }
                }

                @Suppress("EmptyFunctionBlock")
                override fun onCancelItemClick(dialog: ActionDialog?) {

                }

            })
    }

    private fun handleEditItemClicked(position: Int) {
        viewModel.getVcrDetails(
            viewModel.getVcrBy(position)?.id,
            viewModel.getVcrBy(position)?.vendor?.id
        )
            .observe(viewLifecycleOwner, Observer { handleVcrStatusResource(it, position) })
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleVcrStatusResource(resource: Resource, position: Int) {
        when (resource) {

            is Resource.Success<*> -> {
                handleVcrStatusSuccessResource(
                    resource as Resource.Success<BaseWrapper<VcrResponse>>
                )
            }
            is Resource.Error -> {
                handleVcrStatusErrorResource(resource)
            }
            else -> {

            }
        }
    }

    private fun handleVcrStatusSuccessResource(
        success: Resource.Success<BaseWrapper<VcrResponse>>
    ) {
        success.data?.data?.let {
            if (it.vendor?.vcrStatusWrapper?.canProceed == true) {
                navigateToAddVcr(it)
            } else {
                showMessageBottomSheetFragment(it)
            }
        }
    }

    private fun navigateToAddVcr(vcrResponse: VcrResponse) {
        val direction =
            VcrTeacherFragmentDirections.addVcrDirection(vcrResponse.title).apply {
                vcr = vcrResponse
            }
        findNavController().navigate(direction)
    }

    private fun showMessageBottomSheetFragment(vcrResponse: VcrResponse) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = com.calssera.vcr.R.drawable.ic_info,
            title = getString(com.calssera.vcr.R.string.error),
            message = vcrResponse.vendor?.vcrStatusWrapper?.message,
            buttonLeftText = getString(com.calssera.vcr.R.string.close)
        )
    }


    private fun handleVcrStatusErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)

        MessageBottomSheetFragment.show(
            fragment = this,
            image = com.calssera.vcr.R.drawable.ic_info,
            title = getString(com.calssera.vcr.R.string.error),
            message = message,
            buttonLeftText = getString(com.calssera.vcr.R.string.close)
        )
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(com.calssera.vcr.R.string.title_delete_vcr_dialog)
            .setMessage(
                getString(
                    R.string.message_delete_vcr_dialog,
                    viewModel.getVcrBy(position)?.title
                )
            )
            .setPositiveButton(com.calssera.vcr.R.string.button_positive_delete_vcr_dialog) { _, _ ->
                viewModel.deleteVcr(position)
            }
            .setNegativeButton(com.calssera.vcr.R.string.button_negative_delete_vcr_dialog, null)
            .show()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getBrowseContent() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_browse_content, menu)
        this.menuItem = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_browse_content_filter -> {
                FilterActivity.start(
                    this,
                    viewModel.getFilterPreparation(),
                    viewModel.selectedFilter,
                    false
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            swipeRefreshLayout?.isRefreshing = true
            viewModel.getBrowseContentListByFilter(filter)?.observe(this, ::handleResource)
            menuItem?.getItem(0)?.setIcon(R.drawable.menu_item_with_badge)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.selectedFilter = null
            getBrowseContent()
            menuItem?.getItem(0)?.setIcon(R.drawable.ic_filter)
        }
    }

    private fun getVCRUrl(vcrId: String, type: String) {
        viewModel.getVCRURL(vcrId, type).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        when (type) {
                            VCRConfirmViewModel.UPCOMING -> {
                                handleUpComingSuccessVcrUrl(it)
                            }

                            VCRConfirmViewModel.PASSED -> {
                                handlePassedSuccessVcrUrl(it)
                            }
                        }

                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = com.calssera.vcr.R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(com.calssera.vcr.R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleUpComingSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.passcode?.isNotEmpty() == true -> {
                showPasscodeVcrDialog(successResource.passcode, successResource.sessionUrl)
            }
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(com.calssera.vcr.R.string.title_student_vcr_bottom_sheet),
                    getString(com.calssera.vcr.R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(com.calssera.vcr.R.string.no_record_found),
                getString(com.calssera.vcr.R.string.close)
            )
        }
    }

    private fun handlePassedSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(com.calssera.vcr.R.string.title_student_vcr_bottom_sheet_view),
                    getString(com.calssera.vcr.R.string.label_button_vcr_confirm_bottom_view)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(com.calssera.vcr.R.string.no_record_found),
                getString(com.calssera.vcr.R.string.close)
            )
        }
    }

    private fun showPasscodeVcrDialog(
        passcode: String?,
        sessionUrl: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = com.calssera.vcr.R.drawable.ic_info,
            title = getString(com.calssera.vcr.R.string.are_you_sure_you_want_to_start_vcr),
            message = getString(com.calssera.vcr.R.string.this_is_your_zoom_virtual_class_passcode) + "<br>"
                    + "<br>" + "<b>" + passcode + "</b>",
            buttonLeftText = getString(com.calssera.vcr.R.string.copy_and_continue),
            buttonRightText = getString(com.calssera.vcr.R.string.cancel),
        ) {
            when (it) {
                getString(com.calssera.vcr.R.string.copy_and_continue) -> {
                    val clipboard: ClipboardManager? =
                        context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    val clip =
                        ClipData.newPlainText("label", passcode)
                    clipboard?.setPrimaryClip(clip)

                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse(sessionUrl)
                    startActivity(openURL)
                }
            }
        }
    }

    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = com.calssera.vcr.R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(com.calssera.vcr.R.string.label_button_vcr_confirm_bottom_launch)
                || t == getString(com.calssera.vcr.R.string.label_button_vcr_confirm_bottom_view)
            ) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        filterView = null
        swipeRefreshLayout = null
        isFilterInitialized = false
        context?.unregisterReceiver(likeReceiver)
        context?.unregisterReceiver(understandReceiver)
        context?.unregisterReceiver(ratingReceiver)
        super.onDestroyView()
    }

    private companion object {

        private const val INDEX_ZERO = 0
        private const val INDEX_ONE = 1
        private const val INDEX_TWO = 2
        private const val INDEX_THREE = 3
        private const val INDEX_FOUR = 4
        private const val INDEX_FIVE = 5
        private const val INDEX_SIX = 6
    }
}
