package com.classera.courses.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.calssera.vcr.student.VcrStudentViewModel
import com.classera.core.BaseViewModel
import com.classera.data.BuildConfig
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.courses.CourseDetailsWrapper
import com.classera.data.models.courses.PreparationFilterWrapper
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.models.filter.Filterable
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class CourseDetailsViewModel(
    private val arguments: CourseDetailsFragmentArgs,
    private val coursesRepository: CoursesRepository,
    private val attachmentRepository: AttachmentRepository,
    private val vcrRepository: VcrRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var content: CourseDetailsWrapper? = null

    private val _addButtonVisibility = MutableLiveData<Boolean>()
    val addButtonVisibility: LiveData<Boolean> = _addButtonVisibility

    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var preparationFilterList: MutableList<PreparationFilterWrapper> = mutableListOf()
    var selectedFilter: Filterable? = null

    init {
        _addButtonVisibility.value = prefs.userRole == UserRole.TEACHER
    }

    fun getBrowseContent(
        preparationId: String? = null,
        showProgress: Boolean
    ) = liveData {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val courseId = arguments.courseId
        val teacherId = arguments.teacherId

        val resource =
            tryResource { coursesRepository.getCourseDetails(courseId, teacherId, preparationId) }
        content = resource.element<BaseWrapper<CourseDetailsWrapper>>()?.data

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAssignmentsCountBy(type: String): Int {
        return when (type) {
            EXAMS_TYPE -> {
                content?.assignments?.exams?.size ?: 0
            }
            else -> {
                content?.assignments?.homeworks?.size ?: 0
            }
        }
    }

    fun getAssignmentBy(type: String, position: Int): Assignment? {
        return when (type) {
            EXAMS_TYPE -> {
                content?.assignments?.exams?.get(position)
            }
            else -> {
                content?.assignments?.homeworks?.get(position)
            }
        }
    }

    fun getAttachmentsCountBy(type: String): Int {
        return when (type) {
            VIDEOS_TYPE -> {
                content?.attachments?.getVideoCount() ?: 0
            }
            MATERIAL_TYPE -> {
                content?.attachments?.materials?.size ?: 0
            }
            else -> {
                content?.attachments?.getOthersCount() ?: 0
            }
        }
    }

    fun getAttachmentBy(type: String, position: Int): Attachment? {
        return when (type) {
            VIDEOS_TYPE -> {
                content?.attachments?.allVideos?.get(position)
            }
            MATERIAL_TYPE -> {
                content?.attachments?.materials?.get(position)
            }
            else -> {
                content?.attachments?.others?.get(position)
            }
        }
    }

    fun getDiscussionRoomsCount(): Int {
        return content?.discussionRooms?.size ?: 0
    }

    fun getDiscussionRoomBy(position: Int): DiscussionRoom? {
        return content?.discussionRooms?.get(position)
    }

    fun getVcrsCount(): Int {
        return content?.vcrs?.size ?: 0
    }

    fun getVideosCount(): Int {
        return content?.attachments?.allVideos?.size ?: 0
    }

    fun getMaterialsCount(): Int {
        return content?.attachments?.materials?.size ?: 0
    }

    fun getVcrBy(position: Int): VcrResponse? {
        return content?.vcrs?.get(position)
    }

    fun getVCRURL(vcrId: String, type: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        when (type) {
            VcrStudentViewModel.UPCOMING -> {
                val resource = tryResource {
                    vcrRepository.getUpcomingVcrUrl(vcrId)
                }
                emit(resource)
            }
            VcrStudentViewModel.PASSED -> {
                val resource = tryResource {
                    vcrRepository.getPassedVcrUrl(vcrId)
                }
                emit(resource)
            }
        }
        emit(Resource.Loading(show = false))
    }

    fun onLikeClicked(type: String, position: Int) {
        val attachment = getAttachmentBy(type, position) ?: return
        attachment.liked = !attachment.liked

        if (attachment.liked) {
            attachment.likeCount = attachment.likeCount + 1
        } else {
            attachment.likeCount = attachment.likeCount - 1
        }

        _notifyAdapterItemLiveData.postValue(position)

        viewModelScope.launch {
            val resource = tryNoContentResource { attachmentRepository.addLike(attachment.id) }
            if (resource is Resource.Error) {
                attachment.liked = !attachment.liked
                _notifyAdapterItemLiveData.postValue(position)
                _toastLiveData.postValue(resource.error.message)
            }
        }
    }

    fun onSubmitRatingClicked(type: String, position: Int, rating: Float) {
        val attachment = getAttachmentBy(type, position) ?: return
        viewModelScope.launch {
            val resource = tryResource {
                attachmentRepository.addRate(attachment.id, rating.toString(), attachment.userId)
            }
            if (resource is Resource.Error) {
                _toastLiveData.postValue(resource.error.message)
            } else if (resource.isSuccess()) {
                attachment.isRated = true
                attachment.isRate = true
                attachment.rate = rating.toString()
                _notifyAdapterItemLiveData.postValue(position)
            }
        }
    }

    fun canShowAddButton(): Boolean {
        return prefs.userRole == UserRole.TEACHER
    }

    fun deleteVcr(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val vcr = getVcrBy(position)
            vcr?.deleting = true

            val sessionCode = vcr?.sessionCode
            val id = vcr?.id
            val resource = tryNoContentResource {
                vcrRepository.deleteVcr(sessionCode, id)
            }
            if (resource.isSuccess()) {
                content?.vcrs?.removeAt(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            vcr?.deleting = false
        }
    }

    fun isStudent(): Boolean {
        return prefs.userRole == UserRole.STUDENT
    }

    fun isParentRole(): Boolean {
        return prefs.userRole == UserRole.GUARDIAN &&
                prefs.allowParentAccessToSmartClass.equals("1", true)
    }

    fun getCurrentRole(): UserRole? {
        return prefs.userRole
    }

    fun getPreparation() = liveData(Dispatchers.IO) {
        val resource =
            tryResource { coursesRepository.getPreparation(arguments.courseId) }
        preparationFilterList.clear()
        preparationFilterList.addAll(
            resource.element<BaseWrapper<List<PreparationFilterWrapper>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilterPreparation(): Array<out Filterable?>? {
        return preparationFilterList?.toTypedArray()
    }

    fun getBrowseContentListByFilter(filter: Filterable?): LiveData<Resource>? {
        if (filter == selectedFilter) {
            return null
        }
        this.selectedFilter = filter
        return getBrowseContent(filter?.filterId, false)
    }

    fun getVcrDetails(id: String?, vendorId: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource =
                tryResource { vcrRepository.getVcrDetails(id, vendorId) }
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getExamsLink(): String =
        "${BuildConfig.flavorData.apiBaseUrl}/courses/redirect_from_mobile?" +
                "assignment_id=%s&Authtoken=%s&grade=%s&user_id=%s"

    companion object {
        const val VIDEOS_TYPE = "videos"
        const val MATERIAL_TYPE = "materials"
        const val ATTACHMENTS_OTHERS_TYPE = "others"
        const val EXAMS_TYPE = "exams"
        const val HOMEWORKS_TYPE = "homeworks"
        const val DISCUSSIONS_TYPE = "discussions"
        const val VCRS_TYPE = "vcrs"
    }
}
