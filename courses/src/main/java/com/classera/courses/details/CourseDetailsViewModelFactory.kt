package com.classera.courses.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.vcr.VcrRepository

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class CourseDetailsViewModelFactory(
    private val arguments: CourseDetailsFragmentArgs,
    private val coursesRepository: CoursesRepository,
    private val attachmentRepository: AttachmentRepository,
    private val prefs: Prefs,
    private val vcrRepository: VcrRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CourseDetailsViewModel(
            arguments,
            coursesRepository,
            attachmentRepository,
            vcrRepository,
            prefs
        ) as T
    }
}
