package com.classera.courses.details.adapters

import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseViewHolder

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
abstract class CourseDetailsAdapter<V : BaseViewHolder>(protected val type: String) : BaseAdapter<V>()
