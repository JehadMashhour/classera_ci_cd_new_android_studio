package com.classera.courses.details.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.calssera.vcr.databinding.RowVcrBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.courses.details.CourseDetailsViewModel

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrAdapter(
    type: String,
    private val viewModel: CourseDetailsViewModel
) : CourseDetailsAdapter<VcrAdapter.ViewHolder>(type) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowVcrBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getVcrsCount()
    }

    inner class ViewHolder(binding: RowVcrBinding) : BaseBindingViewHolder(binding) {

        private var imageViewMore: ImageView? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_vcr_more)
        }

        override fun bind(position: Int) {
            if (viewModel.isStudent() || viewModel.isParentRole()) {
                imageViewMore?.visibility = View.GONE
            }


            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            bind<RowVcrBinding> {
                vcrItem = viewModel.getVcrBy(position)
            }
        }
    }
}
