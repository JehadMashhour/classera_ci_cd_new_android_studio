package com.classera.courses.teacher

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.courses.R

/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
class CourseActionsBottomSheetFragment : BaseBottomSheetDialogFragment() {

    private var linearLayoutTakeAttendance: LinearLayout? = null
    private var linearLayoutManageContent: LinearLayout? = null
    private var linearLayoutAddBehavior: LinearLayout? = null
    private var linearLayoutAssignments: LinearLayout? = null
    private val navArguments: CourseActionsBottomSheetFragmentArgs by navArgs()

    override val layoutId: Int = R.layout.fragment_course_actions_bottom_sheet

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        linearLayoutTakeAttendance = view?.findViewById(
            R.id.linear_layout_fragment_course_actions_bottom_sheet_take_attendance
        )
        linearLayoutManageContent = view?.findViewById(
            R.id.linear_layout_fragment_course_actions_bottom_sheet_manage_content
        )
        linearLayoutAddBehavior = view?.findViewById(
            R.id.linear_layout_fragment_behavior_actions_bottom_sheet_manage_content
        )
        linearLayoutAssignments = view?.findViewById(
            R.id.linear_layout_fragment_course_actions_bottom_sheet_assignments
        )
    }

    private fun initListeners() {
        linearLayoutTakeAttendance?.setOnClickListener {
            val direction =
                CourseActionsBottomSheetFragmentDirections.takeAttendanceDirection().apply {
                    course = navArguments.course
                }
            findNavController().navigate(direction)
        }

        linearLayoutManageContent?.setOnClickListener {
            val course = navArguments.course
            val courseTitle = course?.courseTitle
            val teacherId = course?.teacherUserId
            val courseId = course?.courseId
            val direction = CourseActionsBottomSheetFragmentDirections.manageContentDirection(
                courseId,
                teacherId,
                courseTitle
            )
            findNavController().navigate(direction)
        }

        linearLayoutAddBehavior?.setOnClickListener {
            findNavController().navigate(CourseActionsBottomSheetFragmentDirections.addBehaviorDirection())
        }

        linearLayoutAssignments?.setOnClickListener {
            findNavController().navigate(CourseActionsBottomSheetFragmentDirections.assignmentDirection())
        }
    }
}
