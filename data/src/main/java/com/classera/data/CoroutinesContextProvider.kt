package com.classera.data

import kotlin.coroutines.CoroutineContext

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
interface CoroutinesContextProvider {

    val main: CoroutineContext

    val io: CoroutineContext
}
