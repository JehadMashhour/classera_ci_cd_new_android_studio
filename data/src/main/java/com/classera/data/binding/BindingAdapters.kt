package com.classera.data.binding

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import at.grabner.circleprogress.CircleProgressView
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.classera.data.models.user.User
import com.github.abdularis.buttonprogress.DownloadButtonProgress
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.material.textfield.TextInputLayout


/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
interface BindingAdapters {

    @BindingAdapter("imageUrl")
    fun ImageView.setImageUrl(url: String?)

    @BindingAdapter("imageUrlWithDefaultProfilePLaceHolder")
    fun ImageView.setImageUrlWithDefaultProfilePlaceholder(url: String?)

    @BindingAdapter("imageUrl", "backgroundResId")
    fun ImageView.setImageUrlWithBackground(url: String?, @DrawableRes backgroundResId: Int?)

    @BindingAdapter("circleImageUrl")
    fun ImageView.setCircleImageUrl(url: String?)

    @BindingAdapter("roundedImageUrl", "radius")
    fun ImageView.setRoundedImageUrl(url: String?, radius: Int)

    @BindingAdapter("circleBorderImageUrl", "borderSize", "borderColor", requireAll = false)
    fun ImageView.setCircleBorderedImageUrl(
        url: String?,
        borderSize: Float?,
        borderColor: Int?
    )

    @BindingAdapter("badge")
    fun ImageView.setBadge(attachment: Attachment?)

    @BindingAdapter("badgeVisibility")
    fun ImageView.setBadgeVisibility(contentType: AttachmentTypes?)

    @BindingAdapter("cpv_value")
    fun CircleProgressView.setCircleProgressValue(value: Int)

    @BindingAdapter("cpv_barColor")
    fun CircleProgressView.setCircleProgressColor(value: Int)

    @BindingAdapter("tint")
    fun ImageView.setImageViewTint(value: Int)

    @BindingAdapter("srcCompat")
    fun ImageView.setImageViewSrcCompat(value: Int)

    @BindingAdapter("srcCompat")
    fun ImageView.setImageViewSrcCompat(value: Drawable)

    @BindingAdapter(value = ["backgroundResId", "pathName", "pathColor"])
    fun ImageView.setImageViewVectorColor(
        @DrawableRes backgroundResId: Int,
        pathName: String?,
        pathColor: String?
    )

    @BindingAdapter("imageUrl", "type", "backgroundResId")
    fun ImageView.setDigitalLibraryAttachment(
        url: String?, digitalLibraryTypes: AttachmentTypes?,
        @DrawableRes backgroundResId: Int?
    )

    @BindingAdapter("enabled")
    fun View.setBindingEnabled(enabled: Boolean)

    @BindingAdapter("idleIconDrawable")
    fun DownloadButtonProgress.setIdleIconDrawable(drawable: Drawable)

    @BindingAdapter("state")
    fun DownloadButtonProgress.setState(state: Int)

    @BindingAdapter("error")
    fun TextInputLayout.setBindingError(error: String?)

    @BindingAdapter("videoUrl")
    fun PlayerView.setVideoUrl(url: String?)

    @BindingAdapter("fontFamily")
    fun TextView.setFontFamily(fontFamily: Int)

    @BindingAdapter("htmlToText")
    fun TextView.setHtmlText(value: String?)

    @BindingAdapter("languageDependentName")
    fun TextView.setNameBasedOnLanguage(user: User?)

    @BindingAdapter("android:visibility")
    fun View.setVisibility(value: Boolean)

    @BindingAdapter("android:textLeft", "android:textRight")
    fun TextView.setConcatenateText(textLeft: String?, textRight: String?)

    @BindingAdapter("textWithFirstCapitalLetter")
    fun TextView.setTextWithCapitalFirstCharacter(value: String)

    @BindingAdapter("flipXonArabicLanguage")
    fun View.flipXonArabicLanguage(value: Boolean)

    @BindingAdapter("layout_constraintGuide_percent")
    fun Guideline.setLayoutConstraintGuidePercent(percent: Float)

}
