package com.classera.data.crypto

import com.google.android.gms.common.util.Hex
import java.security.MessageDigest

/**
 * Project: Classera
 * Created: Dec 23, 2019
 *
 * @author Mohamed Hamdan
 */
fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    val digested = md.digest(toByteArray())
    return Hex.bytesToStringLowercase(digested)
}
