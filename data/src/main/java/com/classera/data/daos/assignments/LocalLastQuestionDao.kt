package com.classera.data.daos.assignments

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.classera.data.models.assignments.UserLastQuestion
import kotlinx.coroutines.flow.Flow


/**
 * Created by Rawan Al-Theeb on 7/5/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Dao
interface LocalLastQuestionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLastQuestion(userLastQuestion: UserLastQuestion)

    @Query("DELETE FROM user_last_question WHERE examId = :id")
    suspend fun deleteLastQuestion(id: String)

    @Query("SELECT * FROM user_last_question WHERE examId = :id LIMIT 1")
    fun getLastQuestion(id : String): Flow<UserLastQuestion>
}
