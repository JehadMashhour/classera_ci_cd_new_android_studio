package com.classera.data.daos.attendance

import com.classera.data.daos.attachment.UPLOAD_ATTACHMENT_TIME
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attendance.AbsenceSection
import com.classera.data.models.attendance.Absences
import com.classera.data.models.attendance.AttendanceTimeSlot
import com.classera.data.models.courses.Course
import com.classera.data.models.user.User
import com.classera.data.network.CustomTimeout
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import retrofit2.http.Url
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.FieldMap
import retrofit2.http.Field
import retrofit2.http.Part
import retrofit2.http.Multipart
import java.util.concurrent.TimeUnit

/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteAttendancesDao {

    @IncludeChildId
    @Pagination
    @GET
    suspend fun getAttendance(
        @Url url: String?,
        @Query("p") page: Int,
        @Query("excused") filterKey: String?
    ): BaseWrapper<List<Absences>>

    @GET("courses/get_cours_list")
    suspend fun getCourses(): BaseWrapper<List<Course>>

    @GET("Absences/get_sections")
    suspend fun getSections(@Query("course_id") courseId: String?): BaseWrapper<List<AbsenceSection>>

    @GET("Absences/get_student_attendance_list")
    suspend fun getStudents(@QueryMap query: Map<String, @JvmSuppressWildcards Any?>): BaseWrapper<List<User>>

    @GET("Absences/get_lectuer_timeslot")
    suspend fun getSectionTimeSlots(@Query("lecture_id") lectureId: String?):
            BaseWrapper<List<AttendanceTimeSlot>>

    @GET("Absences/return_timeslot_to_add")
    suspend fun getAllTimeSlots(): BaseWrapper<List<AttendanceTimeSlot>>

    @FormUrlEncoded
    @POST("Absences/add_timeslots")
    suspend fun createNewTimeSlot(
        @Field("lecture_id") lectureId: String?,
        @Field("timeslot_id") timeSlotId: String?
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("Absences/add_attendance")
    suspend fun changeStatusForStudent(@FieldMap body: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @CustomTimeout(UPLOAD_ATTACHMENT_TIME, TimeUnit.MINUTES)
    @Multipart
    @POST("Absences/add_attendance")
    suspend fun setLeaveWithPermission(
        @Part attachment: List<MultipartBody.Part>
    ): NoContentBaseWrapper

    @CustomTimeout(UPLOAD_ATTACHMENT_TIME, TimeUnit.MINUTES)
    @Multipart
    @POST("Absences/take_attendance_by_face")
    suspend fun takeAttendanceByFace(@Part parts: List<MultipartBody.Part>): NoContentBaseWrapper
}
