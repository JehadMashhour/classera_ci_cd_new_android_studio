package com.classera.data.daos.authentication

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.authentication.forgetusername.ForgetUserNameResponse
import com.classera.data.models.authentication.forgotpassword.ForgetPasswordResponse
import com.classera.data.models.authentication.login.LoginResponse
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.authentication.verificationcode.VerificationCodeResponse
import com.classera.data.network.IgnoreAppType
import com.classera.data.network.IgnoreAuthToken
import com.classera.data.network.IncludeLanguage
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
interface RemoteAuthenticationDao {

    @IgnoreAppType
    @IncludeLanguage
    @IgnoreAuthToken
    @FormUrlEncoded
    @POST("users/users_login")
    suspend fun login(@FieldMap fields: Map<String, String?>): BaseWrapper<LoginResponse>

    @IgnoreAppType
    @IgnoreAuthToken
    @FormUrlEncoded
    @POST("users/forgot_password")
    suspend fun forgetPassword(@FieldMap fields: Map<String, String?>): BaseWrapper<ForgetPasswordResponse>

    @IgnoreAppType
    @IgnoreAuthToken
    @FormUrlEncoded
    @POST("users/check_code")
    suspend fun checkVerificationCode(@FieldMap fields: Map<String, String?>): BaseWrapper<VerificationCodeResponse>

    @IgnoreAppType
    @IgnoreAuthToken
    @FormUrlEncoded
    @POST("users/resend_username")
    suspend fun forgetUsername(@FieldMap fields: Map<String, String?>): BaseWrapper<ForgetUserNameResponse>

    @IgnoreAppType
    @IgnoreAuthToken
    @FormUrlEncoded
    @POST("Users/google_login")
    suspend fun googleLogin(@FieldMap fields: Map<String, String?>): BaseWrapper<LoginResponse>

    @FormUrlEncoded
    @POST("Users/logout")
    suspend fun logout(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @GET("settings/check_f")
    suspend fun getGoogleLoginSettings(): BaseWrapper<GoogleRestrictionSettings>
}
