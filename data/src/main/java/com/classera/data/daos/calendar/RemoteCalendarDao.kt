package com.classera.data.daos.calendar

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.models.calendar.teacher.eventusers.UserSelectionRole
import com.classera.data.models.calendar.teacher.teacherlectures.TeacherLecture
import com.classera.data.network.IncludeChildId
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteCalendarDao {

    @IncludeChildId
    @GET("events/get_user_event_list")
    suspend fun getUserEvents(
        @QueryMap fields: Map<String, String?>
    ): BaseWrapper<List<AcademicCalendarEvent>>

    @FormUrlEncoded
    @POST("events/delete_event")
    suspend fun teacherDeleteEvent(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @GET("teachers/get_teacher_lecture")
    suspend fun teacherLectures(
    ): BaseWrapper<List<TeacherLecture>>

    @FormUrlEncoded
    @POST("events/add_event")
    suspend fun teacherAddEvent(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("events/edit_event")
    suspend fun teacherEditEvent(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper
}
