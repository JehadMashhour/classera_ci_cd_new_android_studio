package com.classera.data.daos.home

import com.classera.data.models.socket.OnlineNow
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.QueryMap

/**
 * Created by Saeed Halawani on 17/1/2019.
 * Classera
 * s.halawani@classera.com
 */
interface RemoteOnlineNowDao {

    @FormUrlEncoded
    @POST("api/onlines/get_roles_count")
    suspend fun getOnlineNow(@FieldMap fields: Map<String, String?>) : OnlineNow

}
