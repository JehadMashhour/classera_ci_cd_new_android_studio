package com.classera.data.daos.parent

import com.classera.data.models.BaseWrapper
import com.classera.data.models.parent.ParentChildWrapper
import retrofit2.http.GET

interface RemoteParentChildsDao {

    @GET("parents/children")
    suspend fun getParentChildList(): BaseWrapper<List<ParentChildWrapper>>

}
