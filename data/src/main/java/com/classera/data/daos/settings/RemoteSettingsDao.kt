package com.classera.data.daos.settings

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.settings.AppVersion
import com.classera.data.models.settings.NotificationStatus
import com.classera.data.models.settings.SettingsWrapper
import com.classera.data.network.IgnoreRefreshToken
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface RemoteSettingsDao {

    @FormUrlEncoded
    @POST("settings/change_language")
    suspend fun changeUserLanguage(@FieldMap fields: Map<String, String?>) : BaseWrapper<SettingsWrapper>

    @FormUrlEncoded
    @POST("Notifications/user_device_data")
    suspend fun notificationRegistration(@FieldMap fields: Map<String, String?>) : NoContentBaseWrapper

    @IgnoreRefreshToken
    @GET("settings/check_app_version")
    suspend fun getAppVersion(@QueryMap fields: Map<String, String?>) : BaseWrapper<AppVersion>

    @GET("Notifications/allow_notification")
    suspend fun getNotificationStatus() : BaseWrapper<NotificationStatus>

    @FormUrlEncoded
    @POST("Notifications/allow_notification")
    suspend fun notificationToggle(@FieldMap fields: Map<String, String?>) : NoContentBaseWrapper

    @FormUrlEncoded
    @POST("notifications/update_notificationLog_status")
    suspend fun updateNotificationStatus(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper
}
