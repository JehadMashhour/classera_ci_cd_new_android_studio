package com.classera.data.daos.switchschools

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchschools.School
import com.classera.data.models.switchschools.SchoolToken
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface RemoteSwitchSchoolsDao {

    @GET("users/get_user_schools")
    suspend fun getSchools(): BaseWrapper<List<School>>

    @FormUrlEncoded
    @POST("users/generate_jws_for_user_school")
    suspend fun changeSchool(@Field("school_user_id") school: String?): BaseWrapper<SchoolToken>
}
