package com.classera.data.daos.user

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.classera.data.models.user.User
import kotlinx.coroutines.flow.Flow

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Dao
interface LocalUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User?)

    @Query("SELECT * FROM user LIMIT 1")
    fun getCurrentUser(): Flow<List<User>?>

    @Query("DELETE FROM user")
    suspend fun deleteAllData()
}
