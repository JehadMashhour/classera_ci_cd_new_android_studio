package com.classera.data.flavor.classeraflavors

class ClasseraEduFlavorData : ClasseraFlavorData() {
    override val production_api_baseUrl: String
        get() = "https://lns-api.classera.com/v2/"
    override val staging_api_baseUrl: String
        get() = "https://lns-api.classera.com/v2/"
    override val development_api_baseUrl: String
        get() = "https://lns-api.classera.com/v2/"
    override val production_web_baseUrl: String
        get() = "https://edu.classera.com/"
    override val staging_web_baseUrl: String
        get() = "https://edu.classera.com/"
    override val development_web_baseUrl: String
        get() = "https://edu.classera.com/"
}
