package com.classera.data.flavor.classeraflavors

import com.classera.data.BuildConfig
import com.classera.data.flavor.BaseFlavorData

open class ClasseraFlavorData : BaseFlavorData {
    override val appType: String
        get() = "classera"
    override val bundleId: String
        get() = if (BuildConfig.FLAVOR_whitelabel == "classera") "com.app.classera" else super.bundleId
    override val production_api_baseUrl: String
        get() = "https://api.classera.com/v2/"
    override val staging_api_baseUrl: String
        get() = "http://qa1.classera.com/"
    override val development_api_baseUrl: String
        get() = "https://yazan-test.classera.com/"
    override val production_web_baseUrl: String
        get() = "https://me.classera.com/"
    override val staging_web_baseUrl: String
        get() = "https://qa2.classera.com/"
    override val development_web_baseUrl: String
        get() = "https://qa2.classera.com/"

}
