package com.classera.data.glide.vimeo

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.os.Environment.getExternalStorageState
import android.os.Environment.isExternalStorageRemovable
import androidx.annotation.VisibleForTesting
import com.classera.data.BuildConfig
import com.classera.data.crypto.md5
import com.jakewharton.disklrucache.DiskLruCache
import java.io.File

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
@SuppressLint("UsableSpace")
class VimeoDiskCache(private val context: Context) {

    @VisibleForTesting
    val diskCache: DiskLruCache by lazy {
        DiskLruCache.open(
            checkUsableSpaceAndExistence(getDiskCacheDir()),
            APP_VERSION,
            MAX_VALUE_COUNT,
            DISK_CACHE_SIZE
        )
    }

    @VisibleForTesting
    fun checkUsableSpaceAndExistence(cacheDir: File): File {
        if (cacheDir.usableSpace >= DISK_CACHE_SIZE) {
            cacheDir.delete()
        }

        if (!cacheDir.exists()) {
            cacheDir.mkdir()
        }
        return cacheDir
    }

    @VisibleForTesting
    fun getDiskCacheDir(): File {
        val cachePath = if (Environment.MEDIA_MOUNTED == getExternalStorageState() || !isExternalStorageRemovable()) {
            context.externalCacheDir?.path
        } else {
            context.cacheDir.path
        }
        return File(cachePath + File.separator + DISK_CACHE_SUB_DIRECTORY)
    }

    fun save(model: Vimeo?, imageUrl: String) {
        val editor = diskCache.edit(getKey(model))
        val outputStream = editor.newOutputStream(0)
        outputStream.write(imageUrl.toByteArray())
        outputStream.close()
        editor.commit()
    }

    fun get(model: Vimeo?): String? {
        return diskCache.get(getKey(model))?.getString(0)
    }

    @VisibleForTesting
    fun getKey(model: Vimeo?): String? {
        if (model == null) {
            return null
        }
        return model.url.md5()
    }

    fun close() {
        diskCache.close()
    }

    @VisibleForTesting
    companion object {

        const val DISK_CACHE_SIZE = 1024L * 1024L * 500L

        private const val APP_VERSION = BuildConfig.VERSION_CODE
        private const val MAX_VALUE_COUNT = 1

        private const val DISK_CACHE_SUB_DIRECTORY = "classera_vimeo_cache"
    }
}
