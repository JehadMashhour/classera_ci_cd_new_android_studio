package com.classera.data.glide.vimeo

import android.os.StrictMode
import androidx.annotation.NonNull
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
class VimeoThumbnailUrlLoader(
    urlLoader: ModelLoader<GlideUrl, InputStream>,
    private val vimeoDiskCache: VimeoDiskCache?,
    private val vimeoApis: VimeoApis,
    private val threadPolicy: StrictMode.ThreadPolicy
) : BaseGlideUrlLoader<Vimeo>(urlLoader) {

    override fun handles(@NonNull model: Vimeo): Boolean {
        return true
    }

    public override fun getUrl(model: Vimeo?, width: Int, height: Int, options: Options): String? {
        val cachedUrl = vimeoDiskCache?.get(model)
        if (cachedUrl != null) {
            return cachedUrl
        }
        StrictMode.setThreadPolicy(threadPolicy)

        val staticMapUrl = vimeoApis.getVideoThumbnail(model)
        if (staticMapUrl != null) {
            vimeoDiskCache?.save(model, staticMapUrl)
        }
        return staticMapUrl
    }
}
