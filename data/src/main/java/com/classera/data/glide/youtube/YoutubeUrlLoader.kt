package com.classera.data.glide.youtube

import android.net.Uri
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import java.io.InputStream
import java.util.*

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
class YoutubeUrlLoader(urlLoader: ModelLoader<GlideUrl, InputStream>) : BaseGlideUrlLoader<String>(urlLoader) {

    override fun handles(url: String): Boolean {
        return isYoutubeUrl(url)
    }

    public override fun getUrl(url: String?, width: Int, height: Int, options: Options): String? {
        return YOUTUBE_THUMBNAIL_BASE_URL.format(Locale.ENGLISH, getYoutubeVideoId(url))
    }

    private fun isYoutubeUrl(url: String?): Boolean {
        return url?.contains("youtube", true) == true
    }

    private fun getYoutubeVideoId(url: String?): String? {
        return Uri.parse(url).getQueryParameter("v")
    }

    private companion object {

        private const val YOUTUBE_THUMBNAIL_BASE_URL = "https://i.ytimg.com/vi/%s/sddefault.jpg"
    }
}
