package com.classera.data.models.announcements


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Announcement(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "details")
    val details: String? = null,

    @Json(name = "end")
    val end: String? = null,

    @Json(name = "start")
    val start: String? = null,

    @Json(name = "title")
    val title: String? = null
) : Parcelable
