package com.classera.data.models.announcements

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class AnnouncementsWrapper(

    @Json(name = "announcements")
    val announcements: List<AnnouncementWrapper>? = null
)
