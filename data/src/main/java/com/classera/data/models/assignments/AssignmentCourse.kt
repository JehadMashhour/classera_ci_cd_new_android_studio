package com.classera.data.models.assignments


import com.classera.data.models.filter.Filterable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class AssignmentCourse(

    @Json(name = "id")
    override val filterId: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false
) : Filterable
