package com.classera.data.models.assignments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Feb 18, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class AssignmentDetails(

    @Json(name = "Assignment")
    val assignment: Assignment,

    @Json(name = "Lecture")
    val lectures: List<Lecture>? = null,

    @Json(name = "Students")
    val students: List<Student>? = null
)
