package com.classera.data.models.assignments

import com.classera.data.R
import com.classera.data.moshi.enums.Json

/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
enum class AssignmentStatus(val label: Int) {

    @Json(name = "Launch")
    LAUNCH(R.string.button_row_assignment_action_launch),

    @Json(name = "Missed")
    MISSED(R.string.button_row_assignment_action_missed),

    @Json(name = "Submission Grade")
    SUBMISSION_GRADE(R.string.button_row_assignment_action_submission_grad),

    @Json(name = "Submission Grade | Start Again")
    START_AGAIN(R.string.button_row_assignment_action_launch_again),

    @Json(name = "Not Attempted")
    NOT_ATTEMPTED(R.string.button_row_assignment_action_not_attempted),

    UNKNOWN(R.string.button_row_assignment_action_launch)
}
