package com.classera.data.models.assignments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AssignmentsResponse(

    @Json(name = "list")
    val assignment: List<Assignment?>? = null,

    @Json(name = "available_courses")
    val availableCourses: List<CourseWrapper>? = null
) {

    fun getCourses(): List<AssignmentCourse?>? {
        return availableCourses?.map { it.course }
    }
}
