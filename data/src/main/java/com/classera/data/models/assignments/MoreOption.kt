package com.classera.data.models.assignments

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Parcelize
data class MoreOption(

    override val id: String?,

    override var selected: Boolean = false,

    override val title: String?

) : Selectable, Parcelable
