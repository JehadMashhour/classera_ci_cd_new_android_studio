package com.classera.data.models.assignments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@JsonClass(generateAdapter = true)
data class Preparation(

    @Json(name = "id")
    var id: String? = null,

    @Json(name = "title")
    var title: String? = null
)
