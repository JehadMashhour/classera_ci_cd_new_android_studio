package com.classera.data.models.assignments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@JsonClass(generateAdapter = true)
data class PublishUser(

    @Json(name = "lectures")
    val lectures: MutableList<Lecture>? = null,

    @Json(name = "students")
    val students: MutableList<Student>? = null
)
