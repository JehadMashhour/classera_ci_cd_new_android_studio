package com.classera.data.models.assignments

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Mar 11, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class SubQuestions(

    @Expose
    @SerializedName("choices")
    @Json(name = "choices")
    val choices: List<String>? = null,

    @Expose
    @SerializedName("Questions")
    @Json(name = "Questions")
    val questions: List<Question>? = null,

    @Expose
    @SerializedName("Questions_ids")
    @Json(name = "Questions_ids")
    val questionIds: List<Question>? = null
) : Parcelable {

    fun getSelectableQuestions(): List<SelectableChoice>? {
        return questions?.map { SelectableChoice(it.id, it.text) }
    }

    fun getSelectableQuestionIds(): List<SelectableChoice>? {
        return questionIds?.map { SelectableChoice(it.id, it.text) }
    }

    fun getAllQuestions(): List<Question> {
        return questions ?: questionIds ?: listOf()
    }
}
