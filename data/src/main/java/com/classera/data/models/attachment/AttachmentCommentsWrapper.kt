package com.classera.data.models.attachment

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class AttachmentCommentsWrapper(

    @Json(name = "comments")
    val comments: List<AttachmentComment>,

    @Json(name = "count")
    val count: Int?
)
