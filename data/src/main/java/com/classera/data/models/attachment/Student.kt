package com.classera.data.models.attachment


import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Student(

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "image")
    val image: String? = null,

    @Json(name = "name")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false

) : Selectable, Parcelable

