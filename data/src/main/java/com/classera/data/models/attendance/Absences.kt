package com.classera.data.models.attendance


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Absences(

    @Json(name = "comments")
    val comments: String? = null,

    @Json(name = "date")
    val date: String? = null,

    @Json(name = "excused")
    val excused: Boolean? = false,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    var isExpanded: Boolean = false
)

