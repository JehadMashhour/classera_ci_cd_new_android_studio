package com.classera.data.models.attendance

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class AttendanceTimeSlot(

    @Json(name = "id")
    val id: String?,

    @Json(name = "title")
    val title: String?
)
