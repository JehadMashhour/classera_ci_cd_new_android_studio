package com.classera.data.models.authentication.login

import com.classera.data.models.user.User
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserData(

    @Json(name = "User")
    var user: User? = null,

    @Json(name = "School")
    val school: School? = null
)
