package com.classera.data.models.behaviours


import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Parcelize
data class Behaviour(

    @Json(name = "action_description")
    val actionDescription: String? = null,

    @Json(name = "action_id")
    val actionId: String? = null,

    @Json(name = "action_point")
    val actionPoint: String? = null,

    @Json(name = "action_title")
    val actionTitle: String? = null,

    @Json(name = "attachment_id")
    val attachmentId: String? = null,

    @Json(name = "behavior_description")
    val behaviorDescription: String? = null,

    @Json(name = "behavior_group_id")
    val behaviorGroupId: String? = null,

    @Json(name = "behavior_group_teacher_enabled")
    val behaviorGroupTeacherEnabled: Boolean? = null,

    @Json(name = "behavior_group_title")
    val behaviorGroupTitle: String? = null,

    @Json(name = "behavior_group_type")
    val behaviorGroupType: String? = null,

    @Json(name = "behavior_id")
    val behaviorId: String? = null,

    @Json(name = "behavior_point")
    val behaviorPoint: String? = null,

    @Json(name = "behavior_title")
    val behaviorTitle: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "course_title_ara")
    val courseTitleAra: String? = null,

    @Json(name = "created_by")
    val createdBy: String? = null,

    @Json(name = "date")
    val date: String? = null,

    @Json(name = "details")
    val details: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "lecture_id")
    val lectureId: String? = null,

    @Json(name = "positive")
    val positive: Int? = null,

    @Json(name = "register_by_image")
    val registerByImage: String? = null,

    @Json(name = "register_by_name")
    val registerByName: String? = null,

    @Json(name = "show_to_guardian")
    val showToGuardian: Boolean? = null,

    @Json(name = "show_to_student")
    val showToStudent: Boolean? = null,

    @Json(name = "student_id")
    val studentId: String? = null,

    @Json(name = "student_user_id")
    val studentUserId: String? = null,

    @Json(name = "student_name")
    val studentName: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "student_avatar")
    val studentAvatar: String? = null,

    @Json(name = "attachment_url")
    val attachmentUrl: String? = null

) : BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var deleting: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleting)
        }

    companion object {
        const val POSITIVE = 1
    }
}
