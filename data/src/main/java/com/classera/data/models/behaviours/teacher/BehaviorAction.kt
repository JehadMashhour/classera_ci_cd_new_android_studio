package com.classera.data.models.behaviours.teacher

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BehaviorAction (

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null

)
