package com.classera.data.models.behaviours.teacher

import com.classera.data.models.user.User
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StudentWraper(
    @Json(name = "User")
    val user: User? = null,

    @Json(name = "Student")
    val student: User? = null

)
