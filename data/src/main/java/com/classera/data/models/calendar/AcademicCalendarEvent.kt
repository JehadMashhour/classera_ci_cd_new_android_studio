package com.classera.data.models.calendar

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.classera.data.moshi.todate.ToDate
import com.classera.data.toString
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Parcelize
data class AcademicCalendarEvent(

    @Json(name = "id")
    var id: String? = null,

    @Json(name = "title")
    var title: String? = null,

    @Json(name = "description")
    var description: String? = null,

    @ToDate
    @Json(name = "start")
    var start: Date? = null,

    @ToDate
    @Json(name = "end")
    var end: Date? = null,

    @Json(name = "all_day")
    var isAllDay: Boolean? = false,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @Transient
    val lecturesIds: List<String>? = null,

    @Transient
    val users: List<String>? = null,

    @Transient
    val toAll: String? = null
) : BaseObservable(), Parcelable {

    fun isFinished(): Boolean {
        return end?.before(Calendar.getInstance().time) == true
    }

    fun getStartDateLabel(): String? {
        return start.toString("yyyy-MM-dd hh:mm")
    }

    fun getStartDate(): String? {
        return start.toString("yyyy-MM-dd")
    }

    fun getStartTime(): String? {
        return start.toString("hh:mm a")
    }

    fun getEndDate(): String? {
        return end.toString("yyyy-MM-dd")
    }

    fun getEndTime(): String? {
        return end.toString("hh:mm a")
    }

    fun getStartDateYear(): Int {
        return Calendar.getInstance().apply { time = start }.get(Calendar.YEAR)
    }

    fun getStartDateMonth(): Int {
        return Calendar.getInstance().apply { time = start }.get(Calendar.MONTH) + 1
    }

    fun getStartDateDay(): Int {
        return Calendar.getInstance().apply { time = start }.get(Calendar.DAY_OF_MONTH)
    }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var deleting: Boolean = false
    set(value) {
        field = value
        notifyPropertyChanged(BR.deleting)
    }

}

