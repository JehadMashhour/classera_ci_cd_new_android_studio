package com.classera.data.models.calendar.teacher.teacherlectures


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass (generateAdapter = true)
data class TeacherLecture(
    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null
)
