package com.classera.data.models.callchildren


import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rawan Al-Theeb on 8/17/2021.
 * Classera
 * r.altheeb@classera.com
 */

@Parcelize
data class CallStatus(

    @Json(name = "msg")
    val msg: String? = null,

    @Json(name = "sender_avatar")
    val senderAvatar: String? = null,

    @Json(name = "sender_name")
    val senderName: String? = null,

    @Json(name = "status")
    var status: String? = null

) : BaseObservable(), Parcelable {
    val callStatusTypes: CallStatusTypes
        get() = CallStatusTypes.getCallStatusType(status)

    val canCall: Boolean
        get() = when (callStatusTypes) {
            CallStatusTypes.UNKNOWN,
            CallStatusTypes.ARRIVED,
            CallStatusTypes.CANCELED -> {
                true
            }
            else -> false
        }
    val isCalling: Boolean
        get() = callStatusTypes == CallStatusTypes.ACTIVE

    val isDismissed: Boolean
        get() = callStatusTypes == CallStatusTypes.DISMISSED
}

