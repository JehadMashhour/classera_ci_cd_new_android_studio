package com.classera.data.models.callchildren

import com.classera.data.moshi.enums.Json

/**
 * Project: Classera
 * Created: 8/18/2021
 *
 * @author Jehad Abdalqader
 */
@Suppress("MagicNumber")
enum class CallStatusTypes(val id: Int) {
    @Json(name = "0")
    ACTIVE(
        0
    ),

    @Json(name = "1")
    DISMISSED(
        1
    ),

    @Json(name = "2")
    CANCELED(
        2
    ),

    @Json(name = "3")
    CHECKEDOUT(
        3
    ),

    @Json(name = "4")
    ARRIVED(
        4
    ),

    UNKNOWN(-1);

    companion object {
        fun getCallStatusType(id: String?): CallStatusTypes {
            id?.let {
                return values().first { it.id == id.toInt() }
            }
            return UNKNOWN
        }
    }
}
