package com.classera.data.models.callchildren

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ChildStudent(

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "name")
    override val studentName: String? = null,

    @Json(name = "profile_picture")
    override val profileImage: String? = null,

    @Json(name = "status")
    override var status: String? = null,

    @Json(name = "status_name")
    override val statusName: String? = null,

    @Json(name = "request_id")
    override var requestId: String? = null,

    @Json(name = "level")
    val level: String? = null,

    @Json(name = "score")
    val score: Score? = Score(),

    @Json(name = "student_id")
    val studentId: String? = null,

    @Json(name = "w3w")
    val w3w: String? = null,

    override var isLoading: Boolean?,

    ) : BaseStudent()
