package com.classera.data.models.callchildren


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rawan Al-Theeb on 8/17/2021.
 * Classera
 * r.altheeb@classera.com
 */

@Parcelize
@JsonClass(generateAdapter = true)
data class DriverStudent(

    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "full_name")
    override val studentName: String? = null,

    @Json(name = "avatar")
    override val profileImage: String? = null,

    @Json(name = "status")
    override var status: String? = null,

    @Json(name = "status_name")
    override val statusName: String? = null,

    @Json(name = "request_id")
    override var requestId: String? = null,

    @Json(name = "email")
    val email: String? = null,

    @Json(name = "name")
    val driverName: String? = null,

    @Json(name = "mobile_number")
    val mobileNumber: String? = null,

    @Json(name = "photo_filename")
    val photoFilename: String? = null,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "w3w")
    val w3w: String? = null,

    override var isLoading: Boolean?,

    ) : BaseStudent()
