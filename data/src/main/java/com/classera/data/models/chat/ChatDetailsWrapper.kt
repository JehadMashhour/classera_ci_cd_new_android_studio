package com.classera.data.models.chat

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created on 13/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Parcelize
@JsonClass(generateAdapter = true)
class ChatDetailsWrapper(

    @Json(name = "Message")
    val message: Message? = null
) : Parcelable
