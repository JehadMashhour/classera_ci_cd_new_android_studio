package com.classera.data.models.chat

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.classera.data.BR
import com.classera.data.R
import com.classera.data.moshi.chattime.ChatTime
import com.classera.data.moshi.duration.Duration
import com.classera.data.moshi.messagebody.MessageBodyConverter
import com.classera.data.moshi.messages.MessageType
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 1/11/2020, Modified by Lana Manaseer
 * Classera
 *
 * o.nazzal@classera.com
 */
@Parcelize
data class Message(

    @Json(name = "thread_id")
    val threadId: String? = null,

    @Json(name = "message_id")
    val messageId: String? = null,

    @Json(name = "from_id")
    val fromId: String? = null,

    @MessageBodyConverter
    @Json(name = "message")
    var body: MessageBody? = null,

    @Json(name = "not_seen")
    val notSeen: String? = null,

    @ChatTime
    @Json(name = "sent_date")
    val sentDate: String? = null,

    @Json(name = "to_id")
    val toId: List<String>? = null,

    @MessageType
    @Json(name = "type")
    val type: MessageTypeEnum? = null,

    @Json(name = "last_message_seen")
    val lastMessageSeen: Boolean? = null,

    @Transient
    var senderPicture: String? = null,

    @Transient
    var senderName: String? = null
) : BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    var errorMessage: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.errorMessage)
        }

    @get:Bindable
    @IgnoredOnParcel
    var error: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.error)
        }

    @Duration
    @IgnoredOnParcel
    @Json(name = "duration")
    var duration: String? = null
        get() {
            return if (field == null) {
                body?.duration
            } else {
                field
            }
        }

    @get:Bindable
    @IgnoredOnParcel
    var uploading: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.uploading)
        }

    @get:Bindable
    @IgnoredOnParcel
    var group: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.group)
        }

    @Transient
    @get:Bindable
    @IgnoredOnParcel
    var playingProgress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.playingProgress)
        }

    @Transient
    @get:Bindable
    @IgnoredOnParcel
    var maxProgress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.maxProgress)
        }

    @Transient
    @IgnoredOnParcel
    var isPlaying = false

    @Transient
    @get:Bindable
    @IgnoredOnParcel
    var icon = R.drawable.ic_play
        set(value) {
            field = value
            notifyPropertyChanged(BR.icon)
        }

    @Transient
    @get:Bindable
    @IgnoredOnParcel
    var currentTime: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.currentTime)
        }

    @Transient
    @get:Bindable
    @IgnoredOnParcel
    var showProgress: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.showProgress)
        }
}
