package com.classera.data.models.chat

import android.view.View
import com.classera.data.R
import com.classera.data.moshi.enums.Json

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Suppress("MagicNumber")
enum class MessageTypeEnum(
    val type: Int,
    val firstLayout: Int,
    val secondLayout: Int,
    val containsTwoDirections: Boolean
) {

    @Json(name = "1")
    TEXT(1, R.layout.row_message_text_sender, R.layout.row_message_text_receiver, true),

    @Json(name = "2")
    MEDIA(2, R.layout.row_message_media_sender, R.layout.row_message_media_receiver, true),

    @Json(name = "3")
    ADDED(3, R.layout.row_message_added, View.NO_ID, false),

    @Json(name = "4")
    REMOVED(4, R.layout.row_message_removed, View.NO_ID, false),

    UNKNOWN(-1, View.NO_ID, View.NO_ID, false)
}
