package com.classera.data.models.chat

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MessagesWrapper(

    @Json(name = "pageState")
    val pageState: String? = null,

    @Json(name = "status")
    val status: Boolean? = null,

    @Json(name = "result")
    var result: List<Message>? = null
)
