package com.classera.data.models.chat

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserStatusWrapper<T>(

    @Json(name = "status")
    val status: Boolean? = null,

    @Json(name = "result")
    var result: T? = null
)
