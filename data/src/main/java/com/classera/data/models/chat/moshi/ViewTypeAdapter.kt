package com.classera.data.models.chat.moshi

import com.classera.data.prefs.Prefs
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import com.classera.data.models.chat.ViewType as ViewTypeEnum

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class ViewTypeAdapter(private val prefs: Prefs) {

    @ToJson
    fun toJson(@ViewType value: ViewTypeEnum?): String? {
        return value?.senderId
    }

    @FromJson
    @ViewType
    fun fromJson(value: String?): ViewTypeEnum? {
        return (if (value == prefs.userId) ViewTypeEnum.RIGHT else ViewTypeEnum.LEFT).apply { senderId = value }
    }
}
