package com.classera.data.models.classvisits


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */

@JsonClass(generateAdapter = true)
data class ClassVisit(
    @Json(name = "assessment_id")
    val assessmentId: String? = null,

    @Json(name = "assessment_title")
    val assessmentTitle: String? = null,

    @Json(name = "date")
    val date: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "lecture_timeslot_title")
    val lectureTimeslotTitle: String? = null,

    @Json(name = "private_visit")
    val privateVisit: Boolean? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "teacher_full_name")
    val teacherFullName: String? = null,

    @Json(name = "teacher_photo")
    val teacherPhoto: String? = null,

    @Json(name = "tsup_user_id")
    val tsupUserId: String? = null
) {
    fun getVisitStatus(): String? {
        return if (privateVisit!!)
            PRIVATE
        else
            PUBLIC
    }

    fun isVisitPrivate(): Boolean{
        return getVisitStatus().equals(PRIVATE,true)
    }

    companion object {
        const val PRIVATE = "Private"
        const val PUBLIC = "Public"
    }
}

