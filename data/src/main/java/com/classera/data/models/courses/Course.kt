package com.classera.data.models.courses

import android.os.Parcelable
import com.classera.data.models.filter.Filterable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 1/7/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Course(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "course_id")
    val courseId: String? = null,

    @Json(name = "course_icon")
    val courseIcon: String? = null,

    @Json(name = "teacher_user_id")
    val teacherUserId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "image_url")
    val teacherImage: String? = null,

    @Json(name = "lecture_id")
    val lectureId: String? = null,

    @Json(name = "progress")
    val progress: Int? = null,

    @Transient
    override var selected: Boolean = false

) : Parcelable, Filterable {

    override val title: String?
        get() = courseTitle

    override val filterId: String?
        get() = courseId
}
