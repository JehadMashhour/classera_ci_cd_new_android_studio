package com.classera.data.models.courses

import com.classera.data.models.digitallibrary.Attachment
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class CourseAttachmentsWrapper(

    @Json(name = "Video")
    val videos: List<Attachment>? = null,

    @Json(name = "Material")
    val materials: List<Attachment>? = null,

    @Json(name = "Scorm")
    val scorms: List<Attachment>? = null,

    @Json(name = "Bweb")
    val bWeb: List<Attachment>? = null,

    @Json(name = "BVideo")
    val bVideo: List<Attachment>? = null,

    @Json(name = "Bshows")
    val bShows: List<Attachment>? = null,

    @Json(name = "Bphoto")
    val bPhoto: List<Attachment>? = null,

    @Json(name = "Bpgm")
    val bPgm: List<Attachment>? = null,

    @Json(name = "Bmp3")
    val bMp3: List<Attachment>? = null,

    @Json(name = "Bflash")
    val bFlash: List<Attachment>? = null,

    @Json(name = "Bdocument")
    val bDocument: List<Attachment>? = null
) {

    val allVideos: List<Attachment> by lazy { (videos + bVideo).sortedByDescending { it.publishDate } }
    val others: List<Attachment> by lazy {
        (scorms + bWeb + bShows + bPhoto + bPgm + bMp3 + bFlash + bDocument).sortedByDescending { it.publishDate }
    }

    fun getOthersCount(): Int {
        return others.size
    }

    fun getVideoCount(): Int {
        return allVideos.size
    }

    operator fun List<Attachment>?.plus(list: List<Attachment>?): List<Attachment> {
        return listOf(*(this ?: mutableListOf()).toTypedArray(), *(list ?: mutableListOf()).toTypedArray())
    }
}
