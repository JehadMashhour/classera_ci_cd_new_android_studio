package com.classera.data.models.digitallibrary

import android.os.Parcelable
import com.classera.data.models.BackgroundColor
import com.classera.data.models.digitallibrary.AttachmentTypes.WEBSITE
import com.classera.data.models.digitallibrary.AttachmentTypes.STREAM
import com.classera.data.models.digitallibrary.AttachmentTypes.VIMEO
import com.classera.data.models.digitallibrary.AttachmentTypes.VIDEO
import com.classera.data.models.digitallibrary.AttachmentTypes.YOUTUBE
import com.classera.data.models.digitallibrary.AttachmentTypes.IMAGE
import com.classera.data.models.digitallibrary.AttachmentTypes.PDF
import com.classera.data.models.digitallibrary.AttachmentTypes.EXTERNAL_RESOURCES
import com.classera.data.moshi.rate.Rate
import com.classera.data.moshi.toint.ToInt
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.text.FieldPosition

/**
 * Project: Classera
 * Created: Dec 23, 2019
 *
 * @author Mohamed Hamdan
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Attachment(

    @Json(name = "id")
    var id: String? = null,

    @Json(name = "original_filename")
    val originalFilename: String? = null,

    @Json(name = "attachment_title")
    val attachmentTitle: String? = null,

    @Json(name = "content_type")
    val contentType: AttachmentTypes? = null,

    @Json(name = "attachment_type")
    val attachmentType: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "teacher_full_name")
    val teacherFullName: String? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "level_title")
    val levelTitle: String? = null,

    @ToInt
    @Json(name = "likes")
    var likesCount: Int = 0,

    @ToInt
    @Json(name = "like_count")
    var likeCount: Int = 0,

    @ToInt
    @Json(name = "read_count")
    var readCount: Int = 0,

    @ToInt
    @Json(name = "total_views")
    var views: Int = 0,

    @ToInt
    @Json(name = "understand_count")
    var understandCount: Int = 0,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "publish_date")
    val publishDate: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "description")
    val description: String? = null,

    @Json(name = "course_id")
    val courseId: String? = null,

    @ToInt
    @Json(name = "total_reactions")
    val totalReactions: Int = 0,

    @Json(name = "i_likes")
    var liked: Boolean = false,

    @Json(name = "i_understand")
    var understand: Boolean = false,

    @Rate
    @Json(name = "rate")
    var rate: String?,

    @Json(name = "img_url")
    val imgUrl: String? = null,

    @Json(name = "i_rated")
    var isRated: Boolean = false,

    @Json(name = "i_rate")
    var isRate: Boolean = false,

    @Json(name = "is_blocked")
    val isBlocked: Boolean = false,

    @Json(name = "block_message")
    var blockMessage: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Json(name = "att_url")
    var attachmentUrl: String? = null,

    @Json(name = "my_rate")
    val myRate: String? = null,

    @Json(name = "stream_url")
    val streamUrl: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "allow_download")
    val allowDownload: String? = null,

    var backgroundColor: BackgroundColor? = null,

    @Json(name = "cover_img_url")
    var coverImgUrl: String? = null,

    @Json(name = "comments")
    val comments: String? = null,

    @Json(name = "sub_attachment")
    val subAttachment: MutableList<SubAttachment>? = null,

    @Json(name = "has_multi_attachment")
    var hasMultiAttachment: Boolean = false,

    var isDetailsExpanded: Boolean = false,


    var selectedSubAttachmentPosition: Int? = null

) : Parcelable {

    fun getTotalViews(): Int {
        return if (readCount == 0) views else readCount
    }

    fun isVideo(): Boolean {
        return contentType == VIDEO || contentType == YOUTUBE ||
                contentType == VIMEO || contentType == STREAM ||
                contentType == EXTERNAL_RESOURCES
    }

    fun getTeacherName(): String? {
        return teacherFullName ?: fullName
    }

    fun getCreatedDate(): String? {
        return created ?: publishDate
    }

    fun getRightImageUrl(): String? {
        return imgUrl ?: imageUrl
    }

    fun getRightCoverImageUrl(): String? {
        return coverImgUrl ?: attachmentUrl
    }

    fun isDownloadable(): Boolean {
        return contentType != YOUTUBE && contentType != VIMEO && type != "Scorm"
                && allowDownload.equals("1")
    }

    fun isCacheFileNeeded(): Boolean {
        return contentType != YOUTUBE && contentType != VIMEO
                && contentType != WEBSITE && contentType != IMAGE && contentType != PDF
    }

    fun rateValue(): Double {
        return rate?.toDouble() ?: 0.0
    }

    fun isRatedValue(): Boolean {
        return isRate || isRated || rateValue() != 0.0
    }

    fun likeCountValue(): Int {
        return when {
            likesCount != 0 -> likesCount
            likeCount != 0 -> likeCount
            else -> 0
        }
    }

    fun increaseViewsCount() {
        if (readCount == 0) {
            views++
        } else {
            readCount++
        }
    }

    fun isWebsiteType(): Boolean? {
        return contentType?.equals(WEBSITE)
    }

}
