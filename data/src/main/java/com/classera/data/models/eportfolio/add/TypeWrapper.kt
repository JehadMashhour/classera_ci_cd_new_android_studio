package com.classera.data.models.eportfolio.add


import com.squareup.moshi.Json

data class TypeWrapper(
    @Json(name = "id")
    val id: String? = "",
    @Json(name = "name")
    val name: String? = ""
)
