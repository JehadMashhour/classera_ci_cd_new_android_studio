package com.classera.data.models.eportfolio.details

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class EPortfolioDetailsWraper(
    @Json(name = "post")
    val eportfolio: EPortfolioDetails? = null
)
