package com.classera.data.models.home.admin

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: classeraandroidv3
 * Created: Mar 06, 2020
 *
 * @author Odai Nazzal
 */
@JsonClass(generateAdapter = true)
data class SmsUsage(

    @Json(name = "title")
    val title: String,

    @Json(name = "value")
    val value: String
)
