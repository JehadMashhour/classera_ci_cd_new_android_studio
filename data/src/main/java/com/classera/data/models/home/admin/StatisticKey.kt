package com.classera.data.models.home.admin

import com.classera.data.R
import com.squareup.moshi.Json

/**
 * Project: classeraandroidv3
 * Created: Mar 12, 2020
 *
 * @author Odai Nazzal
 */

enum class StatisticKey(
    val color: Int,
    val icon: Int
) {
    @Json(name = "Messages")
    Messages(
        color = R.color.admin_home_fragment_content_statistics_messages,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_messages
    ),

    @Json(name = "Answers")
    Answers(
        color = R.color.admin_home_fragment_content_statistics_answers,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_answers
    ),

    @Json(name = "Attachments")
    Attachments(
        color = R.color.admin_home_fragment_content_statistics_attachments,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_attachments
    ),

    @Json(name = "Online Assignments")
    OnlineAssignments(
        color = R.color.admin_home_fragment_content_statistics_online_assignments,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_online_assignments
    ),

    @Json(name = "Submissions")
    Submissions(
        color = R.color.admin_home_fragment_content_statistics_submissions,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_submissions
    ),

    @Json(name = "Questions")
    Questions(
        color = R.color.admin_home_fragment_content_statistics_questions,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_questions
    ),

    @Json(name = "VCR")
    VCR(
        color = R.color.admin_home_fragment_content_statistics_vcr,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_vcr
    ),

    @Json(name = "Preparations")
    Preparations(
        color = R.color.admin_home_fragment_content_statistics_preparations,
        icon = R.drawable.ic_admin_home_fragment_content_statistics_preparations
    )
}
