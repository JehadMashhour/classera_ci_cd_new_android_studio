package com.classera.data.models.home.admin

import com.squareup.moshi.Json

/**
 * Project: classeraandroidv3
 * Created: Mar 06, 2020
 *
 * @author Odai Nazzal
 */
data class TopSectionScore(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "stage_title")
    val stageTitle: String? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "average")
    val average: Int? = null
)
