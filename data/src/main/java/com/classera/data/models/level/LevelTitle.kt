package com.classera.data.models.level

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class LevelTitle(

    @Json(name = "levels")
    val levels: Level? = null
) : Parcelable
