package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InboxDetailsResponse(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "body")
    val body: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "priority")
    val priority: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "sender_id")
    val senderId: String? = null,

    @Json(name = "semester_id")
    val semesterId: String? = null,

    @Json(name = "has_attachments")
    val hasAttachments: Boolean? = null,

    @Json(name = "read")
    val read: Boolean? = null,

    @Json(name = "first_name")
    val firstName: String? = null,

    @Json(name = "family_name")
    val familyName: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Json(name = "attachments")
    val attachments: List<String>? = listOf()
)
