package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InboxResponse(

    @Json(name = "tota_count")
    val totalCount: Int? = null,

    @Json(name = "unread_count")
    val unreadCount: String? = null,

    @Json(name = "mail")
    val mail: List<Inbox>? = null

)
