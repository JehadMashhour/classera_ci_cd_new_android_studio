package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OutboxDetailsResponse(

    @Json(name = "message")
    val message: List<OutboxMessageObject>? = null

)
