package com.classera.data.models.mailbox

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "recipient_user")
@Parcelize
@JsonClass(generateAdapter = true)
data class RecipientUser(

    @PrimaryKey
    @Json(name = "id")
    val id: String,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "role_id")
    val roleId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    var checked: Boolean = false
) : Parcelable
