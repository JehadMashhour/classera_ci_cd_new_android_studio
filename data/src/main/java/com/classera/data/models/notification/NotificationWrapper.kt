package com.classera.data.models.notification


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NotificationWrapper(

    @Json(name = "notifications")
    val notifications: List<NotificationMessage?>? = listOf(),

    @Json(name = "unseenCount")
    val unseenCount: Int? = 0
)
