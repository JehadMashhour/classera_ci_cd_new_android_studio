package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

class TicketNotificationBody(

    @SerializedName("title")
    val title: String? = null,

    @SerializedName("ticketId")
    val id: String? = null
)
