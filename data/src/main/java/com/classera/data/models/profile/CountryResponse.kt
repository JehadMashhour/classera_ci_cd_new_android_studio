package com.classera.data.models.profile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountryResponse (

    @Json(name = "countries")
    val countries: List<Country>? = null

)
