package com.classera.data.models.profile

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Education(

    @Json(name = "city_name")
    val cityName: String? = null,


    @Json(name = "country_id")
    val countryId: String? = null,

    @Json(name = "contry_name")
    val countryName: String? = null,

    @Json(name = "degree")
    val degree: String? = null,

    @Json(name = "description")
    val description: String? = null,

    @Json(name = "field_of_study")
    val fieldOfStudy: String? = null,

    @Json(name = "from_year")
    val fromYear: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "school")
    val school: String? = null,

    @Json(name = "to_year")
    val toYear: String? = null
) : Parcelable
