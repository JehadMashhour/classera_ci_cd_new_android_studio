package com.classera.data.models.profile

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Interest(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "interest")
    val interest: String? = null,

    @Json(name = "description")
    val description: String? = null
) : Parcelable
