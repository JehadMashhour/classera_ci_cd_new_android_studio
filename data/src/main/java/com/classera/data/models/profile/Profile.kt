package com.classera.data.models.profile


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Profile(

    @Json(name = "badges")
    val badges: List<Badge?>? = null,

    @Json(name = "e-protoflio")
    val ePortfolio: List<EPortfolio?>? = null,

    @Json(name = "educations")
    val educations: List<Education?>? = null,

    @Json(name = "interests")
    val interests: List<Interest?>? = null,

    @Json(name = "languages")
    val languages: List<Language?>? = null,

    @Json(name = "skills")
    val skills: List<Skill?>? = null,

    @Json(name = "work_experience")
    val workExperience: List<WorkExperience?>? = null
) : Parcelable
