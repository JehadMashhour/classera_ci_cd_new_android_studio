package com.classera.data.models.profile


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class WorkExperience(

    @Json(name = "company_name")
    val companyName: String? = null,

    @Json(name = "description")
    val description: String? = null,

    @Json(name = "end_date")
    val endDate: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "job_title")
    val jobTitle: String? = null,

    @Json(name = "Job_type")
    val jobType: String? = null,

    @Json(name = "start_date")
    val startDate: String? = null
) : Parcelable
