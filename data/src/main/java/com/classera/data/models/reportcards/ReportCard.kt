package com.classera.data.models.reportcards

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.koushikdutta.ion.future.ResponseFuture
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import java.io.InputStream

data class ReportCard(

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "type")
    val type: ReportCardType = ReportCardType.UNKNOWN,

    @Transient
    var downloadFuture: ResponseFuture<InputStream>? = null
) : BaseObservable() {

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var progress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.progress)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var state: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
        }
}
