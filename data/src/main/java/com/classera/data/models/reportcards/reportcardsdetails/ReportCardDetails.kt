package com.classera.data.models.reportcards.reportcardsdetails


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReportCardDetails(

    @Json(name = "report_card_url")
    val downloadUrl: String? = null,

    @Json(name = "footer")
    val footer: String? = null,

    @Json(name = "header")
    val header: String? = null,

    @Json(name = "report_title")
    val reportTitle: String? = null
)
