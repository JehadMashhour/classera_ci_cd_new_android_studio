package com.classera.data.models.score

import com.squareup.moshi.Json

/**
 * Project: Classera
 * Created: Dec 21, 2019
 *
 * @author Mohamed Hamdan
 */

private const val GREEN_VALUE = 55
private const val GREEN_PERCENT = 0

private const val SILVER_VALUE = 20
private const val SILVER_PERCENT = 55

private const val GOLDEN_VALUE = 10
private const val GOLDEN_PERCENT = 75

private const val PLATINUM_VALUE = 10
private const val PLATINUM_PERCENT = 85

private const val DIAMOND_VALUE = 5
private const val DIAMOND_PERCENT = 95

enum class ScoreClass(val title: String, val value: Int, val percent: Int) {


    @Json(name = "Green")
    GREEN("Green", GREEN_VALUE, GREEN_PERCENT),

    @Json(name = "خضراء")
    GREEN_AR("Green", GREEN_VALUE, GREEN_PERCENT),

    @Json(name = "Golden")
    GOLDEN("Golden", GOLDEN_VALUE, GOLDEN_PERCENT),

    @Json(name = "ذهبي")
    GOLDEN_ARA("Golden", GOLDEN_VALUE, GOLDEN_PERCENT),

    @Json(name = "ماسي")
    DIAMOND_ARA("Diamond", DIAMOND_VALUE, DIAMOND_PERCENT),

    @Json(name = "Diamond")
    DIAMOND("Diamond", DIAMOND_VALUE, DIAMOND_PERCENT);
}
