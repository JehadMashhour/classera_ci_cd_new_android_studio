package com.classera.data.models.semester

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Semester(

    @Json(name = "year_id")
    val yearId: String? = null,

    @Json(name = "semester_id")
    val semesterId: String? = null
) : Parcelable
