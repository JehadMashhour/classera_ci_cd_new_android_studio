package com.classera.data.models.support

import android.os.Parcelable
import com.classera.data.R
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class SupportWrapper(

    @Json(name = "avatar")
    val avatar: String? = null,

    @Json(name = "body")
    val body: String? = null,

    @Json(name = "comments_count")
    val commentsCount: String? = null,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @Json(name = "creator_name")
    val creatorName: String? = null,

    @Json(name = "priority_title")
    val priorityTitle: String? = null,

    @Json(name = "status_id")
    val statusId: String? = null,

    @Json(name = "status_title")
    val statusTitle: String? = null,

    @Json(name = "ticket_created_date")
    val ticketCreatedDate: String? = null,

    @Json(name = "ticket_id")
    val ticketId: String? = null,

    @Json(name = "team_title")
    val teamTitle: String? = null,

    @Json(name = "module_title")
    val moduleTitle: String? = null,

    @Json(name = "problem_link")
    val problemLink: String? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "ticket_title")
    val ticketTitle: String? = null,

    @Json(name = "type_title")
    val typeTitle: String? = null,

    @Json(name = "attachments")
    val attachments: List<String>? = listOf()
) : Parcelable {

    fun getStatusColor(): Int {
        return when (statusId) {
            "1" -> {
                R.color.label_support_fragment_open_color
            }
            "2" -> {
                R.color.label_support_fragment_in_progress_color
            }
            "3" -> {
                R.color.label_support_fragment_pending_client_reply_color
            }
            "4" -> {
                R.color.label_support_fragment_closed_color
            }
            "6" -> {
                R.color.label_support_fragment_pending_client_reply_color
            }
            "7" -> {
                R.color.label_support_fragment_pending_client_reply_color
            }
            else -> {
                R.color.label_support_fragment_closed_color
            }
        }
    }
}
