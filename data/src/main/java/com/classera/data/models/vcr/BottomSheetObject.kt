package com.classera.data.models.vcr

import android.os.Parcelable
import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize


@Parcelize
@JsonClass(generateAdapter = true)
data class BottomSheetObject(
    @Json(name = "id")
    override val id: String? = null,

    @Json(name = "title")
    override val title: String? = null,

    @Transient
    override var selected: Boolean = false

) : Selectable
