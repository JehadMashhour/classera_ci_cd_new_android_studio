package com.classera.data.models.vcr

import com.classera.data.moshi.enums.Json

@Suppress("MagicNumber")
enum class VendorTypes(val id: Int) {
    @Json(name = "1")
    WIZIQ(1),

    @Json(name = "2")
    ZOOM_MEETINGS(2),

    @Json(name = "3")
    WEBEX(3),

    @Json(name = "6")
    MICROSOFT_TEAMS(6),

    @Json(name = "7")
    ZOOM_VIRTUAL_CLASS(7),

    UNKNOWN(-1);
}
