package com.classera.data.models.vcr.lecturetimeslotpreparation


import com.squareup.moshi.Json

data class LectureTimeslotPreparation(

	@Json(name="preparations")
	val preparations: List<Preparation>? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="id")
	val id: String? = null,

	@Json(name="timeslots")
	val timeslots: List<Timeslot>? = null
)
