package com.classera.data.models.vcr.vendor

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class Settings(

    @Json(name = "allow_student_start_vcr")
    var allowStudentStartVcr: Boolean? = true,

    @Json(name = "recurring")
    var recurring: Boolean? = false,

    @Json(name = "weekly_repeated")
    var weeklyRepeated: Boolean? = false,

    @Json(name = "show_zoom_link")
    var showZoomLink: Boolean? = false,

    @Json(name = "durations")
    var durations: List<Int>? = null,

    @Json(name = "show_passcode")
    var showPasscode: Boolean? = false,

    @Json(name = "show_external_link")
    var showExternalLink: Boolean? = false
) : Parcelable

