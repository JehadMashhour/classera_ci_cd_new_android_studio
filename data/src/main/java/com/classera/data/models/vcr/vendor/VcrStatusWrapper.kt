package com.classera.data.models.vcr.vendor

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class VcrStatusWrapper(

    @Json(name = "can_proceed")
    val canProceed: Boolean? = null,

    @Json(name = "message")
    val message: String? = null,

    @Json(name = "settings")
    val settings: Settings? = null


) : Parcelable
