package com.classera.data.models.vcr.vendor

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Vendor(

    @Json(name = "color")
    val color: String? = null,

    @Json(name = "name")
    val name: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "setup")
    var vcrStatusWrapper: VcrStatusWrapper? = null

) : Parcelable
