package com.classera.data.models.weeklyplan

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class WeeklyPlanPreparationWrapper(

    @Json(name = "PreparationsStudentsView")
    val preparation: WeeklyPlanPreparation
)
