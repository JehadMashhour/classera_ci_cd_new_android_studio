package com.classera.data.moshi.enums

/**
 * Project: Classera
 * Created: Jan 01, 2020
 *
 * @author Mohamed Hamdan
 */
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Json(val name: String, val alternatives: Array<String> = [])
