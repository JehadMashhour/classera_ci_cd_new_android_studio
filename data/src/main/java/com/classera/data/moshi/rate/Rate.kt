package com.classera.data.moshi.rate

import com.squareup.moshi.JsonQualifier

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class Rate
