package com.classera.data.moshi.shortday

import com.classera.data.toDate
import com.classera.data.toString
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class ShortDayNameAdapter {

    @ToJson
    fun toJson(@ShortDayName value: String?): String? {
        return value?.toDate("EEE", Locale.getDefault()).toString("EEEE", Locale.ENGLISH)
    }

    @FromJson
    @ShortDayName
    fun fromJson(value: String?): String? {
        return value?.toDate("EEEE", Locale.ENGLISH).toString("EEE", Locale.getDefault())
    }

}
