package com.classera.data.moshi.toStringNumber

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.DecimalFormat
import kotlin.math.floor
import kotlin.math.log10
import kotlin.math.pow

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */

@Suppress("MagicNumber", "ReturnCount")
class ToStringNumberAdapter {

    @ToJson
    fun toJson(@ToStringNumber value: String): String? {
        return value
    }

    @FromJson
    @ToStringNumber
    fun fromJson(value: String?): String? {
        value?.toLong().let {value->
            return prettyCount(value?:0)
        }
    }

    private fun prettyCount(number: Long): String {
        val suffix = charArrayOf(' ', 'k', 'M', 'B', 'T', 'P', 'E')
        val value = floor(log10(number.toDouble())).toInt()
        val base = value / 3
        return if (value >= 3 && base < suffix.size) {
            DecimalFormat("0.#").format(number / 10.0.pow((base * 3).toDouble())) + suffix[base]
        } else {
            DecimalFormat("#,##0").format(number)
        }
    }

}
