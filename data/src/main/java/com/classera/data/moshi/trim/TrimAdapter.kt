package com.classera.data.moshi.trim

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class TrimAdapter {

    @ToJson
    fun toJson(@Trim value: String?): String? {
        return value
    }

    @FromJson
    @Trim
    fun fromJson(value: String?): String? {
        return value?.trim()
    }
}
