package com.classera.data.network.intercepters

import com.classera.data.network.IncludeChildId
import com.classera.data.prefs.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class ChildIdnInterceptor @Inject constructor(private val prefs: Prefs) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (prefs.childId!=null && method?.getAnnotation(IncludeChildId::class.java) != null) {
            var url = request.url().toString()
            url += if (url.contains("?")) "&" else "?"
            url += "user_id=${prefs.childId}"
            request = request.newBuilder().url(url).build()
        }
        return chain.proceed(request)
    }
}
