package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import com.classera.data.network.IgnoreAppType
import com.classera.data.network.IncludeOnlineNowHeaders
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Created by Saeed Halawani on 17/1/2019.
 * Classera
 * s.halawani@classera.com
 */
class OnlineNowInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IncludeOnlineNowHeaders::class.java) == null) {
            request = request.newBuilder()
                .addHeader(ONLINE_NOW_HEADER_ACCEPT, ONLINE_NOW_HEADER_ACCEPT_VALUE)
                .addHeader(ONLINE_NOW_HEADER_REFERER, BuildConfig.flavorData.webBaseUrl)
                .addHeader(ONLINE_NOW_HEADER_ORIGIN, BuildConfig.flavorData.webBaseUrl)
                .addHeader(ONLINE_NOW_HEADER_ACCESS_TOKEN, ONLINE_NOW_HEADER_ACCESS_TOKEN_VALUE)
                .addHeader(ONLINE_NOW_HEADER_USER_AGENT, ONLINE_NOW_HEADER_USER_AGENT_VALUE)
                .addHeader(ONLINE_NOW_HEADER_CONTENT_TYPE, ONLINE_NOW_HEADER_CONTENT_TYPE_VALUE)
                .build()
        }
        return chain.proceed(request)
    }

    private companion object {
        private const val ONLINE_NOW_HEADER_ACCEPT = "Accept"
        private const val ONLINE_NOW_HEADER_ACCEPT_VALUE = "application/json," +
                " text/javascript, /; q=0.01"
        private const val ONLINE_NOW_HEADER_REFERER = "Referer"
        private const val ONLINE_NOW_HEADER_ORIGIN = "Origin"
        private const val ONLINE_NOW_HEADER_ACCESS_TOKEN = "AccessToken"
        private const val ONLINE_NOW_HEADER_ACCESS_TOKEN_VALUE = "Q2xhc3NlcmFMb2dT"
        private const val ONLINE_NOW_HEADER_USER_AGENT = "User-Agent"
        private const val ONLINE_NOW_HEADER_USER_AGENT_VALUE =
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 " +
                    "(KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36"
        private const val ONLINE_NOW_HEADER_CONTENT_TYPE = "Content-Type"
        private const val ONLINE_NOW_HEADER_CONTENT_TYPE_VALUE =
            "application/x-www-form-urlencoded; charset=UTF-8"
    }
}
