package com.classera.data.network.intercepters

import com.classera.data.network.Pagination
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class PaginationInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(Pagination::class.java) != null) {
            var url = request.url().toString()
            url += if (url.contains("?")) "&" else "?"
            url += "r=$DEFAULT_LIMIT"

            request = request.newBuilder().url(url).build()
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val DEFAULT_LIMIT = 10
    }
}
