package com.classera.data.network.intercepters

import com.classera.data.network.CustomTimeout
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Project: Classera
 * Created: August 18, 2020
 *
 * @author Khaled Mohammad
 */
class TimeoutInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()
        val annotation = method?.getAnnotation(CustomTimeout::class.java)

        if (annotation != null) {
            return chain.withReadTimeout(annotation.timeout, annotation.unit)
                .withWriteTimeout(annotation.timeout, annotation.unit)
                .withConnectTimeout(annotation.timeout, annotation.unit)
                .proceed(request)
        }

        return chain.proceed(request)
    }
}
