package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * Created by Odai Nazzal on 12/17/2019.
 * Classera
 * o.nazzal@classera.com
 */
class W3WApiKeyInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader(W3W_API_KEY_HEADER_NAME, BuildConfig.flavorData.w3wApiKey)
            .build()
        return chain.proceed(request)
    }

    private companion object {

        private const val W3W_API_KEY_HEADER_NAME = "X-Api-Key"
    }
}
