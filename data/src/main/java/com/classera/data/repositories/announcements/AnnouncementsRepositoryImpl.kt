package com.classera.data.repositories.announcements

import com.classera.data.daos.announcements.RemoteAnnouncementsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.announcements.Announcement
import com.classera.data.models.announcements.AnnouncementsWrapper

/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AnnouncementsRepositoryImpl(
    private val remoteAnnouncementsDao: RemoteAnnouncementsDao
) : AnnouncementsRepository {

    override suspend fun getAnnouncementsList(
        text: CharSequence?,
        pageNumber: Int
    ): BaseWrapper<AnnouncementsWrapper> {
        val fields = mapOf(
            "p" to pageNumber,
            "search" to (text ?: "")
        )
        return remoteAnnouncementsDao.getAnnouncementsList(fields)
    }

    override suspend fun getAnnouncementDetails(announcementId: String?): BaseWrapper<Announcement> {
        return remoteAnnouncementsDao.getAnnouncementDetails(announcementId)
    }
}
