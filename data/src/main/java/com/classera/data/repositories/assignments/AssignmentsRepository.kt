package com.classera.data.repositories.assignments

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentsResponse
import com.classera.data.models.assignments.Preparation
import com.classera.data.models.assignments.PublishUser
import com.classera.data.models.assignments.AssignmentDetails
import com.classera.data.models.assignments.QuestionsWrapper
import com.classera.data.models.assignments.listquestions.Question
import com.classera.data.models.assignments.QuestionDraft
import com.classera.data.models.assignments.UserLastQuestion
import kotlinx.coroutines.flow.Flow

/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
interface AssignmentsRepository {

    suspend fun getAssignmentsList(
        filterKey: String? = null,
        text: CharSequence? = null,
        page: Int,
        courseId: String?
    ): BaseWrapper<AssignmentsResponse>


    suspend fun getTeacherAssignmentsList(
        filterKey: String? = null,
        text: CharSequence? = null,
        page: Int,
        courseId: String?
    ): BaseWrapper<AssignmentsResponse>

    suspend fun getAssignmentDetails(assignment: Assignment?): BaseWrapper<Assignment>

    suspend fun getQuestions(assignment: Assignment?): BaseWrapper<QuestionsWrapper>

    suspend fun createEssayQuestion(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark: String?,
        difficulty: String?,
        correctAnswer: String?
    ): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun addTrueFalseQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: Int?
    ): NoContentBaseWrapper

    suspend fun getQuestionList(assignmentId: String?): BaseWrapper<List<Question>>

    suspend fun saveAnswers(map: Map<String, Any?>?): NoContentBaseWrapper

    suspend fun saveAsDraft(data: Map<String, Any?>?): NoContentBaseWrapper

    suspend fun submitAnswers(data: Map<String, Any?>?): NoContentBaseWrapper

    @Suppress("LongParameterList")
    suspend fun addMultipleChoiceQuestion(
        assignmentId: String?, type: String?, text: String?,
        mark: String?, difficulty: String?, correctAnswer: String?,
        correctAnswerResponse: String?, wrongAnswerResponse: String?,
        questionHint: String?, order: String?, questionId: String?, answerOne: String?,
        answerTwo: String?, answerThree: String?, answerFour: String?, answerFive: String?,
        answerSix: String?
    ): NoContentBaseWrapper

    suspend fun deleteAssignment(assignmentID: String?): NoContentBaseWrapper

    suspend fun getPublishUser(courseId: String?): BaseWrapper<PublishUser>

    suspend fun getPreparation(courseId: String?): BaseWrapper<List<Preparation>>

    suspend fun addAssignment(fields: Map<String, Any?>): NoContentBaseWrapper

    suspend fun getDraft(assignmentId: String?, submissionId : String?): BaseWrapper<List<QuestionDraft>>

    suspend fun getTeacherAssignmentDetails(assignmentId: String?): BaseWrapper<AssignmentDetails>

    suspend fun editTeacherAssignment(fields: Map<String, Any?>): NoContentBaseWrapper

    suspend fun getLastQuestion(id : String): Flow<UserLastQuestion?>

    suspend fun insertLastQuestion( userLastQuestion: UserLastQuestion)

    suspend fun deleteLastQuestion(id : String)



    suspend fun recallAssignment(assignmentID: String?): NoContentBaseWrapper
}
