package com.classera.data.repositories.assignments

import com.classera.data.daos.assignments.LocalLastQuestionDao
import com.classera.data.daos.assignments.RemoteAssignmentsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentDetails
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.assignments.AssignmentsResponse
import com.classera.data.models.assignments.Preparation
import com.classera.data.models.assignments.PublishUser
import com.classera.data.models.assignments.QuestionDraft
import com.classera.data.models.assignments.QuestionsWrapper
import com.classera.data.models.assignments.UserLastQuestion
import com.classera.data.models.assignments.listquestions.Question
import kotlinx.coroutines.flow.Flow

/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AssignmentsRepositoryImpl(
    private val remoteAssignmentsDao: RemoteAssignmentsDao,
    private val localLastQuestionDao: LocalLastQuestionDao
) :
    AssignmentsRepository {

    override suspend fun createEssayQuestion(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark:String?,
        difficulty: String?,
        correctAnswer: String?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "assignment_id" to assignmentId,
            "type" to type,
            "text" to text,
            "mark" to mark,
            "difficulty" to difficulty,
            "correct_answer[0]" to correctAnswer
        )
        return remoteAssignmentsDao.createEssayQuestion(fields)
    }

    override suspend fun getAssignmentsList(
        filterKey: String?,
        text: CharSequence?,
        page: Int,
        courseId: String?
    ): BaseWrapper<AssignmentsResponse> {
        return remoteAssignmentsDao.getAssignments(filterKey, page, text, courseId)
    }

    override suspend fun getTeacherAssignmentsList(
        filterKey: String?,
        text: CharSequence?,
        page: Int,
        courseId: String?
    ): BaseWrapper<AssignmentsResponse> {
        return remoteAssignmentsDao.getTeacherAssignments(filterKey, page, text, courseId)
    }

    override suspend fun getAssignmentDetails(assignment: Assignment?): BaseWrapper<Assignment> {
        val query = mapOf(
            "assignment_id" to assignment?.id,
            "course_id" to assignment?.courseId,
            "type" to assignment?.type,
            "assignment_type" to assignment?.type
        )
        return if (assignment?.status == AssignmentStatus.LAUNCH) {
            remoteAssignmentsDao.getAssignmentInfo(query)
        } else {
            remoteAssignmentsDao.getAssignmentDetails(query)
        }
    }

    override suspend fun getQuestions(assignment: Assignment?): BaseWrapper<QuestionsWrapper> {
        return remoteAssignmentsDao.getQuestions(assignment?.id, assignment?.courseId)
    }

    override suspend fun addTrueFalseQuestion(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark: String?,
        difficulty: String?,
        correctAnswer: Int?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "assignment_id" to assignmentId,
            "type" to type,
            "text" to text,
            "mark" to mark,
            "difficulty" to difficulty,
            "correct_answer" to correctAnswer
        )
        return remoteAssignmentsDao.addTrueFalseQuestion(fields)
    }

    override suspend fun getQuestionList(assignmentId: String?): BaseWrapper<List<Question>> {
        val query = mapOf(
            "assignment_id" to assignmentId
        )
        return remoteAssignmentsDao.getQuestionList(query)
    }

    override suspend fun saveAnswers(map: Map<String, Any?>?): NoContentBaseWrapper {
        return remoteAssignmentsDao.saveAnswers(map)
    }

    override suspend fun saveAsDraft(data: Map<String, Any?>?): NoContentBaseWrapper {
        return remoteAssignmentsDao.saveAsDraft(data)
    }

    override suspend fun submitAnswers(data: Map<String, Any?>?): NoContentBaseWrapper {
        return remoteAssignmentsDao.submitAnswers(data)
    }


    override suspend fun addMultipleChoiceQuestion(
        assignmentId: String?,
        type: String?,
        text: String?,
        mark: String?,
        difficulty: String?,
        correctAnswer: String?,
        correctAnswerResponse: String?,
        wrongAnswerResponse: String?,
        questionHint: String?,
        order: String?,
        questionId: String?,
        answerOne: String?,
        answerTwo: String?,
        answerThree: String?,
        answerFour: String?,
        answerFive: String?,
        answerSix: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "assignment_id" to assignmentId,
            "type" to type,
            "text" to text,
            "mark" to mark,
            "difficulty" to difficulty,
            "correct_answer" to correctAnswer,
            "order" to order,
            "question_id" to questionId,
            "correct_answer_response" to correctAnswerResponse,
            "wrong_answer_response" to wrongAnswerResponse,
            "question_hint" to questionHint,
            "answer_1" to answerOne,
            "answer_2" to answerTwo,
            "answer_3" to answerThree,
            "answer_4" to answerFour,
            "answer_5" to answerFive,
            "answer_6" to answerSix
        )
        return remoteAssignmentsDao.addMultipleChoiceQuestion(fields)
    }

    override suspend fun deleteAssignment(assignmentID: String?): NoContentBaseWrapper {
        return remoteAssignmentsDao.deleteAssignment(assignmentID)
    }

    override suspend fun getPublishUser(courseId: String?): BaseWrapper<PublishUser> {
        return remoteAssignmentsDao.getPublishUser(courseId)
    }

    override suspend fun getPreparation(courseId: String?): BaseWrapper<List<Preparation>> {
        return remoteAssignmentsDao.getPreparation(courseId)
    }

    override suspend fun addAssignment(fields: Map<String, Any?>): NoContentBaseWrapper {
        return remoteAssignmentsDao.addAssignment(fields)
    }

    override suspend fun getDraft(assignmentId: String?, submissionId: String?): BaseWrapper<List<QuestionDraft>> {
        val fields = mapOf(
            "assignment_id" to assignmentId,
            "submission_id" to submissionId
        )
        return remoteAssignmentsDao.getDraft(fields)
    }


    override suspend fun getTeacherAssignmentDetails(assignmentId: String?): BaseWrapper<AssignmentDetails> {
        return remoteAssignmentsDao.getTeacherAssignmentDetails(assignmentId)
    }

    override suspend fun editTeacherAssignment(fields: Map<String, Any?>): NoContentBaseWrapper {
        return remoteAssignmentsDao.editTeacherAssignment(fields)
    }

    override suspend fun getLastQuestion(id: String): Flow<UserLastQuestion?> {
        return localLastQuestionDao.getLastQuestion(id)
    }

    override suspend fun insertLastQuestion( userLastQuestion: UserLastQuestion) {
        return localLastQuestionDao.insertLastQuestion(userLastQuestion)
    }

    override suspend fun deleteLastQuestion(id : String) {
        return localLastQuestionDao.deleteLastQuestion(id)
    }

    override suspend fun recallAssignment(assignmentID: String?): NoContentBaseWrapper {
        return remoteAssignmentsDao.recallAssignment(assignmentID)
    }
}
