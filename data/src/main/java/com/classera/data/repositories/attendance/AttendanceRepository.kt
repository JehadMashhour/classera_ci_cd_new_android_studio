package com.classera.data.repositories.attendance

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attendance.AbsenceSection
import com.classera.data.models.attendance.Absences
import com.classera.data.models.attendance.AttendanceTimeSlot
import com.classera.data.models.courses.Course
import com.classera.data.models.user.User
import okhttp3.MultipartBody

/**
 * Created by Rawan Al-Theeb on 12/24/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
interface AttendanceRepository {

    suspend fun getAbsences(quickFilterKey: String? = null, page: Int, filterKey: String?): BaseWrapper<List<Absences>>

    suspend fun getCourses(): BaseWrapper<List<Course>>

    suspend fun getSections(courseId: String? = null): BaseWrapper<List<AbsenceSection>>

    suspend fun getStudents(
        sectionId: String? = null,
        courseId: String?,
        lectureId: String?,
        timeSlotId: String?,
        date: String?,
        page: Int
    ): BaseWrapper<List<User>>

    suspend fun getSectionTimeSlots(lectureId: String?): BaseWrapper<List<AttendanceTimeSlot>>

    suspend fun getAllTimeSlots(): BaseWrapper<List<AttendanceTimeSlot>>

    suspend fun createNewTimeSlot(lectureId: String?, timeSlotId: String?): NoContentBaseWrapper

    suspend fun changeStatusForStudent(body: Map<String, Any?>): NoContentBaseWrapper

    suspend fun setLeaveWithPermission(parts: List<MultipartBody.Part>) : NoContentBaseWrapper

    suspend fun takeAttendanceByFace(parts: List<MultipartBody.Part>): NoContentBaseWrapper
}
