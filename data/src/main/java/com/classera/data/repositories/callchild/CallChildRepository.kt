package com.classera.data.repositories.callchild

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.callchildren.*

interface CallChildRepository {

    suspend fun getGuardianChildStudents(): BaseWrapper<List<ChildStudent>>

    suspend fun callChild(userId: String?): BaseWrapper<Request>

    suspend fun changeCallStatus(requestId: String?, status: String?): NoContentBaseWrapper

    suspend fun getPersonalDriverStudents(): BaseWrapper<List<DriverStudent>>

    suspend fun getPersonalDrivers(): BaseWrapper<List<Driver>>

    suspend fun getCallStatus(requestId: String?): BaseWrapper<CallStatus>
}
