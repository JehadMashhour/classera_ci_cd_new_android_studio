package com.classera.data.repositories.chat

import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Block
import com.classera.data.models.user.User

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
interface ChatBlockedUsersRepository {

    suspend fun getBlockedUsers(): BaseWrapper<List<User>>

    suspend fun checkBlockedUser(blockUserID: String?): BaseWrapper<Block>

    suspend fun toggleUserBlockStatus(userBlockID: String?): BaseWrapper<Block>

}
