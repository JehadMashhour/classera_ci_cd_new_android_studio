package com.classera.data.repositories.chat

import com.classera.data.daos.chat.RemoteChatDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Block
import com.classera.data.models.user.User

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatBlockedUsersRepositoryImpl(
    private val remoteChatDao: RemoteChatDao) : ChatBlockedUsersRepository {

    override suspend fun getBlockedUsers(): BaseWrapper<List<User>> {
        return remoteChatDao.getBlockedUsers()
    }

    override suspend fun checkBlockedUser(blockUserID: String?): BaseWrapper<Block> {
        return remoteChatDao.checkBlockedUser(blockUserID)
    }

    override suspend fun toggleUserBlockStatus(userBlockID: String?): BaseWrapper<Block> {
        val fields = mutableMapOf(
            "user_block_id" to userBlockID
        )
        return remoteChatDao.toggleUserBlockStatus(fields)
    }

}
