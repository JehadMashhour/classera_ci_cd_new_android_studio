package com.classera.data.repositories.chat

import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.Thread
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.user.User

/**
 * Created by Odai Nazzal on 12/19/2019, Modified by Lana Manaseer
 * Classera
 * o.nazzal@classera.com
 */
interface ChatRepository {

    suspend fun getThreads(
        limit: String, pagination: String? = "",
        searchName: CharSequence? = "", roleID: String? = ""
    ): BasePaginationWrapper<List<Thread>>

    suspend fun getGroupThreads(
        limit: String, pagination: String? = "",
        searchName: CharSequence? = "", roleID: String? = ""
    ): BasePaginationWrapper<List<ThreadUser>>

    suspend fun getFilteredThreads(
        limit: String, pagination: String? = "",
        searchName: CharSequence? = "", roleID: String? = ""
    ): BasePaginationWrapper<List<Thread>>

    suspend fun getFilteredThreadsWithoutGroups(
        limit: String, pagination: String? = "",
        searchName: CharSequence? = "", roleID: String? = ""
    ): BasePaginationWrapper<List<Thread>>

    suspend fun getRoles(): BaseWrapper<List<Role>>

    suspend fun getListGroupParticipants(threadID: String?): BaseWrapper<List<ThreadUser>>

    suspend fun getUsersBasicDetails(userIDs: List<String?>?): BaseWrapper<List<User>>

}
