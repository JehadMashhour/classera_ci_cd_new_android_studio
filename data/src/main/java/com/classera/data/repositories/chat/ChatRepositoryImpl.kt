package com.classera.data.repositories.chat

import com.classera.data.daos.chat.RemoteChatDao
import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.Thread
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.user.User
import com.classera.data.prefs.Prefs

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan, Modified by Lana Manaseer
 */
class ChatRepositoryImpl(
    private val remoteChatDao: RemoteChatDao,
    private val prefs: Prefs
) : ChatRepository {

    override suspend fun getThreads(
        limit: String, pagination: String?,
        searchName: CharSequence?, roleID: String?
    ): BasePaginationWrapper<List<Thread>> {
        val fields = mutableMapOf(
            "school_id" to prefs.schoolId,
            "limit" to limit.toInt(),
            "search[name]" to searchName,
            "search[role_id]" to roleID
        )

        if (pagination?.isEmpty() == false) {
            fields["pagination"] = pagination
        }

        return remoteChatDao.getThreads(fields)
    }

    override suspend fun getGroupThreads(
        limit: String,
        pagination: String?,
        searchName: CharSequence?,
        roleID: String?
    ): BasePaginationWrapper<List<ThreadUser>> {
        val fields = mutableMapOf(
            "school_id" to prefs.schoolId,
            "limit" to limit.toInt(),
            "search[name]" to searchName,
            "search[role_id]" to roleID
        )

        if (pagination?.isEmpty() == false) {
            fields["pagination"] = pagination
        }

        val threads = remoteChatDao.getGroupThreads(fields)

        val data = (threads.data ?: listOf()).filter { it.group == false }

        return threads.copy(data = data)
    }

    override suspend fun getFilteredThreads(
        limit: String,
        pagination: String?,
        searchName: CharSequence?,
        roleID: String?
    ): BasePaginationWrapper<List<Thread>> {
        val fields = mutableMapOf(
            "school_id" to prefs.schoolId,
            "limit" to limit.toInt(),
            "search[name]" to searchName,
            "search[role_id]" to roleID/*,
            "search[active_threads]" to true*/
        )

        if (pagination?.isEmpty() == false) {
            fields["pagination"] = pagination
        }

        val threads = remoteChatDao.getThreads(fields)

        val data = (threads.data ?: listOf()).filter { it.lastMessage != null || it.group == true }

        return threads.copy(data = data)
    }

    override suspend fun getFilteredThreadsWithoutGroups(
        limit: String,
        pagination: String?,
        searchName: CharSequence?,
        roleID: String?
    ): BasePaginationWrapper<List<Thread>> {
        val fields = mutableMapOf(
            "school_id" to prefs.schoolId,
            "limit" to limit.toInt(),
            "search[name]" to searchName,
            "search[role_id]" to roleID
        )

        if (pagination?.isEmpty() == false) {
            fields["pagination"] = pagination
        }

        val threads = remoteChatDao.getThreads(fields)

        val data = (threads.data ?: listOf()).filter { it.group == false }

        return threads.copy(data = data)
    }

    override suspend fun getRoles(): BaseWrapper<List<Role>> {
        return remoteChatDao.getRoles()
    }

    override suspend fun getListGroupParticipants(threadID: String?): BaseWrapper<List<ThreadUser>> {
        return remoteChatDao.getListGroupParticipants(threadID)
    }

    override suspend fun getUsersBasicDetails(userIDs: List<String?>?): BaseWrapper<List<User>> {
        val fields = mutableMapOf<String, @JvmSuppressWildcards Any?>()
        userIDs?.forEachIndexed { index, user ->
            fields["users_ids[$index]"] = user
        }

        return remoteChatDao.getUsersBasicDetails(fields)
    }

}
