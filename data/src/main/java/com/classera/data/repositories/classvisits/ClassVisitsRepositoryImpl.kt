package com.classera.data.repositories.classvisits

import com.classera.data.daos.classvisits.RemoteClassVisitsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.classvisits.Assessment
import com.classera.data.models.classvisits.ClassVisit
import com.classera.data.models.classvisits.Teacher
import com.classera.data.models.classvisits.Timeslot


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
class ClassVisitsRepositoryImpl(private val remoteClassVisitsDao: RemoteClassVisitsDao) : ClassVisitsRepository {

    override suspend fun classVisits(): BaseWrapper<List<ClassVisit>> {
        return remoteClassVisitsDao.classVisits()
    }

    override suspend fun getTeachers(): BaseWrapper<List<Teacher>> {
        return remoteClassVisitsDao.getTeachers()
    }

    override suspend fun getAssessments(): BaseWrapper<List<Assessment>> {
        return remoteClassVisitsDao.getAssessments()
    }

    override suspend fun getTimeslots(
        date: String?,
        teacherId: String?
    ): BaseWrapper<List<Timeslot>> {
        val fields = mapOf(
            "date" to date,
            "teacher_id" to teacherId
        )
        return remoteClassVisitsDao.getTimeslots(fields)
    }

    override suspend fun addClassVisit(
        date: String?,
        teacherId: String?,
        assessmentId: String?,
        privateVisit: String?,
        timeslotTitle: String?,
        timeslotId: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "date" to date,
            "teacher_id" to teacherId,
            "assessment_id" to assessmentId,
            "private_visit" to privateVisit,
            "lectures_timeslot_title" to timeslotTitle,
            "lectures_timeslot_id" to timeslotId
        )
        return remoteClassVisitsDao.addClassVisit(fields)
    }
}
