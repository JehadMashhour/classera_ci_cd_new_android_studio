package com.classera.data.repositories.courses

import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.PreparationFilterWrapper
import com.classera.data.models.courses.Course
import com.classera.data.models.courses.CourseDetailsWrapper


/**
 * Created by Odai Nazzal on 1/7/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface CoursesRepository {

    suspend fun getCourses(): BaseWrapper<List<Course>>

    suspend fun getCourseDetails(
        courseId: String?,
        teacherId: String?,
        preparationId: String?
    ): BaseWrapper<CourseDetailsWrapper>

    suspend fun getPreparation(courseId: String?): BaseWrapper<List<PreparationFilterWrapper>>
}
