package com.classera.data.repositories.digitallibrary

import com.classera.data.daos.digitallibrary.RemoteDigitalLibraryDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.digitallibrary.Attachment

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class DigitalLibraryRepositoryImpl(
    private val remoteDigitalLibraryDao: RemoteDigitalLibraryDao
) : DigitalLibraryRepository {

    override suspend fun getStudentLibrary(
        pageNumber: Int,
        filter: String?,
        text: CharSequence?
    ): BaseWrapper<List<Attachment>> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to (text ?: ""),
            "sort" to ("id"),
            "filter" to (filter ?: "")
        )

        return remoteDigitalLibraryDao.getStudentLibrary(fields)
    }

    override suspend fun getStudentAttachments(
        pageNumber: Int,
        filter: String,
        text: CharSequence?,
        courseId: String?
    ): BaseWrapper<List<Attachment>> {
        val fields = mutableMapOf(
            "p" to pageNumber,
            "title" to (text ?: ""),
            "type" to filter
        )
        if (!courseId.isNullOrEmpty()) {
            fields["course_id"] = courseId
        }
        return remoteDigitalLibraryDao.getStudentAttachments(fields)
    }
}
