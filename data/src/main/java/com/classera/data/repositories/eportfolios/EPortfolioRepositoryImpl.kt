package com.classera.data.repositories.eportfolios

import com.classera.data.daos.eportfolios.RemoteEPortfolioDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.eportfolio.EPortfolioWraper
import com.classera.data.models.eportfolio.add.SharingLevelWrapper
import com.classera.data.models.eportfolio.add.TypeWrapper
import com.classera.data.models.eportfolio.details.EPortfolioComment
import com.classera.data.models.eportfolio.details.EPortfolioDetailsWraper
import okhttp3.MultipartBody

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioRepositoryImpl(private var remoteEportfolioDao: RemoteEPortfolioDao) : EPortfolioRepository {

    override suspend fun deleteEport(
        ePortfolioId: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "post_id" to ePortfolioId
        )
        return remoteEportfolioDao.deleteEPortfolio(fields)
    }

    override suspend fun getStudentEPortfolios(pageNumber: Int): BaseWrapper<List<EPortfolioWraper>> {
        val fields = mutableMapOf("p" to pageNumber)
        return remoteEportfolioDao.getStudentEPortfolios(fields)
    }

    override suspend fun getStudentEPortfolioDetails(ePortfolioId: String?): BaseWrapper<EPortfolioDetailsWraper> {
        return remoteEportfolioDao.getStudentEPortfolioDetails(ePortfolioId)
    }

    override suspend fun getEPortfolioComments(ePortfolioId: String?, page: Int): BaseWrapper<List<EPortfolioComment>> {
        val fields = mapOf(
            "post_id" to ePortfolioId,
            "p" to page
        )
        return remoteEportfolioDao.getEPortfolioComments(fields)

    }

    override suspend fun addEPortfolioComment(
        ePortfolioId: String?,
        text: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "post_id" to ePortfolioId,
            "comment" to text
        )
        return remoteEportfolioDao.addEPortfolioComment(fields)
    }

    override suspend fun getType(): BaseWrapper<List<TypeWrapper>> {
        return remoteEportfolioDao.getType()
    }

    override suspend fun getSharingLevel(): BaseWrapper<List<SharingLevelWrapper>> {
        return remoteEportfolioDao.getSharingLevel()
    }

    override suspend fun uploadAttachment(parts: List<MultipartBody.Part>) {
        return remoteEportfolioDao.uploadAttachment(parts)
    }
}
