package com.classera.data.repositories.home

import com.classera.data.daos.home.RemoteHomeDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.StudentHomeResponse
import com.classera.data.models.home.TeacherHomeResponse
import com.classera.data.models.home.admin.DayAbsences
import com.classera.data.models.home.admin.SmsUsage
import com.classera.data.models.home.admin.Statistic
import com.classera.data.models.home.admin.TopScoresSchool
import com.classera.data.models.home.admin.TopScoresSchoolGroup
import com.classera.data.models.home.admin.TopSectionScore

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
class HomeRepositoryImpl(private val remoteHomeDao: RemoteHomeDao) : HomeRepository {

    override suspend fun getStudentHome(): BaseWrapper<StudentHomeResponse> {
        return remoteHomeDao.getStudentHome()
    }

    override suspend fun getTeacherHome(): BaseWrapper<TeacherHomeResponse> {
        return remoteHomeDao.getTeacherHome()
    }

    override suspend fun getContentStatistics(): BaseWrapper<List<Statistic>> {
        return remoteHomeDao.getContentStatistics()
    }

    override suspend fun getDashboardTotalAbsences(): BaseWrapper<List<DayAbsences>> {
        return remoteHomeDao.getDashboardTotalAbsences()
    }

    override suspend fun getTopSectionsScores(schoolId: String?): BaseWrapper<List<TopSectionScore>> {
        val query = mapOf(
            "school_id" to schoolId
        )
        return remoteHomeDao.getTopSectionsScores(query)
    }

    override suspend fun getTopScoresSchool(roleId: String?): BaseWrapper<List<TopScoresSchool>> {
        val query = mapOf(
            "role_id" to roleId
        )
        return remoteHomeDao.getTopScoresSchool(query)
    }

    override suspend fun getTopScoresSchoolGroup(roleId: String?): BaseWrapper<List<TopScoresSchoolGroup>> {
        val query = mapOf(
            "role_id" to roleId
        )
        return remoteHomeDao.getTopScoresSchoolGroup(query)
    }

    override suspend fun getTopScoresAllSchools(roleId: String?): BaseWrapper<List<TopScoresSchoolGroup>> {
        val query = mapOf(
            "role_id" to roleId
        )
        return remoteHomeDao.getTopScoresAllSchools(query)
    }

    override suspend fun getAmbassadors(): BaseWrapper<List<TopScoresSchool>> {
        return remoteHomeDao.getAmbassadors()
    }

    override suspend fun getSmsUsage(): BaseWrapper<List<SmsUsage>> {
        return remoteHomeDao.getSmsUsage()
    }
}
