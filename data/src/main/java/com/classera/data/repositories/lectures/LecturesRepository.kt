package com.classera.data.repositories.lectures

import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.Lecture

/**
 * Project: Classera
 * Created: Jan 29, 2020
 *
 * @author Mohamed Hamdan
 */
interface LecturesRepository {

    suspend fun getLectures(): BaseWrapper<List<Lecture>>
}
