package com.classera.data.repositories.profile

import android.util.Log
import com.classera.data.daos.profile.RemoteProfileDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.CountryResponse
import com.classera.data.models.profile.Education
import com.classera.data.models.profile.Interest
import com.classera.data.models.profile.Language
import com.classera.data.models.profile.Skill
import com.classera.data.models.profile.Experiences
import okhttp3.MultipartBody


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class ProfileRepositoryImpl(private val remoteProfileDao: RemoteProfileDao) : ProfileRepository {

    override suspend fun getUserEducations(): BaseWrapper<List<Education>> {
        return remoteProfileDao.getUserEducations()
    }

    override suspend fun createUserEducation(
        countryId: String,
        cityId: String,
        school: String,
        fromYear: String,
        toYear: String,
        degree: String,
        fieldOfStudy: String,
        description: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "country_id" to countryId,
            "city_id" to cityId,
            "school" to school,
            "from_year" to fromYear,
            "to_year" to toYear,
            "degree" to degree,
            "field_of_study" to fieldOfStudy,
            "description" to description
        )

        return remoteProfileDao.createUserEducation(fields)
    }

    override suspend fun editUserEducation(
        id: String?,
        countryId: String,
        cityId: String,
        school: String,
        fromYear: String,
        toYear: String,
        degree: String,
        fieldOfStudy: String,
        description: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id,
            "country_id" to countryId,
            "city_id" to cityId,
            "school" to school,
            "from_year" to fromYear,
            "to_year" to toYear,
            "degree" to degree,
            "field_of_study" to fieldOfStudy,
            "description" to description
        )

        return remoteProfileDao.createUserEducation(fields)
    }

    override suspend fun deleteUserEducation(id: String): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id
        )

        return remoteProfileDao.deleteUserEducation(fields)
    }

    override suspend fun getCountries(): BaseWrapper<CountryResponse> {
        return remoteProfileDao.getCountries()
    }

    override suspend fun getCities(countryId: String): BaseWrapper<CityResponse> {
        val fields = mapOf(
            "country_id" to countryId
        )

        return remoteProfileDao.getCities(fields)
    }

    override suspend fun editUserProfile(
        placeOfBirthCountry: String,
        address: String,
        cityOfBirth: String,
        bio: String,
        email: String,
        phoneNumber: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "place_of_birth_country" to placeOfBirthCountry,
            "address" to address,
            "city_of_birth" to cityOfBirth,
            "bio" to bio,
            "email" to email,
            "phone_number" to phoneNumber
        )

        return remoteProfileDao.editUserProfile(fields)
    }

    override suspend fun getUserInterests(): BaseWrapper<List<Interest>> {
        return remoteProfileDao.getUserInterests()
    }

    override suspend fun deleteUserInterests(interestId: String): NoContentBaseWrapper {
        val fields = mapOf(
            "interest_id" to interestId
        )

        return remoteProfileDao.deleteUserInterest(fields)
    }

    override suspend fun editUserInterests(
        id: String,
        interest: String,
        description: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id,
            "interest" to interest,
            "description" to description
        )

        return remoteProfileDao.editUserInterest(fields)
    }

    override suspend fun createUserInterests(
        interest: String,
        description: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "interest" to interest,
            "description" to description
        )

        return remoteProfileDao.createUserInterest(fields)
    }

    override suspend fun getUserLanguages(): BaseWrapper<List<Language>> {
        return remoteProfileDao.getUserLanguages()
    }

    override suspend fun deleteUserLanguages(languageId: String): NoContentBaseWrapper {
        val fields = mapOf(
            "language_id" to languageId
        )

        return remoteProfileDao.deleteUserLanguages(fields)
    }

    override suspend fun editUserLanguages(
        id: String,
        language: String,
        proficiency: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id,
            "language" to language,
            "proficiency" to proficiency
        )

        return remoteProfileDao.editUserLanguages(fields)
    }

    override suspend fun createUserLanguages(
        language: String,
        proficiency: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "language" to language,
            "proficiency" to proficiency
        )

        return remoteProfileDao.createUserLanguages(fields)
    }

    override suspend fun getUserSkills(): BaseWrapper<List<Skill>> {
        return remoteProfileDao.getUserSkills()
    }

    override suspend fun deleteUserSkill(id: String): NoContentBaseWrapper {
        val fields = mapOf(
            "skill_id" to id
        )

        return remoteProfileDao.deleteUserSkill(fields)
    }

    override suspend fun editUserSkill(
        id: String,
        skill: String,
        skillLevel: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id,
            "skill" to skill,
            "skill_level" to skillLevel
        )

        return remoteProfileDao.editUserSkill(fields)
    }

    override suspend fun createUserSkill(skill: String, skillLevel: String): NoContentBaseWrapper {
        Log.e("skill", skill)
        Log.e("skilllevel", skillLevel)

        val fields = mapOf(
            "skill" to skill,
            "skill_level" to skillLevel
        )

        return remoteProfileDao.createUserSkill(fields)
    }

    override suspend fun changeUserProfilePicture(
        parts: List<MultipartBody.Part>
    ): NoContentBaseWrapper {
        return remoteProfileDao.changeUserProfilePicture(parts)
    }

    override suspend fun getUserExperiences(): BaseWrapper<List<Experiences>> {
        return remoteProfileDao.getUserExperiences()
    }

    override suspend fun deleteUserExperience(id: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id
        )
        return remoteProfileDao.deleteUserExperience(fields)
    }

    override suspend fun addUserExperiences(
        jobTitle: String?,
        jobType: String?,
        companyName: String?,
        startDate: String?,
        endDate: String?,
        description: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "job_title" to jobTitle,
            "job_type" to jobType,
            "company_name" to companyName,
            "start_date" to startDate,
            "end_date" to endDate,
            "description" to description
        )

        return remoteProfileDao.addUserExperiences(fields)
    }

    override suspend fun editUserExperiences(
        id: String?,
        jobTitle: String?,
        jobType: String?,
        companyName: String?,
        startDate: String?,
        endDate: String?,
        description: String?
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "id" to id,
            "job_title" to jobTitle,
            "job_type" to jobType,
            "company_name" to companyName,
            "start_date" to startDate,
            "end_date" to endDate,
            "description" to description
        )

        return remoteProfileDao.addUserExperiences(fields)
    }
}
