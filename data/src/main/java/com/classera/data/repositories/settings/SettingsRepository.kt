package com.classera.data.repositories.settings

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.settings.AppVersion
import com.classera.data.models.settings.NotificationStatus
import com.classera.data.models.settings.SettingsWrapper

interface SettingsRepository {

    suspend fun changeLanguage(language: String?): BaseWrapper<SettingsWrapper>

    suspend fun getAppVersion(appId: String?, version: String?): BaseWrapper<AppVersion>

    suspend fun getNotificationStatus():  BaseWrapper<NotificationStatus>

    suspend fun notificationRegistration(): NoContentBaseWrapper

    suspend fun notificationToggle(): NoContentBaseWrapper

    suspend fun updateNotificationStatus(uuid: String?): NoContentBaseWrapper
}
