package com.classera.data.repositories.settings

import com.classera.data.daos.settings.RemoteSettingsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.settings.AppVersion
import com.classera.data.models.settings.NotificationStatus
import com.classera.data.models.settings.SettingsWrapper
import com.classera.data.prefs.Prefs
import com.google.firebase.messaging.FirebaseMessaging

import kotlinx.coroutines.tasks.await
import java.util.*

class SettingsRepositoryImpl(
    private val remoteSettingsDao: RemoteSettingsDao,
    private val prefs: Prefs
) : SettingsRepository {

    override suspend fun changeLanguage(language: String?): BaseWrapper<SettingsWrapper> {
        val fields = mapOf("lang" to language)

        val changeUserLanguage = remoteSettingsDao.changeUserLanguage(fields = fields)
        if (changeUserLanguage.success == true) {
            prefs.authenticationToken = changeUserLanguage.data?.authToken
        }
        return changeUserLanguage
    }

    override suspend fun getAppVersion(appId: String?, version: String?): BaseWrapper<AppVersion> {
        val fields = mapOf(
            "app_id" to appId,
            "version" to version
        )
        return remoteSettingsDao.getAppVersion(fields)
    }

    override suspend fun getNotificationStatus(): BaseWrapper<NotificationStatus> {
        return remoteSettingsDao.getNotificationStatus()
    }

    override suspend fun notificationToggle(): NoContentBaseWrapper {
        val token = FirebaseMessaging.getInstance().token.await()
        val map = mapOf(
            "device_id" to UUID.randomUUID().toString(),
            "device_id" to prefs.uuid,
            "OS" to "0",
            "device_token" to token
        )
        return remoteSettingsDao.notificationToggle(map)
    }

    override suspend fun notificationRegistration(): NoContentBaseWrapper {
        val token = FirebaseMessaging.getInstance().token.await()
        val map = mapOf(
            "device_id" to prefs.uuid,
            "OS" to "0",
            "device_token" to token
        )
        return remoteSettingsDao.notificationRegistration(map)
    }

    override suspend fun updateNotificationStatus(uuid: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "uuid" to uuid
        )
        return remoteSettingsDao.updateNotificationStatus(fields)
    }
}
