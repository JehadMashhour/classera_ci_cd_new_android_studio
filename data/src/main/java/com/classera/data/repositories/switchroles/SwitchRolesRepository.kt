package com.classera.data.repositories.switchroles

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchroles.AuthWrapper
import com.classera.data.models.switchroles.Role
import com.classera.data.models.user.UserRole

interface SwitchRolesRepository {

    suspend fun getSwitchRolesList(): BaseWrapper<List<Role>>

    suspend fun generateNewAuth(role: UserRole): BaseWrapper<AuthWrapper>
}
