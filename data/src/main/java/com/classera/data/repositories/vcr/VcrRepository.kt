package com.classera.data.repositories.vcr

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vendor.VcrStatusWrapper
import com.classera.data.models.vcr.lecturetimeslotpreparation.LectureTimeslotPreparation
import com.classera.data.models.vcr.teacher.Teacher
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.models.vcr.vendor.VendorResponse
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
interface VcrRepository {

    suspend fun getSmartClassrooms(
        key: String?, pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<VcrResponse>>

    suspend fun getTeacherSmartClassrooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<VcrResponse>>

    suspend fun getUpcomingVcrUrl(vcrId: String): BaseWrapper<VcrConfirmResponse>

    suspend fun getPassedVcrUrl(vcrId: String): BaseWrapper<VcrConfirmResponse>

    suspend fun deleteVcr(sessionCode: String?, vcrId: String?): NoContentBaseWrapper

    suspend fun addVcr(
        vendor: String?,
        teacherId: String?,
        title: String?,
        zoomLink: String?,
        externalLink: String?,
        passcode: String?,
        lectures: List<Lecture?>?,
        startingTime: String?,
        duration: String?,
        sharingStatus: String?,
        allowStart: String?,
        weeklyRepeat: String?,
        repeatUntil: String?,
        recurring: String?
    ): NoContentBaseWrapper

    suspend fun editVcr(
        id: String?,
        vendor: String?,
        title: String?,
        zoomLink: String?,
        externalLink: String?,
        passcode: String?,
        lectures: List<Lecture?>?,
        startingTime: String?,
        duration: String?,
        sharingStatus: String?,
        allowStart: String?,
        weeklyRepeat: String?,
        repeatUntil: String?,
        recurring: String?
    ): NoContentBaseWrapper

    suspend fun getVcrStatus(vendor: Int?): BaseWrapper<VcrStatusWrapper>

    suspend fun getVendors(): BaseWrapper<VendorResponse>

    suspend fun getLectureTimeSlotsPreparations(lecturesIds: List<String?>)
            : BaseWrapper<List<LectureTimeslotPreparation>>


    suspend fun getTeachers(): BaseWrapper<List<Teacher>>

    suspend fun getVcrDetails(id: String?, vendorId: String?): BaseWrapper<VcrResponse>

    suspend fun getUpcomingVCRs(): BaseWrapper<List<VcrResponse>>
}
