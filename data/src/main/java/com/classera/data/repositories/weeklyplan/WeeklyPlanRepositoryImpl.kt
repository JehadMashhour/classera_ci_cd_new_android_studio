package com.classera.data.repositories.weeklyplan

import com.classera.data.daos.weeklyplan.RemoteWeeklyPlanDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.weeklyplan.WeeklyPlanWrapper

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
class WeeklyPlanRepositoryImpl(private val remoteWeeklyPlanDao: RemoteWeeklyPlanDao) : WeeklyPlanRepository {

    override suspend fun getUserWeeklyPlan(from: String?, to: String?): BaseWrapper<WeeklyPlanWrapper> {
        val fields = mutableMapOf<String, String?>()
        if (from != null) fields["date_from"] = from
        if (to != null) fields["date_to"] = to
        return remoteWeeklyPlanDao.getWeeklyPlanCalendar(fields)
    }
}
