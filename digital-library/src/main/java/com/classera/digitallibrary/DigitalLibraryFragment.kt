package com.classera.digitallibrary

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.attachment.rating.RatingBottomSheet
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.BackgroundColor
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.filter.FilterActivity
import javax.inject.Inject
import com.classera.digitallibrary.DigitalLibraryFragmentDirections.actionNavigationFragmentMainActivityAttachmentDetails as attachmentDetailsActivity

/**
 * Project: Classera
 * Created: Dec 23, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Digital Library")
class DigitalLibraryFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DigitalLibraryViewModel

    private var clickedPosition: Int = -1

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: DigitalLibraryAdapter? = null
    private var errorView: ErrorView? = null

    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var quickFilterView: QuickFilterView? = null
    var type: String? = null

    private var menuItem: Menu? = null

    private val likeReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getLibrary(clickedPosition)?.likesCount = intent?.getIntExtra("new_count", 0) ?: 0
            viewModel.getLibrary(clickedPosition)?.liked = intent?.getBooleanExtra("is_liked", false) ?: false
            adapter?.notifyDataSetChanged()
        }
    }

    private val understandReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getLibrary(clickedPosition)?.understandCount = intent?.getIntExtra("new_count", 0) ?: 0
            viewModel.getLibrary(clickedPosition)?.understand = intent?.getBooleanExtra("understand", false) ?: false
            adapter?.notifyDataSetChanged()
        }
    }

    private val ratingReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            viewModel.getLibrary(clickedPosition)?.rate = intent?.getStringExtra("new_rating")
            viewModel.getLibrary(clickedPosition)?.isRated = true
            viewModel.getLibrary(clickedPosition)?.isRate = true
            adapter?.notifyDataSetChanged()
        }
    }

    override val layoutId: Int = R.layout.fragment_digital_library

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        context?.registerReceiver(likeReceiver, IntentFilter("LIKE_CHANGED"))
        context?.registerReceiver(understandReceiver, IntentFilter("UNDERSTAND_CHANGED"))
        context?.registerReceiver(ratingReceiver, IntentFilter("RATING_CHANGED"))
    }

    override fun onResume() {
        super.onResume()
        viewModel.increaseViewsCount(clickedPosition)
        clickedPosition = -1
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.unregisterReceiver(likeReceiver)
        context?.unregisterReceiver(understandReceiver)
        context?.unregisterReceiver(ratingReceiver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initFilter()
        initListeners()
        getCourses()

        type = arguments?.getString("type")
        if (type == MATERIAL_TYPE || type == VIDEO_TYPE) {
            quickFilterView?.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_digital_library, menu)
        this.menuItem = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_digital_library_search)
        val filterMenuItem: MenuItem = menu.findItem(R.id.item_menu_fragment_digital_library_filter)
        if (type == VIDEO_TYPE) {
            filterMenuItem.isVisible = true
        }
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange(lifecycleScope) { getDigitalLibrary() }

        searchView?.setOnCloseListener {
            getDigitalLibrary()
            return@setOnCloseListener false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_digital_library_filter -> {
                FilterActivity.start(this, viewModel.getFilters(), viewModel.getSelectedFilter())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_digital_library)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_digital_library)
        errorView = view?.findViewById(R.id.error_view_fragment_digital_library)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_digital_library)
        quickFilterView = view?.findViewById(R.id.filter_view_fragment_digital_library)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshDigitalLibrary()
        }

        viewModel.notifyAdapterItemLiveData.observe(this) {
            adapter?.notifyItemChanged(it)
            adapter?.notifyDataSetChanged()
        }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun getCourses() {
        viewModel.getCourses().observe(this) {}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            viewModel.courseId = filter?.filterId
            getDigitalLibrary()
            menuItem?.getItem(1)?.setIcon(R.drawable.menu_item_with_badge)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.courseId = null
            getDigitalLibrary()
            menuItem?.getItem(1)?.setIcon(R.drawable.ic_filter)
        }
    }

    private fun getDigitalLibrary(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getDigitalLibrary(
            searchView?.query,
            quickFilterView?.getSelectedFilterKey(),
            courseId = viewModel.courseId,
            pageNumber = pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun refreshDigitalLibrary() {
        viewModel.refreshDigitalLibrary(searchView?.query, quickFilterView?.getSelectedFilterKey())
            .observe(this, this::handleResource)
    }

    private fun initFilter() {
        quickFilterView?.setAdapter(
            R.array.digital_library_filter_entries,
            R.array.digital_library_filter_entry_values
        )
        quickFilterView?.setOnFilterSelectedListener {
            recyclerView?.scrollToPosition(0)
            getDigitalLibrary()
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = DigitalLibraryAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getDigitalLibrary)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.linear_layout_row_digital_library_like -> {
                    clickedPosition = position
                    viewModel.onLikeClicked(position)
                }
                R.id.linear_layout_row_digital_library_rate -> {
                    RatingBottomSheet.show(childFragmentManager) { rating ->
                        viewModel.onSubmitRatingClicked(position, rating)
                    }
                }
                R.id.button_row_digital_library_open_in_browser -> {
                    startActivity(Intent(Intent.ACTION_VIEW, viewModel.getLibrary(position)?.comments?.toUri()))
                }
                else -> {
                    clickedPosition = position
                    val libraryId = viewModel.getLibrary(position)?.id
                    val imageBackground = viewModel.getLibrary(position)?.backgroundColor ?: BackgroundColor.GRAY
                    val args = attachmentDetailsActivity(libraryId,imageBackground)
                    findNavController().navigate(args)
                }
            }

        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getDigitalLibrary() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        searchView = null
        progressBar = null
        adapter = null
        errorView = null
        recyclerView = null
        swipeRefreshLayout = null
        quickFilterView = null
        super.onDestroyView()
    }

    private companion object {

        private const val MATERIAL_TYPE = "Material"
        private const val VIDEO_TYPE = "Video"
    }
}
