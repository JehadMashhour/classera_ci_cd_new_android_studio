package com.classera.discussionrooms

import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.adapter.BaseViewHolder

/**
 * Created by Rawan Al-Theeb on 2/24/2020.
 * Classera
 * r.altheeb@classera.com
 */
abstract class DiscussionsAdapter<V : BaseViewHolder> : BasePagingAdapter<V>()
