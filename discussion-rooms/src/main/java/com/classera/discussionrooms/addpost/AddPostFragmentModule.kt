package com.classera.discussionrooms.addpost

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 3/3/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddPostFragmentModule {

    @Module
    companion object {
        @Provides
        @kotlin.jvm.JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AddPostViewModelFactory
        ): AddPostViewModel {
            return ViewModelProvider(fragment, factory)[AddPostViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddPostFragment): Fragment
}
