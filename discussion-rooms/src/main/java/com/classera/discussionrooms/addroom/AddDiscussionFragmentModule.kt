package com.classera.discussionrooms.addroom

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 2/26/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AddDiscussionFragmentModule {

    @Module
    companion object{

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory :AddDiscussionViewModelFactory
        ): AddDiscussionViewModel{
            return ViewModelProvider(fragment,factory)[AddDiscussionViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideAddDiscussionFragmentArgs(
            fragment: Fragment
        ): AddDiscussionFragmentArgs{
            val args by fragment.navArgs<AddDiscussionFragmentArgs>()
            return args
        }

    }


    @Binds
    abstract fun bindFragment(fragment: AddDiscussionFragment) : Fragment
}
