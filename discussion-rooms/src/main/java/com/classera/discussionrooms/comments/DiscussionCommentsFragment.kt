package com.classera.discussionrooms.comments

import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.observe
import com.classera.data.models.user.UserRole
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.discussionrooms.R
import com.classera.discussionrooms.details.DiscussionDetailsFragment.Companion.APPROVE
import com.classera.discussionrooms.details.DiscussionDetailsFragment.Companion.CLOSE
import com.classera.discussionrooms.details.DiscussionDetailsFragment.Companion.DIS_APPROVE
import javax.inject.Inject

@Screen("Discussion Comments")
class DiscussionCommentsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DiscussionCommentsViewModel

    @Inject
    lateinit var prefs: Prefs

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: DiscussionCommentsAdapter? = null
    private var errorView: ErrorView? = null
    private var relativeLayoutAddCommentParent: RelativeLayout? = null
    private var imageViewAddCommentSubmit: ImageView? = null
    private var editTextAddComment: EditText? = null

    private val args: DiscussionCommentsFragmentArgs by navArgs()

    override val layoutId: Int = R.layout.fragment_discussion_comments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initViewModelListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_discussion_comments)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_discussion_comments)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_discussion_comments)
        errorView = view?.findViewById(R.id.error_view_fragment_discussion_comments)
        imageViewAddCommentSubmit = view?.findViewById(R.id.image_view_fragment_discussion_comments_add_comment_submit)
        editTextAddComment = view?.findViewById(R.id.edit_text_fragment_discussion_comments_add_comment)
        relativeLayoutAddCommentParent =
            view?.findViewById(R.id.relative_layout_fragment_discussion_comments_add_comment_parent)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshComments()
        }

        imageViewAddCommentSubmit?.setOnClickListener {
            if (editTextAddComment?.text?.isNotEmpty() == true && editTextAddComment?.text?.isNotBlank() == true) {
                viewModel.addComment(editTextAddComment?.text?.toString())
                editTextAddComment?.text = null
            }
        }

        viewModel.notifyCommentsAdapterLiveData.observe(this) { commentAdded ->
            adapter?.notifyDataSetChanged()
            if (commentAdded)
                getComments(DEFAULT_PAGE)
        }

        viewModel.notifyAddCommentsReachMaxLiveData.observe(this) { message ->
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }

        getComments()
    }

    private fun getComments(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getComments(pageNumber).observe(this, this::handleResource)
    }

    private fun refreshComments() {
        viewModel.refreshComments().observe(this, this::handleResource)
    }


    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
            relativeLayoutAddCommentParent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE

            if(prefs.userRole != UserRole.GUARDIAN){
                relativeLayoutAddCommentParent?.visibility = View.VISIBLE
            }

            if (args.closeValue.equals(CLOSE))
                relativeLayoutAddCommentParent?.visibility = View.GONE

        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = DiscussionCommentsAdapter(requireActivity(), viewModel, prefs)
        recyclerView?.adapter = adapter
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_discussion_comment_more -> {
                    handleMoreClicked(view, position)
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_discussion_comment_actions, menu.menu)

        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.GUARDIAN) {
            menu.menu.getItem(2).isVisible = false
        }


        val comment = viewModel.getComment(position)
        val approvePostValue: String

        if (comment?.approved == true) {
            approvePostValue = APPROVE
            menu.menu.getItem(2).title = resources.getString(R.string.title_row_discussion_post_dis_approve_post)
        } else {
            approvePostValue = DIS_APPROVE
            menu.menu.getItem(2).title = resources.getString(R.string.title_row_discussion_post_approve_post)
        }

        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_discussion_comment_actions_delete -> {
                    handleDeleteItemClicked(position)
                }

                R.id.item_menu_discussion_comment_actions_approve_post -> {
                    if (approvePostValue == (APPROVE))
                        viewModel.approveTopic(position, DIS_APPROVE).observe(this, this::handleApprovePostResource)
                    else
                        viewModel.approveTopic(position, APPROVE).observe(this, this::handleApprovePostResource)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext(), R.style.AppTheme_AlertDialog)
            .setTitle(context?.getString(R.string.title_delete_discussion_dialog))
            .setMessage(context?.getString(R.string.message_delete_discussion_dialog))
            .setPositiveButton(context?.getString(R.string.button_positive_delete_discussion_dialog))
            { _, _ ->
                viewModel.deleteDiscussionComment(position)
            }
            .setNegativeButton(context?.getString(R.string.button_negative_delete_discussion_dialog), null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) { adapter?.notifyItemRemoved(it) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleApprovePostResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleApprovePostSuccessResource()
            }
            is Resource.Error -> {
                handleApprovePostErrorResource(resource)
            }
        }
    }

    private fun handleApprovePostSuccessResource() {
        refreshComments()
    }

    private fun handleApprovePostErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getComments() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
