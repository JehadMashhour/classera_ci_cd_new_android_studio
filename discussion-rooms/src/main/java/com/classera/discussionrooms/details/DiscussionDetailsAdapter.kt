package com.classera.discussionrooms.details

import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.utils.android.isContainsArabic
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs
import com.classera.discussionrooms.R
import com.classera.discussionrooms.databinding.RowDiscussionPostBinding
import com.classera.discussionrooms.details.DiscussionDetailsFragment.Companion.CLOSE

/**
 * Created by Odai Nazzal on 12/31/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class DiscussionDetailsAdapter(
    private val viewModel: DiscussionDetailsViewModel,
    private val closedValue : String?,
    private val prefs: Prefs
) : BasePagingAdapter<DiscussionDetailsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDiscussionPostBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getPostCount()
    }

    inner class ViewHolder(binding: RowDiscussionPostBinding) : BaseBindingViewHolder(binding) {

        private var commentLayout: LinearLayout? = null
        private var imageViewMore: ImageView? = null
        private var imageViewApproved: ImageView? = null
        private var textViewApproved: TextView? = null
        private var textViewComment: TextView? = null
        private var content: WebView? = null

        init {
            commentLayout = itemView.findViewById(R.id.comment_layout)
            imageViewMore = itemView.findViewById(R.id.image_view_row_discussion_post_more)
            imageViewApproved = itemView.findViewById(R.id.image_view_row_discussion_post_approved)
            textViewApproved = itemView.findViewById(R.id.text_view_row_discussion_post_approved)
            textViewComment = itemView.findViewById(R.id.comment)
            content = itemView.findViewById(R.id.web_view_post_content)

            if (viewModel.getSettings().userRole == UserRole.GUARDIAN) {
                commentLayout?.visibility = View.GONE
            }

            if (viewModel.getSettings().userRole == UserRole.GUARDIAN ||
                viewModel.getSettings().userRole == UserRole.STUDENT
            ) {

                imageViewApproved?.visibility = View.GONE
                textViewApproved?.visibility = View.GONE
            }

            if(closedValue.equals(CLOSE))
                commentLayout?.visibility = View.GONE

        }

        override fun bind(position: Int) {

            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            commentLayout?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            textViewComment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }


            if (viewModel.getPost(position)?.creatorId.toString() == prefs.userId
                || prefs.userRole == UserRole.TEACHER
                || prefs.userRole == UserRole.TEACHER_SUPERVISOR
            )
                imageViewMore?.visibility = View.VISIBLE
            else
                imageViewMore?.visibility = View.GONE

            bind<RowDiscussionPostBinding> {
                this.discussionPost = viewModel.getPost(position)
                this.userImageUrl = viewModel.getUserImageUrl()
                Log.v("creator", this.discussionPost?.creatorId.toString())
            }

            content?.apply {
                setBackgroundColor(Color.TRANSPARENT)
                settings?.javaScriptEnabled = webViewAllowSettings
                webChromeClient = WebChromeClient()
                settings?.defaultFontSize = webViewDefaultTextSize.toInt()
                settings?.allowFileAccess = webViewAllowSettings
                settings?.loadsImagesAutomatically = webViewAllowSettings
                settings?.javaScriptCanOpenWindowsAutomatically =
                    webViewAllowSettings
                settings?.mediaPlaybackRequiresUserGesture = webViewAllowSettings
                settings?.javaScriptCanOpenWindowsAutomatically =
                    webViewAllowSettings
                settings?.loadWithOverviewMode = webViewAllowSettings
            }

            if (viewModel.getPost(position)?.postContent?.isContainsArabic() == true) {
                content?.loadDataWithBaseURL(
                    null, formatAra + viewModel.getPost(position)?.postContent + formatEnd,
                    mimeType, encoding, null
                )
                evaluateJSCode()
            } else {
                content?.loadDataWithBaseURL(
                    null, formatEng + viewModel.getPost(position)?.postContent + formatEnd,
                    mimeType, encoding, null
                )
                evaluateJSCode()
            }

        }

        private fun evaluateJSCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                content?.evaluateJavascript(jsEvaluateFormatCode, null)
            } else {
                content?.loadUrl(jsEvaluateFormatCode)
            }
        }

        private val mimeType = "text/html"
        private val encoding = "UTF-8"
        private val webViewDefaultTextSize = "14"
        private val webViewAllowSettings = true
        private val formatEng =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img {width: 50% !important; word-break: break-all;" +
                    "height:auto !important;}" +
                    "</style> </head><body><div id='height'>"
        private val formatEnd = "</div></body>"
        private val formatAra =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img,p {width: 50% !important; text-align: right !important; " +
                    "word-break: break-all;" +
                    "height:auto !important;}</style> " +
                    "</head><body dir='rtl'><div id='height'>"
        private val jsEvaluateFormatCode =
            "var images = document.getElementsByTagName('img'); \n" +
                    "var srcList = [];\n" +
                    "for(var i = 0; i < images.length; i++) {\n" +
                    "    srcList.push(images[i].style.height= 'auto !important');\n" +
                    "    srcList.push(images[i].style.width= '50% !important');\n" +
                    "}"

    }

}
