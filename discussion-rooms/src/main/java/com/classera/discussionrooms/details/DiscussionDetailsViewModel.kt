package com.classera.discussionrooms.details

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.DiscussionPost
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.data.repositories.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class DiscussionDetailsViewModel(
    private val discussionDetailsFragmentArgs: DiscussionDetailsFragmentArgs,
    private val discussionRoomsRepository: DiscussionRoomsRepository,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : BaseViewModel(), LifecycleObserver {

    var roomId: String? = null
    var instruction: String? = null
    private var userImageUrl: String? = null
    private var postsList: MutableList<DiscussionPost?>? = mutableListOf()

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            roomId = discussionDetailsFragmentArgs.roomId
            instruction = discussionDetailsFragmentArgs.instruction
            userImageUrl = userRepository.getLocalUser().first()?.firstOrNull()?.imageUrl
        }
    }

    fun getPosts(text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        return getPosts(text, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    fun refreshPosts(text: CharSequence?) = getPosts(text, DEFAULT_PAGE, false)

    private fun getPosts(text: CharSequence?, pageNumber: Int, showProgress: Boolean) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                discussionRoomsRepository.getDiscussionDetails(
                    roomId,
                    pageNumber,
                    text
                )
            }
            if (pageNumber == DEFAULT_PAGE) {
                postsList?.clear()
            }
            postsList?.addAll(
                resource.element<BaseWrapper<List<DiscussionPost>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getRoomDetails() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            discussionRoomsRepository.getRoomDetails(roomId)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun approveTopic(position: Int, approve: String?) = liveData(Dispatchers.IO) {

        val post = getPost(position)
        val postId = post?.id

        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {
            discussionRoomsRepository.approveTopic(postId, approve)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun editRoomSettings(close: String?, approve: String?) = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {
            discussionRoomsRepository.editRoomSettings(roomId, close, approve)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }


    fun getPostCount(): Int {
        return postsList?.size ?: 0
    }

    fun getPost(position: Int): DiscussionPost? {
        return postsList?.get(position)
    }

    fun getUserImageUrl(): String? {
        return userImageUrl
    }

    fun getSettings(): Prefs{
        return prefs
    }

    fun deletePost(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val post = getPost(position)
            post?.deleting = true

            val postId = post?.id
            val resource = tryNoContentResource {
                discussionRoomsRepository.deleteDiscussionComment(postId,"2")
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            post?.deleting = false
        }
    }

    private fun deleteItem(position: Int) {
        postsList?.removeAt(position)
    }
}
