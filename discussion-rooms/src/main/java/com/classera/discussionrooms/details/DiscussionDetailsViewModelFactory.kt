package com.classera.discussionrooms.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.data.repositories.user.UserRepository

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class DiscussionDetailsViewModelFactory(
    private val discussionDetailsFragmentArgs: DiscussionDetailsFragmentArgs,
    private val discussionRoomsRepository: DiscussionRoomsRepository,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DiscussionDetailsViewModel(
            discussionDetailsFragmentArgs, discussionRoomsRepository,
            userRepository, prefs
        ) as T
    }
}
