package com.classera.discussionrooms.student

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.classera.discussionrooms.DiscussionsAdapter
import com.classera.discussionrooms.DiscussionsFragment
import com.classera.discussionrooms.DiscussionsViewModel
import com.classera.discussionrooms.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class StudentDiscussionFragment : DiscussionsFragment() {

    @Inject
    lateinit var viewModel: StudentDiscussionViewModel

    @Inject
    lateinit var adapter: StudentDiscussionAdapter

    private var floatingActionButton: FloatingActionButton? = null

    override fun getDiscussionViewModel(): DiscussionsViewModel {
        return viewModel
    }

    override fun getDiscussionAdapter(): DiscussionsAdapter<*> {
        return adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun findViews() {
        floatingActionButton = view?.findViewById(R.id.floating_action_button_fragment_add_discussion)
    }

    private fun initVisibility() {
        floatingActionButton?.visibility = View.GONE
    }

    private fun initListeners() {
        handleAddListeners()
    }

    override fun handleAddListeners() {
        // No impl
    }

    override fun initAdapterListeners() {
        adapter.setOnItemClickListener { _, position ->
            val room = viewModel.getDiscussionRoom(position)
            val id = room?.id
            val title = room?.title
            val instruction = room?.instruction
            findNavController().navigate(
                StudentDiscussionFragmentDirections.discussionRoomDetailsDirection(
                    title, id).apply {
                        this.instruction = instruction
                })
        }
    }
}

