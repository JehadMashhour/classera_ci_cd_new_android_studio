package com.classera.discussionrooms.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.discussionrooms.DiscussionsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by Rawan Al-Theeb on 2/25/2020.
 * Classera
 * r.altheeb@classera.com
 */
class TeacherDiscussionViewModel(private val discussionRoomsRepository: DiscussionRoomsRepository) :
    DiscussionsViewModel(discussionRoomsRepository) {

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    override fun deleteRoom(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val room = getDiscussionRoom(position)
            room?.deleting = true

            val roomId = room?.id
            val resource = tryNoContentResource {
                discussionRoomsRepository.deleteRoom(roomId)
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            room?.deleting = false
        }
    }
}

