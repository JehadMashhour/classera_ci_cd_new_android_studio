package com.classera.discussionrooms.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.discussionrooms.DiscussionsViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */

@Module
abstract class TeacherDiscussionsFragmentModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: DiscussionsViewModelFactory
        ): TeacherDiscussionViewModel{
            return ViewModelProvider(fragment, factory)[TeacherDiscussionViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideDiscussionAdapter(
            viewModel: TeacherDiscussionViewModel
        ): TeacherDiscussionAdapter {
            return TeacherDiscussionAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindActivity(activity: TeacherDiscussionFragment): Fragment
}
