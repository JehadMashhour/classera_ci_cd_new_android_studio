package com.classera.eportfolio

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Module
abstract class EPortfolioFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: EPortfolioViewModelFactory
        ): EPortfolioViewModel {
            return ViewModelProvider(fragment, factory)[EPortfolioViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: EPortfolioFragment): Fragment

}
