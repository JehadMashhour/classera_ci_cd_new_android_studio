package com.classera.eportfolio.add

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.eportfolio.R
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: APR 21, 2020
 *
 * @author Saeed Halawani
 */
@Screen("Add E-portfolio")
@Suppress("ComplexMethod")
class AddEportfolioFragment : BaseValidationFragment(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var viewModel: AddEportfolioViewModel

    private val args: AddEportfolioFragmentArgs by navArgs()

    @NotEmpty(message = "validation_fragment_add_eport_title")
    private var editTextTitle: EditText? = null

    @NotEmpty(message = "validation_fragment_add_eport_description")
    private var editTextDescription: EditText? = null

    @NotEmpty(message = "validation_fragment_add_eport_post")
    private var editTextPost: EditText? = null

    private var editTextDate: EditText? = null

    private var autoCompleteTextViewSharingLevel: AutoCompleteTextView? = null
    private var autoCompleteTextViewCourses: AutoCompleteTextView? = null
    private var autoCompleteTextViewType: AutoCompleteTextView? = null

    private var coursesLayout: TextInputLayout? = null

    private var imageViewAttachCoverImage: ImageView? = null
    private var imageViewAttachFile: ImageView? = null
    private var image: MediaFile? = null
    private var file: MediaFile? = null
    private var textViewAttachedCoverName: TextView? = null
    private var textViewAttachedFileName: TextView? = null

    private var buttonUpload: Button? = null
    private var progressBar: ProgressBar? = null
    private var progressBarUpload: ProgressBar? = null
    private var errorView: ErrorView? = null

    private var linearLayoutCover: LinearLayout? = null
    private var linearLayoutFile: LinearLayout? = null

    private val currentDayCalendar = Calendar.getInstance()

    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)


    private var selectedSharingLevelId: String? = null
    private var selectedSharingLevelPosition = 0

    private var selectedCourseId: String? = null
    private var selectedCoursesPosition = 0

    private var selectedTypeId: String? = null
    private var selectedTypePosition = 0

    private var date: String? = null
    private var linearLayoutParent: LinearLayout? = null

    override val layoutId: Int = R.layout.fragment_add_eportfolio


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        getEportData()
        initSubmitButtonListener()
        initDateListener()
        initErrorViewListener()
        initAttachmentListener()
        changeTitles()
    }

    private fun getEportData() {
        viewModel.getCourses().observe(this, ::handleCoursesResource)
        viewModel.getSharingLevel().observe(this, ::handleSharingLevelResource)
        viewModel.getTypes().observe(this, ::handleTypeResource)
    }

    private fun handleCoursesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCoursesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCoursesSuccessResource()
            }
            is Resource.Error -> {
                handleCoursesErrorResource(resource)
            }
        }
    }

    private fun handleCoursesErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleCoursesLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleCoursesSuccessResource() {
        initCoursesAdapter()
        linearLayoutCover?.visibility = View.VISIBLE
        linearLayoutFile?.visibility = View.VISIBLE
    }

    private fun handleSharingLevelResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSharingLevelLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSharingLevelSuccessResource()
            }
            is Resource.Error -> {
                handleSharingLevelErrorResource(resource)
            }
        }
    }

    private fun handleSharingLevelErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSharingLevelLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSharingLevelSuccessResource() {
        initSharingLevelAdapter()
    }

    private fun changeTitles() {
        if (args.eportfolio != null) {
            buttonUpload?.text = getString(R.string.save_eport_data)
            (activity as AppCompatActivity).supportActionBar?.title = args.eportfolio?.title
            loadEportDataToEdit()
        } else {
            (activity as AppCompatActivity).supportActionBar?.title =
                getString(R.string.title_fragment_add_eportfolio)
        }
    }

    private fun loadEportDataToEdit() {
        editTextTitle?.setText(args.eportfolio?.title)
        editTextPost?.setText(args.eportfolio?.brief)
        editTextDescription?.setText(args.eportfolio?.details)
        editTextDate?.setText(args.eportfolio?.getDate())
    }


    private fun handleTypeResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleTypeLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleTypeSuccessResource()
            }
            is Resource.Error -> {
                handleTypeErrorResource(resource)
            }
        }
    }

    private fun handleTypeErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleTypeLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleTypeSuccessResource() {
        initTypeAdapter()
    }

    private fun initSharingLevelAdapter() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getSharingLevelTitles()
            )
        autoCompleteTextViewSharingLevel?.setAdapter(adapter)
        autoCompleteTextViewSharingLevel?.setOnItemClickListener { _, _, position, _ ->
            viewModel.onSharingLevelSelected(position)
            selectedSharingLevelPosition = position
            selectedSharingLevelId = viewModel.getSelectedSharingLevelId()
        }
        if (args.eportfolio != null) {
            val sharingLevelIds = viewModel.getSharingLevelIds().toList()
            val sharingLevel = viewModel.getSharingLevelTitles().toList()
            val selectedSharingLevelPosition =
                sharingLevelIds.indexOf(args.eportfolio?.sharingLevel)
            if (selectedSharingLevelPosition != -1) {
                val selectedSharingLevel = sharingLevel[selectedSharingLevelPosition]
                autoCompleteTextViewSharingLevel?.setText(
                    selectedSharingLevel,
                    false
                )
            }
        }
    }

    private fun initCoursesAdapter() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getCourseTitles()
            )
        autoCompleteTextViewCourses?.setAdapter(adapter)
        autoCompleteTextViewCourses?.setOnItemClickListener { _, _, position, _ ->
            viewModel.onCoursesSelected(position)
            selectedCoursesPosition = position
            selectedCourseId = viewModel.getSelectedCourseId()
        }
        if (args.eportfolio != null) {
            val courseIds = viewModel.getCourseIds().toList()
            val course = viewModel.getCourseTitles().toList()
            val selectedCoursesPosition = courseIds.indexOf(args.eportfolio?.courseId)
            if (selectedCoursesPosition != -1) {
                val selectedCourse = course[selectedCoursesPosition]
                autoCompleteTextViewCourses?.setText(
                    selectedCourse,
                    false
                )
            }
        }
    }

    private fun initTypeAdapter() {
        val adapter =
            ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                viewModel.getTypeTitles()
            )
        autoCompleteTextViewType?.setAdapter(adapter)
        autoCompleteTextViewType?.setOnItemClickListener { _, _, position, _ ->
            viewModel.onTypeSelected(position)
            selectedTypePosition = position
            selectedTypeId = viewModel.getSelectedTypeId()
        }
        if (args.eportfolio != null) {
            val typeIds = viewModel.getTypeIds().toList()
            val type = viewModel.getTypeTitles().toList()
            val selectedTypePosition = typeIds.indexOf(args.eportfolio?.typeId)
            if (selectedTypePosition != -1) {
                val selectedType = type[selectedTypePosition]
                autoCompleteTextViewType?.setText(
                    selectedType,
                    false
                )
            }
        }
    }

    private fun initAttachmentListener() {
        imageViewAttachCoverImage?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowVideos(false)
                    .setShowImages(true)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, ATTACH_IMAGE_REQUEST_CODE)
        }

        imageViewAttachFile?.setOnClickListener {
            val intent = Intent(requireContext(), FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(true)
                    .setShowImages(true)
                    .enableImageCapture(false)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, ATTACH_FILE_REQUEST_CODE)
        }
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            getEportData()
        }
    }

    private fun initSubmitButtonListener() {
        buttonUpload?.setOnClickListener { validator.validate() }
    }

    override fun onValidationSucceeded() {

        if (args.eportfolio != null) {

            val pathFile = file?.path ?: ""
            val mimeTypeFile = file?.mimeType ?: ""

            val pathImage = image?.path ?: ""
            val mimeTypeImage = image?.mimeType ?: ""

            date = editTextDate?.text?.toString()

            val data = mutableMapOf(
                "id" to args.eportfolio?.id,
                "title" to editTextTitle?.text?.toString(),
                "details" to editTextDescription?.text?.toString(),
                "brief" to editTextPost?.text?.toString(),
                "date" to date,
                "sharing_status" to if (selectedSharingLevelId != null) selectedSharingLevelId
                else viewModel.getSharingLevelIds()[viewModel.getSharingLevelIds()
                    .indexOf(args.eportfolio?.sharingLevel)],
                "type_id" to if (selectedTypeId != null) selectedTypeId
                else viewModel.getTypeIds()[viewModel.getTypeIds()
                    .indexOf(args.eportfolio?.typeId)],
                "course_id" to if (selectedCourseId != null) selectedCourseId
                else viewModel.getCourseIds()[viewModel.getCourseIds()
                    .indexOf(args.eportfolio?.courseId)]
            )
            viewModel.uploadAttachment(data, pathFile, mimeTypeFile, pathImage, mimeTypeImage)
                .observe(this, ::handleSubmitResource)
        } else {
            val pathFile = file?.path ?: ""
            val mimeTypeFile = file?.mimeType ?: ""

            val pathImage = image?.path ?: ""
            val mimeTypeImage = image?.mimeType ?: ""

            date = editTextDate?.text?.toString()

            val data = mutableMapOf(
                "id" to null,
                "title" to editTextTitle?.text?.toString(),
                "details" to editTextDescription?.text?.toString(),
                "brief" to editTextPost?.text?.toString(),
                "date" to date,
                "sharing_status" to if (selectedSharingLevelId != null) selectedSharingLevelId
                else viewModel.getSharingLevelIds()[viewModel.getSharingLevelIds()
                    .indexOf(args.eportfolio?.sharingLevel)],
                "type_id" to if (selectedTypeId != null) selectedTypeId
                else viewModel.getTypeIds()[viewModel.getTypeIds()
                    .indexOf(args.eportfolio?.typeId)],
                "course_id" to if (selectedCourseId != null) selectedCourseId
                else viewModel.getCourseIds()[viewModel.getCourseIds()
                    .indexOf(args.eportfolio?.courseId)]
            )
            viewModel.uploadAttachment(data, pathFile, mimeTypeFile, pathImage, mimeTypeImage)
                .observe(this, ::handleSubmitResource)
        }
    }


    private fun handleSubmitResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSubmitLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSubmitSuccessResource()
            }
            is Resource.Error -> {
                handleSubmitErrorResource(resource)
            }
        }
    }

    private fun handleSubmitLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarUpload?.visibility = View.VISIBLE
            buttonUpload?.text = ""
            buttonUpload?.isEnabled = false
        } else {
            progressBarUpload?.visibility = View.GONE
            buttonUpload?.setText(R.string.button_fragment_add_attachment_upload)
            buttonUpload?.isEnabled = true
        }
    }

    private fun handleSubmitSuccessResource() {
        findNavController().popBackStack()
    }

    private fun handleSubmitErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        currentDayCalendar.set(Calendar.YEAR, year)
        currentDayCalendar.set(Calendar.MONTH, month)
        currentDayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        editTextDate?.setText(sdf.format(currentDayCalendar.time))
    }

    private fun findViews() {
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_eport_parent)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_attachment)
        errorView = view?.findViewById(R.id.error_view_fragment_eport)
        buttonUpload = view?.findViewById(R.id.button_fragment_eport_upload)
        progressBarUpload = view?.findViewById(R.id.progress_bar_fragment_eport_upload)

        linearLayoutCover = view?.findViewById(R.id.layout_fragment_eport_cover_image)
        linearLayoutFile = view?.findViewById(R.id.layout_fragment_eport_attach_file)

        editTextTitle = view?.findViewById(R.id.edit_text_fragment_eport_title)
        editTextDescription = view?.findViewById(R.id.edit_text_fragment_eport_details)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_eport_publish_date)
        editTextPost = view?.findViewById(R.id.edit_text_fragment_eport_post)

        textViewAttachedCoverName = view?.findViewById(R.id.text_view_fragment_eport_cover_name)
        textViewAttachedFileName = view?.findViewById(R.id.text_view_fragment_eport_file_name)

        autoCompleteTextViewSharingLevel =
            view?.findViewById(R.id.auto_complete_text_view_fragment_eport_sharing_level)

        autoCompleteTextViewCourses =
            view?.findViewById(R.id.auto_complete_text_view_fragment_eport_courses)

        coursesLayout =
            view?.findViewById(R.id.courses_layout)

        if (disableCourses()) {
            coursesLayout?.visibility = View.GONE
            autoCompleteTextViewCourses?.visibility = View.GONE
        }

        autoCompleteTextViewType =
            view?.findViewById(R.id.auto_complete_text_view_fragment_eport_type)

        imageViewAttachCoverImage =
            view?.findViewById(R.id.image_view_fragment_eport_attach_cover_image)
        imageViewAttachFile = view?.findViewById(R.id.image_view_fragment_eport_attach_file)

    }

    private fun disableCourses(): Boolean {
        return prefs.userRole == UserRole.GUARDIAN || prefs.userRole == UserRole.ADMIN
    }

    private fun initDateListener() {
        editTextDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ATTACH_IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val images = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            image = images?.firstOrNull()
            textViewAttachedCoverName?.text = image?.name

        } else if (requestCode == ATTACH_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val files = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            file = files?.firstOrNull()
            textViewAttachedFileName?.text = file?.name
        }
    }

    private companion object {
        private const val ATTACH_IMAGE_REQUEST_CODE = 104
        private const val ATTACH_FILE_REQUEST_CODE = 105

    }

}
