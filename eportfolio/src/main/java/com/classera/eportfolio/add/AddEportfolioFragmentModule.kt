package com.classera.eportfolio.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.eportfolio.EPortfolioViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AddEportfolioFragmentModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factoryAdd: EPortfolioViewModelFactory
        ): AddEportfolioViewModel {
            return ViewModelProvider(fragment, factoryAdd)[AddEportfolioViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(fragmentAdd: AddEportfolioFragment): Fragment
}
