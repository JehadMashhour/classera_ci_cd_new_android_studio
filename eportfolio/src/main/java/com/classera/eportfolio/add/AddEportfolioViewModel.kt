package com.classera.eportfolio.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.Course
import com.classera.data.models.eportfolio.add.SharingLevelWrapper
import com.classera.data.models.eportfolio.add.TypeWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddEportfolioViewModel(
    private val ePortfolioRepository: EPortfolioRepository,
    private val coursesRepository: CoursesRepository
) : BaseViewModel() {

    private var selectedCoursePosition = 0
    private val courses = mutableListOf<Course>()

    private var selectedSharingLevelPosition = 0
    private val sharingLevel = mutableListOf<SharingLevelWrapper>()

    private var selectedSTypePosition = 0
    private val type = mutableListOf<TypeWrapper>()

    fun getCourses() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { coursesRepository.getCourses() }

        courses.clear()
        courses.addAll(resource.element<BaseWrapper<List<Course>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCourseIds(): List<String> {
        return courses.mapNotNull { it.courseId }
    }

    fun getTypeIds(): List<String> {
        return type.mapNotNull { it.id }
    }

    fun getSharingLevelIds(): List<String> {
        return sharingLevel.mapNotNull { it.id }
    }

    fun getCourseTitles(): List<String> {
        return courses.mapNotNull { it.courseTitle }
    }

    fun getSelectedCourseId(): String? {
        return when {
            courses.isEmpty() -> "-1"
            else -> courses[selectedCoursePosition].courseId
        }
    }

    fun getSharingLevel() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { ePortfolioRepository.getSharingLevel() }

        sharingLevel.clear()
        sharingLevel.addAll(resource.element<BaseWrapper<List<SharingLevelWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSharingLevelTitles(): List<String> {
        return sharingLevel.mapNotNull { it.name }
    }

    fun getSelectedSharingLevelId(): String? {
        return when {
            sharingLevel.isEmpty() -> "-1"
            else -> sharingLevel[selectedSharingLevelPosition].id
        }
    }

    fun getTypes() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { ePortfolioRepository.getType() }

        type.clear()
        type.addAll(resource.element<BaseWrapper<List<TypeWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getTypeTitles(): List<String> {
        return type.mapNotNull { it.name }
    }

    fun getSelectedTypeId(): String? {
        return when {
            type.isEmpty() -> "-1"
            else -> type[selectedSTypePosition].id
        }
    }

    fun onCoursesSelected(position: Int) {
        this.selectedCoursePosition = position
    }

    fun onSharingLevelSelected(position: Int) {
        this.selectedSharingLevelPosition = position
    }

    fun onTypeSelected(position: Int) {
        this.selectedSTypePosition = position
    }

    fun uploadAttachment(
        data: Map<String, Any?>,
        filePath: String,
        fileType: String,
        imagePath: String,
        imageType: String
    ) = liveData {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()

        dataPart(paramsList, data)
        filesParts(filePath, fileType, paramsList, imagePath, imageType)

        val resource = tryThirdPartyResource { ePortfolioRepository.uploadAttachment(paramsList) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun filesParts(
        filePath: String,
        fileType: String,
        paramsList: ArrayList<MultipartBody.Part>,
        imagePath: String,
        imageType: String
    ) {
        if (filePath.isNotBlank() || filePath.isNotEmpty()) {
            val file = File(filePath)
            val attachmentRequestBody = RequestBody.create(MediaType.parse(fileType), file)
            paramsList.add(MultipartBody.Part.createFormData("attachment", file.name, attachmentRequestBody))
        }

        if (imagePath.isNotBlank() || imagePath.isNotEmpty()) {
            val image = File(imagePath)
            val imageRequestBody = RequestBody.create(MediaType.parse(imageType), image)
            paramsList.add(MultipartBody.Part.createFormData("PhotoFile", image.name, imageRequestBody))
        }
    }

    private fun dataPart(paramsList: ArrayList<MultipartBody.Part>, data: Map<String, Any?>) {
        if (! data["id"].toString().equals("null",true)) {
            paramsList.add(MultipartBody.Part.createFormData("id", data["id"].toString()))
        }

        // required parameters to send
        requiredData(paramsList, data)

        //optional data to send
        optionalData(paramsList, data)
    }

    private fun optionalData(
        paramsList: ArrayList<MultipartBody.Part>,
        data: Map<String, Any?>
    ) {
        val dateValue = data["date"].toString()
        if (dateValue.isNotBlank() || dateValue.isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("date",dateValue))
        }

        val sharingLevelId = data["sharing_status"].toString()
        if (sharingLevelId.isNotBlank() || sharingLevelId.isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("sharing_status", sharingLevelId))
        }

        val typeId =  data["type_id"].toString()
        if (typeId.isNotBlank() || typeId.isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("type_id", typeId))
        }


        val courseId = data["course_id"].toString()
        if (courseId.isNotBlank() || courseId.isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("course_id",courseId))
        }
    }

    private fun requiredData(
        paramsList: ArrayList<MultipartBody.Part>,
        data: Map<String, Any?>
    ) {
        paramsList.add(MultipartBody.Part.createFormData("title", data["title"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("details", data["details"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("brief", data["brief"].toString()))
    }


}
