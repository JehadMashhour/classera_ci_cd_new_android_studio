package com.classera.homelocation

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class HomeLocationFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: HomeLocationViewModelFactory): HomeLocationViewModel {
            return ViewModelProvider(fragment, factory)[HomeLocationViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: HomeLocationFragment): Fragment
}
