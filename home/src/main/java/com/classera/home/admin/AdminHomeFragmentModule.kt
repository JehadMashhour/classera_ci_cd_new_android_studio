package com.classera.home.admin

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class AdminHomeFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AdminHomeViewModelFactory
        ): AdminHomeViewModel {
            return ViewModelProvider(fragment, factory)[AdminHomeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: AdminHomeFragment): Fragment
}
