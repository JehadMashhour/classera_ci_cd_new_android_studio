package com.classera.home.admin.activesections

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeSchoolActiveSectionsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    private var recyclerViewActiveSections: RecyclerView? = null
    private var progressBarSchoolActiveSection: ProgressBar? = null

    override val layoutId: Int = R.layout.fragment_admin_home_school_active_section

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initViewModelListeners()
    }

    private fun findViews() {
        recyclerViewActiveSections = view?.findViewById(R.id.recycler_view_admin_home_school_active_section)
        progressBarSchoolActiveSection =
            view?.findViewById(R.id.progress_bar_fragment_home_dashboard_school_active_section)
    }

    private fun initViewModelListeners() {
        viewModel.getSchoolActiveSection().observe(this, this::handleSchoolActiveSectionResource)

    }

    @Suppress("UNCHECKED_CAST")
    private fun handleSchoolActiveSectionResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleContentStatisticsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleContentStatisticsSuccessResource()
            }
            is Resource.Error -> {
                handleContentStatisticsErrorResource(resource)
            }
        }
    }

    private fun handleContentStatisticsLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarSchoolActiveSection?.visibility =
            View.VISIBLE else progressBarSchoolActiveSection?.visibility = View.GONE
    }

    private fun handleContentStatisticsSuccessResource() {
        val adapter =
            AdminHomeSchoolActiveSectionsAdapter(viewModel)
        recyclerViewActiveSections?.adapter = adapter
    }

    private fun handleContentStatisticsErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewActiveSections = null
        progressBarSchoolActiveSection = null
    }
}
