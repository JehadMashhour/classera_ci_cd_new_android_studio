package com.classera.home.admin.schoolambassadors

import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.classera.home.databinding.RowSchoolAmbassadorBinding

/**
 * Project: Classera
 * Created: Dec 21, 2019
 *
 * @author Mohamed Hamdan
 */
class AdminHomeSchoolAmbassadorsAdapter(private val viewModel: AdminHomeViewModel) :
    BaseAdapter<AdminHomeSchoolAmbassadorsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSchoolAmbassadorBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getSchoolAmbassadorsCount()
    }

    inner class ViewHolder(binding: RowSchoolAmbassadorBinding) : BaseBindingViewHolder(binding) {
        private var imageViewContentStatisticIcon: ImageView? = null
        private var cardViewContentStatistic: CardView? = null

        init {
            imageViewContentStatisticIcon = itemView.findViewById(R.id.image_view_row_content_statistic_icon)
            cardViewContentStatistic = itemView.findViewById(R.id.card_view_row_content_statistic)
        }

        override fun bind(position: Int) {
            bind<RowSchoolAmbassadorBinding> {
                schoolAmbassador = viewModel.getSchoolAmbassador(position)
            }
        }
    }
}
