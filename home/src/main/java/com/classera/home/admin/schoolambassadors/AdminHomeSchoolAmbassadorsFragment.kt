package com.classera.home.admin.schoolambassadors

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeSchoolAmbassadorsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    private var recyclerViewActiveUsers: RecyclerView? = null
    private var progressBarSchoolActiveUser: ProgressBar? = null

    override val layoutId: Int = R.layout.fragment_admin_home_school_ambassadors

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initViewModelListeners()
    }

    private fun findViews() {
        recyclerViewActiveUsers = view?.findViewById(R.id.recycler_view_admin_home_school_ambassador)
        progressBarSchoolActiveUser =
            view?.findViewById(R.id.progress_bar_fragment_home_dashboard_school_ambassador)
    }


    private fun initViewModelListeners() {
        viewModel.getSchoolAmbassadors().observe(this, this::handleSchoolAmbassadorsResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleSchoolAmbassadorsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSchoolAmbassadorsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSchoolAmbassadorsSuccessResource()
            }
            is Resource.Error -> {
                handleSchoolAmbassadorsErrorResource(resource)
            }
        }
    }

    private fun handleSchoolAmbassadorsLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarSchoolActiveUser?.visibility =
            View.VISIBLE else progressBarSchoolActiveUser?.visibility = View.GONE
    }

    private fun handleSchoolAmbassadorsSuccessResource() {
        val adapter =
            AdminHomeSchoolAmbassadorsAdapter(viewModel)
        recyclerViewActiveUsers?.adapter = adapter
    }

    private fun handleSchoolAmbassadorsErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewActiveUsers = null
        progressBarSchoolActiveUser = null
    }
}
