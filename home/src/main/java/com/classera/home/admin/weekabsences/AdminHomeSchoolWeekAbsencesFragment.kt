package com.classera.home.admin.weekabsences

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.ChartDayFormatter
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.admin.DayAbsences
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.snapics.screenshot.Screenshot
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@SuppressWarnings("MagicNumber")
class AdminHomeSchoolWeekAbsencesFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    @Inject
    lateinit var screenshot: Screenshot

    private var progressBarWeekAbsences: ProgressBar? = null
    private var imageViewWeekAbsencesDownload: ImageView? = null
    private var barChartWeekAbsences: BarChart? = null

    override val layoutId: Int = R.layout.fragment_admin_home_week_absenses

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initBarChart()
        initViewModelListeners()
        initViewListeners()
    }

    private fun findViews() {
        progressBarWeekAbsences = view?.findViewById(R.id.progress_bar_fragment_home_dashboard_school_week_absences)
        barChartWeekAbsences = view?.findViewById(R.id.bar_chart_admin_home_school_week_absences)
        imageViewWeekAbsencesDownload = view?.findViewById(R.id.image_view_admin_home_school_week_absences_download)
    }

    private fun initBarChart() {
        barChartWeekAbsences?.description?.isEnabled = false
        barChartWeekAbsences?.setMaxVisibleValueCount(7)
        barChartWeekAbsences?.setPinchZoom(false)
        barChartWeekAbsences?.isHovered = false
        barChartWeekAbsences?.setTouchEnabled(false)
        barChartWeekAbsences?.setDrawBarShadow(false)
        barChartWeekAbsences?.setDrawGridBackground(false)
        barChartWeekAbsences?.xAxis?.let { xAxis ->
            xAxis.position = XAxisPosition.BOTTOM
            xAxis.setDrawGridLines(false)
            xAxis.position = XAxisPosition.BOTTOM

            xAxis.setDrawGridLines(false)
            xAxis.granularity = 1f

            xAxis.labelCount = 7
            xAxis.textSize = 10f
            xAxis.valueFormatter = ChartDayFormatter(requireContext())
        }

        barChartWeekAbsences?.axisLeft?.setDrawGridLines(false)
        barChartWeekAbsences?.animateY(1500)
    }

    private fun initViewModelListeners() {
        viewModel.getWeekAbsences().observe(this, this::handleWeekAbsencesResource)
    }

    private fun initViewListeners() {
        imageViewWeekAbsencesDownload?.setOnClickListener {
            saveToGallary()
        }
    }

    private fun saveToGallary() {
        screenshot.take(this, barChartWeekAbsences!!)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleWeekAbsencesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleWeekAbsencesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleWeekAbsencesSuccessResource(resource as Resource.Success<BaseWrapper<List<DayAbsences>>>)
            }
            is Resource.Error -> {
                handleWeekAbsencesErrorResource(resource)
            }
        }
    }

    private fun handleWeekAbsencesLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarWeekAbsences?.visibility =
            View.VISIBLE else progressBarWeekAbsences?.visibility = View.GONE
    }

    private fun handleWeekAbsencesSuccessResource(success: Resource.Success<BaseWrapper<List<DayAbsences>>>) {
        success.data?.data?.let { weekAbsences ->
            val values = ArrayList<BarEntry>()

            weekAbsences.forEach { dayAbsences ->
                values.add(
                    BarEntry(
                        dayAbsences.day.toFloat(),
                        dayAbsences.count.toFloat()
                    )
                )
            }

            val dataSet =
                BarDataSet(values, getString(R.string.label_admin_home_fragment_school_week_absences))
            dataSet.setColors(*ColorTemplate.VORDIPLOM_COLORS)
            dataSet.setDrawValues(true)

            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(dataSet)
            val data = BarData(dataSets)
            barChartWeekAbsences?.data = data
            barChartWeekAbsences?.setFitBars(true)
            barChartWeekAbsences?.invalidate()
            barChartWeekAbsences?.data?.dataSets?.forEach { it.setDrawValues(true) }
            barChartWeekAbsences?.invalidate()
        }
    }

    private fun handleWeekAbsencesErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        barChartWeekAbsences = null
        progressBarWeekAbsences = null
        imageViewWeekAbsencesDownload = null
    }
}
