package com.classera.home.other

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class OtherRolesHomeFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: OtherRolesHomeViewModelFactory
        ): OtherRolesHomeViewModel {
            return ViewModelProvider(fragment, factory)[OtherRolesHomeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: OtherRolesHomeFragment): Fragment
}
