package com.classera.home.other.models

import com.classera.data.models.user.UserRole
import com.classera.home.R

class FloorSupervisorRoleItems : OtherRolesItem() {

    override val userRole: UserRole
        get() = UserRole.FLOOR_SUPERVISOR

    override val shortcutIcons: Array<Int>
        get() = arrayOf(
            R.drawable.ic_events,
            R.drawable.ic_take_attendance
        )

    override val shortcutColors: Array<Int>
        get() = arrayOf(
            R.color.color_events,
            R.color.color_take_attendance
        )

    override val shortcutLabels: Array<Int>
        get() = arrayOf(
            R.string.title_row_dashboard_shortcut_events,
            R.string.title_row_dashboard_shortcut_edit_attendance
        )

    override val shortcutKeys: Array<String>
        get() = arrayOf(
            "events",
            "attendance"
        )

    override val directions: Map<String, Int>
        get() = mapOf(
            "events" to R.id.item_menu_activity_main_navigation_view_calendar,
            "attendance" to R.id.item_menu_activity_main_navigation_view_course_edit_attendance
        )
}
