package com.classera.home.student

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.databinding.RowDashboardShortcutBinding

/**
 * Project: Classera
 * Created: Dec 21, 2019
 *
 * @author Mohamed Hamdan
 */
class StudentHomeAdapter(private val viewModel: StudentHomeViewModel) : BaseAdapter<StudentHomeAdapter.ViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDashboardShortcutBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getShortcutCount()
    }

    inner class ViewHolder(binding: RowDashboardShortcutBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowDashboardShortcutBinding> {
                shortcut = viewModel.getShortcut(position)
            }
        }
    }
}
