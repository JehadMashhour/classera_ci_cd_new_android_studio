package com.classera.home.student

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class StudentHomeFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: StudentHomeViewModelFactory
        ): StudentHomeViewModel {
            return ViewModelProvider(fragment, factory)[StudentHomeViewModel::class.java]
        }
    }


    @Binds
    abstract fun bindActivity(activity: StudentHomeFragment): Fragment
}
