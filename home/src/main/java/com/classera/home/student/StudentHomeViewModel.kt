package com.classera.home.student

import android.app.Application
import android.content.res.Resources
import android.os.Bundle
import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.core.utils.android.toColorResource
import com.classera.core.utils.android.toDrawableResource
import com.classera.core.utils.android.toStringResource
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.DashboardShortcut
import com.classera.data.models.home.Key
import com.classera.data.models.home.StudentHomeResponse
import com.classera.data.models.user.User
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.repositories.vcr.VcrRepository
import com.classera.home.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Suppress("MagicNumber")
class StudentHomeViewModel(
    application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val vcrRepository: VcrRepository,
    private val prefs: Prefs
) : BaseAndroidViewModel(application), LifecycleObserver {

    private var directions = mapOf(
        "homework" to R.id.item_menu_activity_main_navigation_view_assignments,
        "exam" to R.id.item_menu_activity_main_navigation_view_exams,
        "discussion" to R.id.item_menu_activity_main_navigation_view_discussion_rooms,
        "vcr" to R.id.item_menu_activity_main_navigation_view_virtual_class_room,
        "video" to R.id.item_menu_activity_main_navigation_view_video_lecture,
        "assessments" to R.id.item_menu_activity_main_navigation_view_assessments,
        "report" to R.id.item_menu_activity_main_navigation_view_report_cards,
        "events" to R.id.item_menu_activity_main_navigation_view_calendar
    )

    private var arguments = mapOf(
        "exam" to bundleOf("selectedFilter" to "Exam")
    )

    private var shortcuts: MutableList<DashboardShortcut>? = null

    private var vcrList: MutableList<VcrResponse?> = mutableListOf()

    private val _userProgressLiveData = MutableLiveData<Float>()
    var userProgressLiveData: LiveData<Float> = _userProgressLiveData

    private val _directionLiveData = MutableLiveData<Pair<Int, Bundle?>>()
    var directionLiveData: LiveData<Pair<Int, Bundle?>> = _directionLiveData

    private val _ratioIndicatorVisibility = MutableLiveData<Int>()
    var ratioIndicatorVisibility: LiveData<Int> = _ratioIndicatorVisibility

    var userLiveData = MutableLiveData<User>()


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getLocalUser().collect { users ->
                val user = users?.firstOrNull()
                val ratio = user?.scoreInfo?.userRatio ?: 0f
                if (ratio < MIN_RATIO) {
                    _ratioIndicatorVisibility.postValue(Gravity.END)
                } else {
                    _ratioIndicatorVisibility.postValue(Gravity.START)
                }
                userLiveData.postValue(user)

                val screenWidth = Resources.getSystem().displayMetrics.widthPixels.toFloat()
                _userProgressLiveData.postValue(screenWidth * "${ratio * 0.01}".toFloat())
            }
        }
    }

    fun getShortcuts() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { homeRepository.getStudentHome() }
        shortcuts = parseData(resource.element<BaseWrapper<StudentHomeResponse>>()?.data)
            ?.toMutableList()
        if(isAssessmentBlocked()) {
            shortcuts?.removeAt(7)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun isAssessmentBlocked(): Boolean {
        return prefs.isAssessmentBlocked == IS_ASSESSMENT_BLOCKED
    }

    private fun parseData(data: StudentHomeResponse?): List<DashboardShortcut>? {
        val fields = data?.javaClass?.declaredFields
        val groupedFields = fields?.groupBy {
            val key = it.getAnnotation(Key::class.java)
            key?.value to key?.order
        }
        return groupedFields?.map { entry ->
            val title = SHORTCUT_TITLE_IDENTIFIER.format(Locale.ENGLISH, entry.key.first)
                .toStringResource(application)
            val icon = SHORTCUT_ICON_IDENTIFIER.format(Locale.ENGLISH, entry.key.first)
                .toDrawableResource(application)
            val color = SHORTCUT_COLOR_IDENTIFIER.format(Locale.ENGLISH, entry.key.first)
                .toColorResource(application)

            val percentageField = entry.value[1]
            percentageField.isAccessible = true
            val progress = percentageField.get(data) as Int

            val countField = entry.value.first()
            countField.isAccessible = true
            val count = countField.get(data) as Int

            DashboardShortcut(
                title = title,
                icon = icon,
                color = ContextCompat.getColor(application, color),
                progress = progress,
                count = count,
                order = entry.key.second,
                key = entry.key.first
            )
        }?.sortedBy { it.order }
    }

    fun getShortcutCount(): Int {
        return shortcuts?.size ?: 0
    }

    fun getShortcut(position: Int): DashboardShortcut? {
        return shortcuts?.get(position)
    }

    fun onShortcutClicked(position: Int) {
        val direction = directions[shortcuts?.get(position)?.key]
        if (direction != null) {
            _directionLiveData.value = direction to arguments[shortcuts?.get(position)?.key]
        }
    }

    fun isRTL(): Boolean {
        return prefs.language == "ar"
    }


    fun getVcrCount(): Int {
        return vcrList.size
    }

    fun getVcr(position: Int): VcrResponse? {
        return vcrList[position]
    }


    fun getUpcomingVCRs(
    ) = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource = tryResource { vcrRepository.getUpcomingVCRs() }

        vcrList.addAll(
            resource.element<BaseWrapper<List<VcrResponse>>>()?.data ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getUpcomingVcrUrl(vcrId: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            vcrRepository.getUpcomingVcrUrl(vcrId)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }


    private companion object {

        private const val MIN_RATIO = 20
        private const val IS_ASSESSMENT_BLOCKED = "1"
        private const val SHORTCUT_TITLE_IDENTIFIER = "title_row_dashboard_shortcut_%s"
        private const val SHORTCUT_ICON_IDENTIFIER = "ic_%s"
        private const val SHORTCUT_COLOR_IDENTIFIER = "color_%s"
    }
}
