package com.classera.home.student

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class StudentHomeViewModelFactory @Inject constructor(
    private val application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val vcrRepository: VcrRepository,
    private val prefs: Prefs
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StudentHomeViewModel(
            application,
            userRepository,
            homeRepository,
            vcrRepository,
            prefs
        ) as T
    }
}
