package com.classera.home.teacher

import android.app.Application
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.DashboardShortcut
import com.classera.data.models.home.TeacherHomeResponse
import com.classera.data.models.user.User
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.repositories.vcr.VcrRepository
import com.classera.home.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class TeacherHomeViewModel(
    application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val vcrRepository: VcrRepository,
) : BaseAndroidViewModel(application), LifecycleObserver {

    private val shortcutIcons = arrayOf(
        R.drawable.ic_assignments,
        R.drawable.ic_vcr,
        R.drawable.ic_discussion,
        R.drawable.ic_events,
        R.drawable.ic_take_attendance,
        R.drawable.ic_behaviours
    )

    private val shortcutColors = arrayOf(
        R.color.color_assignments,
        R.color.color_vcr,
        R.color.color_discussion,
        R.color.color_events,
        R.color.color_take_attendance,
        R.color.color_behaviors
    )

    private val shortcutLabels = arrayOf(
        R.string.title_row_dashboard_shortcut_take_assignments,
        R.string.title_row_dashboard_shortcut_vcr,
        R.string.title_row_dashboard_shortcut_discussion,
        R.string.title_row_dashboard_shortcut_events,
        R.string.title_row_dashboard_shortcut_take_attendance,
        R.string.title_row_dashboard_shortcut_take_behaviors
    )

    private val shortcutKeys = arrayOf(
        "assignments",
        "vcr",
        "discussion",
        "events",
        "attendance",
        "behaviors"
    )

    private val directions = mapOf(
        "assignments" to R.id.item_menu_activity_main_navigation_view_assignments,
        "vcr" to R.id.item_menu_activity_main_navigation_view_virtual_class_room,
        "discussion" to R.id.item_menu_activity_main_navigation_view_discussion_rooms,
        "events" to R.id.item_menu_activity_main_navigation_view_calendar,
        "attendance" to R.id.item_menu_activity_main_navigation_view_course_take_attendance,
        "behaviors" to R.id.item_menu_activity_main_navigation_view_discipline_and_behavior
    )

    private val arguments = mapOf(
        "assignments" to bundleOf("selectedFilter" to "Homework")
    )

    private var shortcuts: List<DashboardShortcut>? = null

    private var vcrList: MutableList<VcrResponse?> = mutableListOf()

    private val _directionLiveData = MutableLiveData<Pair<Int, Bundle?>>()
    val directionLiveData: LiveData<Pair<Int, Bundle?>> = _directionLiveData

    private val _userLiveData = MutableLiveData<User>()
    val userLiveData: LiveData<User> = _userLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getLocalUser().collect { users ->
                val user = users?.firstOrNull()
                _userLiveData.postValue(user)
            }
        }
    }

    fun getShortcuts() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { homeRepository.getTeacherHome() }
        shortcuts = parseData(resource.element<BaseWrapper<TeacherHomeResponse>>()?.data)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun parseData(data: TeacherHomeResponse?): List<DashboardShortcut>? {
        val result = mutableListOf<DashboardShortcut>()
        shortcutLabels.forEachIndexed { index, label ->
            val dashboardShortcut = if (label == R.string.title_row_dashboard_shortcut_events) {
                DashboardShortcut(
                    label,
                    shortcutIcons[index],
                    ContextCompat.getColor(application, shortcutColors[index]),
                    data?.eventsPercentage ?: 0,
                    data?.unviewedEventsCount ?: 0,
                    Int.MAX_VALUE,
                    shortcutKeys[index]
                )
            } else {
                DashboardShortcut(
                    label,
                    shortcutIcons[index],
                    ContextCompat.getColor(application, shortcutColors[index]),
                    MAX_PROGRESS,
                    Int.MIN_VALUE,
                    Int.MAX_VALUE,
                    shortcutKeys[index]
                )
            }
            result.add(dashboardShortcut)
        }
        return result
    }

    fun getShortcutCount(): Int {
        return shortcuts?.size ?: 0
    }

    fun getShortcut(position: Int): DashboardShortcut? {
        return shortcuts?.get(position)
    }

    fun onShortcutClicked(position: Int) {
        val direction = directions[shortcuts?.get(position)?.key]
        if (direction != null) {
            _directionLiveData.value = direction to arguments[shortcuts?.get(position)?.key]
        }
    }

    fun getVcrCount(): Int {
        return vcrList.size
    }


    fun getVcr(position: Int): VcrResponse? {
        return vcrList[position]
    }


    fun getUpcomingVCRs() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource = tryResource { vcrRepository.getUpcomingVCRs() }

        vcrList.addAll(
            resource.element<BaseWrapper<List<VcrResponse>>>()?.data ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getUpcomingVcrUrl(vcrId: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            vcrRepository.getUpcomingVcrUrl(vcrId)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }


    private companion object {

        private const val MAX_PROGRESS = 100
    }
}
