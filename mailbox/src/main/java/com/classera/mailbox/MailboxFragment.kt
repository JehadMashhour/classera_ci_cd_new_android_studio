package com.classera.mailbox


import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.classera.core.Screen
import com.classera.core.custom.views.CustomViewPager
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout

/**
 * Project: Classera
 * Created: Dec 6, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Mailbox")
class MailboxFragment : BaseFragment() {

    private var viewpager: CustomViewPager? = null
    private var tabLayout: TabLayout? = null
    private var searchView: SearchView? = null
    private var viewpagerAdapter: MailboxViewPagerAdapter? = null
    private var composeFAB: FloatingActionButton? = null

    override val layoutId: Int = R.layout.fragment_mailbox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        prepareViews()
        initListeners()
    }

    private fun initListeners() {
        composeFAB?.setOnClickListener {
            findNavController().navigate(
                MailboxFragmentDirections.mailboxComposeDirection(
                    false,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                )
            )
        }

        viewpager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
                //No Impl
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                //No Impl
            }
            override fun onPageSelected(position: Int) {
                (viewpager?.adapter?.instantiateItem(
                    viewpager!!,
                    viewpager?.currentItem!!
                ) as MailboxViewPagerFragment)
                    .search(searchView?.query.toString())
            }

        })
    }

    private fun findViews() {
        viewpager = view?.findViewById(R.id.mailbox_viewpager)
        composeFAB = view?.findViewById(R.id.mailbox_fab_compose)
        tabLayout = view?.findViewById(R.id.mailbox_tab_layout)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_mailbox, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_mailbox)
        searchView = (searchMenuItem.actionView as? SearchView?)

        searchView?.onDebounceQueryTextChange {
            (viewpager?.adapter?.instantiateItem(
                viewpager!!,
                viewpager?.currentItem!!
            ) as MailboxViewPagerFragment)
                .search(it.toString())
        }

        searchView?.setOnCloseListener {
            return@setOnCloseListener false
        }
    }

    private fun prepareViews() {
        // disable swiping between pages
        viewpager?.isEnabled = false
        viewpagerAdapter = MailboxViewPagerAdapter(childFragmentManager, requireContext())
        viewpager!!.adapter = viewpagerAdapter
        tabLayout!!.setupWithViewPager(viewpager)
        hideKeyboard()
    }

    override fun onStart() {
        searchView?.isFocusable = false
        searchView?.hideKeyboard()
        super.onStart()
    }

    override fun onResume() {
        searchView?.isFocusable = false
        searchView?.hideKeyboard()
        searchView?.setQuery("", false)
        searchView?.clearFocus()
        searchView?.isIconified = true
        super.onResume()
    }

    override fun onDestroyView() {
        viewpager = null
        tabLayout = null
        viewpagerAdapter = null
        searchView = null
        composeFAB = null
        super.onDestroyView()
    }


}
