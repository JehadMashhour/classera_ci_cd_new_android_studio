package com.classera.mailbox

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.mailbox.MailboxRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.mailbox.compose.ComposeViewModel
import com.classera.mailbox.details.MailboxDetailsViewModel
import com.classera.mailbox.draft.DraftViewModel
import com.classera.mailbox.inbox.InboxViewModel
import com.classera.mailbox.outbox.OutboxViewModel
import com.classera.mailbox.recipients.RecipientViewModel
import com.classera.mailbox.trash.TrashViewModel
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Suppress("ComplexMethod")
class MailboxViewModelFactory @Inject constructor(
    private val mailboxRepository: MailboxRepository,
    private val userRepository: UserRepository,
    private val mailboxDao: LocalMailboxDao,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MailboxViewModel::class.java -> MailboxViewModel(mailboxRepository) as T
            MailboxDetailsViewModel::class.java -> MailboxDetailsViewModel(
                mailboxRepository,
                userRepository
            ) as T
            InboxViewModel::class.java -> InboxViewModel(mailboxRepository) as T
            OutboxViewModel::class.java -> OutboxViewModel(mailboxRepository) as T
            RecipientViewModel::class.java -> RecipientViewModel(mailboxRepository, prefs) as T
            TrashViewModel::class.java -> TrashViewModel(mailboxRepository, userRepository) as T
            DraftViewModel::class.java -> DraftViewModel(mailboxRepository) as T
            ComposeViewModel::class.java -> ComposeViewModel(mailboxRepository, mailboxDao) as T
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }
    }

}
