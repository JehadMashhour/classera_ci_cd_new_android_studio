package com.classera.mailbox

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.classera.mailbox.draft.DraftFragment
import com.classera.mailbox.inbox.InboxFragment
import com.classera.mailbox.outbox.OutboxFragment
import com.classera.mailbox.trash.TrashFragment

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Suppress("MagicNumber")
class MailboxViewPagerAdapter(
    fragmentManager: FragmentManager,
    private val context: Context
) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        var returnedFragment = Fragment()
        when (position) {
            0 -> returnedFragment = InboxFragment.newInstance()
            1 -> returnedFragment = OutboxFragment.newInstance()
            2 -> returnedFragment = TrashFragment.newInstance()
            3 -> returnedFragment = DraftFragment.newInstance()
        }

        return returnedFragment
    }

    override fun getCount() = 4

    override fun getPageTitle(position: Int): CharSequence? {
        var title: CharSequence = "Unknown" // default
        when (position) {
            0 -> title = context.getString(R.string.inbox)
            1 -> title = context.getString(R.string.outbox)
            2 -> title = context.getString(R.string.trash)
            3 -> title = context.getString(R.string.draft)
        }

        return title
    }

}
