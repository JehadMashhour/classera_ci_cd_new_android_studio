package com.classera.mailbox

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface MailboxViewPagerFragment {

    fun search(searchValue: String)

}
