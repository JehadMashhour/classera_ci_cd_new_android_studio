package com.classera.mailbox.compose

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isEmpty
import androidx.lifecycle.observe
import androidx.navigation.navArgs
import com.classera.core.Screen
import com.classera.core.activities.BaseValidationActivity
import com.classera.core.custom.views.CustomChip
import com.classera.core.utils.android.hideKeyboard
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.network.errorhandling.Resource
import com.classera.mailbox.R
import com.classera.mailbox.compose.BackComposeBottomSheet.Companion.DELETE
import com.classera.mailbox.compose.BackComposeBottomSheet.Companion.SAVE
import com.classera.mailbox.recipients.RecipientActivity
import com.google.android.material.chip.ChipGroup
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 12, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Compose")
class ComposeActivity : BaseValidationActivity() {

    @Inject
    lateinit var viewModel: ComposeViewModel

    @NotEmpty(message = "validation_activity_compose_subject")
    private var editTextSubject: EditText? = null

    @NotEmpty(message = "validation_activity_compose_body")
    private var editTextBody: EditText? = null

    @NotEmpty(message = "validation_activity_compose_recipient")
    private var editTextRecipients: EditText? = null

    private var chipGroup: ChipGroup? = null

    private var toTextView: AppCompatTextView? = null
    private var prioritySwitch: SwitchMaterial? = null
    private var addRecipient: AppCompatImageView? = null
    private var textInputLayoutRecipients: TextInputLayout? = null

    // navigation args
    private var isReply: Boolean? = null
    private var msgId: String? = null
    private var msgTitle: String? = null
    private var recipientId: String? = null
    private var recipientName: String? = null
    private var recipientImgUrl: String? = null

    private var attachmentList: ArrayList<MediaFile?>? = null
    private var textViewAttachmentName: TextView? = null

    private var isSending: Boolean = false // default (prevent user from multiple clicks)
    private var recipients: MutableList<RecipientByRole>? = null

    private var sendButtonMenuItem: MenuItem? = null

    private var attachmentButtonMenuItem: MenuItem? = null

    private var messageType: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compose)
        findViews()
        initListener()
        extractArgs()
        prepareMsg()
    }

    override fun onResume() {
        super.onResume()
        prepareMsg()
    }

    private fun extractArgs() {
        val args: ComposeActivityArgs by navArgs()

        isReply = args.isReply
        msgId = args.messageId
        msgTitle = args.messageTitle
        recipientId = args.recipientId
        recipientName = args.recipientName
        recipientImgUrl = args.recipientImgUrl
        editTextBody?.setText(args.messageBody ?: "")
        editTextSubject?.setText(args.messageTitle ?: "")
        title = if (args.messageTitle == null) {
            getString((R.string.title_menu_compose))
        } else {
            msgTitle
        }
    }

    private fun findViews() {
        editTextSubject = findViewById(R.id.compose_subject_edit_text)
        editTextBody = findViewById(R.id.compose_message_edit_text)
        chipGroup = findViewById(R.id.compose_chip_group)
        toTextView = findViewById(R.id.compose_to)
        prioritySwitch = findViewById(R.id.compose_priority_switch)
        addRecipient = findViewById(R.id.compose_add_recipient)
        textViewAttachmentName = findViewById(R.id.text_view_fragment_compose_message_file_name)
        textInputLayoutRecipients = findViewById(R.id.text_input_layout_activity_compose_recipients)
        editTextRecipients = findViewById(R.id.edit_text_activity_compose_recipients)
    }

    private fun initListener() {
        toTextView?.setOnClickListener {
            startActivity(
                Intent(this, RecipientActivity::class.java).putParcelableArrayListExtra(
                    "recipients",
                    ArrayList<RecipientByRole>(recipients ?: mutableListOf())
                )
            )
        }

        addRecipient?.setOnClickListener {
            startActivity(
                Intent(this, RecipientActivity::class.java).putParcelableArrayListExtra(
                    "recipients",
                    ArrayList<RecipientByRole>(recipients ?: mutableListOf())
                )
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fragment_compose_reply, menu)
        sendButtonMenuItem = menu?.findItem(R.id.item_menu_compose)
        attachmentButtonMenuItem = menu?.findItem(R.id.item_menu_attachment)
        initVisibility()
        return true
    }

    @Suppress("ComplexMethod")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> if (hasBackButton()) {
                viewModel.deleteAllLocalRecipients()
                onBackPressed()
            }
            sendButtonMenuItem?.itemId -> {
                if (!isSending) {
                    if(chipGroup?.isEmpty() != false){
                        textInputLayoutRecipients?.visibility = View.VISIBLE
                    }else{
                        textInputLayoutRecipients?.visibility = View.GONE
                    }
                    validator.validate()
                }
            }
            R.id.item_menu_attachment -> {
                val intent = Intent(this, FilePickerActivity::class.java)
                intent.putExtra(
                    FilePickerActivity.CONFIGS, Configurations.Builder()
                        .setMaxSelection(-1)
                        .setCheckPermission(true)
                        .setShowFiles(true)
                        .setShowImages(true)
                        .setShowAudios(true)
                        .setShowVideos(true)
                        .enableImageCapture(false)
                        .setSkipZeroSizeFiles(true)
                        .build()
                )
                startActivityForResult(intent, ATTACH_ATTACHMENT_REQUEST_CODE)
            }

            R.id.item_menu_draft -> {
                if ((editTextSubject?.text.toString()
                        .isNotEmpty() || editTextSubject?.text.toString()
                        .isNotBlank()) && (editTextBody?.text.toString()
                        .isNotEmpty() || editTextBody?.text.toString().isNotBlank())
                ) {
                    if (!isSending) {
                        messageType = MESSAGE_TYPE_DRAFT
                        onValidationSucceeded()
                    }
                } else {
                    validator.validate()
                    removeError(editTextRecipients?.parent?.parent as TextInputLayout)
                }
            }
        }
        return true
    }

    private fun initVisibility() {
        if (isReply == true) {
            attachmentButtonMenuItem?.isVisible = false
        }
    }

    override fun onValidationSucceeded() {
        textInputLayoutRecipients?.visibility = View.GONE
        hideKeyboard()

        val msgTitle = editTextSubject?.text.toString()
        val msgBody = editTextBody?.text.toString()

        val attachmentPathList = ArrayList<String>()
        attachmentList?.forEachIndexed { index, value ->
            attachmentPathList.add(index, value?.path ?: "")
        }

        val attachmentMimeTypeList = ArrayList<String>()
        attachmentList?.forEachIndexed { index, value ->
            attachmentMimeTypeList.add(
                index,
                value?.mimeType ?: ""
            )
        }

        if (isReply == true) {
            Toast.makeText(
                this,
                getString(R.string.title_message_sending), Toast.LENGTH_SHORT
            ).show()
            viewModel.replyMessage(
                msgId!!,
                msgTitle,
                msgBody,
                recipientId!!
            ).observe(this, this::handleResource)
        } else {
            if (messageType != MESSAGE_TYPE_DRAFT) {
                messageType = MESSAGE_TYPE_SENT
                Toast.makeText(
                    this,
                    getString(R.string.title_message_sending), Toast.LENGTH_SHORT
                ).show()
            }
            messageType?.let {
                viewModel.sendMessage(
                    msgTitle,
                    msgBody,
                    if (prioritySwitch?.isChecked!!) "1" else "0",
                    attachmentPathList,
                    attachmentMimeTypeList,
                    it
                ).observe(this, this::handleResource)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun prepareMsg() {
        chipGroup?.removeAllViews() // clear chip group
        if (isReply == true) {
            // add recipient
            val chip =
                CustomChip.createImageChip(this, recipientName ?: "Unknown", recipientImgUrl ?: "")
            chipGroup?.addView(chip)
            // add title
            editTextSubject?.setText("RE: $msgTitle")
            // set focus on body
            editTextBody?.requestFocus()
            // hide add recipient option
            addRecipient?.visibility = View.GONE
        } else {
            // add recipients
            recipients = viewModel.getLocalRecipientUsers()
            recipients?.forEach {
                addChip(it)
            }
        }
    }

    private fun addChip(recipient: RecipientByRole) {
        val chip = CustomChip.createCancellableChip(
            this,
            recipient.fullName ?: "Unknown",
            recipient.id
        )

        chip.setOnCloseIconClickListener {
            recipient.checked = false
            viewModel.deleteRecipient(recipient)
            recipients?.remove(recipient)
            chipGroup?.removeView(chip)
        }

        chipGroup?.addView(chip)
        textInputLayoutRecipients?.visibility = View.GONE
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        if (messageType == MESSAGE_TYPE_DRAFT) {
            Toast.makeText(
                this, resources.getString(R.string.validation_activity_compose_draft_msg),
                Toast.LENGTH_LONG
            ).show()
        } else {
            Toast.makeText(
                this, resources.getString(R.string.validation_activity_compose_sent_msg),
                Toast.LENGTH_LONG
            ).show()
        }
        isSending = false

        if (isReply == false)
            viewModel.deleteAllLocalRecipients()

        viewModel.deleteAllLocalRecipients()
        super.onBackPressed()
    }

    override fun onBackPressed() {
        BackComposeBottomSheet.show((this as AppCompatActivity).supportFragmentManager) {
            when (it) {
                SAVE -> {
                    messageType = MESSAGE_TYPE_DRAFT
                    onValidationSucceeded()
                }
                DELETE -> {
                    viewModel.deleteAllLocalRecipients()
                    super.onBackPressed()
                }
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        isSending = resource.show
        sendButtonMenuItem?.isEnabled = false
        sendButtonMenuItem?.icon?.alpha = SEND_BUTTON_ICON_ALPHA_BACKGROUND
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(this, resource.error.message, Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ATTACH_ATTACHMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val images = data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            textViewAttachmentName?.text = getString(
                R.string.title_attachment_name,
                images?.joinToString(",\n") { it.name }
            )
            attachmentList = images
        }
    }

    override fun onDestroy() {
        editTextSubject = null
        editTextBody = null
        chipGroup = null
        super.onDestroy()
    }

    private companion object {

        private const val ATTACH_ATTACHMENT_REQUEST_CODE = 104
        private const val SEND_BUTTON_ICON_ALPHA_BACKGROUND = 130
        private const val MESSAGE_TYPE_DRAFT = "draft"
        private const val MESSAGE_TYPE_SENT = "sent"
    }
}
