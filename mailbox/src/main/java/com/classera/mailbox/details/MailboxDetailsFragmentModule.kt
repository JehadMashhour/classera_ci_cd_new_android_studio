package com.classera.mailbox.details

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class MailboxDetailsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MailboxViewModelFactory
        ): MailboxDetailsViewModel {
            return ViewModelProvider(fragment, factory)[MailboxDetailsViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: MailboxDetailsFragment): Fragment

}
