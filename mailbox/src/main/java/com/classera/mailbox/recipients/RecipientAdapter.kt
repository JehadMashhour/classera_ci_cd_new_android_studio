package com.classera.mailbox.recipients

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.mailbox.databinding.RecipientListRowItemBinding

/**
 * Project: Classera
 * Created: Jan 10, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class RecipientAdapter(
    private val viewModel: RecipientViewModel
) : BaseAdapter<RecipientAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RecipientListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getFilteredRemoteRecipientUsersSize()
    }

    inner class ViewHolder(binding: RecipientListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<RecipientListRowItemBinding> {
                this.role = viewModel.currentRole?:""
                if(viewModel.getFilteredRemoteRecipientUsersSize() > 0)
                    this.recipient = viewModel.getFilteredRemoteRecipientUsers()[position]
            }
        }
    }

}
