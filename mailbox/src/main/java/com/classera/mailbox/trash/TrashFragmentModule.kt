package com.classera.mailbox.trash

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import com.classera.mailbox.outbox.OutboxFragment
import com.classera.mailbox.outbox.OutboxViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class TrashFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MailboxViewModelFactory
        ): TrashViewModel {
            return ViewModelProvider(fragment, factory)[TrashViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: TrashFragment): Fragment

}
