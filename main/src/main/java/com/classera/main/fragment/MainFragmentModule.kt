package com.classera.main.fragment

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.main.MainViewModel
import com.classera.main.MainViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class MainFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MainViewModelFactory
        ): MainViewModel {
            return ViewModelProvider(fragment, factory)[MainViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: MainFragment): Fragment
}
