package com.classera.mycard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.mycard.MyCradRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
class MyCardViewModelFactory @Inject constructor(
    private val myCardRepository: MyCradRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyCardViewModel(myCardRepository) as T
    }
}
