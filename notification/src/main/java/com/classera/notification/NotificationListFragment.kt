package com.classera.notification

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.models.notification.NotificationMessage
import com.classera.data.network.CNS_DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.notigation.NavigationHandler
import com.google.gson.Gson
import javax.inject.Inject

@Suppress("MagicNumber")
@Screen("Notification List")
class NotificationListFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: NotificationListViewModel

    @Inject
    lateinit var navigationHandler: NavigationHandler

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: NotificationListAdapter? = null
    private var errorView: ErrorView? = null

    override val layoutId: Int = R.layout.fragment_notification_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        getNotificationList()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_notification_list)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_notification_list)
        swipeRefreshLayout =
            view?.findViewById(R.id.swipe_refresh_layout_fragment_notification_list)
        errorView = view?.findViewById(R.id.error_view_fragment_notification_list)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            getNotificationList(CNS_DEFAULT_PAGE)
        }
    }

    private fun getNotificationList(pageNumber: Int = CNS_DEFAULT_PAGE) {
        if (pageNumber == CNS_DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getUserNotifications(pageNumber).observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            recyclerView?.visibility = View.VISIBLE
            swipeRefreshLayout?.isRefreshing = false
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = NotificationListAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreNotificationListener(::getNotificationList)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { _, position ->
            viewModel.updateNotificationStatus(viewModel.getNotificationItem(position)?.uuid ?: "")
            handelNotificationNavigation(viewModel.getNotificationItem(position))
        }
    }

    private fun handelNotificationNavigation(notificationItem: NotificationMessage?) {
        notificationItem?.let {
            val builder = NavOptions.Builder()
                .setLaunchSingleTop(true)
            requireActivity().findNavController(com.classera.main.R.id.fragment_activity_main_navigation)
                .navigate(
                    R.id.item_menu_fragment_main_navigation_notification_list,
                    null,
                    builder.build()
                )
            navigationHandler.process(
                it.event.orEmpty(),
                Gson().toJson(it.body),
                requireActivity().findNavController(com.classera.main.R.id.fragment_activity_main_navigation)
            )
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getNotificationList() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
