package com.classera.notigation

import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.core.text.HtmlCompat
import androidx.navigation.NavController
import com.classera.data.models.notification.DiscussionCommentNotificationBody
import com.google.gson.Gson

class DiscussionCommentNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("Discussion Room Comment", true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val discussionCommentNotificationBody =
            Gson().fromJson(notificationBody, DiscussionCommentNotificationBody::class.java)
        navController.navigate(R.id.item_menu_activity_main_navigation_view_discussion_rooms)
        navController.navigate(
            R.id.item_menu_activity_main_navigation_view_discussion_room_details,
            Bundle().apply {
                putString("title", discussionCommentNotificationBody.roomTitle ?: "")
                putString("roomId", discussionCommentNotificationBody.roomId ?: "")
            })
        navController.navigate(
            R.id.item_menu_activity_main_navigation_view_discussion_room_comments,
            Bundle().apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    putString(
                        "title",
                        Html.fromHtml(discussionCommentNotificationBody.commentTitle ?: "",
                            HtmlCompat.FROM_HTML_MODE_LEGACY)
                            .toString()
                    )
                }else{
                    putString(
                        "title",
                        Html.fromHtml(discussionCommentNotificationBody.commentTitle ?: "")
                            .toString()
                    )
                }
                putString("postId", discussionCommentNotificationBody.postId ?: "")
                putString("closeValue", null)
                putString("ApproveValue", null)
            })
    }
}
