package com.classera.notigation

import androidx.navigation.NavController

interface NavigationHandler {

    fun process(
        event: String,
        notificationBody: String?,
        navController: NavController? = null
    ): Boolean
}
