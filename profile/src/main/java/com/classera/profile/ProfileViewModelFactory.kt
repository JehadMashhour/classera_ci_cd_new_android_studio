package com.classera.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.profile.education.EducationViewModel
import com.classera.profile.education.addEducation.AddEducationViewModel
import com.classera.profile.experiences.ExperiencesViewModel
import com.classera.profile.experiences.addExperiences.AddExperienceBottomSheet
import com.classera.profile.experiences.addExperiences.AddExperienceViewModel
import com.classera.profile.personal.PersonalViewModel
import com.classera.profile.personal.addcity.AddCountryCityViewModel
import com.classera.profile.skill.SkillsViewModel
import com.classera.profile.skill.addskill.AddModifySkillViewModel
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class ProfileViewModelFactory @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST", "ComplexMethod")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            ProfileViewModel::class.java -> ProfileViewModel() as T
            SkillsViewModel::class.java -> SkillsViewModel(profileRepository) as T
            AddModifySkillViewModel::class.java -> AddModifySkillViewModel(profileRepository) as T
            PersonalViewModel::class.java -> PersonalViewModel(profileRepository, userRepository, prefs) as T
            AddCountryCityViewModel::class.java -> AddCountryCityViewModel(profileRepository) as T
            EducationViewModel::class.java -> EducationViewModel(profileRepository) as T
            AddEducationViewModel::class.java -> AddEducationViewModel(profileRepository) as T
            ExperiencesViewModel::class.java -> ExperiencesViewModel(profileRepository) as T
            AddExperienceViewModel::class.java -> AddExperienceViewModel(profileRepository) as T
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }
    }

}
