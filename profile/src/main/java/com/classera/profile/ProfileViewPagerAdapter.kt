package com.classera.profile

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.classera.profile.education.EducationFragment
import com.classera.profile.experiences.ExperiencesFragment
import com.classera.profile.personal.PersonalFragment
import com.classera.profile.skill.SkillsFragment

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class ProfileViewPagerAdapter(
    fragmentManager: FragmentManager,
    private val context: Context
    ) : FragmentPagerAdapter(fragmentManager)  {

    override fun getItem(position: Int): Fragment {
        var returnedFragment = Fragment() // default
        when (position) {
            ZERO -> returnedFragment = PersonalFragment()
            ONE -> returnedFragment = SkillsFragment()
            TWO -> returnedFragment = EducationFragment()
            THREE -> returnedFragment = ExperiencesFragment()
        }

        return returnedFragment
    }

    override fun getCount() = FOUR

    override fun getPageTitle(position: Int): CharSequence? {
        var title: CharSequence = "Unknown" // default
        when (position) {
            ZERO -> title = context.getString(R.string.p_info)
            ONE -> title = context.getString(R.string.skills)
            TWO -> title = context.getString(R.string.education)
            THREE -> title = context.getString(R.string.title_tab_experiences)
        }

        return title
    }

    private companion object{
        private const val ZERO = 0
        private const val ONE = 1
        private const val TWO = 2
        private const val THREE = 3
        private const val FOUR = 4
    }

}
