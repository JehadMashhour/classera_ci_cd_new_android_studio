package com.classera.profile.education

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.SkillListRowItemBinding
import com.classera.profile.skill.SkillsViewModel

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class EducationAdapter(
    private val viewModel: EducationViewModel
) : BaseAdapter<EducationAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = EducationListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getEducationsCount()
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: EducationListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<EducationListRowItemBinding> {
                this.education = viewModel.getEducation(position)
            }
        }
    }
}
