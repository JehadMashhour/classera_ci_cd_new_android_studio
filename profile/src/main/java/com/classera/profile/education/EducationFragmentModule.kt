package com.classera.profile.education

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import com.classera.profile.skill.SkillsFragment
import com.classera.profile.skill.SkillsViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class EducationFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): EducationViewModel {
            return ViewModelProvider(fragment, factory)[EducationViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: EducationFragment): Fragment

}
