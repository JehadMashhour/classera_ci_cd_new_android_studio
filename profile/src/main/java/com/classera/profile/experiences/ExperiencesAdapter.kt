package com.classera.profile.experiences

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.profile.databinding.ExperiencesListRowItemBinding


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
class ExperiencesAdapter(
    private val viewModel: ExperiencesViewModel
) : BaseAdapter<ExperiencesAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ExperiencesListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getExperiencesCount()
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: ExperiencesListRowItemBinding) :
        BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<ExperiencesListRowItemBinding> {
                this.experiences = viewModel.getExperience(position)
            }
        }
    }
}
