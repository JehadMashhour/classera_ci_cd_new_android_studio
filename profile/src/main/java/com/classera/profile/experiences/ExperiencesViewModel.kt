package com.classera.profile.experiences

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.Experiences
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.profile.ProfileRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
class ExperiencesViewModel(private val repository: ProfileRepository) : BaseViewModel() {

    private var experiences: MutableList<Experiences>? = mutableListOf()

    fun getExperiences() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            repository.getUserExperiences()
        }

        experiences = resource.element<BaseWrapper<List<Experiences>>>()?.data?.toMutableList()

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun deleteExperience(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                val experience = getExperience(position)
                repository.deleteUserExperience(experience?.id)
            }
            emit(resource)
        }

    fun removeFromList(position: Int) {
        experiences?.removeAt(position)
    }

    fun getExperience(position: Int): Experiences?{
        return experiences?.get(position)
    }

    fun getExperiencesCount(): Int {
        return experiences?.size ?: 0
    }

}
