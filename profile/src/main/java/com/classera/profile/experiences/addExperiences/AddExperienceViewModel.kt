package com.classera.profile.experiences.addExperiences

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.profile.skill.addskill.MODE_CREATE
import com.classera.profile.skill.addskill.MODE_MODIFY
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */

class AddExperienceViewModel(private val repository: ProfileRepository) : BaseViewModel() {

    @Suppress("LongParameterList")
    fun addUserExperiences(
        mode: Int,
        id: String?,
        jobTitle: String?,
        jobType: String?,
        companyName: String?,
        startDate: String?,
        endDate: String?,
        description: String?
    ): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(true))

        val resource = tryNoContentResource {
            when (mode) {
                MODE_CREATE -> {
                    repository.addUserExperiences(
                        jobTitle,
                        jobType,
                        companyName,
                        startDate,
                        endDate,
                        description
                    )
                }

                MODE_MODIFY -> {
                    repository.editUserExperiences(
                        id,
                        jobTitle,
                        jobType,
                        companyName,
                        startDate,
                        endDate,
                        description
                    )
                }

                else -> throw IllegalStateException()
            }
        }
        emit(resource)
        emit(Resource.Loading(false))
    }

}
