package com.classera.profile.personal

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class PersonalFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): PersonalViewModel {
            return ViewModelProvider(fragment, factory)[PersonalViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: PersonalFragment): Fragment

}
