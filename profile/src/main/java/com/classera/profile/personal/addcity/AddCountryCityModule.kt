package com.classera.profile.personal.addcity

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.profile.ProfileViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class AddCountryCityModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ProfileViewModelFactory
        ): AddCountryCityViewModel {
            return ViewModelProvider(fragment, factory)[AddCountryCityViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: AddCountryCityBottomSheet): Fragment

}
