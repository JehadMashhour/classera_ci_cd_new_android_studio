package com.classera.profile.publicprofile

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class PublicProfileModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: PublicProfileViewModelFactory
        ): PublicProfileViewModel{
            return ViewModelProvider(activity, factory)[PublicProfileViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: PublicProfileActivity): AppCompatActivity
}
