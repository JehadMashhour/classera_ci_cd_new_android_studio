package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.LanguagesListRowItemBinding


class LanguagePublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<LanguagePublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = LanguagesListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.languages?.size!!
    }

    inner class ViewHolder(binding: LanguagesListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<LanguagesListRowItemBinding> {
                this.language = publicProfileData?.profile?.languages?.get(position)
            }
        }
    }
}
