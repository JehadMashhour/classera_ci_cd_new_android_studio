package com.classera.profile.publicprofile.adapter

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.classera.data.models.profile.PublicProfileWrapper
import kotlin.reflect.KClass

/**
 * Project: Classera
 * Created: Mar 08, 2020
 *
 * @author Mohamed Hamdan
 */
class ProfileTabProvider(
    private val kClass: KClass<out Fragment>,
    private val publicProfileWrapper: PublicProfileWrapper?
) {

    fun create(): Fragment {
        return kClass.java.newInstance().apply { arguments = bundleOf("public_profile" to publicProfileWrapper) }
    }
}
