package com.classera.profile.publicprofile.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.publicprofile.fragments.PublicProfileBadgesFragment
import com.classera.profile.publicprofile.fragments.PublicProfileEPortoflioFragment
import com.classera.profile.publicprofile.fragments.PublicProfileEducationFragment
import com.classera.profile.publicprofile.fragments.PublicProfileInterestsFragment
import com.classera.profile.publicprofile.fragments.PublicProfileLanguagesFragment
import com.classera.profile.publicprofile.fragments.PublicProfilePersonalFragment
import com.classera.profile.publicprofile.fragments.PublicProfileSkillsFragment
import com.classera.profile.publicprofile.fragments.PublicProfileWorkFragment

/**
 * Project: Classera
 * Created: March 7, 2020
 *
 * @author Saeed Halawani
 */
class PublicProfileViewPagerAdapter(
    activity: AppCompatActivity,
    publicProfileWrapper: PublicProfileWrapper?
) : FragmentStateAdapter(activity) {

    private val fragments = arrayOf(
        ProfileTabProvider(PublicProfilePersonalFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileEducationFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileWorkFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileInterestsFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileSkillsFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileLanguagesFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileEPortoflioFragment::class, publicProfileWrapper),
        ProfileTabProvider(PublicProfileBadgesFragment::class, publicProfileWrapper)
    )

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position].create()
    }
}
