package com.classera.profile.publicprofile.fragments

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.navigation.BR

/**
 * Project: Classera
 * Created: Mar 08, 2020
 *
 * @author Mohamed Hamdan
 */
abstract class BaseProfileFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = DataBindingUtil.bind<ViewDataBinding>(view)
        binding?.setVariable(BR.publicProfile, arguments?.getParcelable<PublicProfileWrapper>("public_profile"))
        binding?.executePendingBindings()
    }
}
