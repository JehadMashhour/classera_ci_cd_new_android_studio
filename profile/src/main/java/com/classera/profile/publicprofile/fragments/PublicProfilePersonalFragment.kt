package com.classera.profile.publicprofile.fragments

import com.classera.core.Screen
import com.classera.profile.R
/**
 * Project: Classera
 * Created: March 7, 2020
 *
 * @author Saeed Halawani
 */
@Screen("Public Profile Personal")
class PublicProfilePersonalFragment : BaseProfileFragment(R.layout.fragment_personal_public_profile)
