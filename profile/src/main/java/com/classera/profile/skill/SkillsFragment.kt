package com.classera.profile.skill

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.custom.callbacks.SwipeToDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.skill.addskill.AddModifySkillBottomSheet
import com.classera.profile.skill.addskill.MODE_CREATE
import com.classera.profile.skill.addskill.MODE_MODIFY
import com.github.florent37.expansionpanel.ExpansionHeader
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Skills")
class SkillsFragment : BaseFragment(), SkillFragmentRefresh {

    @Inject
    lateinit var viewModel: SkillsViewModel

    // skill
    private var skillExpansionHeader: ExpansionHeader? = null
    private var skillRecyclerView: RecyclerView? = null
    private var addSkill: AppCompatImageView? = null
    private var skillAdapter: SkillsAdapter? = null
    private var swipeToDeleteSkill: SwipeToDeleteCallback? = null
    // language
    private var languageExpansionHeader: ExpansionHeader? = null
    private var languageRecyclerView: RecyclerView? = null
    private var addLanguage: AppCompatImageView? = null
    private var languageAdapter: SkillsAdapter? = null
    private var swipeToDeleteLanguage: SwipeToDeleteCallback? = null
    // interest
    private var interestExpansionHeader: ExpansionHeader? = null
    private var interestRecyclerView: RecyclerView? = null
    private var addInterest: AppCompatImageView? = null
    private var interestAdapter: SkillsAdapter? = null
    private var swipeToDeleteInterest: SwipeToDeleteCallback? = null

    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var content: LinearLayout? = null
    private var dialog: AlertDialog? = null

    override val layoutId: Int = R.layout.fragment_skills

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        getData()
        initListeners()
        observeSwipeToDeleteListener()
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        content?.visibility = View.VISIBLE
        initAdapters()
        initSwipeToDeleteListener()
    }

    private fun initSwipeToDeleteListener() {

        if (viewModel.getInterestsCount() != 0) {
            val itemTouchHelperInterest = ItemTouchHelper(swipeToDeleteInterest!!)
            itemTouchHelperInterest.attachToRecyclerView(interestRecyclerView)
        }

        if (viewModel.getSkillsCount() != 0) {
            val itemTouchHelperSkill = ItemTouchHelper(swipeToDeleteSkill!!)
            itemTouchHelperSkill.attachToRecyclerView(skillRecyclerView)
        }

        if (viewModel.getLanguagesCount() != 0) {
            val itemTouchHelperLanguage = ItemTouchHelper(swipeToDeleteLanguage!!)
            itemTouchHelperLanguage.attachToRecyclerView(languageRecyclerView)
        }

    }

    private fun observeSwipeToDeleteListener() {

        swipeToDeleteInterest?.getSwiped()?.observe(this) { it ->
            if (it) {
                swipeToDeleteInterest?.updateSwipe(false)
                showConfirmToDeleteDialog(TYPE_INTEREST, swipeToDeleteInterest?.position!!)
            }
        }

        swipeToDeleteLanguage?.getSwiped()?.observe(this) {
            if (it) {
                swipeToDeleteLanguage?.updateSwipe(false)
                showConfirmToDeleteDialog(TYPE_LANGUAGE, swipeToDeleteLanguage?.position!!)
            }
        }

        swipeToDeleteSkill?.getSwiped()?.observe(this) {
            if (it) {
                swipeToDeleteSkill?.updateSwipe(false)
                showConfirmToDeleteDialog(TYPE_SKILL, swipeToDeleteSkill?.position!!)
            }
        }
    }

    private fun showConfirmToDeleteDialog(type: Int, position: Int) {
        dialog = AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.delete))
            .setMessage(
                getString(
                    R.string.confirm_delete
                )
            )
            .setPositiveButton(android.R.string.yes) { _, _ ->
                when (type) {
                    TYPE_INTEREST -> viewModel.delete(position, type).observe(this, this::handleDeleteInterestResource)
                    TYPE_SKILL -> viewModel.delete(position, type).observe(this, this::handleDeleteSkillResource)
                    else -> viewModel.delete(position, type).observe(this, this::handleDeleteLanguageResource)
                }
                dialog?.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                dialog?.dismiss()
            }
            .setOnDismissListener {
                interestAdapter?.notifyItemChanged(position)
                skillAdapter?.notifyItemChanged(position)
                languageAdapter?.notifyItemChanged(position)
            }
            .show()

    }

    private fun handleDeleteLanguageResource(resource: Resource) {
        dismissDialog()
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        } else {
            languageAdapter?.removeItem(swipeToDeleteLanguage?.position!!)
        }

    }

    private fun dismissDialog() {
        dialog?.dismiss()
    }

    private fun handleDeleteSkillResource(resource: Resource) {
        dismissDialog()
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
        } else {
            skillAdapter?.removeItem(swipeToDeleteSkill?.position!!)
        }
    }

    private fun handleDeleteInterestResource(resource: Resource) {
        dismissDialog()
        if (resource is Resource.Error) {
            if (resource.error.message == null) {
                Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            }
        } else {
            interestAdapter?.removeItem(swipeToDeleteInterest?.position!!)
        }

    }

    private fun initAdapters() {
        // interest
        interestAdapter = SkillsAdapter(viewModel, TYPE_INTEREST)
        interestRecyclerView?.adapter = interestAdapter
        interestAdapter?.setOnItemClickListener { _, position ->
            val interest = viewModel.getInterest(position)
            AddModifySkillBottomSheet(
                this,
                TYPE_INTEREST,
                MODE_MODIFY,
                interest?.interest,
                interest?.description,
                interest?.id
            ).show(fragmentManager!!, "")
        }

        // skill
        skillAdapter = SkillsAdapter(viewModel, TYPE_SKILL)
        skillRecyclerView?.adapter = skillAdapter
        skillAdapter?.setOnItemClickListener { _, position ->
            val skill = viewModel.getSkill(position)
            AddModifySkillBottomSheet(
                this,
                TYPE_SKILL,
                MODE_MODIFY,
                skill?.skill,
                skill?.skillLevel,
                skill?.id
            ).show(fragmentManager!!, "")
        }

        // language
        languageAdapter = SkillsAdapter(viewModel, TYPE_LANGUAGE)
        languageRecyclerView?.adapter = languageAdapter
        languageAdapter?.setOnItemClickListener { _, position ->
            val language = viewModel.getLanguage(position)
            AddModifySkillBottomSheet(
                this,
                TYPE_LANGUAGE,
                MODE_MODIFY,
                language?.language,
                language?.proficiency,
                language?.id
            ).show(fragmentManager!!, "")
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
            content?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        progressBar?.visibility = View.GONE
        errorView?.visibility = View.VISIBLE
        content?.visibility = View.GONE

        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getData() }
    }

    private fun initListeners() {
        addInterest?.setOnClickListener {
            add(TYPE_INTEREST)
        }

        addSkill?.setOnClickListener {
            add(TYPE_SKILL)
        }

        addLanguage?.setOnClickListener {
            add(TYPE_LANGUAGE)
        }
    }

    private fun add(type: Int) {
        AddModifySkillBottomSheet(
            this, type, MODE_CREATE, null, null, null
        ).show(fragmentManager!!, "")
    }

    private fun findViews() {
        skillExpansionHeader = view?.findViewById(R.id.profile_skill_expansion_header_skill)
        languageExpansionHeader = view?.findViewById(R.id.profile_skill_expansion_header_languages)
        interestExpansionHeader = view?.findViewById(R.id.profile_skill_expansion_header_interest)
        skillRecyclerView = view?.findViewById(R.id.profile_skill_rv_skill)
        languageRecyclerView = view?.findViewById(R.id.profile_skill_rv_language)
        interestRecyclerView = view?.findViewById(R.id.profile_skill_rv_interest)
        addSkill = view?.findViewById(R.id.profile_skill_add_skill)
        addLanguage = view?.findViewById(R.id.profile_skill_add_language)
        addInterest = view?.findViewById(R.id.profile_skill_add_interest)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_skills)
        errorView = view?.findViewById(R.id.error_view_fragment_skills)
        content = view?.findViewById(R.id.profile_skill_content)

        swipeToDeleteInterest = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT,
            context!!,
            interestRecyclerView
        )

        swipeToDeleteSkill = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT,
            context!!,
            skillRecyclerView
        )

        swipeToDeleteLanguage = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT,
            context!!,
            languageRecyclerView
        )
    }

    override fun refresh() {
        getData()
    }

    private fun getData() {
        viewModel.getInfo().observe(this, this::handleResource)
    }

    override fun onDestroyView() {
        skillExpansionHeader = null
        languageExpansionHeader = null
        interestExpansionHeader = null
        skillRecyclerView = null
        languageRecyclerView = null
        interestRecyclerView = null
        addSkill = null
        addLanguage = null
        addInterest = null
        progressBar = null
        errorView = null
        content = null
        super.onDestroyView()
    }

}
