package com.classera.reportcards

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject


@Screen("Report Cards")
class ReportCardsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ReportCardsViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: ReportCardsAdapter? = null
    private var errorView: ErrorView? = null

    private var clickedPosition: Int = 0

    override val layoutId: Int = R.layout.fragment_report_cards

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_report_cards)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_report_cards)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_report_cards)
        errorView = view?.findViewById(R.id.error_view_fragment_report_cards)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getReportCards()
        }
        getReportCards()
        viewModel.toastLiveData.observe(this) { Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show() }
    }

    private fun getReportCards() {
        viewModel.getReportCardsList().observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = ReportCardsAdapter(viewModel)
        adapter?.setOnItemClickListener { _, position ->
            clickedPosition = position
            if(requestDownloadReportPermission()) {
                viewModel.onCardClicked(position)
            }
        }
        recyclerView?.adapter = adapter
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getReportCards() }
        adapter?.finishLoading()
    }

    private fun requestDownloadReportPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requireActivity().let{
                ActivityCompat.requestPermissions(it,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    RC_STORAGE_PERMISSION)
            }
            false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == RC_STORAGE_PERMISSION
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            viewModel.onCardClicked(clickedPosition)
        } else {
            Toast.makeText(requireContext(),
                getString(R.string.message_storage_permission), Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    private companion object {

        private const val RC_STORAGE_PERMISSION = 101
    }
}
