package com.classera.reportcards

import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.BuildConfig
import com.classera.data.models.BaseWrapper
import com.classera.data.models.reportcards.ReportCard
import com.classera.data.models.reportcards.ReportCardType
import com.classera.data.models.reportcards.ReportCardsWrapper
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.reportcards.ReportCardsRepository
import com.classera.storage.Storage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*

class ReportCardsViewModel(
    private val context: Context,
    private val reportCardsRepository: ReportCardsRepository,
    private val storage: Storage,
    private val prefs: Prefs,
    private val customTabsIntent: CustomTabsIntent
) : BaseViewModel() {

    private var reportCardsList: MutableList<ReportCard> = mutableListOf()

    private val _toastLiveData: MutableLiveData<String> = MutableLiveData()
    val toastLiveData: LiveData<String> = _toastLiveData

    fun getReportCardsList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { reportCardsRepository.getReportCardsList() }
        val reportCards = resource.element<BaseWrapper<ReportCardsWrapper>>()?.data?.reportCards
            ?: mutableListOf<ReportCard>()
        reportCardsList.clear()
        reportCardsList.addAll(reportCards)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getReportCard(position: Int): ReportCard {
        return reportCardsList[position]
    }

    fun getReportCardsCount(): Int {
        return reportCardsList.size
    }

    private fun getDownloadPdfUrl(): String =
        BuildConfig.flavorData.apiBaseUrl + "%s?access_token=${BuildConfig.flavorData.accessToken}&rid=%s"

    fun onCardClicked(position: Int) {
        val reportCard = reportCardsList[position]
        if (reportCard.type == ReportCardType.PDF) {
            val url = if (prefs.userRole == UserRole.GUARDIAN) {
                getDownloadPdfUrl().format(
                    Locale.ENGLISH, reportCard.type.url,
                    reportCard.id
                ) + "&user_id=" + prefs.childId
            } else {
                getDownloadPdfUrl().format(
                    Locale.ENGLISH,
                    reportCard.type.url, reportCard.id
                )
            }
            openFileIfExistOrStartDownloadPdf(url, reportCard)
        } else {
            getDynamicDownloadUrlAndStartDownloadOrOpenTheFileIfExist(reportCard)
        }
    }

    private fun getDynamicDownloadUrlAndStartDownloadOrOpenTheFileIfExist(reportCard: ReportCard) {
        viewModelScope.launch {
            val resource =
                tryResource {
                    reportCardsRepository.getReportCardDetails(
                        reportCard.type.url,
                        reportCard.id
                    )
                }
            if (resource is Resource.Success<*> &&
                (resource as? Resource.Success<BaseWrapper<ReportCardDetails>>?)?.data?.data != null
            ) {
                val downloadUrl =
                    resource.element<BaseWrapper<ReportCardDetails>>()?.data?.downloadUrl
                val fileExtension = getFileExtension(downloadUrl)
                startDownload(downloadUrl, reportCard, fileExtension)
            } else {
                if (resource is Resource.Error) {
                    _toastLiveData.postValue(context.getString(resource.error.resourceMessage))
                } else {
                    _toastLiveData.postValue(resource.element<BaseWrapper<*>>()?.message)
                }
            }
        }
    }

    private fun openFileIfExistOrStartDownloadPdf(downloadUrl: String?, reportCard: ReportCard) {
        startDownload(downloadUrl, reportCard, "pdf")
    }

    private fun openFile(reportCard: ReportCard, extension: String?) {
        val uri = storage.getFileUri("${reportCard.id}.$extension")
        val mimeTypeMap = MimeTypeMap.getSingleton()
        val mimeType = mimeTypeMap.getMimeTypeFromExtension(File(uri.toString()).extension)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(uri, mimeType)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        try {
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                context, context.getString(R.string.there_is_no_application),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun startDownload(downloadUrl: String?, reportCard: ReportCard, extension: String?) {
        val request: DownloadManager.Request = DownloadManager.Request(
            Uri.parse(downloadUrl)
        )
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(
            DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
        )
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            reportCard.id + "." + extension
        )
        request.addRequestHeader("Authtoken", prefs.authenticationToken)
        val dm: DownloadManager? = context?.getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
        dm?.enqueue(request)
    }

    private fun getFileExtension(url: String?): String? {
        var extension = url?.substring(url.lastIndexOf('.') + 1)
        val lastIndexOfFirstCharacterAfterExtension = extension?.indexOf('%') ?: -1
        if (lastIndexOfFirstCharacterAfterExtension != -1) {
            extension = extension?.substring(0, lastIndexOfFirstCharacterAfterExtension)
        }
        return extension
    }

}
