package com.classera.reportcards.reportcardsdetails

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.repositories.reportcards.ReportCardsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ReportCardDetailsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ReportCardDetailsViewModelFactory
        ): ReportCardDetailsViewModel {
            return ViewModelProvider(fragment, factory)[ReportCardDetailsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideViewModelFactory(
            fragment: Fragment,
            reportCardsRepository: ReportCardsRepository
        ): ReportCardDetailsViewModelFactory {
            val args by fragment.navArgs<ReportCardDetailsFragmentArgs>()
            return ReportCardDetailsViewModelFactory(args, reportCardsRepository)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ReportCardDetailsFragment): Fragment
}
