package com.classera.schedule

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.classera.schedule.days.DaysAdapter
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Schedule")
class ScheduleFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ScheduleViewModel

    private var daysAdapter: DaysAdapter? = null
    private var slotsAdapter: SchedulesAdapter? = null

    private var progressBar: ProgressBar? = null
    private var recyclerViewDays: RecyclerView? = null
    private var recyclerViewSlots: RecyclerView? = null
    private var errorView: ErrorView? = null

    override val layoutId: Int = R.layout.fragment_schedule

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        getSchedules()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_schedule)
        recyclerViewDays = view?.findViewById(R.id.recycler_view_fragment_schedule_days)
        recyclerViewSlots = view?.findViewById(R.id.recycler_view_fragment_schedule_slots)
        errorView = view?.findViewById(R.id.error_view_fragment_schedule)
    }

    private fun getSchedules() {
        viewModel.getSchedules().observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerViewDays?.visibility = View.GONE
            recyclerViewSlots?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            recyclerViewDays?.visibility = View.VISIBLE
            recyclerViewSlots?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun initAdapter() {
        daysAdapter = DaysAdapter(viewModel)
        recyclerViewDays?.adapter = daysAdapter

        slotsAdapter = SchedulesAdapter(viewModel, daysAdapter!!.selectedPosition)
        recyclerViewSlots?.adapter = slotsAdapter

        initAdapterListener()
    }

    private fun initAdapterListener() {
        daysAdapter?.setOnItemClickListener { _, position ->
            slotsAdapter = SchedulesAdapter(viewModel, position)
            recyclerViewSlots?.adapter = slotsAdapter
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getSchedules() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        daysAdapter = null
        slotsAdapter = null
        errorView = null
        progressBar = null
        recyclerViewDays = null
        recyclerViewSlots = null
    }
}
