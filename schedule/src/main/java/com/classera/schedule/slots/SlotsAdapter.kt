package com.classera.schedule.slots

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.schedule.ScheduleViewModel
import com.classera.schedule.databinding.RowScheduleSlotBinding

class SlotsAdapter(
    private val viewModel: ScheduleViewModel,
    private val selectedDayPosition: Int,
    private val schedulePosition: Int
) : BaseAdapter<SlotsAdapter.ViewHolder>() {

    init {
        disableAnimations()
        disableEmptyView()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowScheduleSlotBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getSlotCountBy(selectedDayPosition, schedulePosition)
    }

    inner class ViewHolder(binding: RowScheduleSlotBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowScheduleSlotBinding> {
                this.slot = viewModel.getSlotBy(selectedDayPosition, schedulePosition, position)
            }
        }
    }
}
