package com.classera.selection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.selection.Selectable
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class MultiSelectionActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MultiSelectionViewModelFactory
        ): MultiSelectionViewModel {
            return ViewModelProvider(activity, factory)[MultiSelectionViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideArguments(activity: AppCompatActivity): Array<out Selectable?>? {
            val filters = MultiSelectionActivity.getFilters(activity)
            val selectedFilters = MultiSelectionActivity.getSelectedFilter(activity)
            filters?.map {
                it?.selected = false
                it
            }?.filter { filter ->
                selectedFilters?.firstOrNull { it.id == filter?.id } != null
            }?.forEach { it?.selected = true }
            return filters
        }
    }

    @Binds
    abstract fun bindActivity(activity: MultiSelectionActivity): AppCompatActivity
}
