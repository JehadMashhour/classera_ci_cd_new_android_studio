package com.classera.selection

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BaseViewHolder
import com.classera.selection.databinding.RowMultiSelectionBinding

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiSelectionAdapter(
    private val viewModel: MultiSelectionViewModel
) : BaseAdapter<BaseViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            R.layout.row_multi_selection_all -> {
                val view = inflater?.inflate(viewType, parent, false)
                AllViewHolder(view)
            }
            else -> {
                val binding = RowMultiSelectionBinding.inflate(inflater!!, parent, false)
                SelectableViewHolder(binding)
            }
        }
    }

    override fun bind(holder: BaseViewHolder, position: Int) {
        super.bind(holder, position - 1)
    }

    override fun getItemsCount(): Int {
        return if (viewModel.getItemsCount() > 0) viewModel.getItemsCount() + 1 else viewModel.getItemsCount()
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return R.layout.row_multi_selection_all
        }
        return super.getItemViewType(position)
    }

    inner class SelectableViewHolder(binding: RowMultiSelectionBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowMultiSelectionBinding> { selectable = viewModel.getSelectable(position) }
            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.toggleFilter(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }

    inner class AllViewHolder(view: View?) : BaseViewHolder(view) {

        override fun bind(position: Int) {
            itemView.setOnClickListener {
                viewModel.toggleAll()
                notifyDataSetChanged()
            }
        }
    }
}
