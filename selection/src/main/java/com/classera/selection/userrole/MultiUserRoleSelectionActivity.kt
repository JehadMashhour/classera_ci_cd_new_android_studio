package com.classera.selection.userrole

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.activities.BaseToolbarActivity
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.utils.android.observe
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.selection.R
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Feb 04, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiUserRoleSelectionActivity : BaseToolbarActivity() {

    @Inject
    lateinit var viewModel: MultiUserRoleSelectionViewModel

    private var recyclerViewFilter: RecyclerView? = null
    private var quickFilter: QuickFilterView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_user_role_selection)
        findViews()
        initViewModelListeners()
        getRoles()
    }

    private fun initViewModelListeners() {
        viewModel.notifyAdapterLiveData.observe(this) {
            initAdapter()
        }
    }

    private fun findViews() {
        quickFilter = findViewById(R.id.quick_filter_view_activity_multi_selection)
        recyclerViewFilter = findViewById(R.id.recycler_view_activity_filter)
        progressBar = findViewById(R.id.progress_bar_activity_multi_user_role_selection)
        errorView = findViewById(R.id.error_view_activity_multi_user_role_selection)
    }

    private fun initAdapter() {
        recyclerViewFilter?.adapter = MultiUserRoleSelectionAdapter(viewModel)
    }

    private fun getRoles() {
        viewModel.getUserRoles().observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        val values = viewModel.getQuickFilterValues()
        quickFilter?.setAdapter(values.first, values.second)
        quickFilter?.setOnFilterSelectedListener {
            viewModel.onFilterSelected(it)
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener {
            getRoles()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_multi_selection, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_activity_filter_done -> {
                val data = Intent()
                data.putExtra(EXTRA_SELECTED_FILTER, viewModel.getSelectedItems())
                setResult(Activity.RESULT_OK, data)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val REQUEST_CODE = 100
        private const val EXTRA_URL = "url"
        private const val EXTRA_SELECTED_FILTER = "filters"

        fun start(fragment: Fragment, url: String?) {
            val intent = Intent(fragment.requireContext(), MultiUserRoleSelectionActivity::class.java)
            intent.putExtra(EXTRA_URL, url)
            fragment.startActivityForResult(
                intent,
                REQUEST_CODE
            )
        }

        internal fun getUrl(activity: AppCompatActivity): String? {
            return activity.intent.getStringExtra(EXTRA_URL)
        }

        @Suppress("UNCHECKED_CAST")
        fun getSelectedFilterFromIntent(intent: Intent?): Array<out Selectable>? {
            return intent?.getParcelableArrayExtra(EXTRA_SELECTED_FILTER)?.map { it as Selectable }?.toTypedArray()
        }

        fun isDone(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
            return requestCode == REQUEST_CODE
                    && resultCode == Activity.RESULT_OK
                    && data?.extras?.containsKey(EXTRA_SELECTED_FILTER) == true
        }
    }
}
