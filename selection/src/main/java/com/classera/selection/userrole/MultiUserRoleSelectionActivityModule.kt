package com.classera.selection.userrole

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class MultiUserRoleSelectionActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MultiUserRoleSelectionViewModelFactory
        ): MultiUserRoleSelectionViewModel {
            return ViewModelProvider(activity, factory)[MultiUserRoleSelectionViewModel::class.java]
        }

        @Url
        @Provides
        @JvmStatic
        fun provideUrl(activity: AppCompatActivity): String? {
            return MultiUserRoleSelectionActivity.getUrl(activity)
        }
    }

    @Binds
    abstract fun bindActivity(activity: MultiUserRoleSelectionActivity): AppCompatActivity
}
