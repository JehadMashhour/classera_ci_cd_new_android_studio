package com.classera.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.settings.SettingsRepository
import javax.inject.Inject

class SettingsViewModelFactory @Inject constructor(
    private val settingsRepository: SettingsRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SettingsViewModel(settingsRepository, prefs) as T
    }
}
