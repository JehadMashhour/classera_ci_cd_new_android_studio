package com.classera.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.classera.core.Activities
import com.classera.core.AddressableActivity
import com.classera.core.Screen
import com.classera.core.activities.BaseActivity
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.settings.AppVersion
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.google.gson.Gson
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Splash Screen")
class SplashActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: SplashViewModel

    private var notificationEvent: String? = null
    private var notificationBody: String? = null
    private var notificationUUID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        viewModel.getAppVersion(packageName, BuildConfig.flavorData.appVersionName)
            .observe(this, this::handleResource)
        saveNotificationUUID()
        handelNotificationIntent()
        intent.getStringExtra(ERROR_INTENT_EXTRA_NAME)?.let { message->
            if(!(message.isEmpty() && message.isNullOrBlank())) {
               val errorMessage =  Gson().fromJson(message, BaseWrapper::class.java).message
               Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun saveNotificationUUID() {
        if (prefs.uuid.isNullOrEmpty() || prefs.uuid.isNullOrBlank())
            prefs.uuid = UUID.randomUUID().toString()
    }

    private fun handelNotificationIntent() {
        val notificationDataExtra = Activities.Splash.NOTIFICATION_DATA_EVENT_EXTRA
        if (intent.hasExtra(notificationDataExtra)) {
            notificationEvent = intent.getStringExtra(notificationDataExtra)
            notificationBody = intent.getStringExtra(Activities.Splash.NOTIFICATION_DATA_BODY_EXTRA)
            notificationUUID = intent.getStringExtra(Activities.Splash.NOTIFICATION_DATA_UUID_EXTRA)
        }
    }

    override fun onBackPressed() {
        // No impl
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessResource(resource)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<*>) {
        when {
            resource.element<BaseWrapper<AppVersion>>()?.data == null -> initListeners()
            resource.element<BaseWrapper<AppVersion>>()?.data?.force == true -> {
                forceUpdateApp(resource.element<BaseWrapper<AppVersion>>()?.data?.store)
            }
            else -> {
                initListeners()
            }
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if(BuildConfig.FLAVOR_whitelabel.equals(FLAVOR_CLIENT,true)){
            initListeners()
        }else{
            Toast.makeText(this, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun forceUpdateApp(storeUrl: String?) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.title_update_application_dialog))
            .setMessage(getString(R.string.message_update_application_dialog))
            .setPositiveButton(R.string.button_positive_update_application_dialog) { dialog, _ ->
                dialog.dismiss()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(storeUrl)))
            }

            .setCancelable(false)
            .show()
    }

    private fun updateApp(storeUrl: String?) {
        AlertDialog.Builder(this)
            .setTitle(R.string.title_update_application_dialog)
            .setMessage(getString(R.string.message_update_application_dialog))
            .setPositiveButton(R.string.button_positive_update_application_dialog) { dialog, _ ->
                dialog.dismiss()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(storeUrl)))
            }
            .setNegativeButton(R.string.button_negative_update_application_dialog) { dialog, _ ->
                dialog.dismiss()
                initListeners()
            }
            .setCancelable(false)
            .show()
    }

    private fun initListeners() {
        viewModel.getIntentAction().observe(this) { activity ->
            handelSplashNavigation(activity)
        }
    }

    private fun handelSplashNavigation(activity: AddressableActivity) {
            if (prefs.userRole != UserRole.GUARDIAN || prefs.childId != null) {
                val intent = intentTo(activity)
                if (activity is Activities.Main) {
                    notificationEvent?.let {
                        intent.putExtra(Activities.Splash.NOTIFICATION_DATA_EVENT_EXTRA, notificationEvent)
                        intent.putExtra(Activities.Splash.NOTIFICATION_DATA_BODY_EXTRA, notificationBody)
                        intent.putExtra(Activities.Splash.NOTIFICATION_DATA_UUID_EXTRA, notificationUUID)
                    }
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                val intent = intentTo(Activities.ParentChildActivity)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }

    private companion object {

        private const val ERROR_INTENT_EXTRA_NAME = "error"
        private const val FLAVOR_CLIENT = "moeiraq"
    }
}
