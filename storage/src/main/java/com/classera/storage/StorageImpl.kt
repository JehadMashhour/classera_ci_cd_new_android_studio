package com.classera.storage
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.classera.storage.Storage.Companion.APP_FOLDER_NAME
import com.classera.storage.Storage.Companion.BUFFER_SIZE
import java.io.File
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
@RequiresApi(Build.VERSION_CODES.Q)
class StorageImpl(private val context: Context) : Storage {

    private val contentResolver by lazy { context.contentResolver }

    override fun saveFile(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val mimeType = getMimeType(fileName)
        return when {
            isPhoto(mimeType) -> {
                saveImage(fileName, inputStream, showToast)
            }
            isVideo(mimeType) -> {
                saveVideo(fileName, inputStream, showToast)
            }
            isAudio(mimeType) -> {
                saveAudio(fileName, inputStream, showToast)
            }
            else -> {
                saveOther(fileName, inputStream, showToast)
            }
        }
    }

    private fun isPhoto(mimeType: String?): Boolean {
        return mimeType?.startsWith("image") == true
    }

    private fun saveImage(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val contentUri = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
        val uri = contentResolver.insert(contentUri, getContentValues(fileName))
        return writeToDisk(inputStream, uri, showToast)
    }

    private fun isVideo(mimeType: String?): Boolean {
        return mimeType?.startsWith("video") == true
    }

    private fun saveVideo(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val contentUri = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
        val uri = contentResolver.insert(contentUri, getContentValues(fileName))
        return writeToDisk(inputStream, uri, showToast)
    }

    private fun isAudio(mimeType: String?): Boolean {
        return mimeType?.startsWith("audio") == true
    }

    private fun saveAudio(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val contentUri = MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
        val uri = contentResolver.insert(contentUri, getContentValues(fileName))
        return writeToDisk(inputStream, uri, showToast)
    }

    private fun saveOther(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val contentUri = MediaStore.Downloads.getContentUri(MediaStore.VOLUME_EXTERNAL)
        val uri = contentResolver.insert(contentUri, getContentValues(fileName))
        return writeToDisk(inputStream, uri, showToast)
    }

    private fun getContentValues(fileName: String?): ContentValues {
        val mimeType = getMimeType(fileName)
        return ContentValues().apply {
            put(MediaStore.Downloads.DISPLAY_NAME, fileName)
            put(MediaStore.Downloads.RELATIVE_PATH, getRelativePath(fileName))
            put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
        }
    }

    private fun getRelativePath(fileName: String?): String {
        val mimeType = getMimeType(fileName)
        val directory = when {
            isPhoto(mimeType) -> Environment.DIRECTORY_PICTURES
            isVideo(mimeType) -> Environment.DIRECTORY_MOVIES
            isAudio(mimeType) -> Environment.DIRECTORY_MUSIC
            else -> Environment.DIRECTORY_DOWNLOADS
        }
        return "$directory${File.separator}$APP_FOLDER_NAME"
    }

    private fun getMimeType(fileName: String?): String? {
        val extension = fileName?.substring(fileName.lastIndexOf('.') + 1)
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getMimeTypeFromExtension(extension)
    }

    private fun writeToDisk(inputStream: InputStream, uri: Uri?, showToast: Boolean): Uri? {
        try {
            if (uri != null) {
                val outputStream = contentResolver.openOutputStream(uri)
                val buffer = ByteArray(BUFFER_SIZE)
                var read: Int
                while (inputStream.read(buffer).also { read = it } != -1) {
                    outputStream?.write(buffer, 0, read)
                }
                outputStream?.flush()
            }
        } catch (e: Exception) {
            Log.e(StorageImpl::class.java.simpleName, e.message, e)
        } finally {
            inputStream.close()
        }

        if(showToast) {
            Toast.makeText(
                context,
                context.getString(R.string.exo_download_completed), Toast.LENGTH_LONG
            ).show()
        }
        return uri
    }

    override fun checkIfExist(fileName: String?): Boolean {
        return getFileUri(fileName) != null
    }

    override fun getFileUri(fileName: String?): Uri? {
        var uri: Uri? = null
        try {
            val cursor = contentResolver?.query(
                getUri(fileName),
                getProjection(fileName),
                getSelection(fileName),
                null,
                null
            )
            if (cursor?.moveToFirst() == true) {
                uri = getFileUriFrom(fileName = fileName, cursor = cursor)
            }
            cursor?.close()
        } catch (ignored: Exception) {
            ignored.message
        }
        return uri
    }

    override fun canProceedWithDownload(): Boolean {
        return true
    }

    private fun getUri(fileName: String?): Uri {
        val mimeType = getMimeType(fileName)
        return when {
            isPhoto(mimeType) -> {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }
            isVideo(mimeType) -> {
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            }
            isAudio(mimeType) -> {
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            }
            else -> {
                MediaStore.Downloads.EXTERNAL_CONTENT_URI
            }
        }
    }

    private fun getProjection(fileName: String?): Array<String> {
        val mimeType = getMimeType(fileName)
        return when {
            isPhoto(mimeType) -> {
                arrayOf(MediaStore.Images.Media.TITLE, MediaStore.Images.Media._ID)
            }
            isVideo(mimeType) -> {
                arrayOf(MediaStore.Video.Media.TITLE, MediaStore.Video.Media._ID)
            }
            isAudio(mimeType) -> {
                arrayOf(MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media._ID)
            }
            else -> {
                arrayOf(MediaStore.Downloads.TITLE, MediaStore.Downloads._ID)
            }
        }
    }

    private fun getSelection(fileName: String?): String {
        val mimeType = getMimeType(fileName)
        return when {
            isPhoto(mimeType) -> {
                MediaStore.Images.Media.TITLE + "='${fileName?.substring(
                    0,
                    fileName.lastIndexOf('.')
                )}'"
            }
            isVideo(mimeType) -> {
                MediaStore.Video.Media.TITLE + "='${fileName?.substring(0, fileName.lastIndexOf('.'))}'"
            }
            isAudio(mimeType) -> {
                MediaStore.Audio.Media.TITLE + "='${fileName?.substring(0, fileName.lastIndexOf('.'))}'"
            }
            else -> {
                MediaStore.Downloads.TITLE + "='${fileName?.substring(
                    0,
                    fileName.lastIndexOf('.')
                )}'"
            }
        }
    }

    private fun getFileUriFrom(fileName: String?, cursor: Cursor): Uri {
        val mimeType = getMimeType(fileName)
        val idColumn = when {
            isPhoto(mimeType) -> {
                MediaStore.Images.Media._ID
            }
            isVideo(mimeType) -> {
                MediaStore.Video.Media._ID
            }
            isAudio(mimeType) -> {
                MediaStore.Audio.Media._ID
            }
            else -> {
                MediaStore.Downloads._ID
            }
        }
        val externalContentUri = getUri(fileName).toString() + File.separator
        val contentId = cursor.getString(cursor.getColumnIndex(idColumn))
        val fullContentUri = externalContentUri + contentId
        return Uri.parse(fullContentUri)
    }
}
