package com.classera.support.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.ListPopupWindow
import androidx.browser.customtabs.CustomTabsIntent
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.isCorrectUrl
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onLoadMore
import com.classera.data.models.BaseWrapper
import com.classera.data.models.support.SupportWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.support.R
import com.classera.support.databinding.FragmentSupportDetailsBinding
import com.classera.support.dialog.CustomAlertDialog
import com.google.android.material.button.MaterialButton
import javax.inject.Inject


@Screen("SupportDetails")
class SupportDetailsFragment : BaseBindingFragment() {

    override val layoutId: Int = R.layout.fragment_support_details

    @Inject
    lateinit var viewModel: SupportDetailsViewModel

    @Inject
    lateinit var prefs: Prefs

    private var canLoadMoreComments = false
    private var commentsAdapter: SupportDetailsCommentsAdapter? = null

    private var recyclerViewComments: RecyclerView? = null
    private var cardViewRoot: CardView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var relativeLayoutAddCommentSubmit: View? = null
    private var editTextAddComment: EditText? = null
    private var scrollView: NestedScrollView? = null
    private var viewActionsLine: View? = null
    private var supportDetailsInfoLayout: View? = null
    private var checkBoxShowDetailsInfo: AppCompatCheckBox? = null
    private var buttonAttachmentButton: MaterialButton? = null

    private var textViewPriority: AppCompatTextView? = null
    private var textViewType: AppCompatTextView? = null
    private var textViewModule: AppCompatTextView? = null
    private var textViewStatus: AppCompatTextView? = null

    private var textViewProblemLink: AppCompatTextView? = null

    private val listTypePopupView by lazy { ListPopupWindow(requireContext()) }
    private val listModulePopupView by lazy { ListPopupWindow(requireContext()) }
    private val listPriorityPopupView by lazy { ListPopupWindow(requireContext()) }
    private val listStatusPopupView by lazy { ListPopupWindow(requireContext()) }

    @Inject
    lateinit var customTabsIntent: CustomTabsIntent

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initViewsListeners()
        initViewModelListeners()
        getDetails()
    }

    private fun findViews() {
        cardViewRoot = view?.findViewById(R.id.card_view_fragment_support_details_root)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_support_details)
        errorView = view?.findViewById(R.id.error_view_fragment_support_details)
        recyclerViewComments = view?.findViewById(R.id.recycler_view_fragment_support_details_comments)
        editTextAddComment = view?.findViewById(R.id.edit_text_fragment_support_details_add_comment)
        scrollView = view?.findViewById(R.id.scroll_view_fragment_support_details)
        viewActionsLine = view?.findViewById(R.id.view_fragment_support_details_actions_line)
        relativeLayoutAddCommentSubmit =
            view?.findViewById(R.id.relative_layout_fragment_support_details_add_comment_submit_parent)
        supportDetailsInfoLayout = view?.findViewById(R.id.support_details)
        checkBoxShowDetailsInfo = view?.findViewById(R.id.check_box_support_details_section_show_hide)
        buttonAttachmentButton = view?.findViewById(R.id.button_support_details_section_ticket_download_button)
        textViewPriority = view?.findViewById(R.id.text_view_support_details_section_ticket_priority_value)
        textViewType = view?.findViewById(R.id.text_view_support_details_section_ticket_type_value)
        textViewModule = view?.findViewById(R.id.text_view_support_details_section_ticket_module_value)
        textViewStatus = view?.findViewById(R.id.text_view_row_support_type_value)
        textViewProblemLink = view?.findViewById(R.id.text_view_support_details_section_ticket_problem_link_value)
    }

    private fun initViewsListeners() {
        errorView?.setOnRetryClickListener(::getDetails)
        relativeLayoutAddCommentSubmit?.setOnClickListener {
            if (editTextAddComment?.text?.isNotEmpty() == true && editTextAddComment?.text?.isNotBlank() == true) {
                viewModel.addComment(editTextAddComment?.text?.toString())
                editTextAddComment?.text = null
            }
        }

        scrollView?.onLoadMore {
            if (canLoadMoreComments) {
                canLoadMoreComments = false
                getComments()
            }
        }

        textViewProblemLink?.setOnClickListener {
            val url = viewModel.getSupportProblemFullLink()
            if (url.isCorrectUrl(url)) {
                customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                customTabsIntent.launchUrl(requireContext(), Uri.parse(textViewProblemLink?.text.toString()))
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.validation_fragment_add_support_invalid_url),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun supportDetailsInfoSectionListener() {
        checkBoxShowDetailsInfo?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                checkBoxShowDetailsInfo?.setButtonDrawable(R.drawable.ic_keyboard_arrow_hide_black)
                supportDetailsInfoLayout?.visibility = View.VISIBLE
            } else {
                checkBoxShowDetailsInfo?.setButtonDrawable(R.drawable.ic_arrow_drop_show_black)
                supportDetailsInfoLayout?.visibility = View.GONE
            }
        }

        buttonAttachmentButton?.setOnClickListener {
            viewModel.getSupportAttachmentList()?.let { attachments ->
                val dialog = CustomAlertDialog(
                    requireActivity(), attachments
                )
                dialog.show()
            }
        }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
    }

    private fun initViewModelListeners() {
        viewModel.notifyCommentsAdapterLiveData.observe(this) {
            commentsAdapter?.notifyDataSetChanged()

            scrollView?.post { scrollView?.smoothScrollTo(0, viewActionsLine?.y?.toInt() ?: 0) }
        }
    }

    private fun getDetails() {
        viewModel.getDetails().observe(this, ::handleDetailsResource)
        getComments()
    }

    private fun handleSupportStatusesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportStatusesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportStatusesSuccessResource()
            }
            is Resource.Error -> {
                handleSupportStatusesErrorResource(resource)
            }
        }
    }

    private fun handleSupportStatusesErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportStatusesLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportStatusesSuccessResource() {
        val dataList = viewModel.getSupportStatusesTitles()
        listStatusPopupView.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, dataList))
        listStatusPopupView.setOnItemClickListener { _, _, position, _ ->
            textViewStatus?.text = dataList[position]
            listStatusPopupView.dismiss()
            viewModel.updateStatus(position)
        }
        listStatusPopupView.anchorView = textViewStatus
        listStatusPopupView.width = LinearLayout.LayoutParams.MATCH_PARENT
        textViewStatus?.setOnClickListener { listStatusPopupView.show() }
    }

    private fun handleSupportPriorityResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportPriorityLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportPrioritySuccessResource()
            }
            is Resource.Error -> {
                handleSupportPriorityErrorResource(resource)
            }
        }
    }

    private fun handleSupportPriorityErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportPriorityLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportPrioritySuccessResource() {
        val dataList = viewModel.getSupportPrioritiesTitles()
        listPriorityPopupView.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, dataList))
        listPriorityPopupView.setOnItemClickListener { _, _, position, _ ->
            textViewPriority?.text = dataList[position]
            listPriorityPopupView.dismiss()
            viewModel.updatePriority(position)
        }
        listPriorityPopupView.anchorView = textViewPriority
        listPriorityPopupView.width = LinearLayout.LayoutParams.MATCH_PARENT
        if (prefs.userId == viewModel.getCreatorId()) {
            textViewPriority?.setOnClickListener { listPriorityPopupView.show() }
        } else {
            textViewPriority?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
    }

    private fun handleSupportTypeResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportTypeLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportTypeSuccessResource()
            }
            is Resource.Error -> {
                handleSupportTypeErrorResource(resource)
            }
        }
    }

    private fun handleSupportTypeErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportTypeLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportTypeSuccessResource() {
        val dataList = viewModel.getSupportTypeTitles()
        listTypePopupView.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, dataList))
        listTypePopupView.setOnItemClickListener { _, _, position, _ ->
            textViewType?.text = dataList[position]
            listTypePopupView.dismiss()
            viewModel.updateType(position)
        }
        listTypePopupView.anchorView = textViewType
        listTypePopupView.width = LinearLayout.LayoutParams.MATCH_PARENT
        if (prefs.userId == viewModel.getCreatorId()) {
            textViewType?.setOnClickListener { listTypePopupView.show() }
        } else {
            textViewType?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
    }

    private fun handleSupportModuleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSupportModuleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSupportModuleSuccessResource()
            }
            is Resource.Error -> {
                handleSupportModuleErrorResource(resource)
            }
        }
    }

    private fun handleSupportModuleErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSupportModuleLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleSupportModuleSuccessResource() {
        val dataList = viewModel.getSupportModulesTitles()
        listModulePopupView.setAdapter(ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, dataList))
        listModulePopupView.setOnItemClickListener { _, _, position, _ ->
            textViewModule?.text = dataList[position]
            listModulePopupView.dismiss()
            viewModel.updateModule(position)
        }
        listModulePopupView.anchorView = textViewModule
        listModulePopupView.width = LinearLayout.LayoutParams.MATCH_PARENT
        if (prefs.userId == viewModel.getCreatorId()) {
            textViewModule?.setOnClickListener { listModulePopupView.show() }
        } else {
            textViewModule?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
    }

    private fun getComments() {
        viewModel.getComments().observe(this, ::handleCommentsResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleDetailsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleDetailsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<SupportWrapper>>)
            }
            is Resource.Error -> {
                handleDetailsErrorResource(resource)
            }
        }
    }

    private fun handleDetailsLoadingResource(resource: Resource.Loading) {
        progressBar?.post {
            progressBar?.visibility = if (resource.show) View.VISIBLE else View.GONE
            cardViewRoot?.visibility = if (!resource.show) View.VISIBLE else View.GONE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<SupportWrapper>>) {
        success.data?.data?.let {
            bind<FragmentSupportDetailsBinding> { this?.supportTickets = it }
        }

        handleAdminView()
        viewModel.getSupportStatuses().observe(this, ::handleSupportStatusesResource)
        supportDetailsInfoSectionListener()
    }

    private fun handleDetailsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleCommentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCommentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCommentsSuccessResource()
            }
        }
    }

    private fun handleAdminView() {
        if (prefs.userRole != UserRole.ADMIN) {
            viewModel.getSupportTypes().observe(this, ::handleSupportTypeResource)
            viewModel.getSupportModules().observe(this, ::handleSupportModuleResource)
            viewModel.getSupportPriorities().observe(this, ::handleSupportPriorityResource)
        } else {
            textViewPriority?.setCompoundDrawables(null,null,null,null)

            textViewModule?.setCompoundDrawables(null,null,null,null)

            textViewType?.setCompoundDrawables(null,null,null,null)
        }
    }

    private fun handleCommentsLoadingResource(resource: Resource.Loading) {
        canLoadMoreComments = !resource.show
    }

    private fun handleCommentsSuccessResource() {
        if (commentsAdapter == null) {
            initAdapter()
            return
        }
        commentsAdapter?.notifyDataSetChanged()
    }

    private fun initAdapter() {
        commentsAdapter = SupportDetailsCommentsAdapter(requireActivity(), viewModel)
        recyclerViewComments?.adapter = commentsAdapter
    }
}
