package com.classera.support.details

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

class SupportDetailsViewModelFactory @Inject constructor(
    private val args: SupportDetailsFragmentArgs?,
    private val supportRepository: SupportRepository,
    private val userRepository: UserRepository,
    private val application: Application
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SupportDetailsViewModel(args?.ticketId, supportRepository, userRepository, application) as T
    }
}
