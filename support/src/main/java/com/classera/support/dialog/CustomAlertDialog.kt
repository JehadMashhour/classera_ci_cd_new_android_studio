package com.classera.support.dialog

import android.app.Activity
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.RecyclerView
import com.classera.storage.Storage
import com.classera.storage.StorageImpl
import com.classera.storage.StorageLegacyImpl
import com.classera.support.R
import com.github.abdularis.buttonprogress.DownloadButtonProgress

class CustomAlertDialog(
    private val activity: Activity,
    private val commentsAttachments: List<String>
) : AppCompatDialog(activity, R.style.AppTheme_FullWidthDialog) {

    private var recyclerView: RecyclerView? = null
    private var adapter: DialogListAdapter? = null

    private lateinit var storage: Storage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_view)
        initStorage()
        findViews()
        initViews()
    }

    private fun initStorage() {
        storage = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
            StorageImpl(activity)
        } else {
            StorageLegacyImpl(activity)
        }
    }

    private fun initViews() {
        val attachments = commentsAttachments.map { Attachment(it) }
        attachments.forEach {
            if (storage.checkIfExist(it.name)) {
                it.state = DownloadButtonProgress.STATE_FINISHED
            }
        }
        adapter = DialogListAdapter(attachments, storage)
        recyclerView?.adapter = adapter
    }

    private fun findViews() {
        recyclerView = findViewById(R.id.recycler_view_dialog_support_comments_attachment)
    }
}
