package com.classera.surveys

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.surveys.databinding.RowSurveyBinding


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
class SurveysAdapter (
    private val viewModel: SurveysViewModel
) : BasePagingAdapter<SurveysAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSurveyBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getSurveysCount()
    }

    inner class ViewHolder(binding: RowSurveyBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowSurveyBinding> { surveyItem = viewModel.getSurvey(position) }
        }
    }
}
