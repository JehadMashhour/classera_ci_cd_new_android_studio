package com.classera.surveys

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.surveys.Survey
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.surveys.SurveysRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
class SurveysViewModel(private val surveysRepository: SurveysRepository) :
    BaseViewModel() {

    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var surveys: MutableList<Survey> = mutableListOf()

    fun getListSurveys(
        pageNumber: Int,
        text: CharSequence?
    ): LiveData<Resource> {
        return getListSurveys(pageNumber, text, pageNumber == DEFAULT_PAGE)
    }

    fun refreshSurveys(text: CharSequence?) =
        getListSurveys(DEFAULT_PAGE, text, false)

    private fun getListSurveys(
        pageNumber: Int,
        text: CharSequence?,
        showProgress: Boolean
    ) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                surveysRepository.getListSurveys(pageNumber, text)
            }
            if (pageNumber == DEFAULT_PAGE) {
                surveys.clear()
            }
            surveys.addAll(
                resource.element<BaseWrapper<List<Survey>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getSurvey(position: Int): Survey? {
        return surveys[position]
    }

    fun getSurveysCount(): Int {
        return surveys.size
    }
}
