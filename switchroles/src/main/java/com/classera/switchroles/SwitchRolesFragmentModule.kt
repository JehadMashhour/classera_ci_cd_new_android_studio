package com.classera.switchroles

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SwitchRolesFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SwitchRolesViewModelFactory
        ): SwitchRolesViewModel {
            return ViewModelProvider(fragment, factory)[SwitchRolesViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SwitchRolesFragment): Fragment
}
