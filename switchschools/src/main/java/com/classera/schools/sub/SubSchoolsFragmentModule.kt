package com.classera.schools.sub

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.models.switchschools.School
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SubSchoolsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: SubSchoolsViewModelFactory): SubSchoolsViewModel {
            return ViewModelProvider(fragment, factory)[SubSchoolsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideSchools(fragment: Fragment): List<School> {
            val args by fragment.navArgs<SubSchoolsFragmentArgs>()
            return args.schools.toList()
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SubSchoolsFragment): Fragment
}

