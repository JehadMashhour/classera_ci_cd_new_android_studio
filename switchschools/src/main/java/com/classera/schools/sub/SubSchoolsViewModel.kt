package com.classera.schools.sub

import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.switchschools.School
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SubSchoolsViewModel(
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val schools: List<School>,
    private val prefs: Prefs
) : BaseViewModel() {


    var currentSearchValue: String = ""
    private var filteredSchoolsList: List<School> = listOf()

    init {
        filteredSchoolsList = schools
    }

    fun getSchool(position: Int): School {
        return filteredSchoolsList[position]
    }

    fun getSchoolCount(): Int {
        return filteredSchoolsList.size
    }

    fun getSelectedSchoolId(): String? {
        return prefs.schoolId
    }

    fun changeSelectedSchool(position: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            switchSchoolsRepository.changeSchool(schools[position])
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilteredSchoolList() {
        viewModelScope.launch {
            filteredSchoolsList = if (currentSearchValue.isEmpty())
                schools.filter {
                    it.schoolName?:"" == it.schoolName
                }
            else {
                schools.filter {
                    it.schoolName?.contains(currentSearchValue, true)?: false
                }
            }

        }
    }
}
