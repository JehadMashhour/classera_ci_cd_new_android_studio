package com.classera.switchsemester

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.switchsemester.databinding.RowSwitchSemstersBinding

class SwitchSemesterAdapter(private val viewModel: SwitchSemesterViewModel) :
    BasePagingAdapter<SwitchSemesterAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowSwitchSemstersBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getSwitchSemesterListCount()
    }

    inner class ViewHolder(binding: RowSwitchSemstersBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowSwitchSemstersBinding> {
                switchSemesterItem = viewModel.getSemester(position)
                selectedSemesterId = viewModel.getSelectedSemesterId()
            }
        }
    }
}
