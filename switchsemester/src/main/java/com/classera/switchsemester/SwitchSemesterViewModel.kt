package com.classera.switchsemester

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchroles.Role
import com.classera.data.models.switchsemester.Semester
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchsemester.SwitchSemesterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SwitchSemesterViewModel(
    private val switchSemesterRepository: SwitchSemesterRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private val _notifyAdapterLiveData = MutableLiveData<Unit>()
    val notifyAdapterLiveData: LiveData<Unit> = _notifyAdapterLiveData

    private var switchSemesterList: MutableList<Semester> = mutableListOf()

    var currentSearchValue: String = ""
    private var filteredSemesterList: List<Semester> = listOf()

    fun getSemesterList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { switchSemesterRepository.getSwitchSemesterList() }
        switchSemesterList.clear()
        switchSemesterList.addAll(resource.element<BaseWrapper<List<Semester>>>()?.data ?: mutableListOf())
        getFilteredSemesterList()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSemester(position: Int): Semester {
        return filteredSemesterList[position]
    }

    fun getSwitchSemesterListCount(): Int {
        return filteredSemesterList.size
    }

    fun generateSemester(semesterId: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            switchSemesterRepository.generateSemester(semesterId)
        }

        if (resource.isSuccess()) {
            _notifyAdapterLiveData.postValue(Unit)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSelectedSemesterId(): String? {
        return prefs.semesterId
    }

    fun getFilteredSemesterList() {
        viewModelScope.launch {
            filteredSemesterList = if (currentSearchValue.isEmpty())
                switchSemesterList.filter {
                    it.title?:"" == it.title
                }
            else {
                switchSemesterList.filter {
                    it.title?.contains(currentSearchValue, true)?: false
                }
            }

        }
    }
}
