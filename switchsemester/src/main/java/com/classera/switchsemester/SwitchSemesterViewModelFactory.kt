package com.classera.switchsemester

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchsemester.SwitchSemesterRepository
import javax.inject.Inject

class SwitchSemesterViewModelFactory  @Inject constructor(
    private val switchSemesterRepository: SwitchSemesterRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SwitchSemesterViewModel(switchSemesterRepository, prefs) as T
    }
}
