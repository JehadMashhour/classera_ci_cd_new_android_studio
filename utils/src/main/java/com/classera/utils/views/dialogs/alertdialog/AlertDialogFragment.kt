package com.classera.utils.views.dialogs.alertdialog

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.fragments.BaseDialogBindingFragment
import com.classera.utils.R
import com.classera.utils.databinding.FragmentAlertDialogBinding
import java.util.*

/**
 * Project: Classera
 * Created: 6/13/2021
 *
 * @author Jehad Abdalqader
 */
class AlertDialogFragment : BaseDialogBindingFragment(), AlertDialogHandler {
    override val layoutId: Int = R.layout.fragment_alert_dialog


    val args by navArgs<AlertDialogFragmentArgs>()

    override fun isCancelable(): Boolean {
        return args.cancelable
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindData()
    }

    private fun bindData() {
        bind<FragmentAlertDialogBinding> {
            this?.alertDialogHandler = this@AlertDialogFragment
            this?.alertDialogFragmentArgs = args
        }
    }

    override fun onLeftButtonClicked() {
        findNavController().navigateUp()
        setResult(args.buttonLeftText)
    }

    override fun onRightButtonClicked() {
        findNavController().navigateUp()
        setResult(args.buttonRightText)
    }

    private fun setResult(buttonName: String?) {
        setFragmentResult(
            DEFAULT_REQUEST_KEY,
            bundleOf(BUTTON_CLICKED_NAME to buttonName)
        )
    }

    companion object {
        const val DEFAULT_REQUEST_KEY = "default_request_key"
        private const val IMAGE = "image"
        private const val TITLE = "title"
        private const val MESSAGE = "message"
        private const val BUTTON_LEFT_TEXT = "buttonLeftText"
        private const val BUTTON_RIGHT_TEXT = "buttonRightText"
        private const val CANCELABLE = "cancelable"
        private const val BUTTON_CLICKED_NAME = "buttonClicked"

        fun show(
            fragment: Fragment,
            image: Int = -1,
            title: String? = null,
            message: String? = null,
            buttonLeftText: String? = null,
            buttonRightText: String? = null,
            cancelable: Boolean = true,
            listener: ((buttonName: String?) -> Unit)? = null
        ) {

            fragment.setFragmentResultListener(DEFAULT_REQUEST_KEY) { _, bundle ->
                listener?.invoke(bundle.get(BUTTON_CLICKED_NAME) as String?)
            }
            fragment.findNavController().navigate(R.id.alertDialogActionDirection,
                Bundle().apply {
                    putInt(IMAGE, image)
                    putString(TITLE, title)
                    putString(MESSAGE, message)
                    putString(BUTTON_LEFT_TEXT, buttonLeftText)
                    putString(BUTTON_RIGHT_TEXT, buttonRightText)
                    putBoolean(CANCELABLE, cancelable)
                }
            )
        }
    }
}
