package com.classera.utils.views.dialogs.datepickerbottomdialog

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.CalendarView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment
import com.classera.core.utils.android.*
import com.classera.utils.R
import com.classera.utils.databinding.FragmentDateBottomSheetBinding
import java.io.Serializable
import java.util.*

class DateBottomSheetFragment : BaseBottomSheetDialogBindingFragment() {

    override val layoutId: Int = R.layout.fragment_date_bottom_sheet

    val args by navArgs<DateBottomSheetFragmentArgs>()

    private lateinit var calendarView: CalendarView

    private var selectedDate: Date? = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args.selectedDate?.let {
            selectedDate = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        setupDatePicker()
        bindData()
    }

    private fun findViews() {
        view?.let {
            calendarView = it.findViewById(R.id.fragment_select_date_calendarView)
        }
    }


    private fun bindData() {
        bind<FragmentDateBottomSheetBinding>
        {
            this?.dateBottomSheetFragment = this@DateBottomSheetFragment
        }
    }


    private fun setupDatePicker() {
        args.minimumDate?.let {
            calendarView.minDate(it)
        }

        args.selectedDate?.let {
            calendarView.setDate(it, animate = true, center = true)
        }
    }

    fun onSelectButtonClicked() {
        setFragmentResult(
            args.key,
            bundleOf(DATA to selectedDate)
        )
        findNavController().navigateUp()
    }

    fun onDateChanged(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        selectedDate = calendar.time
    }


    companion object {
        const val DEFAULT_REQUEST_KEY = "default_request_key"
        const val DATA = "data"

        inline fun show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            title: String?,
            selectedDate: Date? = null,
            minimumDate: Date? = null,
            crossinline listener: ((requestKey: String, date: Date) -> Unit)
        ) {
            fragment.setFragmentResultListener(requestKey) { key, bundle ->
                listener(key, bundle.get(DATA) as Date)
            }

            fragment.findNavController().navigate(R.id.dateActionDirection, Bundle().apply {
                putString("key", requestKey)
                putString("title", title)
                if (Parcelable::class.java.isAssignableFrom(Date::class.java)) {
                    putParcelable("selectedDate", selectedDate as Parcelable?)
                } else if (Serializable::class.java.isAssignableFrom(Date::class.java)) {
                    putSerializable("selectedDate", selectedDate as Serializable?)
                }
                if (Parcelable::class.java.isAssignableFrom(Date::class.java)) {
                    putParcelable("minimumDate", minimumDate as Parcelable?)
                } else if (Serializable::class.java.isAssignableFrom(Date::class.java)) {
                    putSerializable("minimumDate", minimumDate as Serializable?)
                }
            })
        }

    }
}
