package com.classera.utils.views.dialogs.datepickerbottomdialog

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class DateBottomSheetModule {



    @Binds
    abstract fun bindFragment(fragment: DateBottomSheetFragment): Fragment
}
