package com.classera.utils.views.dialogs.timepickerbottomdialog

import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.NumberPicker
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment

import com.classera.core.utils.android.getTime
import com.classera.core.utils.android.setTime
import com.classera.utils.R
import com.classera.utils.databinding.FragmentTimeBottomSheetBinding
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


class TimeBottomSheetFragment : BaseBottomSheetDialogBindingFragment() {
    override val layoutId: Int = R.layout.fragment_time_bottom_sheet

    val args by navArgs<TimeBottomSheetFragmentArgs>()

    private lateinit var timePicker: TimePicker

    private var selectedTime: Date? = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args.selectedTime?.let {
            selectedTime = args.selectedTime
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        setupTimePicker()
        bindData()
    }

    private fun findViews() {
        view?.let {
            timePicker = it.findViewById(R.id.fragment_select_time_timePicker_from)
        }

    }

    private fun bindData() {
        bind<FragmentTimeBottomSheetBinding>
        {
            this?.timeBottomSheetFragment = this@TimeBottomSheetFragment
        }
    }

    private fun setupTimePicker() {
        setTimePickerInterval(timePicker)
        setTimePickerTime()
    }

    private fun setTimePickerInterval(timePicker: TimePicker) {
        try {

            val minutePicker = timePicker.findViewById(
                Resources.getSystem().getIdentifier(
                    MINUTE, ID, ANDROID
                )
            ) as NumberPicker
            minutePicker.minValue = MIN_VALUE
            minutePicker.maxValue = MAX_VALUE / TIME_PICKER_INTERVAL - MINUS_TIME_INTERVAL
            val displayedValues: MutableList<String> = ArrayList()
            var i = MIN_VALUE
            while (i < MAX_VALUE) {
                displayedValues.add(String.format("%02d", i))
                i += TIME_PICKER_INTERVAL
            }
            minutePicker.displayedValues = displayedValues.toTypedArray()
        } catch (e: Exception) {
            Log.e(TimeBottomSheetFragment::class.java.name, "Exception: $e")
        }
    }

    private fun setTimePickerTime() {
        args.selectedTime?.let {
            try {
                timePicker.setTime(it, TIME_PICKER_INTERVAL)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    fun onSelectButtonClicked() {
        setFragmentResult(
            args.key,
            bundleOf(DATA to timePicker.getTime(TIME_PICKER_INTERVAL))
        )
        findNavController().navigateUp()
    }

    fun onTimeChanged(hourOfDay: Int, minute: Int) {
        //TODO
    }


    companion object {
        const val DEFAULT_REQUEST_KEY = "default_request_key"
        const val DATA = "data"
        private const val MINUTE = "minute"
        private const val ID = "id"
        private const val ANDROID = "android"
        const val TIME_PICKER_INTERVAL = 5
        const val MIN_VALUE = 0
        const val MAX_VALUE = 60
        const val MINUS_TIME_INTERVAL = 1


        inline fun show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            title: String?,
            selectedTime: Date? = null,
            crossinline listener: ((requestKey: String, date: Date) -> Unit)
        ) {
            fragment.setFragmentResultListener(requestKey) { key, bundle ->
                listener(key, bundle.get(DATA) as Date)
            }

            fragment.findNavController().navigate(R.id.timeActionDirection,
                Bundle().apply {
                    putString("key", requestKey)
                    putString("title", title)
                    if (Parcelable::class.java.isAssignableFrom(Date::class.java)) {
                        putParcelable("selectedTime", selectedTime as Parcelable?)
                    } else if (Serializable::class.java.isAssignableFrom(Date::class.java)) {
                        putSerializable("selectedTime", selectedTime as Serializable?)
                    }
                }
            )
        }
    }
}
