package com.classera.utils.views.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.selection.Selectable
import com.classera.utils.R
import javax.inject.Inject


class ListFragment : BaseBindingFragment() {

    override val layoutId: Int = R.layout.fragment_list

    @Inject
    lateinit var listViewModel: ListViewModel

    @Inject
    lateinit var listAdapter: ListAdapter


    private val args by navArgs<ListFragmentArgs>()

    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        setupTitle()
        setupRecyclerView()
        initToggleListener()
    }

    private fun findViews() {
        requireView().run {
            recyclerView = findViewById(R.id.fragment_list_recycler_view)
        }
    }

    private fun setupTitle() {
        (activity as AppCompatActivity).supportActionBar?.title =
            args.title
    }

    private fun setupRecyclerView() {
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerView.adapter = listAdapter
        listAdapter.setOnItemClickListener { view, position ->
            handleItemClicked(view, position)
        }
        Handler(Looper.getMainLooper()).postDelayed({
            args.selectedId?.let { it ->
                listViewModel.getSelectedItemPosition(it)
                    ?.let { recyclerView.smoothScrollToPosition(it + 1) }
            }
        }, DELAY)


    }

    private fun initToggleListener() {
        listViewModel.changedPosition.observe(this) {
            listAdapter.notifyItemChanged(it)
        }
    }


    private fun handleItemClicked(view: View, position: Int) {
        setFragmentResult(
            args.key,
            bundleOf(DATA to listViewModel.getSelectable(position))
        )
        findNavController().navigateUp()
    }


    companion object {
         const val DEFAULT_REQUEST_KEY = "default_request_key"

        const val DELAY = 500L
        const val DATA = "data"

        inline fun <T : Selectable> show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            title: String?,
            selectableList: Array<Selectable>,
            selectedId: String? = null,
            crossinline listener: ((requestKey: String, t: T) -> Unit)
        ) {
            fragment.setFragmentResultListener(requestKey) { key, bundle ->
                listener(key, bundle.get(DATA) as T)
            }

            fragment.findNavController().navigate(
                R.id.listBottomSheetFragmentDirection, Bundle().apply {
                    putString("key", requestKey)
                    putString("title", title)
                    putParcelableArray("selectableList", selectableList)
                    putString("selectedId", selectedId)
                }
            )
        }

    }
}
