package com.calssera.vcr

import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.adapter.BaseViewHolder

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
abstract class VcrAdapter<V : BaseViewHolder> : BasePagingAdapter<V>()
