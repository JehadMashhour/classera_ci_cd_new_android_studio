package com.calssera.vcr

import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.calssera.vcr.admin.VcrAdminFragment
import com.calssera.vcr.confirmvcr.VCRConfirmBottomSheet
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.calssera.vcr.teacher.VcrTeacherFragment
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 20, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("VCR")
abstract class VcrFragment : BaseFragment() {

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var errorView: ErrorView? = null
    private var quickFilterView: QuickFilterView? = null
    private var constraintLayoutList: ConstraintLayout? = null
    protected var constraintLayoutAddVCR: ConstraintLayout? = null
    var startMaterialButton: MaterialButton? = null
    var floatingActionButtonAdd: FloatingActionButton? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_vcr

    @Inject
    lateinit var prefs: Prefs

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        findViews()
        initFilter()
        initListeners()
    }

    abstract fun getVcrViewModel(): VcrViewModel

    abstract fun getVcrAdapter(): VcrAdapter<*>


    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_vcr)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_vcr)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_vcr)
        errorView = view?.findViewById(R.id.error_view_fragment_vcr)
        quickFilterView = view?.findViewById(R.id.filter_view_fragment_vcr)
        constraintLayoutList = view?.findViewById(R.id.fragment_vcr_constraint_layout_list)
        constraintLayoutAddVCR = view?.findViewById(R.id.fragment_vcr_constraint_layout_addVcr)
        startMaterialButton = view?.findViewById(R.id.fragment_vcr_button_start)
        floatingActionButtonAdd = view?.findViewById(R.id.floating_action_button_fragment_vcr_add)
    }


    private fun initListeners() {
        if (this !is VcrAdminFragment) {
            swipeRefreshLayout?.setOnRefreshListener {
                getVcrAdapter().resetPaging()
                getVirtualClassRooms()
            }
            getVirtualClassRooms()
        }
    }

    private fun getVirtualClassRooms(pageNumber: Int = DEFAULT_PAGE) {
        getVcrViewModel().getVirtualClassRooms(
            quickFilterView?.getSelectedFilterKey(),
            pageNumber,
            searchView?.query
        )
            .observe(viewLifecycleOwner, this::handleResource)
    }

    private fun initFilter() {
        quickFilterView?.setAdapter(R.array.vcr_filter_entries, R.array.vcr_filter_entry_values)
        quickFilterView?.setSelectedFilter("0")
        quickFilterView?.setOnFilterSelectedListener {
            getVcrAdapter().resetPaging()
            recyclerView?.scrollToPosition(0)
            getVirtualClassRooms(1)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_vcr_toolbar, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_vcr_rooms_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            getVirtualClassRooms()
        }

        searchView?.inputType = (
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)

        searchView?.setOnCloseListener {
            getVirtualClassRooms()
            return@setOnCloseListener false
        }
    }


    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<List<VcrResponse>>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }


    private fun handleSuccessUIVisibility(vcrResponseList: List<VcrResponse>?) {
        if (vcrResponseList?.isEmpty() == true && getVcrAdapter().getItemsCount() == 0) {
            constraintLayoutAddVCR?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
        } else {
            constraintLayoutList?.visibility = View.VISIBLE
            constraintLayoutAddVCR?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<BaseWrapper<List<VcrResponse>>>) {
        handleSuccessUIVisibility(resource.data?.data)

        if (getVcrViewModel().currentPage == DEFAULT_PAGE) {
            initAdapter()
            handleAdapterListeners()
        } else {
            getVcrAdapter().notifyDataSetChanged()
        }
        getVcrAdapter().finishLoading()
    }

    private fun initAdapter() {
        recyclerView?.adapter = getVcrAdapter()
        getVcrAdapter().setOnLoadMoreListener(::getVirtualClassRooms)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if (getVcrAdapter().getItemsCount() > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }

        errorView?.visibility = View.VISIBLE
        constraintLayoutAddVCR?.visibility = View.GONE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getVirtualClassRooms() }
        getVcrAdapter().finishLoading()
    }


    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        errorView = null
        quickFilterView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    private fun handleAdapterListeners() {
        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.TEACHER ||
            prefs.allowParentAccessToSmartClass.equals("1", true)
        ) {
            getVcrAdapter().setOnItemClickListener { view, position ->
                handleItemClicked(view, position)
            }
        }
    }

    open fun handleItemClicked(view: View, position: Int) {

    }


}
