package com.calssera.vcr.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class AddVcrModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: AddVcrViewModelFactory): AddVcrViewModel {
            return ViewModelProvider(fragment, factory)[AddVcrViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAddVcrFragmentArgs(fragment: Fragment): AddVcrFragmentArgs {
            val args by fragment.navArgs<AddVcrFragmentArgs>()
            return args
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddVcrFragment): Fragment
}
