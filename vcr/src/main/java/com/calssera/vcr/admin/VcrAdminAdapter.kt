package com.calssera.vcr.admin

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.calssera.vcr.VcrAdapter
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.databinding.RowVcrBinding
import com.classera.core.adapter.BaseBindingViewHolder


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrAdminAdapter(private val viewModel: VcrViewModel) : VcrAdapter<VcrAdminAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowVcrBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getVcrCount()
    }

    inner class ViewHolder(binding: RowVcrBinding) : BaseBindingViewHolder(binding) {

        private var imageViewMore: ImageView? = null

        init {
            imageViewMore = itemView.findViewById(R.id.image_view_row_vcr_more)
        }

        override fun bind(position: Int) {
            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }
            bind<RowVcrBinding> {
                this.vcrItem = viewModel.getVcr(position)
            }

        }
    }
}
