package com.calssera.vcr.confirmvcr

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.VcrViewModelFactory
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.vcr.VcrRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Rawan Al-Theeb on 12/22/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class VCRConfirmModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: VcrViewModelFactory): VCRConfirmViewModel {
            return ViewModelProvider(fragment, factory)[VCRConfirmViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideVcrViewModelFactory(
            vcrRepository: VcrRepository,
            lecturesRepository: LecturesRepository,
            prefs: Prefs
        ): VcrViewModelFactory {
            return VcrViewModelFactory(vcrRepository, lecturesRepository,prefs)
        }
    }

    @Binds
    abstract fun bindActivity(activity: VCRConfirmBottomSheet): Fragment
}
