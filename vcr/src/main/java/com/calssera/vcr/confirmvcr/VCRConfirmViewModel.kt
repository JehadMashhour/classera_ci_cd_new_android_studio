package com.calssera.vcr.confirmvcr

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 12/22/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VCRConfirmViewModel(private val vcrRepository: VcrRepository) : BaseViewModel() {

    fun getVCRURL(vcrId: String, type: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        when (type) {
            UPCOMING -> {
                val resource = tryResource {
                    vcrRepository.getUpcomingVcrUrl(vcrId)
                }
                emit(resource)
            }
            PASSED -> {
                val resource = tryResource {
                    vcrRepository.getPassedVcrUrl(vcrId)
                }
                emit(resource)
            }
        }
        emit(Resource.Loading(show = false))
    }

    companion object {

        const val UPCOMING = "0"
        const val PASSED = "1"
    }

}
