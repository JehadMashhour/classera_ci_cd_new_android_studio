package com.calssera.vcr.lecturefragment

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class LectureFragmentModule {

    @Module
    companion object {


        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: LectureFragmentViewModelFactory
        ): LectureFragmentViewModel {
            return ViewModelProvider(fragment, factory)[LectureFragmentViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideLectureFragmentArgs(fragment: Fragment): LectureFragmentArgs {
            val args by fragment.navArgs<LectureFragmentArgs>()
            return args
        }

        @Provides
        @JvmStatic
        fun provideListBottomSheetAdapter(lectureFragmentViewModel: LectureFragmentViewModel): LectureFragmentAdapter {
            return LectureFragmentAdapter(lectureFragmentViewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: LectureFragment): Fragment
}
