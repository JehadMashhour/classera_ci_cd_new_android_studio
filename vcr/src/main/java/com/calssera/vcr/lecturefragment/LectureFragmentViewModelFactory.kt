package com.calssera.vcr.lecturefragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.selection.Selectable
import com.classera.data.models.vcr.Lecture
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject


class LectureFragmentViewModelFactory @Inject constructor(
    private val vcrRepository: VcrRepository,
    private val lecturesRepository: LecturesRepository,
    private val lectureFragmentArgs: LectureFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LectureFragmentViewModel(vcrRepository, lecturesRepository, lectureFragmentArgs) as T
    }
}
