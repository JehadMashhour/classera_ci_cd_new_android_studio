package com.calssera.vcr.student

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import com.calssera.vcr.R
import com.calssera.vcr.VcrAdapter
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.databinding.RowVcrBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs

/**
 * Created by Rawan Al-Theeb on 12/22/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VcrStudentAdapter(private val viewModel: VcrViewModel,
                        private val prefs: Prefs) : VcrAdapter<VcrStudentAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowVcrBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getVcrCount()
    }

    inner class ViewHolder(binding: RowVcrBinding) : BaseBindingViewHolder(binding) {

        private var relativeLayoutMore: RelativeLayout? = null

        init {
            relativeLayoutMore = itemView.findViewById(R.id.relative_layout_row_vcr_more)

            relativeLayoutMore?.visibility = View.GONE
        }

        override fun bind(position: Int) {
            bind<RowVcrBinding> {
                vcrItem = viewModel.getVcr(position)
            }
        }
    }
}
