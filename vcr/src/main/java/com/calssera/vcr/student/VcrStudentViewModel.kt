package com.calssera.vcr.student

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.calssera.vcr.VcrViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class VcrStudentViewModel(private val vcrRepository: VcrRepository, private val prefs: Prefs) :
    VcrViewModel() {

    override suspend fun getRooms(
        key: String?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<VcrResponse>> {
        return vcrRepository.getSmartClassrooms(key, pageNumber,text)
    }

    fun getVCRURL(vcrId: String, type: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        when (type) {
            UPCOMING -> {
                val resource = tryResource {
                    vcrRepository.getUpcomingVcrUrl(vcrId)
                }
                emit(resource)
            }
            PASSED -> {
                val resource = tryResource {
                    vcrRepository.getPassedVcrUrl(vcrId)
                }
                emit(resource)
            }
        }
        emit(Resource.Loading(show = false))
    }


    override fun deleteVcr(position: Int) {
        throw IllegalAccessException("The student should not be able to delete a VCR")
    }

    override fun getCurrentRole(): UserRole? {
        return prefs.userRole
    }

    companion object {

        const val UPCOMING = "0"
        const val PASSED = "1"
    }
}
