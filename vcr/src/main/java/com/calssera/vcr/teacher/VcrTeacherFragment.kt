package com.calssera.vcr.teacher

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.calssera.vcr.R
import com.calssera.vcr.VcrAdapter
import com.calssera.vcr.VcrFragment
import com.calssera.vcr.VcrViewModel
import com.calssera.vcr.confirmvcr.VCRConfirmViewModel
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.VendorTypes
import com.classera.data.models.vcr.vcrconfirm.VcrConfirmResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.actionsheetbottomdialog.ActionDialog
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class VcrTeacherFragment : VcrFragment() {

    @Inject
    lateinit var viewModel: VcrTeacherViewModel

    @Inject
    lateinit var adapter: VcrTeacherAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initViewModelListeners()
    }

    private fun initListeners() {
        floatingActionButtonAdd?.setOnClickListener {
            val direction = VcrTeacherFragmentDirections.createVCRActionDirection()
            findNavController().navigate(direction)
        }

        startMaterialButton?.setOnClickListener {
            val direction = VcrTeacherFragmentDirections.createVCRActionDirection()
            findNavController().navigate(direction)
        }
    }


    override fun getVcrViewModel(): VcrViewModel {
        return viewModel
    }

    override fun getVcrAdapter(): VcrAdapter<*> {
        return adapter
    }

    override fun handleItemClicked(view: View, position: Int) {
        when (view.id) {
            R.id.image_view_row_vcr_more -> {
                handleMoreClicked(view, position)
            }
            else -> {
                val vcr = viewModel.getVcr(position)

                vcr?.run {
                    if (this.isUpComing == true) {
                        getVCRUrl(this.id!!, VCRConfirmViewModel.UPCOMING)
                    } else {
                        getVCRUrl(this.sessionCode!!, VCRConfirmViewModel.PASSED)
                    }
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        ActionDialog(requireContext())
            .apply {
                setTitle(getString(R.string.choose_option))
                addAction(getString(R.string.edit))
                addAction(getString(R.string.delete), true)
                show()
            }.setEventListener(object : ActionDialog.OnEventListener {
                override fun onActionItemClick(
                    dialog: ActionDialog,
                    item: ActionDialog.ActionItem,
                    itemPosition: Int
                ) {
                    when (item.title) {
                        getString(R.string.edit) -> {
                            handleEditItemClicked(position)
                        }

                        getString(R.string.delete) -> {
                            handleDeleteItemClicked(position)
                        }
                    }
                }

                @Suppress("EmptyFunctionBlock")
                override fun onCancelItemClick(dialog: ActionDialog?) {

                }

            })
    }


    private fun getVCRUrl(vcrId: String, type: String) {
        viewModel.getVCRURL(vcrId, type).observe(viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Loading -> {

                    }

                    is Resource.Success<*> -> {
                        when (type) {
                            VCRConfirmViewModel.UPCOMING -> {
                                handleUpComingSuccessVcrUrl(it)
                            }

                            VCRConfirmViewModel.PASSED -> {
                                handlePassedSuccessVcrUrl(it)
                            }
                        }

                    }

                    is Resource.Error -> {
                        MessageBottomSheetFragment.show(
                            fragment = this,
                            image = R.drawable.ic_info,

                            message = it.error.message ?: it.error.cause?.message,
                            buttonLeftText = getString(R.string.close)
                        )
                    }
                }
            })
    }

    private fun handleUpComingSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet),
                    getString(R.string.label_button_vcr_confirm_bottom_launch)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }

    private fun handlePassedSuccessVcrUrl(resource: Resource) {
        val successResource = resource.element<BaseWrapper<VcrConfirmResponse>>()?.data
        when {
            successResource?.sessionUrl?.isNotEmpty() == true -> {
                showSuccessOrErrorVcrDialog(
                    successResource.sessionUrl,
                    getString(R.string.title_student_vcr_bottom_sheet_view),
                    getString(R.string.label_button_vcr_confirm_bottom_view)
                )
            }
            else -> showSuccessOrErrorVcrDialog(
                successResource?.sessionUrl,
                getString(R.string.no_record_found),
                getString(R.string.close)
            )
        }
    }

    private fun showSuccessOrErrorVcrDialog(
        sessionUrl: String?,
        title: String?,
        buttonLeftText: String?
    ) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = title,
            buttonLeftText = buttonLeftText
        ) { t ->
            if (t == getString(R.string.label_button_vcr_confirm_bottom_launch)
                || t == getString(R.string.label_button_vcr_confirm_bottom_view)
            ) {
                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse(sessionUrl)
                startActivity(openURL)
            }
        }
    }

    private fun handleEditItemClicked(position: Int) {
        viewModel.getVcrDetails(
            viewModel.getVcr(position)?.id,
            viewModel.getVcr(position)?.vendor?.id
        )
            .observe(viewLifecycleOwner, Observer { handleVcrStatusResource(it, position) })
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleVcrStatusResource(resource: Resource, position: Int) {
        when (resource) {

            is Resource.Success<*> -> {
                handleVcrStatusSuccessResource(
                    resource as Resource.Success<BaseWrapper<VcrResponse>>
                )
            }
            is Resource.Error -> {
                handleVcrStatusErrorResource(resource)
            }
            else -> {

            }
        }
    }

    private fun handleVcrStatusSuccessResource(
        success: Resource.Success<BaseWrapper<VcrResponse>>
    ) {
        success.data?.data?.let {
            if (it.vendor?.vcrStatusWrapper?.canProceed == true) {
                navigateToAddVcr(it)
            } else {
                showMessageBottomSheetFragment(it)
            }
        }
    }

    private fun navigateToAddVcr(vcrResponse: VcrResponse) {
        val direction =
            VcrTeacherFragmentDirections.addVcrDirection(vcrResponse.title).apply {
                vcr = vcrResponse
            }
        findNavController().navigate(direction)
    }

    private fun showMessageBottomSheetFragment(vcrResponse: VcrResponse) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,

            message = vcrResponse.vendor?.vcrStatusWrapper?.message,
            buttonLeftText = getString(R.string.close)
        )
    }

    private fun handleVcrStatusErrorResource(resource: Resource.Error) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,

            message = resource.error.message,
            buttonLeftText = getString(R.string.close)
        )
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_delete_vcr_dialog)
            .setMessage(
                getString(
                    R.string.message_delete_vcr_dialog,
                    viewModel.getVcr(position)?.title
                )
            )
            .setPositiveButton(R.string.button_positive_delete_vcr_dialog) { _, _ ->
                viewModel.deleteVcr(
                    position
                )
            }
            .setNegativeButton(R.string.button_negative_delete_vcr_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) { adapter.notifyItemRemoved(it) }
    }

}
