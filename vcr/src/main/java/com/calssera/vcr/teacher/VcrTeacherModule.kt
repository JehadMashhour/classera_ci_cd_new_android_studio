package com.calssera.vcr.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.VcrViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class VcrTeacherModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: VcrViewModelFactory): VcrTeacherViewModel {
            return ViewModelProvider(fragment, factory)[VcrTeacherViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideVcrAdapter(viewModel: VcrTeacherViewModel): VcrTeacherAdapter {
            return VcrTeacherAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: VcrTeacherFragment): Fragment
}
