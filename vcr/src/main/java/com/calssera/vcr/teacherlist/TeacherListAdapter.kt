package com.calssera.vcr.teacherlist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.databinding.RowListTeacherBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter

class TeacherListAdapter(private val teacherListViewModel: TeacherListViewModel) :
    BasePagingAdapter<TeacherListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowListTeacherBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return teacherListViewModel.getItemsCount()
    }

    inner class ViewHolder(binding: RowListTeacherBinding) :
        BaseBindingViewHolder(binding) {

        init {
            setItemViewClickListener()
        }

        private fun setItemViewClickListener() {
            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    teacherListViewModel.toggleClickedSelectable(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }

        override fun bind(position: Int) {
            bind<RowListTeacherBinding> {
                this.selectable = teacherListViewModel.getTeacher(position)
            }
        }

    }


}
