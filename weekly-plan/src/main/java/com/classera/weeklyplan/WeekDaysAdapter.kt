package com.classera.weeklyplan

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BaseSingleSelectionAdapter
import com.classera.weeklyplan.databinding.RowWeekDayBinding

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 * o.nazzal@classera.com
 */
class WeekDaysAdapter(
    private val viewModel: WeeklyPlanViewModel
) : BaseSingleSelectionAdapter<WeekDaysAdapter.ViewHolder>() {

    override var selectedPosition: Int = 0

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowWeekDayBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getWeekDaysCount()
    }

    inner class ViewHolder(binding: RowWeekDayBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowWeekDayBinding> {
                this.selectedPosition = this@WeekDaysAdapter.selectedPosition
                this.currentPosition = position
                this.text = viewModel.getWeekDay(position)?.weekDay
            }
        }
    }
}
